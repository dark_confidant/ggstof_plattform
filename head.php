<?php

function ggstof_head($secure = false){

    $_SERVER["DOCUMENT_ROOT"] = $_SERVER["DOCUMENT_ROOT"].'/public_html/plattform';

    session_start();

    if( $secure ){
        if( !isset( $_SESSION["userID"] ) ) {
            $uri = basename( $_SERVER["REQUEST_URI"] );
            header( "Location: logout.php?request=$uri" );
            exit;
        }
    }

    function __autoload( $cls ) {
        require_once( "class/$cls.php" );
    }
    foreach( glob( "class/site/*.php" ) as $cls ) {
        require_once( $cls );
    }
    foreach( glob( "function/*.php" ) as $fnc ) {
        require_once( $fnc );
    }
    if (array_key_exists("userID", $_SESSION)) {
        $_SESSION["rights"] = userRights( $_SESSION["userID"] );
    }
    set_language();

}
