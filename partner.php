<?php
    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }


?>




<!-- start head -->
<?php
include("include/head.inc.php");
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Partner, Sponsoren und G�nner" ); ?></title>
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/accordeon.js"></script>
<script type="text/javascript" src="script/viewTabContent.js"></script>
</head>
<!-- end head -->




<!-- start body -->
<body>




<!-- start #navigationLeft -->
<?php
include("include/navigationLeft.inc.php");
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
include("include/navigationTop.inc.php");
?>
<!-- end #navigationTop -->



<!-- start #content -->
<div id="content" class="partner">



<h1>Partner, Sponsoren und G�nner</h1>


<p><?php echo translate( "Wir bedanken uns herzlichen bei allen Partnern, Sponsoren und G�nnern. Ohne ihre grossz�gige Unterst�tzung w�re der Aufbau unseres Netzwerks nie m�glich gewesen... <br /> 3. Zeile" ) ?></p>


<!-- start tabs, credits to facebook.com -->
<div class="home_main_item">
<div class="newsfeed_header">
<div id="newsfeed_tabs_wrapper">
<div id="newsfeed_tabs" class="Tabset_tabset">
<div class="HomeTabs">
<!-- spans to avoid href -->
<span id="One" class="HomeTabs_tab HomeTabs_first Tabset_selected"><?php echo translate( "Partner" ) ?></span>
<span id="Two" class="HomeTabs_tab"><?php echo translate( "Sponsoren" ) ?></span>
<span id="Three" class="HomeTabs_tab"><?php echo translate( "G�nner" ) ?></span>
<span id="Four" class="HomeTabs_tab"><?php echo translate( "Ihre Unterst�tzung" ) ?></span>
<span id="Five" class="HomeTabs_tab"><?php echo translate( "Impressum" ) ?></span>
</div>
</div>
</div>
</div>
</div>
<!-- end tabs -->


<!-- start #tab1 -->
<div class="tabContent" id="tabOne">

<p><?php echo translate( "Der strategischen Partner bekennt sich l�ngerfristig zu den Zielen der GGstOf. Seine Pr�senz geht �ber die Plattform hinaus (Netzwerk-Anl�sse der GGstOf, Publikationen oder Projektaktivit�ten). Er unterst�tzt die Aktivit�ten der GGstOf w�hrend mindestens drei Jahren mit j�hrlich CHF 1'000 oder mehr. Die Anzahl m�glicher Partner pro Jahr ist auf f�nf beschr�nkt." ) ?></p>

<table class="tableGrid">
<thead>
<tr>
<th class="firstColumn">Partner</th>
<th>Beitrag / Jahr</th>
<th>bis</th>
</tr>
</thead>
<tbody>
<tr>
<td class="firstColumn">Oberst i Gst Hans Meier</td>
<td class="currencyRight">CHF 5'000</td>
<td>2011</td>
</tr>
<tr>
<td class="firstColumn">Oberst i Gst Hans Meier</td>
<td class="currencyRight">CHF 5'000</td>
<td>2011</td>
</tr>
</tbody>
</table>

</div>
<!-- end #tab1 -->




<!-- start #tab2 -->
<div class="tabContent" id="tabTwo">

<p><?php echo translate( "Sponsoren unterst�tzen spezifische Elemente der Netzwerknutzung, so etwa einen Teil der Plattform oder die gezielte Erweiterung der Plattform um eine gew�nschte Funktionalit�t. Der Name (bzw. das Logo) des Sponsors wird direkt mit dem Profil des Gst Of verbunden. Ab CHF 200 ist man als Sponsor dabei." ) ?></p>

<div id="accordeon" class="accordeon accordeonProfile">
<div class="parent show"><a onclick="noScrollingToTop(); return false;" class="menuParent" href='#'><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Liste der Elemente welche mit Ihrem Beitrag realisiert werden k�nnen" ) ?></span></a>

<div class="groupChildren">

<div class="child">
<table class="tableGrid tableMargin">
<thead>
<tr>
<th class="firstColumn">Element</th>
<th>Betrag</th>
</tr>
</thead>
<tbody>
<tr>
<td class="firstColumn">XZ</td>
<td class="currencyRight">CHF 5'000</td>
</tr>
<tr>
<td class="firstColumn">XZ</td>
<td class="currencyRight">CHF 5'000</td>
</tr>
</tbody>
</table>

</div>
<!-- end .child -->

</div>
<!-- end .groupChildren -->

</div>
<!-- end .parent -->

<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href='#'><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Gesponserte Elemente" ) ?></span></a>

<div class="groupChildren">

<div class="child">
<table class="tableGrid tableMargin">
<thead>
<tr>
<th class="firstColumn">Element</th>
<th>Sponsor</th>
<th>Beitrag</th>
</tr>
</thead>
<tbody>
<tr>
<td class="firstColumn">XZ</td>
<td>XZ</td>
<td class="currencyRight">CHF 5'000</td>
</tr>
<tr>
<td class="firstColumn">XZ</td>
<td>XZ</td>
<td class="currencyRight">CHF 5'000</td>
</tr>
</tbody>
</table>

</div>
<!-- end .child -->

</div>
<!-- end .groupChildren -->

</div>
<!-- end .parent -->

</div>
<!-- end .accordeon -->

</div>
<!-- end #tab2 -->



<!-- start #tab3 -->
<div class="tabContent" id="tabThree">

<p><?php echo translate( "G�nner unterst�tzen die Idee einer besseren Nutzung des Gst Of Netzwerks. Ihr finanzieller Beitrag wird prim�r f�r den Aufbau und den Betrieb der Plattform verwendet. Der minimale Beitrag betr�gt CHF 50." ) ?></p>

<p>G�nner 2008</p>

<ul class="partnerList">
<li>- XY</li>
<li>- XY</li>
<li>- XY</li>
<li>- XY</li>
</ul>

</div>
<!-- end #tab3 -->



<!-- start #tab4 -->
<div class="tabContent" id="tabFour">

<p><?php echo translate( "Sie haben die Absicht, die GGstOf zu unterst�tzen. Hier haben wir f�r Sie hilfreiche Informationen zusammengestellt." ) ?></p>

<p><a href="pdf/sponsoring_konzept.pdf"><?php echo translate( "Sponsoring-Konzept (PDF)" ) ?></a></p>

<p><?php echo translate( "Ablauf" ) ?></p>

<ul class="partnerList">
<li>- <?php echo translate( "G�nner: Bezahlen Sie den G�nnerbeitrag auf unser Postkonto xx-xxxx-x mit dem Vermerk \"G�nner\" ein" ) ?></li>
<li>- <?php echo translate( "Sponsor: Kontaktieren Sie bitte den Netzwerk-Verantwortlichen" ) ?>, <a href="member_profile.php?member=1"><?php echo translate( "Maj i Gst" ) ?> Markus M. M�ller</a>.</li>
<li>- <?php echo translate( "Partner: Kontaktieren Sie bitte den Vereinspr�sidenten" ) ?>, <a href="member_profile.php?member=1"><?php echo translate( "Div" ) ?> Martin v. Orelli</a>.</li>
</ul>

</div>
<!-- end #tab4 -->



<!-- start #tab5 -->
<div class="tabContent" id="tabFive">

<p class="partnerNoMarginBottom"><?php echo translate( "Idee, Konzept, Projektleitung, Datenbankmanagement:" ) ?></p>
<p class="partnerNoMarginTop"><a href="member_profile.php?member=1">Maj i Gst Markus M. M�ller</a></p>

<p class="partnerNoMarginBottom"><?php echo translate( "Realisierung" ) ?></p>
<p class="partnerNoMarginTop"><script type="text/javascript">
//<![CDATA[
    document.write('<a href="');
    var by2 = new Array(
        -122,-118,-126,-123,-99,-120,-45,-125,-98,-101,
        -126,-87,-100,-118,-101,-117,-118,-116,-127,-57,
        -116,-120,-122 );
    var ix;
    var ln = by2.length;
    for (ix = 0; ix < ln; ix++) 
    { document.write('&');
      document.write('#' + (-by2[ix] ^ ln) + ';'); }
    document.write('"');
    document.write('>Juri Sarbach</a>');
//]]>
</script>, <script type="text/javascript">
//<![CDATA[
    document.write('<a href="');
    var by1 = new Array(
        -77,-65,-73,-76,-84,-79,-26,-77,-73,-67,
        -72,-65,-69,-76,-14,-73,-77,-83,-84,-69,
        -80,-70,-96,-71,-77,-65,-73,-76,-14,-67,
        -79,-77 );
    var ix;
    var ln = by1.length;
    for (ix = 0; ix < ln; ix++) 
    { document.write('&');
      document.write('#' + (-by1[ix] ^ ln) + ';'); }
    document.write('"');
    document.write('>Michael Imstepf</a>');
//]]>
</script></p>

</div>
<!-- end #tab5 -->




</div>
<!-- end #content -->



<?php
    include("include/footer.inc.php");
?>

</body>
</html>