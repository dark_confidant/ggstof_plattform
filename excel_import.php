<?php

class Excel_import {

    public function __construct() {
        
    }
    
    public function init(){
        $xlsx = urldecode(stripslashes($_POST['select']));
        $url = "uploads/" . $xlsx;
        if (!file_exists($url) && $url != "uploads") {
            return false;
        } else {
            require_once 'module/PHPExcel/IOFactory.php';
//  Read your Excel workbook
            try {

                $inputFileName = $url;
                $inputFileName = "uploads/GLG II_14_Teilnehmerliste_140225.xlsx";

                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }

//  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            // Loop through each row of the worksheet in turn
            $data = [['Nr', 'Grad', 'Vorname', 'Nachname', 'Komplettname', 'Geb1', 'Geb2', 'Sprache', 'Beruf', 'Adresse', 'PLZ', 'Wohnort', 'Email', 'Tel', 'Natel', 'Gs Vb', 'Arbeitgeber']];
            for ($row = 12; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $rows = $sheet->rangeToArray('A' . $row . ':' . 'Q' . $row, NULL, TRUE, FALSE);
                $data[] = $rows[0];
            }
            
            $output = 'data = [';
            foreach($data as $k=>$row) {
                $output .= '[';
                foreach($row as $i=>$value) {
                    $output .= '"'.$value.'",';
                }
                $output .= '],';
            }
            $output .= ']';
        }
        return $output;

    }
 
}

$autostart = new Excel_import;
?>

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

<script src="http://handsontable.com/dist/handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="http://handsontable.com/dist/handsontable.full.css">
<link rel="stylesheet" media="screen" href="http://handsontable.com/demo/css/samples.css">
<link rel="stylesheet" media="screen" href="http://handsontable.com/demo/css/samples.css">
<link rel="stylesheet" media="screen" href="http://handsontable.com/demo/css/samples.css">
        <script  type="text/javascript">
        $(document).ready(function () {

  var
    <?= $autostart->init() ?>,
    container = document.getElementById('example'),
    hot;
  
  hot = new Handsontable(container, {
    data: data,
    minSpareRows: 1,
    colHeaders: false,
    contextMenu: false
  });
  
  
      $('button[name=save]').click(function () {
          console.log(hot.getData())
              $.ajax({
                url: 'php/save.php',
                data: {data: hot.getData()}, // returns all cells' data
                dataType: 'json',
                type: 'POST',
                success: function (res) {
                  if (res.result === 'ok') {
                    console.log('Data saved');
                  }
                  else {
                    console.log('Save error');
                  }
                },
                error: function () {
                 console.log('Save error');
                }
              });
              });

});</script>
    </head>
    <body>
       <div id="example" class="handsontable"></div> 
       <button name="save">Save</button>
    </body>

    