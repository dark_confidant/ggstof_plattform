<?php

    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }



    if( !$_SESSION["rights"]["editContent"] && !$_SESSION["rights"]["roleID"] == 4 ) {
        die( translate( "Keine Berechtigung." ) );
    }

    $action = $_REQUEST["action"];
    $emailID = empty( $_REQUEST["item"] ) ? NULL : $_REQUEST["item"];
    $language = empty( $_REQUEST["language"] ) ? $_SESSION["lang"] : $_REQUEST["language"];
    $titel = empty( $_POST["titel"] ) ? NULL : $_POST["titel"];
    $absenderID = empty( $_POST["absenderID"] ) ? NULL : $_POST["absenderID"];
    $subject = empty( $_POST["subject"] ) ? NULL : $_POST["subject"];
    $body = empty( $_POST["body"] ) ? NULL : $_POST["body"];
    $caller = $_REQUEST["caller"];
    $context = $_REQUEST["context"];

    if( $action == "UPDATE" && isset( $subject ) && $_SESSION["rights"]["editContent"] ) {
        setEmailContent( $titel, $absenderID, $subject, $body, $language, $emailID );
        header( "Location: $caller?context=$context" );
    }
    if( $action == "DELETE" && $_SESSION["rights"]["editContent"] ) {
        deleteEmailContent( $emailID );
        header( "Location: $caller?context=$context" );
    }
?>

<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ); ?> (GGstOf)</title>
</head>

<body>

<?php
    include( "include/navigationLeft.inc.php" );
?>

<?php
    include( "include/navigationTop.inc.php" );
?>

<div id="content">

<?php if( $action == "EDIT" || $action == "ADD" ) { ?>

<?php
    if( $emailID ) {
        $email = getEmailContent( $emailID, $language );
    }
?>
<form class="formUpdateProfil formEmailTemplate" name="formEmailTemplate" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <table>
            <tr><td><label><?php echo translate( "Titel" ); ?></label><input type="text" name="titel" class="emailtemplate" value="<?php echo $email["titel"]; ?>" /></td></tr>
            <tr><td><label><?php echo translate( "Absender" ); ?></label><select name="absenderID"><?php makeOptions( "linkEmailAbsender", $email["absenderID"] ); ?></select></td></tr>
            <tr><td><label><?php echo translate( "Betreff" ) . " ({$_REQUEST["language"]})"; ?></label><input type="text" name="subject" class="emailtemplate" value="<?php echo $email["subject"]; ?>" /></td></tr>
            <tr><td><label><?php echo translate( "Text" ) . " ({$_REQUEST["language"]})"; ?></label><textarea name="body" class="emailtemplate" rows="5" cols="5"><?php echo $email["body"]; ?></textarea></td></tr>
            <tr><td><label>
                <input type="hidden" name="item" value="<?php echo $emailID; ?>" />
                <input type="hidden" name="language" value="<?php echo $language; ?>" />
                <input type="hidden" name="caller" value="<?php echo $caller; ?>" />
                <input type="hidden" name="context" value="<?php echo $context; ?>" />
                <input type="hidden" name="action" value="UPDATE" /></label>
                <input type="submit" class="formsSubmitButton" value="<?php echo translate( "Aktualisieren" ); ?>" onclick="return (document.forms['formEmailTemplate'].elements['titel'].value!='' && document.forms['formEmailTemplate'].elements['subject'].value!='');" />
                <input type="reset" value="<?php echo translate( "Abbrechen" ); ?>" onclick="location='<?php echo $caller . "?context=" . $context; ?>'" />
            </td></tr>
        </table>
    </fieldset>
</form>

<p>
<strong><?php echo translate( "Variablen" ); ?>:</strong><br />
<?php
    $vars = new variableSet();
    $vars->output();
?>
</p>
<?php } ?>

</div>

<?php
    include( "include/footer.inc.php" );
?>

</body>

</html>