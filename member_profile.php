<?php
    require_once('head.php');
    ggstof_head(true);

    date_default_timezone_set( "Europe/Zurich" );


    if( empty( $_REQUEST["member"] ) && !$_SESSION["rights"]["addProfile"] ) {
        die( translate( "Keine Berechtigung." ) );
    }
    $member = empty( $_REQUEST["member"] ) ? NULL : $_REQUEST["member"];
    $pers = new person( $member );

    if( $pers->id ) {
        $_SESSION["rights"] = userRights( $_SESSION["userID"] );
        $_SESSION["rights"]["editProfile"] |= ( $_SESSION["personID"] == $pers->id );
        $_SESSION["rights"]["viewNonpublicProfile"] |= ( $_SESSION["personID"] == $pers->id );
        $_SESSION["rights"]["viewPayment"] |= ( $_SESSION["personID"] == $pers->id );

        $pers->getSocietyData();
        $region = array();
        for( $i=0; $i<count( $pers->verein ); $i++ ) {
            if( $pers->verein[$i]->organisationRegionID >= 3 && $pers->verein[$i]->organisationRegionID <= 10 && $pers->verein[$i]->datumEnde >= date( "Y-m-d" ) ) {
                $region[] = $pers->verein[$i]->organisationRegionID;
            }
        }
        $_SESSION["rights"]["editProfile"] |= ( $_SESSION["rights"]["roleID"] == 2 && in_array( $_SESSION["rights"]["regionID"], $region ) );
        $_SESSION["rights"]["viewNonpublicProfile"] |= ( $_SESSION["rights"]["roleID"] == 2 && in_array( $_SESSION["rights"]["regionID"], $region ) );
    }

    if( in_array( $pers->person->veroeffentlichungID, array( 0, 3 ) ) && !$_SESSION["rights"]["viewNonpublicProfile"] && !$_SESSION["rights"]["addProfile"] ) {
        die( translate( "Profil ist nicht öffentlich." ) );
    }
    if(!array_key_exists('shortprofile', $_REQUEST)) {
        $_REQUEST['shortprofile'] = false;
    }
//     $shortprofile = $_SESSION["rights"]["viewNonpublicProfile"] && $_REQUEST["shortprofile"] == "true" || !$_SESSION["rights"]["viewNonpublicProfile"] && $pers->person->veroeffentlichungID == 0;
    $shortprofile = $_SESSION["rights"]["viewNonpublicProfile"] && $_REQUEST["shortprofile"] == "true" || !$_SESSION["rights"]["viewNonpublicProfile"] && $pers->person->veroeffentlichungID == 2;

    $action  = array_key_exists('action', $_REQUEST) ? $_REQUEST["action"]: '';
    $context = array_key_exists('context', $_REQUEST) ? $_REQUEST["context"]: '';
    $element = array_key_exists('element', $_REQUEST) ? $_REQUEST["element"]: '';
    $e = 0;

    if( $action == "UPDATE" ) {
        $data = $_POST;

        if( !$pers->id ) {
            $data["inputVerein"]["organisationRegionID"] = 1;
            $data["inputVerein"]["funktionID"] = 99;
        }

        if( $_SESSION["rights"]["editProfile"] ) {
            if( empty( $data ) ) {
                $pers->setPersonData();
            }

            if( isset( $data["inputPrivat"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputPrivat"]["modifierID"] = $_SESSION["personID"];
                }

                if( isset( $data["inputPrivat"]["support"] ) ) {
                    $data["inputPrivat"]["support"] = array_diff_key( $data["inputPrivat"]["support"], array( "dummy"=>0 ) );
                    $data["inputPrivat"] = array_merge( $data["inputPrivat"], array( "supportID"=>array_sum( $data["inputPrivat"]["support"] ) ) );
                    $data["inputPrivat"] = array_diff_key( $data["inputPrivat"], array( "support"=>0 ) );
                }
                if( $data["inputPrivat"]["tf"] == "+41 xx xxx xxxx" ) {
                    $data["inputPrivat"]["tf"] = "";
                }
                if( $data["inputPrivat"]["mobil"] == "+41 xx xxx xxxx" ) {
                    $data["inputPrivat"]["mobil"] = "";
                }
                if( $data["inputPrivat"]["url"] == "http://" ) {
                    $data["inputPrivat"]["url"] = "";
                }
                if( !isset( $data["inputPrivat"]["datenschutz"] ) ) {
                    $data["inputPrivat"] = array_merge( $data["inputPrivat"], array( "datenschutz"=>0 ) );
                }
                if( !isset( $data["inputPrivat"]["codex"] ) ) {
                    $data["inputPrivat"] = array_merge( $data["inputPrivat"], array( "codex"=>0 ) );
                }

                if( isset( $data["inputPrivat"]["email"] ) && $data["inputPrivat"]["email"] != $pers->person->email && $_SESSION["personID"] == $pers->id ) {
                    $_SESSION["userID"] = $data["inputPrivat"]["email"];
                    logUserChange( $pers->person->email, $_SESSION["userID"] );
                }
                $pers->setPersonData( $data["inputPrivat"] );

                if( !empty( $_FILES["bild"]["name"] ) ) {
                    $imgName = "image/profile/{$pers->id}.jpg";
                    move_uploaded_file( $_FILES["bild"]["tmp_name"], $imgName );

                    list( $width, $height, $type ) = getimagesize( $imgName );
                    switch( $type ) {
                        case 1: $imgOld = imagecreatefromgif( $imgName ); break;
                        case 2: $imgOld = imagecreatefromjpeg( $imgName ); break;
                        case 3: $imgOld = imagecreatefrompng( $imgName ); break;
                    }
                    imagealphablending( $imgOld, true );
                    imagesavealpha( $imgOld, true );
                    if( $width > 400 || $height > 400 ) {
                        $factor = min( 400/$width, 400/$height );
                        $imgNew = imagecreatetruecolor( floor( $width*$factor ), floor( $height*$factor ) );
//                         imageantialias( $imgNew, true );
                        imagecopyresampled( $imgNew, $imgOld, 0, 0, 0, 0, floor( $width*$factor ), floor( $height*$factor ), $width, $height );
                    } else {
                        $imgNew = $imgOld;
                    }
                    imagejpeg( $imgNew, $imgName );
                }
            }

            if( isset( $data["inputMilitaer"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputMilitaer"]["modifierID"] = $_SESSION["personID"];
                }

                if( $id = $data["inputMilitaer"]["militaerID"] ) {
                    $data["inputMilitaer"] = array_diff_key( $data["inputMilitaer"], array( "militaerID"=>0 ) );
                    if( empty( $data["inputMilitaer"]["datumEnde"] ) ) {
                        $data["inputMilitaer"]["datumEnde"] = "9999-12-31";
                    }
                    if( $data["inputMilitaer"]["url"] == "http://" ) {
                        $data["inputMilitaer"]["url"] = "";
                    }

                    $pers->setMilitaryData( $data["inputMilitaer"], $id );
                } else {
                    if( empty( $data["inputMilitaer"]["datumEnde"] ) ) {
                        $data["inputMilitaer"] = array_diff_key( $data["inputMilitaer"], array( "datumEnde"=>0 ) );
                    }
                    if( $data["inputMilitaer"]["url"] == "http://" ) {
                        $data["inputMilitaer"]["url"] = "";
                    }

                    if( $_REQUEST["promotion"] == "true" ) {
                        $pers->getMilitaryData();
                        $pers->setMilitaryData( array( "datumEnde"=>date( "Y-m-d", strtotime( "last day of previous month" ) ) ), $pers->current->militaer->militaerID );
                    }

                    $pers->setMilitaryData( $data["inputMilitaer"] );

                    if( $_REQUEST["promotion"] == "true" && $_REQUEST["promotionmail"] == "true"  ) {
                        $promomail = new email( $pers->isMember() ? 6 : 7, $pers->id );
                        $promomail->send();
                    }
                }
            }

            if( isset( $data["inputBeruf"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputBeruf"]["modifierID"] = $_SESSION["personID"];
                }

                if( $id = $data["inputBeruf"]["berufID"] ) {
                    $data["inputBeruf"] = array_diff_key( $data["inputBeruf"], array( "berufID"=>0 ) );
                    if( empty( $data["inputBeruf"]["datumEnde"] ) ) {
                        $data["inputBeruf"]["datumEnde"] = "9999-12-31";
                    }
                    if( $data["inputBeruf"]["tf"] == "+41 xx xxx xxxx" ) {
                        $data["inputBeruf"]["tf"] = "";
                    }
                    if( $data["inputBeruf"]["url"] == "http://" ) {
                        $data["inputBeruf"]["url"] = "";
                    }

                    $pers->setJobData( $data["inputBeruf"], $id );
                } else {
                    if( empty( $data["inputBeruf"]["datumEnde"] ) ) {
                        $data["inputBeruf"] = array_diff_key( $data["inputBeruf"], array( "datumEnde"=>0 ) );
                    }
                    if( $data["inputBeruf"]["tf"] == "+41 xx xxx xxxx" ) {
                        $data["inputBeruf"]["tf"] = "";
                    }
                    if( $data["inputBeruf"]["url"] == "http://" ) {
                        $data["inputBeruf"]["url"] = "";
                    }

                    $pers->setJobData( $data["inputBeruf"] );
                }
            }

            if( isset( $data["inputAusbildung"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputAusbildung"]["modifierID"] = $_SESSION["personID"];
                }

                if( $id = $data["inputAusbildung"]["ausbildungID"] ) {
                    $data["inputAusbildung"] = array_diff_key( $data["inputAusbildung"], array( "ausbildungID"=>0 ) );

                    $pers->setEducationData( $data["inputAusbildung"], $id );
                } else {
                    $pers->setEducationData( $data["inputAusbildung"] );
                }
            }

            if( isset( $data["inputPublic"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputPublic"]["modifierID"] = $_SESSION["personID"];
                }

                if( $id = $data["inputPublic"]["publicID"] ) {
                    $data["inputPublic"] = array_diff_key( $data["inputPublic"], array( "publicID"=>0 ) );
                    if( empty( $data["inputPublic"]["datumEnde"] ) ) {
                        $data["inputPublic"]["datumEnde"] = "9999-12-31";
                    }
                    if( $data["inputPublic"]["url"] == "http://" ) {
                        $data["inputPublic"]["url"] = "";
                    }

                    $pers->setPublicData( $data["inputPublic"], $id );
                } else {
                    if( empty( $data["inputPublic"]["datumEnde"] ) ) {
                        $data["inputPublic"] = array_diff_key( $data["inputPublic"], array( "datumEnde"=>0 ) );
                    }
                    if( $data["inputPublic"]["url"] == "http://" ) {
                        $data["inputPublic"]["url"] = "";
                    }

                    $pers->setPublicData( $data["inputPublic"] );
                }
            }

            if( isset( $data["inputMilitaer2"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputMilitaer2"]["modifierID"] = $_SESSION["personID"];
                }

                $pers->setPersonData( $data["inputMilitaer2"] );
            }

            if( isset( $data["inputBeruf2"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputBeruf2"]["modifierID"] = $_SESSION["personID"];
                }

                $pers->setPersonData( $data["inputBeruf2"] );
            }

            if( isset( $data["inputAdmin"] ) && $_SESSION["rights"]["roleID"] == 5 ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputAdmin"]["modifierID"] = $_SESSION["personID"];
                }

//                 if( !isset( $data["inputAdmin"]["veroeffentlichungID"] ) ) {
//                     $data["inputAdmin"] = array_merge( $data["inputAdmin"], array( "veroeffentlichungID"=>0 ) );
//                 }

                if( $pers->person->todesdatum == "0000-00-00" && !empty( $data["inputAdmin"]["todesdatum"] ) ) {
                    $pers->closeFile( $data["inputAdmin"]["todesdatum"] );
                }
                $pers->setPersonData( $data["inputAdmin"] );
                $pers->getPersonData();
            }
        }

        if( $_SESSION["rights"]["editPayment"] ) {
            if( isset( $data["inputEinstellungen"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputEinstellungen"]["modifierID"] = $_SESSION["personID"];
                }

                $pers->setPersonData( $data["inputEinstellungen"] );
            }

            if( isset( $data["inputZahlungen"] ) ) {
                $data["inputZahlungen"]["modifierID"] = $_SESSION["personID"];

                if( $id = $data["inputZahlungen"]["zahlungID"] ) {
                    $data["inputZahlungen"] = array_diff_key( $data["inputZahlungen"], array( "zahlungID"=>0 ) );
                    $pers->setPaymentData( $data["inputZahlungen"], $id );
                } else {
                    $pers->setPaymentData( $data["inputZahlungen"] );
                }
            }
        }

        if( $_SESSION["rights"]["roleID"] >= 4 ) {
            if( isset( $data["inputVerein"] ) ) {
                if( $_SESSION["personID"] != $pers->id ) {
                    $data["inputVerein"]["modifierID"] = $_SESSION["personID"];
                }

                if( $id = $data["inputVerein"]["vereinID"] ) {
                    $data["inputVerein"] = array_diff_key( $data["inputVerein"], array( "vereinID"=>0 ) );
                    if( empty( $data["inputVerein"]["datumEnde"] ) ) {
                        $data["inputVerein"]["datumEnde"] = "9999-12-31";
                    }

                    $pers->setSocietyData( $data["inputVerein"], $id );
                } else {
                    if( empty( $data["inputVerein"]["datumEnde"] ) ) {
                        $data["inputVerein"] = array_diff_key( $data["inputVerein"], array( "datumEnde"=>0 ) );
                    }

                    $pers->setSocietyData( $data["inputVerein"] );
                }
            }
        }

        if( $_SESSION["personID"] == $pers->id ) {
            logProfileChange( $pers->person->email );
        }

        header( "Location: {$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&context=$context" );
    }

    if( $action == "DELETE" ) {
        switch( $context ) {
            case "militaer":
                if( $_SESSION["rights"]["roleID"] == 5 ) {
                    $pers->deleteMilitaryData( $_GET["militaerID"] );
                }
                break;
            case "beruf":
                if( $_SESSION["rights"]["roleID"] == 5 ) {
                    $pers->deleteJobData( $_GET["berufID"] );
                }
                break;
            case "ausbildung":
                if( $_SESSION["rights"]["roleID"] == 5 ) {
                    $pers->deleteEducationData( $_GET["ausbildungID"] );
                }
                break;
            case "public":
                if( $_SESSION["rights"]["roleID"] == 5 ) {
                    $pers->deletePublicData( $_GET["publicID"] );
                }
                break;
            case "verein":
                if( $_SESSION["rights"]["roleID"] == 5 ) {
                    $pers->deleteSocietyData( $_GET["vereinID"] );
                }
                break;
            case "zahlungen":
                if( $_SESSION["rights"]["editPayment"] ) {
                    $pers->deletePaymentData( $_GET["zahlungID"] );
                }
                break;
        }

        header( "Location: {$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&context=$context" );
    }

    $r = userRights( $_SESSION["userID"] );
    $_SESSION["rights"]["datenschutz"] = $r["datenschutz"];
    $_SESSION["rights"]["codex"] = $r["codex"];
    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }

    if( isset( $pers->id ) ) {
        $pers->getPersonData();
        $pers->getMilitaryData();
        $pers->getJobData();
        $pers->getEducationData();
        $pers->getPublicData();
        $pers->getSocietyData();
        $pers->getPaymentData();
    }

    if( $action == "EXPORT" ) {
        switch( $_REQUEST["format"] ) {
            case "pdf":
                $pers->printPDF();
                break;

            case "vcf":
                $pers->buildVcard();
                break;
        }
        exit;
    }

    if( $action == "MAKEPDF" ) {
        switch( $_REQUEST["template"] ) {
            case "pdfLetterInvoice":
                $retval = $pers->invoice();
                if( $retval ) {
                    die( $retval );
                }
                $pdf = new pdfLetterInvoice( "P", "mm", "A4" );
                $channelID = $pers->person->paymentChannelID;
                break;

            case "pdfLetterPaymentLastCall":
                $pdf = new pdfLetterPaymentLastCall( "P", "mm", "A4" );
                $channelID = $pers->person->paymentChannelID;
                break;

            default:
                $pdf = new pdfLetter( "P", "mm", "A4" );
                $channelID = $pers->person->channelID;
                break;
        }
        $pdf->Init();
        $pdf->Compose( $pers->id, $channelID );
        $pdf->Download();
        exit;
    }

    if( $_SESSION["rights"]["roleID"] == 1 && $_SESSION["personID"] != $pers->id ) {
        $pers->counter();
    }

    // Instantiate site objects
    $addButton = new imgAddButton;
    $addButton->class = "";
    $deleteButton = new imgDeleteButton;
    $editButton = new imgEditButton;
    $handoverButton = new imgButton;
    $handoverButton->class = "handoverButton";
    $handoverButton->src = "image/template/handover.png";
    $promotionButton = new imgButton;
    $promotionButton->class = "promotionButton";
    $promotionButton->src = "image/template/promotion.png";
    $mailButton = new imgButton;
    $mailButton->src = "image/template/mail2.png";
    $errorButton = new imgButton;
    if(!defined('brevetierungsdatum')){
        $pers->person->brevetierungsdatum = '';
    }
    $errorButton->href = "mailto:plattform@ggstof.ch?subject=" . translate( "Fehler bei" ) . " " . translate( $pers->current->militaer->dienstgrad, $pers->person->sprache ) . " " . translate( $pers->current->militaer->zusatzDg, $pers->person->sprache ) . " {$pers->person->fullName}&amp;body=" . str_replace( array( "\n", "[Vorname]", "[Name]", "[Jahr]" ), array( "%0A", $pers->person->vorname, $pers->person->name, substr( $pers->person->brevetierungsdatum, 0, 4 ) ), translate( 240 ) . "\n{$_SESSION["dienstgrad"]} {$_SESSION["userName"]}" );
    $errorButton->src = "image/template/alert.gif";
    $errorButton->title = translate( "Fehler melden" );
?>



<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Mitgliederprofil von" ) . " {$pers->person->fullName}"; ?></title>
<link rel="stylesheet" type="text/css" href="style/lightbox.css" media="screen" />
<style type="text/css">
/*div#station { display: block }
div#abschluss { display: none }*/
</style>
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/jquery.lightbox.js"></script>
<script type="text/javascript" src="script/accordeon.js"></script>
<script type="text/javascript" src="script/password.js"></script>
<script type="text/javascript" src="script/viewTabContent.js"></script>
<script type="text/javascript" src="script/xmlHttp.js"></script>
</head>


<body>


<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>


<div id="content">

<?php
    if( !$member && !$pers->person->personID || $pers->person->personID ) {
?>

<form class="formUpdateProfil" id="formMemberProfil" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">

<?php
// check whether profile is being viewed or updated
    if( $action == "EDIT" && $element == $e ) {
?>
<fieldset id="updateProfileAboveTabs" class="fieldsetEditForm">
<table>
<tr><td><label><?php echo translate( "Anrede" ); ?></label><?php $select = new select( "inputPrivat[anredeID]", "linkPersonAnrede", $pers->person->anredeID ); $select->output(); ?></td></tr>
<tr><td><label><?php echo translate( "Vorname" ); ?> *</label><input type="text" name="inputPrivat[vorname]" value="<?php echo $pers->person->vorname; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Weitere Vornamen" ); ?></label><input type="text" name="inputPrivat[vorname2]" value="<?php echo $pers->person->vorname2; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Nachname" ); ?> *</label><input type="text" name="inputPrivat[name]" value="<?php echo $pers->person->name; ?>" /></td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "Geburtsdatum" ); ?> (<?php echo translate( "Format" ) ?>: <?php echo translate( "JJJJ-MM-TT" ) ?>)</label><input type="text" name="inputPrivat[geburtsdatum]" value="<?php echo str_replace( "0000-00-00", "", $pers->person->geburtsdatum ); ?>" /></td></tr>
<tr><td><label><?php echo translate( "Sprache" ); ?></label><?php $select = new select( "inputPrivat[spracheID]", "linkPersonSprache", $pers->person->spracheID ); $select->output(); ?></td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "Bild hochladen" ); ?> (<?php echo translate( "erlaubte Dateitypen" ) ?>: JPEG, JPG, PNG, GIF)</label><input name="bild" type="file" accept="image/jpeg image/png image/gif" /></td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "Strasse und Hausnummer" ); ?> *</label><input type="text" name="inputPrivat[adresse]" value="<?php echo $pers->person->adresse; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Postfach" ); ?></label><input type="text" name="inputPrivat[postfach]" value="<?php echo $pers->person->postfach; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Postleitzahl" ); ?> *</label><input type="text" name="inputPrivat[plz]" value="<?php echo $pers->person->plz; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Ort" ); ?> *</label><input type="text" name="inputPrivat[ort]" value="<?php echo $pers->person->ort; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Land" ); ?></label><input type="text" name="inputPrivat[land]" value="<?php echo !isset( $pers->person->land ) ? translate( "Schweiz" ) : $pers->person->land; ?>" />
<a href="http://maps.google.com/" onmousedown="this.href='http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q='+document.forms['formMemberProfil'].elements['inputPrivat[adresse]'].value.replace(' ', '+')+',+'+document.forms['formMemberProfil'].elements['inputPrivat[ort]'].value.replace(' ', '+')+',+'+document.forms['formMemberProfil'].elements['inputPrivat[land]'].value.replace(' ', '+')+'&amp;iwloc=addr'" onclick="window.open(this.href); return false;" title="<?php echo translate( "Adresse prüfen" ); ?>"><?php echo translate( "Adresse prüfen" ); ?></a>
</td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "E-Mail privat" ); ?> *</label><input type="text" name="inputPrivat[email]" value="<?php echo $pers->person->email; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Telefon Festnetz" ); ?></label><input type="text" name="inputPrivat[tf]" value="<?php echo !isset( $pers->person->tf ) ? "+41 xx xxx xxxx" : $pers->person->tf; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Telefon mobil" ); ?></label><input type="text" name="inputPrivat[mobil]" value="<?php echo !isset( $pers->person->mobil ) ? "+41 xx xxx xxxx" : $pers->person->mobil; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Website privat" ); ?></label><input type="text" name="inputPrivat[url]" value="<?php echo !isset( $pers->person->url ) ? "http://" : $pers->person->url; ?>" /></td></tr>
</table>
<table>
<tr><td><label>URL <?php echo translate( "Xing-Profil" ); ?></label><input type="text" name="inputPrivat[xing]" value="<?php echo $pers->person->xing; ?>" /></td></tr>
<tr><td><label>URL <?php echo translate( "LinkedIn-Profil" ); ?></label><input type="text" name="inputPrivat[linkedin]" value="<?php echo $pers->person->linkedin; ?>" /></td></tr>
<tr><td><label>URL <?php echo translate( "facebook-Profil" ); ?></label><input type="text" name="inputPrivat[facebook]" value="<?php echo $pers->person->facebook; ?>" /></td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "Ich akzeptiere" ); ?> <a href="privacy.php" onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=600,left=50,top=50,status'); return false"><?php echo translate( "die Datenschutzbestimmungen" ); ?></a></label><input type="checkbox" name="inputPrivat[datenschutz]" value="1" <?php if( $pers->person->datenschutz == 1 ) { echo "checked=\"checked\" "; } ?>/></td></tr>
<tr><td><label><?php echo translate( "Ich akzeptiere" ); ?> <a href="codex.php" onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=600,left=50,top=50,status'); return false"><?php echo translate( "den Codex" ); ?></a></label><input type="checkbox" name="inputPrivat[codex]" value="1" <?php if( $pers->person->codex == 1 ) { echo "checked=\"checked\" "; } ?>/></td></tr>
</table>
<table>
<tr><td class="legend">* = <?php echo translate( "Minimaleinträge" ); ?></td></tr>
</table>
<table>
<tr><td><input type="hidden" name="member" value="<?php echo $pers->id; ?>" />
<input type="hidden" name="action" value="UPDATE" />
<input type="submit" class="formsSubmitButton" value="<?php echo translate( "Aktualisieren" ); ?>" onclick="var form = document.forms['formMemberProfil']; if( form.elements['inputPrivat[vorname]'].value != '' && form.elements['inputPrivat[name]'].value != '' && form.elements['inputPrivat[plz]'].value != '' && form.elements['inputPrivat[ort]'].value != '' ) { if( form.elements['inputPrivat[geburtsdatum]'].value && form.elements['inputPrivat[geburtsdatum]'].value < '<?php echo ( date( "Y" )-100 ); ?>-01-01' ) { return window.confirm('<?php echo translate( "Geburtsdatum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ); ?>'); } } else { window.alert('<?php echo translate( "Bitte alle mit einem * markierten Felder ausfüllen" ); ?>.'); return false; }" />
<?php if( isset( $member ) ) { ?>
<input type="reset" value="<?php echo translate( "Abbrechen" ); ?>" onclick="location='<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>?member=<?php echo $pers->id; ?>'" />
<?php } ?>
</td></tr>
</table>
</fieldset>


<?php
        unset( $select );
    } else {
?>

<!-- start profile above tabs -->
<div id="profilePhotoContainer">
<?php if( file_exists( "image/profile/" . $pers->id . ".jpg" ) ) { ?>
<a class="lightbox" title="<?php echo $pers->person->fullName; ?>" rel="group" href="include/profilePhoto.php?member=<?php echo $pers->id; ?>&amp;deceased=<?php echo ( $pers->person->todesdatum != "0000-00-00" ); ?>"><img id="profilePhoto" src="include/profilePhoto.php?member=<?php echo $pers->id; ?>&amp;deceased=<?php echo ( $pers->person->todesdatum != "0000-00-00" ); ?>" alt="<?php echo $pers->person->fullName; ?>" title="<?php echo translate( "Foto vergrössern" )?>" /></a>
<?php } else { ?>
<img id="profilePhoto" src="include/profilePhoto.php?member=<?php echo $pers->id; ?>&amp;deceased=<?php echo ( $pers->person->todesdatum != "0000-00-00" ); ?>" alt="<?php echo $pers->person->fullName; ?>" />
<?php } ?>
</div>

<div class="profileUpperPartColumnLeft">

<div class="profileUpperPart"><?php echo translate( $pers->current->militaer->dienstgrad, $pers->person->sprache ) . ( $pers->person->todesdatum == "0000-00-00" ? " " . translate( $pers->current->militaer->zusatzDg, $pers->person->sprache ) : "" ); ?></div>

<div class="profileUpperPart bold"><?php echo $pers->person->fullName; ?></div>

<?php
    if(!defined('titel')){
        $titel = '';
    }

    if( !$shortprofile ) {
        if( $pers->person->geburtsdatum != "0000-00-00" ) {
            echo "<div class=\"profileUpperPart\">";
            if( $_SESSION["rights"]["roleID"] == 5 ) {
                echo ( $pers->person->todesdatum == "0000-00-00" ) ? $pers->person->geburtsdatum : "{$pers->person->geburtsdatum} - {$pers->person->todesdatum}";
            } else {
                echo ( $pers->person->todesdatum == "0000-00-00" ) ? translate( "Jahrgang" ) . " " . substr( $pers->person->geburtsdatum, 0, 4 ) : substr( $pers->person->geburtsdatum, 0, 4 ) . " - " . substr( $pers->person->todesdatum, 0, 4 );
            }
            echo "</div>";
        }

        for( $i=0; $i<count( $pers->beruf ); $i++ ) {
            $titel .= $pers->beruf[$i]->titelBezeichnung ? $pers->beruf[$i]->titelBezeichnung . ", " : "";
        }
        if( !empty( $titel ) ) {
            echo "<div class=\"profileUpperPart\">" . substr( $titel, 0, -2 ) . "</div>";
        }
    }

    if( $_SESSION["rights"]["editProfile"] && !$shortprofile ) {
        $editButton->output( array( "class"=>"", "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;element=$e", "imgclass"=>"icon", "title"=>translate( "Profil bearbeiten" ) ) );
    }
    if( $_SESSION["rights"]["viewNonpublicProfile"] ) {
        echo "<a href=\"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}" . ( $shortprofile ? "" : "&amp;shortprofile=true" ) . "\"><img class=\"icon\" src=\"image/template/page_user_dark.gif\" alt=\"\"></a>";
    }

    if( $shortprofile && $pers->person->email && $pers->person->todesdatum == "0000-00-00" ) {
        $emailID = ( $pers->current->verein->funktionID == 1 ) ? 26 : 28;
        $mailButton->output( array( "href"=>"emailer.php?emailID=$emailID&amp;to={$pers->id}", "onclick"=>"window.open( this.href ); return false;", "imgclass"=>"icon", "title"=>"{$pers->person->fullName} " . translate( "kontaktieren" ) ) );
    }
    if( $shortprofile ) {
        $errorButton->output();
    }
?>

</div>
<!-- end #profileUpperPartColumnLeft -->

<?php
    if( !$shortprofile ) {
?>
<div class="profileUpperPartColumnRight">

<?php if( $pers->person->adresse || $pers->person->plz || $pers->person->ort ) { ?> <div class="profileUpperPart"><a href="http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q=<?php echo str_replace( " ", "+", $pers->person->adresse ); ?>,+<?php echo str_replace( " ", "+", $pers->person->ort); ?>,+<?php echo str_replace( " ", "+", $pers->person->land ); ?>&amp;iwloc=addr" onclick="window.open(this.href); return false;" title="<?php echo translate( "Adresse auf GoogleMaps anzeigen" ) ?>"><?php echo $pers->person->adresse; if( $pers->person->adresse ) { echo ", "; } if( $pers->person->postfach ) { echo $pers->person->postfach . ", "; } echo $pers->person->plz . " " . $pers->person->ort; if( $pers->person->land != "Schweiz" && $pers->person->land != "Suisse" && $pers->person->land != "Svizzera" && $pers->person->land != "Switzerland" && $pers->person->land != "" ) { echo ", " . $pers->person->land; } ?></a></div> <?php } ?>

<div class="profileUpperPart"><a href="mailto:<?php echo $pers->person->email; ?>" title="<?php echo translate( "E-Mail schreiben" ) ?>"><?php echo $pers->person->email; ?></a><?php if( $_SESSION["rights"]["roleID"] == 5 && $pers->person->password ) { echo " <img src=\"image/template/key.gif\" alt=\"pw\" />"; } ?></div>

<?php
    if( $pers->person->tf || $pers->person->mobil ) {
?>
<div class="profileUpperPart">
<?php
        if( $pers->person->tf ) {
?>
<a href="callto:<?php echo str_replace( array( " ", "(0)" ), "", $pers->person->tf ); ?>" title="<?php echo translate( "Anrufen" ); ?>"><?php echo $pers->person->tf; ?></a>
<?php
        }
        if( $pers->person->tf && $pers->person->mobil ) {
            echo " / ";
        }
        if( $pers->person->mobil ) {
?>
<a href="callto:<?php echo str_replace( array( " ", "(0)" ), "", $pers->person->mobil ); ?>" title="<?php echo translate( "Anrufen" ); ?>"><?php echo $pers->person->mobil; ?></a>
<?php
        }
?>
</div>
<?php
    }
?>

<?php if( $pers->person->url ) { ?><a href="<?php echo $pers->person->url; ?>" onclick="window.open(this.href); return false;"><img src="image/template/www.gif" class="icon xing" alt="www" title="<?php echo translate( "Website öffnen" ) ?>" /></a><?php } ?>
<?php if( $pers->person->xing ) { ?><a href="<?php echo $pers->person->xing; ?>" onclick="window.open(this.href); return false;"><img src="image/template/xing.png" class="icon xing" alt="xing" title="<?php echo translate( "xing-Profil öffnen" ) ?>" /></a><?php } ?>
<?php if( $pers->person->facebook ) { ?><a href="<?php echo $pers->person->facebook; ?>" onclick="window.open(this.href); return false;"><img src="image/template/facebook.png" class="icon xing" alt="facebook" title="<?php echo translate( "facebook-Profil öffnen" ) ?>" /></a><?php } ?>
<?php if( $pers->person->linkedin ) { ?><a href="<?php echo $pers->person->linkedin; ?>" onclick="window.open(this.href); return false;"><img src="image/template/linkedin.png" class="icon xing" alt="linkedin" title="<?php echo translate( "linkedin-Profil öffnen" ) ?>" /></a><?php } ?>
<a href="<?php echo $_SERVER["SCRIPT_NAME"]; ?>?member=<?php echo $pers->id; ?>&amp;action=EXPORT&amp;format=vcf"><img src="image/template/vcard.png" class="icon xing" alt="vCard" title="<?php echo translate( "elektronische Visitenkarte herunterladen" ) ?>" /></a>
<a href="<?php echo $_SERVER["SCRIPT_NAME"]; ?>?member=<?php echo $pers->id; ?>&amp;action=EXPORT&amp;format=pdf"><img src="image/template/pdf.png" class="icon xing" alt="PDF" title="<?php echo translate( "Profil als PDF ausgeben" ) ?>" /></a>
<!--<a href="#"><img src="image/template/buddy.png" class="icon" alt="Buddy" title="<?php echo translate( "Als Freund hinzufügen" ) ?>" /></a>-->
<?php
    if( $_SESSION["rights"]["editProfile"] && strtotime( $pers->modDateOwner ) < time()-60*60*24*30*9 || $_SESSION["rights"]["roleID"] == 5 ) {
?>
<br /><input type="button" value="<?php echo translate( "Mein Profil ist vollständig und aktuell" ); ?>" onclick="window.location='<?php echo $_SERVER["SCRIPT_NAME"]; ?>?member=<?php echo $pers->id; ?>&amp;action=UPDATE'" />
<?php
    }
?>

</div>
<!-- end #profileUpperPartColumnRight -->
<?php
    } // if( !$shortprofile )
?>


<?php if( false ) { // if( $_SESSION["rights"]["editProfile"] ) { ?>

<div id="miscInfo">
<div class="accordeon accordeonProfile">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Diverses" ) ?></span></a>
<div class="groupChildren">
<div class="child">

...

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->
</div><!-- end #familyInfo -->

<?php } ?>



<!-- end profile above tabs -->
<?php
// end check whether profile is being viewed or updated
    }
    $e++;
?>




<!-- start tabs, credits to facebook.com -->
<div class="home_main_item">
<div class="newsfeed_header">
<div id="newsfeed_tabs_wrapper">
<div id="newsfeed_tabs" class="Tabset_tabset">
<div class="HomeTabs">
<!-- spans to avoid href -->
<span id="Militaer" class="HomeTabs_tab HomeTabs_first<?php if( empty( $context ) || $context == "militaer" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Militär" ) ?></span>
<?php
    if( !$shortprofile ) {
?>
<span id="Beruf" class="HomeTabs_tab<?php if( $context == "beruf" || $context == "ausbildung" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Beruf" ) ?></span>
<span id="Public" class="HomeTabs_tab<?php if( $context == "public" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Vereine & Organisationen" ) ?></span>
<span id="Verein" class="HomeTabs_tab<?php if( $context == "verein" || $context == "zahlungen" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "GGstOf" ) ?></span>
<!-- span id="Buddies" class="HomeTabs_tab<?php if( $context == "buddies" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Mein Netzwerk" ) ?></span -->
<?php
        if( $_SESSION["rights"]["editProfile"] || $_SESSION["rights"]["editPayment"] ) {
?>
<span id="Einstellungen" class="HomeTabs_tab<?php if( $context == "einstellungen" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Einstellungen" ) ?></span>
<?php
            if( $_SESSION["rights"]["roleID"] == 5 ) {
?>
<span id="Admin" class="HomeTabs_tab<?php if( $context == "admin" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Admin" ) ?></span>
<?php
            }
        }
    }
?>
</div>
</div>
</div>
</div>
</div>
<!-- end tabs -->



<!-- new tab -->

<div class="tabContent" id="tabMilitaer"<?php if( empty( $context ) || $context == "militaer" ) { echo " style=\"display:block\""; } ?>>

<div class="accordeon accordeonProfile show">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/expanded.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Funktion(en)" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $selectMilitaerZusatzDg = new select( "inputMilitaer[zusatzDgID]", "linkMilitaerZusatz" );
    $selectMilitaerZusatzFkt = new select( "inputMilitaer[zusatzFktID]", "linkMilitaerZusatz" );
    $selectMilitaerDienstgrad = new select( "inputMilitaer[dienstgradID]", "linkMilitaerDienstgrad" );

    $i = 0;
    while( $pers->militaer[$i]->datumEnde >= date( "Y-m-d" ) ) {
?>

<?php
    if( $action == "EDIT" && $element == $e ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"inputMilitaer[militaerID]\" value=\"{$pers->militaer[$i]->militaerID}\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputMilitaer[datumBeginn]\" value=\"{$pers->militaer[$i]->datumBeginn}\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputMilitaer[datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $pers->militaer[$i]->datumEnde ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Einteilung" ) . " *</label><input type=\"text\" name=\"inputMilitaer[einteilung]\" value=\"{$pers->militaer[$i]->einteilung}\" />" . ( $_SESSION["rights"]["roleID"] == 5 ? " <input type=\"text\" name=\"inputMilitaer[vorgesVb]\" value=\"{$pers->militaer[$i]->vorgesVb}\" />" : "" ) . "</td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><input type=\"text\" name=\"inputMilitaer[funktion]\" value=\"{$pers->militaer[$i]->funktion}\" /> "; $selectMilitaerZusatzFkt->output( array( "selected"=>$pers->militaer[$i]->zusatzFktID ) ); echo "</td></tr>";
        echo "<tr><td><label>" . translate( "Dienstgrad" ) . "</label>"; $selectMilitaerDienstgrad->output( array( "selected"=>$pers->militaer[$i]->dienstgradID, "lang"=>$pers->person->sprache ) ); echo " "; $selectMilitaerZusatzDg->output( array( "selected"=>$pers->militaer[$i]->zusatzDgID, "lang"=>$pers->person->sprache ) ); echo "</td></tr>";
        echo "<tr><td><label>" . translate( "Website" ) . "</label><input type=\"text\" name=\"inputMilitaer[url]\" value=\"" . ( $pers->militaer[$i]->url ? $pers->militaer[$i]->url : "http://" ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea name=\"inputMilitaer[bemerkungen]\" rows=\"5\" cols=\"5\">{$pers->militaer[$i]->bemerkungen}</textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"{$pers->id}\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"militaer\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputMilitaer[datumBeginn]'].value && form.elements['inputMilitaer[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputMilitaer[datumEnde]'].value && form.elements['inputMilitaer[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=militaer'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>
<!-- start #tabmilitaer profile in viewing mode -->

<dl class="list">
<?php
    if ( $i != 0 ) {
        echo "<dd><div class=\"seperator\"></div></dd>";
    }
?>
    <dt>
<?php
    $datumEnde = substr( $pers->militaer[$i]->datumEnde, 0, 4 );
    $datumBeginn = substr( $pers->militaer[$i]->datumBeginn, 0, 4 );
    if( $pers->militaer[$i]->datumBeginn > date( "Y-m-d" ) ) {
        echo translate( "Ab" ) . " $datumBeginn";
    } elseif( $datumEnde == "9999" ) {
        echo translate( "Seit" ) . " $datumBeginn";
    } else {
        echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " $datumEnde" );
    }

/*    if( $datumBeginn == date( "Y" ) ) {
        echo translate( "Seit" ) . " $datumBeginn";
    } elseif( $datumBeginn > date( "Y" ) ) {
    } else {
        echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " " . str_replace( "9999", translate( "heute" ), $datumEnde ) );
    }*/
?>
    </dt>
    <dd>
        <span class="bold">
            <?php if( $pers->militaer[$i]->funktion ) { echo translate( $pers->militaer[$i]->funktion ) . " " . translate( $pers->militaer[$i]->zusatzFkt ); ?>
            <a href="search.php?field=qryMilitaer.funktionMilitaerFull&amp;expr=<?php echo urlencode( $pers->militaer[$i]->funktion . " " . $pers->militaer[$i]->zusatzFkt ); ?>&amp;datumBeginn=<?php echo $pers->militaer[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->militaer[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
            <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum die gleiche Funktion hatten" ); ?>" /></a>
            <?php } ?>
        </span>
    </dd>

<?php if( $pers->militaer[$i]->einteilung ) { ?>
    <dt></dt>
    <dd>
<?php if( $pers->militaer[$i]->url ) { ?>
        <a href="<?php echo $pers->militaer[$i]->url; ?>" onclick="window.open(this.href); return false;">
<?php }
    echo translate( $pers->militaer[$i]->einteilung );
?>
<?php if( $pers->militaer[$i]->url ) { ?>
        </a>
<?php } ?>
        <a href="search.php?field=qryMilitaer.einteilung&amp;expr=<?php echo urlencode( $pers->militaer[$i]->einteilung ); ?>&amp;datumBeginn=<?php echo $pers->militaer[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->militaer[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum eingeteilt waren bei" ) . " " . $pers->militaer[$i]->einteilung; ?>" /></a>
    </dd>
<?php } ?>

<?php if( $pers->militaer[$i]->dienstgrad ) { ?>
    <dt></dt>
    <dd><?php echo translate( $pers->militaer[$i]->dienstgrad, $pers->person->sprache ) . " " . translate( $pers->militaer[$i]->zusatzDg, $pers->person->sprache ); ?></dd>
<?php } ?>

<?php if( $pers->militaer[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->militaer[$i]->bemerkungen ); ?></dd>
<?php } ?>

<?php
    if( $_SESSION["rights"]["editProfile"] && !$shortprofile ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=militaer&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=militaer&amp;militaerID={$pers->militaer[$i]->militaerID}" ) );
            $handoverButton->output( array( "href"=>"handover.php?member0={$pers->id}&amp;militaerID={$pers->militaer[$i]->militaerID}&amp;context=militaer", "onclick"=>"window.open( this.href ); return false;", "alt"=>"Funktion übertragen", "title"=>"Funktion übertragen" ) );
            if( $pers->militaer[$i]->militaerID == $pers->current->militaer->militaerID ) {
                $promotionButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=militaer&amp;action=ADD&amp;promotion=true", "alt"=>"Beförderung", "title"=>"Beförderung" ) );
            }
//             echo "<a href=\"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=militaer&amp;action=ADD&amp;promotion=true\"><img class=\"icon\" src=\"image/template/promotion.png\" alt=\"Beförderung\" title=\"Beförderung\"></a>";
        }
        echo "</dt>";
    }
?>
</dl>

<?php
    }  // end if
    $e++;
?>

<?php
    $i++;
    }  // end while

    if( $action !="ADD" && $_SESSION["rights"]["editProfile"] && !$shortprofile ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=militaer" ) );
    }
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php
    if( $action == "ADD" && $context == "militaer" ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputMilitaer[datumBeginn]\" value=\"" . ( $_REQUEST["promotion"] == "true" ? date( "Y-m" ) . "-01" : "" ) . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputMilitaer[datumEnde]\" value=\"\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Einteilung" ) . " *</label><input type=\"text\" name=\"inputMilitaer[einteilung]\" value=\"" . ( $_REQUEST["promotion"] == "true" ? $pers->current->militaer->einteilung : "" ) . "\" />" . ( $_SESSION["rights"]["roleID"] == 5 ? " <input type=\"text\" name=\"inputMilitaer[vorgesVb]\" value=\"" . ( $_REQUEST["promotion"] == "true" ? $pers->current->militaer->vorgesVb : "" ) . "\" />" : "" ) . "</td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><input type=\"text\" name=\"inputMilitaer[funktion]\" value=\"{$pers->current->militaer->funktion}\" /> "; if( $_REQUEST["promotion"] == "true" ) { $selectMilitaerZusatzFkt->selected = $pers->current->militaer->zusatzFktID; } $selectMilitaerZusatzFkt->output(); echo "</td></tr>";
        echo "<tr><td><label>" . translate( "Dienstgrad" ) . "</label>"; $selectMilitaerDienstgrad->output( array( "selected"=>$pers->current->militaer->dienstgradID+( $_REQUEST["promotion"] == "true" ? 1 : 0 ), "lang"=>$pers->person->sprache ) ); echo " "; $selectMilitaerZusatzDg->output( array( "lang"=>$pers->person->sprache ) ); echo "</td></tr>";
        echo "<tr><td><label>" . translate( "Website" ) . "</label><input type=\"text\" name=\"inputMilitaer[url]\" value=\"" . ( $_REQUEST["promotion"] == "true" ? $pers->current->militaer->url : "http://" ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea name=\"inputMilitaer[bemerkungen]\" rows=\"5\" cols=\"5\"></textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"{$pers->id}\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"militaer\" />";
        echo "<input type=\"hidden\" name=\"promotion\" value=\"{$_REQUEST["promotion"]}\" />";
        echo "<input type=\"hidden\" name=\"promotionmail\" value=\"false\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Hinzufügen" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; " . ( $_REQUEST["promotion"] == "true" ? "form.elements['promotionmail'].value = window.confirm( '" . translate( "E-Mail verschicken?" ) . "' );" : "" ) . " if( form.elements['inputMilitaer[datumBeginn]'].value && form.elements['inputMilitaer[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputMilitaer[datumEnde]'].value && form.elements['inputMilitaer[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&context=militaer'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    }
?>


<div class="accordeon accordeonProfile<?php if( $action == "EDIT" && $context == "militaer" || !$pers->militaer[$i] ) { echo " show"; } ?>">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Frühere Funktionen" ) . " (" . ( count( $pers->militaer )-$i ) . ")"; ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    // first element in the below written while loop needs to be identified in order to avoid div.seperator
    $counterForSeperator = 0;

    while( $pers->militaer[$i]->datumBeginn ) {
?>

<?php
    if( $action == "EDIT" && $element == $e ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"inputMilitaer[militaerID]\" value=\"{$pers->militaer[$i]->militaerID}\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputMilitaer[datumBeginn]\" value=\"{$pers->militaer[$i]->datumBeginn}\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputMilitaer[datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $pers->militaer[$i]->datumEnde ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Einteilung" ) . " *</label><input type=\"text\" name=\"inputMilitaer[einteilung]\" value=\"{$pers->militaer[$i]->einteilung}\" />" . ( $_SESSION["rights"]["roleID"] == 5 ? " <input type=\"text\" name=\"inputMilitaer[vorgesVb]\" value=\"{$pers->militaer[$i]->vorgesVb}\" />" : "" ) . "</td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><input type=\"text\" name=\"inputMilitaer[funktion]\" value=\"{$pers->militaer[$i]->funktion}\" /> "; $selectMilitaerZusatzFkt->output( array( "selected"=>$pers->militaer[$i]->zusatzFktID ) ); echo "</td></tr>";
        echo "<tr><td><label>" . translate( "Dienstgrad" ) . "</label>"; $selectMilitaerDienstgrad->output( array( "selected"=>$pers->militaer[$i]->dienstgradID, "lang"=>$pers->person->sprache ) ); echo " "; $selectMilitaerZusatzDg->output( array( "selected"=>$pers->militaer[$i]->zusatzDgID, "lang"=>$pers->person->sprache ) ); echo "</td></tr>";
        echo "<tr><td><label>" . translate( "Website" ) . "</label><input type=\"text\" name=\"inputMilitaer[url]\" value=\"" . ( $pers->militaer[$i]->url ? $pers->militaer[$i]->url : "http://" ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea name=\"inputMilitaer[bemerkungen]\" rows=\"5\" cols=\"5\">{$pers->militaer[$i]->bemerkungen}</textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"{$pers->id}\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"militaer\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputMilitaer[datumBeginn]'].value && form.elements['inputMilitaer[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputMilitaer[datumEnde]'].value && form.elements['inputMilitaer[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&context=militaer'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>

<dl class="list">
<?php
    if( $counterForSeperator != 0 ) {
        echo "<dd><div class=\"seperator\"></div></dd>";
    }
?>
    <dt>
<?php
    $datumEnde = substr( $pers->militaer[$i]->datumEnde, 0, 4 );
    $datumBeginn = substr( $pers->militaer[$i]->datumBeginn, 0, 4 );
    echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " " . str_replace( "9999", translate( "heute" ), $datumEnde ) );
?>
    </dt>
    <dd>
        <span class="bold">
            <?php if( $pers->militaer[$i]->funktion ) { echo translate( $pers->militaer[$i]->funktion ) . " " . translate( $pers->militaer[$i]->zusatzFkt ); ?>
            <a href="search.php?field=qryMilitaer.funktionMilitaerFull&amp;expr=<?php echo urlencode( $pers->militaer[$i]->funktion . " " . $pers->militaer[$i]->zusatzFkt ); ?>&amp;datumBeginn=<?php echo $pers->militaer[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->militaer[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
            <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum die gleiche Funktion hatten" ); ?>" /></a>
            <?php } ?>
        </span>
    </dd>

<?php if( $pers->militaer[$i]->einteilung ) { ?>
    <dt></dt>
    <dd>
<?php if( $pers->militaer[$i]->url ) { ?>
        <a href="<?php echo $pers->militaer[$i]->url; ?>" onclick="window.open(this.href); return false;">
<?php }
    echo translate( $pers->militaer[$i]->einteilung );
?>
<?php if( $pers->militaer[$i]->url ) { ?>
        </a>
<?php } ?>
        <a href="search.php?field=qryMilitaer.einteilung&amp;expr=<?php echo urlencode( $pers->militaer[$i]->einteilung ); ?>&amp;datumBeginn=<?php echo $pers->militaer[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->militaer[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum eingeteilt waren bei" ) . " " . $pers->militaer[$i]->einteilung; ?>" /></a>
    </dd>
<?php } ?>

<?php if( $pers->militaer[$i]->dienstgrad ) { ?>
    <dt></dt>
    <dd><?php echo translate( $pers->militaer[$i]->dienstgrad, $pers->person->sprache ) . " " .  translate( $pers->militaer[$i]->zusatzDg, $pers->person->sprache ); ?></dd>
<?php } ?>

<?php if( $pers->militaer[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->militaer[$i]->bemerkungen ); ?></dd>
<?php } ?>

<?php
    if( $_SESSION["rights"]["editProfile"] && !$shortprofile ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=militaer&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=militaer&amp;militaerID={$pers->militaer[$i]->militaerID}" ) );
            $handoverButton->output( array( "href"=>"handover.php?member0={$pers->id}&amp;militaerID={$pers->militaer[$i]->militaerID}&amp;context=militaer", "onclick"=>"window.open( this.href ); return false;", "alt"=>"Funktion übertragen", "title"=>"Funktion übertragen" ) );
        }
        echo "</dt>";
    }
?>
</dl>

<?php
        }  // end if
        $e++;

        // first element needs to be identified in order to avoid div.seperator
        $counterForSeperator++;
        $i++;
    }  // end while
    unset( $selectMilitaerZusatz, $selectMilitaerDienstgrad );

    if( $action !="ADD" && $_SESSION["rights"]["editProfile"] && !$shortprofile ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=militaer" ) );
    }
?>

<?php if( $pers->person->brevetierungsjahr ) { ?>
<dl class="list">
    <dd><div class="seperator"></div></dd>
    <dt><?php echo translate( "Brevetierungsjahr Gst" ); ?></dt>
    <dd>
        <?php echo substr( $pers->person->brevetierungsjahr, 0, 4 ); ?>
        <a href="search.php?field=brevetierungsdatum&amp;expr=<?php echo substr( $pers->person->brevetierungsjahr, 0, 4 ); ?>" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die das gleiche Brevetierungsjahr haben" ); ?>" /></a>
    </dd>
</dl>
<?php } ?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<?php if( !$shortprofile ) { ?>
<div class="accordeon accordeonProfile show">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Militärische Heimat" ) ?></span></a>
<div class="groupChildren">
<div class="child">


<?php
    if( $action == "EDIT" && $element == $e ) {
        $selectMilitaerTruppengattung = new select( "inputMilitaer2[truppengattungID]", "linkMilitaerTruppengattung", $pers->person->truppengattungID );
        $selectMilitaerTruppengattung->emptyrow = true;
        $selectMilitaerRS = new select( "inputMilitaer2[rsID]", "linkMilitaerRS", $pers->person->rsID );
        $selectMilitaerRS->emptyrow = true;

        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Truppengattung" ) . " *</label>"; $selectMilitaerTruppengattung->output(); echo "</td></tr>";
        echo "<tr><td><label>" . translate( "Meine Truppengattung befindet sich nicht in der Liste." ) . "<br />" . translate( "Bitte fügen Sie folgenden Eintrag hinzu" ) . ":</label>" . " <input class=\"inputDoubleLine\" name=\"truppengattung\" value=\"\" /> <input type=\"button\" value=\"" . translate( "Senden" ) . "\" onclick=\"sendXmlHttpRequest( 'missingEntry.php', 'personID={$pers->id}&name={$pers->person->name}&vorname={$pers->person->vorname}&truppengattung=' + document.forms['formMemberProfil'].elements['truppengattung'].value )\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Aushebung als" ) . " *</label>"; $selectMilitaerRS->output(); echo "</td></tr>";
        echo "<tr><td><label>" . translate( "Bitte fügen Sie folgenden Eintrag hinzu" ) . ":</label>" . " <input class=\"inputDoubleLine\" name=\"rs\" value=\"\" /> <input type=\"button\" value=\"" . translate( "Senden" ) . "\" onclick=\"sendXmlHttpRequest( 'missingEntry.php', 'personID={$pers->id}&name={$pers->person->name}&vorname={$pers->person->vorname}&rs=' + document.forms['formMemberProfil'].elements['rs'].value )\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"{$pers->id}\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"militaer\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=militaer'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";

        unset( $selectMilitaerTruppengattung, $selectMilitaerRS );
    } else {
?>

<dl class="list">
    <dt><?php echo translate( "RS als" ); ?></dt>
    <dd>
<?php
    if( $pers->person->rs ) {
        echo translate( $pers->person->rs );
        echo " <a href=\"search.php?field=linkMilitaerRS.rs&amp;expr=" . urlencode( $pers->person->rs ) . "\" onclick=\"window.open(this.href); return false;\">";
        echo "<img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die die gleiche RS absolviert haben" ) . "\" /></a>";
    } else {
        echo translate( "fehlt" );
    }
?>
    </dd>
    <dt><?php echo translate( "Ursprüngl. Truppengattung" ); ?></dt>
    <dd>
<?php
    if( $pers->person->truppengattung ) {
        echo translate( $pers->person->truppengattung );
        echo " <a href=\"search.php?field=linkMilitaerTruppengattung.truppengattung&amp;expr=" . urlencode( $pers->person->truppengattung ) . "\" onclick=\"window.open(this.href); return false;\">";
        echo "<img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die die gleiche Truppengattung haben" ) . "\" /></a>";
    } else {
        echo translate( "fehlt" );
    }
?>
    </dd>
<?php
    if( $_SESSION["rights"]["editProfile"] ) {
        echo "<dt>";
        $editButton->output( array( "class"=>"editButtonRight", "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=militaer&amp;element=$e" ) );
        echo "</dt>";
    }
?>
</dl>

<?php
    }  // end if
    $e++;
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->
<?php } // if( !$shortprofile ) ?>

</div><!-- end #tabmilitaer -->


<?php
    if( !$shortprofile ) {
?>
<!-- new tab -->

<div class="tabContent" id="tabBeruf"<?php if( $context == "beruf" || $context == "ausbildung" ) { echo " style=\"display:block\""; } ?>>

<div class="accordeon accordeonProfile show">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/expanded.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Tätigkeiten" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $i = 0;
    while( $pers->beruf[$i]->datumEnde >= date( "Y-m-d" ) ) {
        if( $action == "EDIT" && $element == $e ) {
            echo "<fieldset class=\"fieldsetEditForm\">";
            echo "<table>";
            echo "<tr><td><input type=\"hidden\" name=\"inputBeruf[berufID]\" value=\"" . $pers->beruf[$i]->berufID . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputBeruf[datumBeginn]\" value=\"" . $pers->beruf[$i]->datumBeginn . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputBeruf[datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $pers->beruf[$i]->datumEnde ) . "\" /></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><label>" . translate( "Branche" ) . "</label><input type=\"text\" name=\"inputBeruf[branche]\" value=\"" . $pers->beruf[$i]->branche . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Organisation" ) . " *</label><input type=\"text\" name=\"inputBeruf[organisation]\" value=\"" . $pers->beruf[$i]->organisation . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Abteilung" ) . "</label><input type=\"text\" name=\"inputBeruf[abteilung]\" value=\"" . $pers->beruf[$i]->abteilung . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><input type=\"text\" name=\"inputBeruf[funktion]\" value=\"" . $pers->beruf[$i]->funktion . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Website geschäftlich" ) . "</label><input type=\"text\" name=\"inputBeruf[url]\" value=\""; echo !$pers->beruf[$i]->url ? "http://" : $pers->beruf[$i]->url; echo "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Position" ) . " *</label><select name=\"inputBeruf[positionID]\">"; makeOptions( "linkBerufPosition", $pers->beruf[$i]->positionID, true ); echo "</select></td></tr>";
            echo "<tr><td><label>" . translate( "Typ" ) . " *</label><select name=\"inputBeruf[typID]\">"; makeOptions( "linkBerufTyp", $pers->beruf[$i]->typID, true ); echo "</select></td></tr>";
            echo "<tr><td><label>" . translate( "Stichworte" ) . "</label><input type=\"text\" name=\"inputBeruf[tags]\" value=\"" . $pers->beruf[$i]->tags . "\" /></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><label>" . translate( "Adresse" ) . "</label><input type=\"text\" name=\"inputBeruf[adresse]\" value=\"" . $pers->beruf[$i]->adresse . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Postfach" ) . "</label><input type=\"text\" name=\"inputBeruf[postfach]\" value=\"" . $pers->beruf[$i]->postfach . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Postleitzahl" ) . "</label><input type=\"text\" name=\"inputBeruf[plz]\" value=\"" . $pers->beruf[$i]->plz . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Ort" ) . "</label><input type=\"text\" name=\"inputBeruf[ort]\" value=\"" . $pers->beruf[$i]->ort . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Land" ) . "</label><input type=\"text\" name=\"inputBeruf[land]\" value=\"" . $pers->beruf[$i]->land . "\" /> <a href=\"http://maps.google.com/\" onmousedown=\"this.href='http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q='+document.forms['formMemberProfil'].elements['inputBeruf[adresse]'].value.replace(' ', '+')+',+'+document.forms['formMemberProfil'].elements['inputBeruf[ort]'].value.replace(' ', '+')+',+'+document.forms['formMemberProfil'].elements['inputBeruf[land]'].value.replace(' ', '+')+'&amp;iwloc=addr'\" onclick=\"window.open(this.href); return false;\" title=\"" . translate( "Adresse prüfen" ) . "\">" . translate( "Adresse prüfen" ) . "</a></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><label>" . translate( "Telefon" ) . "</label><input type=\"text\" name=\"inputBeruf[tf]\" value=\""; echo !$pers->beruf[$i]->tf ? "+41 xx xxx xxxx" : $pers->beruf[$i]->tf; echo "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "E-Mail geschäftlich" ) . "</label><input type=\"text\" name=\"inputBeruf[email]\" value=\"" . $pers->beruf[$i]->email . "\" /></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea name=\"inputBeruf[bemerkungen]\" rows=\"5\" cols=\"5\">{$pers->beruf[$i]->bemerkungen}</textarea></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
            echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
            echo "<input type=\"hidden\" name=\"context\" value=\"beruf\" />";
            echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputBeruf[positionID]'].value != 0 && form.elements['inputBeruf[typID]'].value != 0 ) { if( form.elements['inputBeruf[datumBeginn]'].value && form.elements['inputBeruf[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputBeruf[datumEnde]'].value && form.elements['inputBeruf[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
            echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=beruf'\" /></td></tr>";
            echo "</table>";
            echo "</fieldset>";
        } else {
?>

<dl class="list">
<?php if( $i != 0 ) { echo "<dd><div class=\"seperator\"></div></dd>"; } ?>
    <dt>
<?php
    $datumEnde = substr( $pers->beruf[$i]->datumEnde, 0, 4 );
    $datumBeginn = substr( $pers->beruf[$i]->datumBeginn, 0, 4 );
    if( $pers->beruf[$i]->datumBeginn > date( "Y-m-d" ) ) {
        echo translate( "Ab" ) . " $datumBeginn";
    } elseif( $datumEnde == "9999" ) {
        echo translate( "Seit" ) . " $datumBeginn";
    } else {
        echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " $datumEnde" );
    }
?>
    </dt>
    <dd>
        <span class="bold">
            <?php if( $pers->beruf[$i]->funktion ) { echo $pers->beruf[$i]->funktion; ?>
            <a href="search.php?field=qryBeruf.funktion&amp;expr=<?php echo urlencode( $pers->beruf[$i]->funktion ); ?>&amp;datumBeginn=<?php echo $pers->beruf[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->beruf[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
            <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum die gleiche Funktion hatten" ); ?>" /></a>
            <?php } echo $pers->beruf[$i]->titelBezeichnung; ?>
        </span>
    </dd>
    <dt></dt>
    <dd>
<?php
    if ( $pers->beruf[$i]->organisation ) {
        if ( $pers->beruf[$i]->url ) {
            echo "<a href=\"" . $pers->beruf[$i]->url . "\" onclick=\"window.open(this.href); return false;\">";
        }
        echo $pers->beruf[$i]->organisation;
        if ( $pers->beruf[$i]->url ) {
            echo "</a>";
        }
?>
        <a href="search.php?field=qryBeruf.organisation&amp;expr=<?php echo urlencode( $pers->beruf[$i]->organisation ); ?>&amp;datumBeginn=<?php echo $pers->beruf[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->beruf[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum angestellt waren bei" ) . " " . $pers->beruf[$i]->organisation; ?>" /></a>
<?php
    }
    if ( $pers->beruf[$i]->abteilung ) {
        echo ", " . $pers->beruf[$i]->abteilung;
?>
        <a href="search.php?field=qryBeruf.abteilung&amp;expr=<?php echo urlencode( $pers->beruf[$i]->abteilung ); ?>&amp;datumBeginn=<?php echo $pers->beruf[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->beruf[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum in der gleichen Abteilung arbeiteten" ); ?>" /></a>
<?php
    }
?>
    </dd>

<?php
    if( $_SESSION["rights"]["editProfile"] ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=beruf&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=beruf&amp;berufID={$pers->beruf[$i]->berufID}" ) );
            $handoverButton->output( array( "href"=>"handover.php?member0={$pers->id}&amp;berufID={$pers->beruf[$i]->berufID}&amp;context=beruf", "onclick"=>"window.open( this.href ); return false;", "alt"=>"Funktion übertragen", "title"=>"Funktion übertragen" ) );
        }
        echo "</dt>";
    }
?>
</dl>

<dl class="secondList">
<?php if( $pers->beruf[$i]->branche ) { ?>
    <dt></dt>
    <dd>
        <?php echo $pers->beruf[$i]->branche; ?>
        <a href="search.php?field=qryBeruf.branche&amp;expr=<?php echo urlencode( $pers->beruf[$i]->branche ); ?>&amp;datumBeginn=<?php echo $pers->beruf[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->beruf[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum in der gleichen Branche tätig waren" ); ?>" /></a>
    </dd>
<?php } ?>

<?php if( $pers->beruf[$i]->position || $pers->beruf[$i]->typ ) { ?>
    <dt></dt>
    <dd><?php echo $pers->beruf[$i]->typ; if( $pers->beruf[$i]->position && $pers->beruf[$i]->typ ) { echo ", "; } echo $pers->beruf[$i]->position; ?></dd>
<?php } ?>

<?php if( $pers->beruf[$i]->adresse || $pers->beruf[$i]->plz || $pers->beruf[$i]->ort ) { ?>
    <dt></dt>
    <dd><a href="http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q=<?php echo str_ireplace( " ", "+", $pers->beruf[$i]->adresse) ; ?>,+<?php echo str_ireplace( " ", "+", $pers->beruf[$i]->ort ); ?>,+<?php echo str_ireplace( " ", "+", $pers->beruf[$i]->land ); ?>&amp;iwloc=addr" onclick="window.open(this.href); return false;" title="<?php echo translate( "Adresse auf GoogleMaps anzeigen" ) ?>"><?php echo $pers->beruf[$i]->adresse; if( $pers->beruf[$i]->adresse ) { echo ", "; } echo $pers->beruf[$i]->postfach; if( $pers->beruf[$i]->postfach ) { echo ", "; } echo $pers->beruf[$i]->plz . " " . $pers->beruf[$i]->ort; if( $pers->beruf[$i]->land != "Schweiz" && $pers->beruf[$i]->land != "Suisse" && $pers->beruf[$i]->land != "Svizzera" && $pers->beruf[$i]->land != "Switzerland" && $pers->beruf[$i]->land != "" ) { echo ", " . $pers->beruf[$i]->land; } ?></a></dd>
<?php } ?>

<?php if( $pers->beruf[$i]->tf ) { ?>
    <dt></dt>
    <dd><a href="callto:<?php echo str_ireplace( array( " ", "(0)" ), "", $pers->beruf[$i]->tf ); ?>" title="<?php echo translate( "Anrufen" ) ?>"><?php echo $pers->beruf[$i]->tf; ?></a></dd>
<?php } ?>

<?php if( $pers->beruf[$i]->email ) { ?>
    <dt></dt>
    <dd><a href="mailto:<?php echo $pers->beruf[$i]->email; ?>" title="<?php echo translate( "E-Mail schreiben" ) ?>"><?php echo $pers->beruf[$i]->email; ?></a></dd>
<?php } ?>

<?php if( $pers->beruf[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->beruf[$i]->bemerkungen ); ?></dd>
<?php } ?>

<?php if( $pers->beruf[$i]->tags ) { ?>
    <dt></dt>
    <dd><?php echo $pers->beruf[$i]->tags; ?></dd>
<?php } ?>
</dl>

<?php
        }  // end if
        $e++;
?>

<?php
        $i++;
    }  // end while

    if( $action !="ADD" && $_SESSION["rights"]["editProfile"] ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=beruf" ) );
    }
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php
    if( $action == "ADD" && $context == "beruf" ) {
        echo "<fieldset class=\"fieldsetEditForm\">";

        echo "<table>";
        echo "<tr><td class=\"station\"><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputBeruf[datumBeginn]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputBeruf[datumEnde]\" value=\"\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"station\"><label>" . translate( "Branche" ) . "</label><input type=\"text\" name=\"inputBeruf[branche]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Organisation" ) . " *</label><input type=\"text\" name=\"inputBeruf[organisation]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Abteilung" ) . "</label><input type=\"text\" name=\"inputBeruf[abteilung]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Funktion" ) . " *</label><input type=\"text\" name=\"inputBeruf[funktion]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Website geschäftlich" ) . "</label><input type=\"text\" name=\"inputBeruf[url]\" value=\"http://\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Position" ) . " *</label><select name=\"inputBeruf[positionID]\">"; makeOptions( "linkBerufPosition", 0, true ); echo "</select></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Typ" ) . " *</label><select name=\"inputBeruf[typID]\">"; makeOptions( "linkBerufTyp", 0, true ); echo "</select></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Stichworte" ) . "</label><input type=\"text\" name=\"inputBeruf[tags]\" value=\"\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"station\"><label>" . translate( "Adresse" ) . "</label><input type=\"text\" name=\"inputBeruf[adresse]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Postfach" ) . "</label><input type=\"text\" name=\"inputBeruf[postfach]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Postleitzahl" ) . "</label><input type=\"text\" name=\"inputBeruf[plz]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Ort" ) . "</label><input type=\"text\" name=\"inputBeruf[ort]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "Land" ) . "</label><input type=\"text\" name=\"inputBeruf[land]\" value=\"" . translate( "Schweiz" ) . "\" /> <a href=\"http://maps.google.com/\" onmousedown=\"this.href='http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q='+document.forms['formMemberProfil'].elements['inputBeruf[adresse]'].value.replace(' ', '+')+',+'+document.forms['formMemberProfil'].elements['inputBeruf[ort]'].value.replace(' ', '+')+',+'+document.forms['formMemberProfil'].elements['inputBeruf[land]'].value.replace(' ', '+')+'&amp;iwloc=addr'\" onclick=\"window.open(this.href); return false;\" title=\"" . translate( "Adresse prüfen" ) . "\">" . translate( "Adresse prüfen" ) . "</a></td></tr>";
        echo "<table>";
        echo "<tr><td class=\"station\"><label>" . translate( "Telefon geschäftlich" ) . "</label><input type=\"text\" name=\"inputBeruf[tf]\" value=\"+41 xx xxx xxxx\" /></td></tr>";
        echo "<tr><td class=\"station\"><label>" . translate( "E-Mail geschäftlich" ) . "</label><input type=\"text\" name=\"inputBeruf[email]\" value=\"\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"station\"><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputBeruf[bemerkungen]\"></textarea></td></tr>";
        echo "</table>";

        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"beruf\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Hinzufügen" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputBeruf[positionID]'].value != 0 && form.elements['inputBeruf[typID]'].value != 0 ) { if( form.elements['inputBeruf[datumBeginn]'].value && form.elements['inputBeruf[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputBeruf[datumEnde]'].value && form.elements['inputBeruf[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=beruf'\" /></td></tr>";
        echo "</table>";

        echo "</fieldset>";
    }
?>

<div class="accordeon accordeonProfile<?php if( $action == "EDIT" && $context == "beruf" || !$pers->beruf[$i] ) { echo " show"; } ?>">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Frühere Tätigkeiten" ) . " (" . ( count( $pers->beruf )-$i ) . ")"; ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    // first element in the below written while loop needs to be identified in order to avoid div.seperator
    $counterForSeperator = 0;

    while( $pers->beruf[$i]->datumBeginn ) {
        if( $action == "EDIT" && $element == $e ) {
            echo "<fieldset class=\"fieldsetEditForm\">";
            echo "<table>";
            echo "<tr><td><input type=\"hidden\" name=\"inputBeruf[berufID]\" value=\"" . $pers->beruf[$i]->berufID . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputBeruf[datumBeginn]\" value=\"" . $pers->beruf[$i]->datumBeginn . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputBeruf[datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $pers->beruf[$i]->datumEnde ) . "\" /></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><label>" . translate( "Branche" ) . "</label><input type=\"text\" name=\"inputBeruf[branche]\" value=\"" . $pers->beruf[$i]->branche . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Organisation" ) . "* </label><input type=\"text\" name=\"inputBeruf[organisation]\" value=\"" . $pers->beruf[$i]->organisation . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Abteilung" ) . "</label><input type=\"text\" name=\"inputBeruf[abteilung]\" value=\"" . $pers->beruf[$i]->abteilung . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Funktion" ) . "* </label><input type=\"text\" name=\"inputBeruf[funktion]\" value=\"" . $pers->beruf[$i]->funktion . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Website geschäftlich" ) . "</label><input type=\"text\" name=\"inputBeruf[url]\" value=\""; echo !$pers->beruf[$i]->url ? "http://" : $pers->beruf[$i]->url; echo "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Position" ) . "* </label><select name=\"inputBeruf[positionID]\">"; makeOptions( "linkBerufPosition", $pers->beruf[$i]->positionID, true ); echo "</select></td></tr>";
            echo "<tr><td><label>" . translate( "Typ" ) . "* </label><select name=\"inputBeruf[typID]\">"; makeOptions( "linkBerufTyp", $pers->beruf[$i]->typID, true ); echo "</select></td></tr>";
            echo "<tr><td><label>" . translate( "Stichworte" ) . "</label><input type=\"text\" name=\"inputBeruf[tags]\" value=\"" . $pers->beruf[$i]->tags . "\" /></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><label>" . translate( "Adresse" ) . "</label><input type=\"text\" name=\"inputBeruf[adresse]\" value=\"" . $pers->beruf[$i]->adresse . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Postfach" ) . "</label><input type=\"text\" name=\"inputBeruf[postfach]\" value=\"" . $pers->beruf[$i]->postfach . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Postleitzahl" ) . "</label><input type=\"text\" name=\"inputBeruf[plz]\" value=\"" . $pers->beruf[$i]->plz . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Ort" ) . "</label><input type=\"text\" name=\"inputBeruf[ort]\" value=\"" . $pers->beruf[$i]->ort . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Land" ) . "</label><input type=\"text\" name=\"inputBeruf[land]\" value=\"" . $pers->beruf[$i]->land . "\" /> <a href=\"http://maps.google.com/\" onmousedown=\"this.href='http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q='+document.forms['formMemberProfil'].elements['inputBeruf[adresse]'].value.replace(' ', '+')+',+'+document.forms['formMemberProfil'].elements['inputBeruf[ort]'].value.replace(' ', '+')+',+'+document.forms['formMemberProfil'].elements['inputBeruf[land]'].value.replace(' ', '+')+'&amp;iwloc=addr'\" onclick=\"window.open(this.href); return false;\" title=\"" . translate( "Adresse prüfen" ) . "\">" . translate( "Adresse prüfen" ) . "</a></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><label>" . translate( "Telefon" ) . "</label><input type=\"text\" name=\"inputBeruf[tf]\" value=\""; echo !$pers->beruf[$i]->tf ? "+41 xx xxx xxxx" : $pers->beruf[$i]->tf; echo "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "E-Mail geschäftlich" ) . "</label><input type=\"text\" name=\"inputBeruf[email]\" value=\"" . $pers->beruf[$i]->email . "\" /></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputBeruf[bemerkungen]\">" . $pers->beruf[$i]->bemerkungen . "</textarea></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
            echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
            echo "<input type=\"hidden\" name=\"context\" value=\"beruf\" />";
            echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputBeruf[positionID]'].value != 0 && form.elements['inputBeruf[typID]'].value != 0 ) { if( form.elements['inputBeruf[datumBeginn]'].value && form.elements['inputBeruf[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputBeruf[datumEnde]'].value && form.elements['inputBeruf[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
            echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=beruf'\" /></td></tr>";
            echo "</table>";
            echo "</fieldset>";
        } else {
?>

<dl class="list">
<?php
    if ( $counterForSeperator != 0 ) {
        echo "<dd><div class=\"seperator\"></div></dd>";
    }
?>
    <dt>
<?php
    $datumEnde = substr( $pers->beruf[$i]->datumEnde, 0, 4 );
    $datumBeginn = substr( $pers->beruf[$i]->datumBeginn, 0, 4 );
    echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " " . str_replace( "9999", translate( "heute" ), $datumEnde ) );
?>
    </dt>
    <dd>
        <span class="bold">
            <?php if( $pers->beruf[$i]->funktion ) { echo $pers->beruf[$i]->funktion; ?>
            <a href="search.php?field=qryBeruf.funktion&amp;expr=<?php echo urlencode( $pers->beruf[$i]->funktion ); ?>&amp;datumBeginn=<?php echo $pers->beruf[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->beruf[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
            <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum die gleiche Funktion hatten" ); ?>" /></a>
            <?php } echo $pers->beruf[$i]->titelBezeichnung; ?>
        </span>
    </dd>
    <dt></dt>
    <dd>
<?php
    if ( $pers->beruf[$i]->organisation ) {
        if ( $pers->beruf[$i]->url ) {
            echo "<a href=\"" . $pers->beruf[$i]->url . "\" onclick=\"window.open(this.href); return false;\">";
        }
        echo $pers->beruf[$i]->organisation;
        if ( $pers->beruf[$i]->url ) {
            echo "</a>";
        }
?>
        <a href="search.php?field=qryBeruf.organisation&amp;expr=<?php echo urlencode( $pers->beruf[$i]->organisation ); ?>&amp;datumBeginn=<?php echo $pers->beruf[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->beruf[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum angestellt waren bei" ) . " " . $pers->beruf[$i]->organisation; ?>" /></a>
<?php
    }
    if ( $pers->beruf[$i]->abteilung ) {
        echo ", " . $pers->beruf[$i]->abteilung;
?>
        <a href="search.php?field=qryBeruf.abteilung&amp;expr=<?php echo urlencode( $pers->beruf[$i]->abteilung ); ?>&amp;datumBeginn=<?php echo $pers->beruf[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->beruf[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum in der gleichen Abteilung arbeiteten" ); ?>" /></a>
<?php
    }
?>
    </dd>
<?php
    if( $pers->beruf[$i]->branche ) {
?>
    <dt></dt>
    <dd>
        <?php echo $pers->beruf[$i]->branche; ?>
        <a href="search.php?field=qryBeruf.branche&amp;expr=<?php echo urlencode( $pers->beruf[$i]->branche ); ?>&amp;datumBeginn=<?php echo $pers->beruf[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->beruf[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum in der gleichen Branche tätig waren" ); ?>" /></a>
    </dd>
<?php
    }
?>

<?php if( $pers->beruf[$i]->position || $pers->beruf[$i]->typ ) { ?>
    <dt></dt>
    <dd><?php echo $pers->beruf[$i]->typ; if( $pers->beruf[$i]->position && $pers->beruf[$i]->typ ) { echo ", "; } echo $pers->beruf[$i]->position; ?></dd>
<?php } ?>

<?php if( $pers->beruf[$i]->ort ) { ?>
    <dt></dt>
    <dd><a href="http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q=<?php echo str_ireplace( " ", "+", $pers->beruf[$i]->adresse ); ?>,+<?php echo str_ireplace( " ", "+", $pers->beruf[$i]->ort ); ?>,+<?php echo str_ireplace( " ", "+", $pers->beruf[$i]->land ); ?>&amp;iwloc=addr" onclick="window.open(this.href); return false;" title="<?php echo translate( "Adresse auf GoogleMaps anzeigen" ) ?>"><?php echo $pers->beruf[$i]->ort; if( $pers->beruf[$i]->land != "Schweiz" && $pers->beruf[$i]->land != "Suisse" && $pers->beruf[$i]->land != "Svizzera" && $pers->beruf[$i]->land != "Switzerland" && $pers->beruf[$i]->land != "" ) { echo ", " . $pers->beruf[$i]->land; } ?></a></dd>
<?php } ?>

<?php if( $pers->beruf[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->beruf[$i]->bemerkungen ); ?></dd>
<?php } ?>

<?php
    if( $_SESSION["rights"]["editProfile"] ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=beruf&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=beruf&amp;berufID={$pers->beruf[$i]->berufID}" ) );
            $handoverButton->output( array( "href"=>"handover.php?member0={$pers->id}&amp;berufID={$pers->beruf[$i]->berufID}&amp;context=beruf", "onclick"=>"window.open( this.href ); return false;", "alt"=>"Funktion übertragen", "title"=>"Funktion übertragen" ) );
        }
        echo "</dt>";
    }
?>
</dl>


<?php
        }  // end if
        $e++;
?>

<?php
        // first element needs to be identified in order to avoid div.seperator
        $counterForSeperator++;
        $i++;
    }  // end while

    if( $action !="ADD" && $_SESSION["rights"]["editProfile"] ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=beruf" ) );
    }
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile<?php if( $context == "ausbildung" || !$pers->ausbildung[0] ) { echo " show"; } ?>">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Abschlüsse" ) . " (" . ( count( $pers->ausbildung ) ) . ")"; ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $i = 0;
    // first element in the below written while loop needs to be identified in order to avoid div.seperator
    $counterForSeperator = 0;
    $selectAusbildungTypID = new select( "inputAusbildung[typID]", "linkBerufAusbildungTyp" );
    $selectAusbildungTypID->emptyrow = true;

    while( $pers->ausbildung[$i]->datumEnde ) {
        if( $action == "EDIT" && $element == $e ) {
            echo "<fieldset class=\"fieldsetEditForm\">";
            echo "<table>";
            echo "<tr><td><input type=\"hidden\" name=\"inputAusbildung[ausbildungID]\" value=\"" . $pers->ausbildung[$i]->ausbildungID . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Abschlussdatum" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputAusbildung[datumEnde]\" value=\"" . str_replace( "0000-00-00", "", $pers->ausbildung[$i]->datumEnde ) . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Institution" ) . " *</label><input type=\"text\" name=\"inputAusbildung[institution]\" value=\"" . $pers->ausbildung[$i]->institution . "\" /></td></tr>";
            echo "<tr><td><label>" . translate( "Titel" ) . "</label><input type=\"text\" name=\"inputAusbildung[titel]\" value=\"" . $pers->ausbildung[$i]->titel . "\" /></td></tr>";
            if( $_SESSION["rights"]["roleID"] == 5 ) {
                echo "<tr><td><label>" . translate( "Typ" ) . "</label>"; $selectAusbildungTypID->output( array( "selected"=>$pers->ausbildung[$i]->typID ) ); echo "</td></tr>";
            }
            echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputAusbildung[bemerkungen]\">" . $pers->ausbildung[$i]->bemerkungen . "</textarea></td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
            echo "</table>";
            echo "<table>";
            echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
            echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
            echo "<input type=\"hidden\" name=\"context\" value=\"ausbildung\" />";
            echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputAusbildung[datumEnde]'].value && form.elements['inputAusbildung[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); }\" />";
            echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=ausbildung'\" /></td></tr>";
            echo "</table>";
            echo "</fieldset>";
        } else {
?>

<dl class="list">
<?php
    if ( $counterForSeperator != 0 ) {
        echo "<dd><div class=\"seperator\"></div></dd>";
    }
?>
    <dt>
<?php
    $datum = substr( $pers->ausbildung[$i]->datumEnde, 0, 4 );
    echo str_replace( "0000", "???", $datum );
?>
    </dt>
    <dd>
<?php
    if( $pers->ausbildung[$i]->titel ) {
?>
        <span class="bold"><?php echo $pers->ausbildung[$i]->titel; ?></span>
        <a href="search.php?field=qryBerufAusbildung.titel&amp;expr=<?php echo urlencode( $pers->ausbildung[$i]->titel ); ?>" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder mit diesem Abschluss" ); ?>" /></a>
<?php
    }
?>
    </dd>
    <dt></dt>
    <dd>
        <?php echo $pers->ausbildung[$i]->institution; ?>
        <a href="search.php?field=qryBerufAusbildung.institution&amp;expr=<?php echo urlencode( $pers->ausbildung[$i]->institution ); ?>" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die einen Abschluss haben von" ) . " " . $pers->ausbildung[$i]->institution; ?>" /></a>
    </dd>
<?php
    if( $pers->ausbildung[$i]->typ && $_SESSION["rights"]["roleID"] == 5 ) {
?>
    <dt></dt>
    <dd>
        <?php echo $pers->ausbildung[$i]->typ; ?>
    </dd>
<?php
    }
?>

<?php if( $pers->ausbildung[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->ausbildung[$i]->bemerkungen); ?></dd>
<?php } ?>

<?php
    if( $_SESSION["rights"]["editProfile"] ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=ausbildung&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=ausbildung&amp;ausbildungID={$pers->ausbildung[$i]->ausbildungID}" ) );
        }
        echo "</dt>";
    }
?>
</dl>


<?php
        }  // end if
        $e++;
?>

<?php
        // first element needs to be identified in order to avoid div.seperator
        $counterForSeperator++;
        $i++;
    }  // end while

    if( $action !="ADD" && $_SESSION["rights"]["editProfile"] ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=ausbildung" ) );
    }
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php
    if( $action == "ADD" && $context == "ausbildung" ) {
        echo "<fieldset class=\"fieldsetEditForm\">";

        echo "<table>";
        echo "<tr><td class=\"abschluss\"><label>" . translate( "Abschlussdatum" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputAusbildung[datumEnde]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"abschluss\"><label>" . translate( "Institution" ) . " *</label><input type=\"text\" name=\"inputAusbildung[institution]\" value=\"\" /></td></tr>";
        echo "<tr><td class=\"abschluss\"><label>" . translate( "Titel" ) . "</label><input type=\"text\" name=\"inputAusbildung[titel]\" value=\"\" /></td></tr>";
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            echo "<tr><td><label>" . translate( "Typ" ) . "</label>"; $selectAusbildungTypID->output( array( "selected"=>$pers->ausbildung[$i]->typID ) ); echo "</td></tr>";
        }
        echo "<tr><td class=\"abschluss\"><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputAusbildung[bemerkungen]\"></textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"ausbildung\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Hinzufügen" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputAusbildung[datumEnde]'].value && form.elements['inputAusbildung[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=ausbildung'\" /></td></tr>";
        echo "</table>";

        echo "</fieldset>";
    }

    unset( $selectAusbildungTypID );
?>


<div class="accordeon accordeonProfile<?php if( $action == "EDIT" && $context == "beruf" ) { echo " show"; } ?>">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Publikationen, Quellen und Diverses" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    if( $action == "EDIT" && $element == $e ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Publikationen" ) . "</label><textarea rows=\"5\" cols=\"5\" type=\"text\" name=\"inputBeruf2[publikationen]\">" . $pers->person->publikationen . "</textarea></td></tr>";
        echo "<tr><td><label>" . translate( "Quellen" ) . "</label><textarea rows=\"5\" cols=\"5\" type=\"text\" name=\"inputBeruf2[quellen]\">" . $pers->person->quellen . "</textarea></td></tr>";
        echo "<tr><td><label>" . translate( "Diverses" ) . "</label><textarea rows=\"5\" cols=\"5\" type=\"text\" name=\"inputBeruf2[diverses]\">" . $pers->person->diverses . "</textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"beruf\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=beruf'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>

<?php if( $pers->person->publikationen || !$pers->person->quellen || $pers->person->diverses ) { ?>
<dl class="list">
<?php if( $pers->person->publikationen ) { ?>
    <dt><?php echo translate( "Publikationen" ); ?></dt>
    <dd><?php echo $pers->person->publikationen; ?></dd>
<?php } ?>
<?php if( $pers->person->quellen ) { ?>
    <dt><?php echo translate( "Quellen" ); ?></dt>
    <dd><?php echo $pers->person->quellen; ?></dd>
<?php } ?>
<?php if( $pers->person->diverses ) { ?>
    <dt><?php echo translate( "Diverses" ); ?></dt>
    <dd><?php echo $pers->person->diverses; ?></dd>
<?php } ?>
<?php
    if( $_SESSION["rights"]["editProfile"] ) {
        echo "<dt>";
        $editButton->output( array( "class"=>"editButton2", "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=beruf&amp;element=$e" ) );
        echo "</dt>";
    }
?>
</dl>
<?php
    } // end if( $pers->person->publikationen || !$pers->person->quellen || $pers->person->diverses )

    } // end if
    $e++;
?>


</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->
<!-- end #tabberuf profile in viewing mode -->

</div><!-- end #tabberuf -->



<!-- new tab -->

<div class="tabContent" id="tabPublic"<?php if( $context == "public" ) { echo " style=\"display:block\""; } ?>>

<div class="accordeon accordeonProfile show">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/expanded.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Funktion(en)" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $i = 0;
    while( $pers->public[$i]->datumEnde >= date( "Y-m-d" ) ) {
?>

<?php
    if( $action == "EDIT" && $element == $e ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"inputPublic[publicID]\" value=\"" . $pers->public[$i]->publicID . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputPublic[datumBeginn]\" value=\"" . $pers->public[$i]->datumBeginn . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputPublic[datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $pers->public[$i]->datumEnde ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Organisation" ) . " *</label><input type=\"text\" name=\"inputPublic[organisation]\" value=\"" . $pers->public[$i]->organisation . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Website" ) . "</label><input type=\"text\" name=\"inputPublic[url]\" value=\""; echo !$pers->public[$i]->url ? "http://" : $pers->public[$i]->url; echo "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><input type=\"text\" name=\"inputPublic[publicFunktion]\" value=\"" . $pers->public[$i]->publicFunktion . "\" ></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputPublic[bemerkungen]\">" . $pers->public[$i]->bemerkungen . "</textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"public\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputPublic[organisation]'].value != '' && form.elements['inputPublic[publicFunktion]'].value != '' ) { if( form.elements['inputPublic[datumBeginn]'].value && form.elements['inputPublic[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputPublic[datumEnde]'].value && form.elements['inputPublic[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=public'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>

<dl class="list">
<?php
    if ( $i != 0 ) {
        echo "<dd><div class=\"seperator\"></div></dd>";
    }
?>
    <dt>
<?php
    $datumEnde = substr( $pers->public[$i]->datumEnde, 0, 4 );
    $datumBeginn = substr( $pers->public[$i]->datumBeginn, 0, 4 );
    if( $pers->public[$i]->datumBeginn > date( "Y-m-d" ) ) {
        echo translate( "Ab" ) . " $datumBeginn";
    } elseif( $datumEnde == "9999" ) {
        echo translate( "Seit" ) . " $datumBeginn";
    } else {
        echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " $datumEnde" );
    }
?>
    </dt>
    <dd>
        <span class="bold">
<?php if( $pers->public[$i]->url ) { ?>
            <a href="<?php echo $pers->public[$i]->url; ?>" onclick="window.open(this.href); return false;">
<?php }
    echo $pers->public[$i]->organisation;
    if( $pers->public[$i]->url ) { ?>
            </a>
<?php } ?>
        </span>
        <a href="search.php?field=tblPublic.organisation&amp;expr=<?php echo urlencode( $pers->public[$i]->organisation ); ?>&amp;datumBeginn=<?php echo $pers->public[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->public[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum Mitglied waren bei" ) . " " . $pers->public[$i]->organisation; ?>" /></a>
    </dd>

<?php
    if( $_SESSION["rights"]["editProfile"] ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=public&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=public&amp;publicID={$pers->public[$i]->publicID}" ) );
        }
        echo "</dt>";
    }
?>
</dl>

<dl class="secondList">
<?php if( $pers->public[$i]->publicFunktion ) { ?>
    <dt></dt>
    <dd><?php echo $pers->public[$i]->publicFunktion; ?></dd>
<?php } ?>
<?php if( $pers->public[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->public[$i]->bemerkungen ); ?></dd>
<?php } ?>
</dl>

<?php
    }  // end if
    $e++;
?>

<?php
    $i++;
    }  // end while

    if( $action !="ADD" && $_SESSION["rights"]["editProfile"] ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=public" ) );
    }
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php
    if( $action == "ADD" && $context == "public" ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputPublic[datumBeginn]\" value=\"\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputPublic[datumEnde]\" value=\"\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Organisation" ) . " *</label><input type=\"text\" name=\"inputPublic[organisation]\" value=\"\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Website" ) . "</label><input type=\"text\" name=\"inputPublic[url]\" value=\"http://\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><input type=\"text\" name=\"inputPublic[publicFunktion]\" value=\"\" ></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputPublic[bemerkungen]\"></textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"public\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Hinzufügen" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputPublic[organisation]'].value != '' && form.elements['inputPublic[publicFunktion]'].value != '' ) { if( form.elements['inputPublic[datumBeginn]'].value && form.elements['inputPublic[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputPublic[datumEnde]'].value && form.elements['inputPublic[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=public'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    }
?>

<div class="accordeon accordeonProfile<?php if( $action == "EDIT" && $context == "public" || !$pers->public[$i] ) { echo " show"; } ?>">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Frühere Funktionen" ) . " (" . (count( $pers->public )-$i) . ")"; ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    // first element in the below written while loop needs to be identified in order to avoid div.seperator
    $counterForSeperator = 0;

    while( $pers->public[$i]->datumBeginn ) {
?>

<?php
    if( $action == "EDIT" && $element == $e ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"inputPublic[publicID]\" value=\"" . $pers->public[$i]->publicID . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputPublic[datumBeginn]\" value=\"" . $pers->public[$i]->datumBeginn . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputPublic[datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $pers->public[$i]->datumEnde ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Organisation" ) . " *</label><input type=\"text\" name=\"inputPublic[organisation]\" value=\"" . $pers->public[$i]->organisation . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Website" ) . "</label><input type=\"text\" name=\"inputPublic[url]\" value=\""; echo !$pers->public[$i]->url ? "http://" : $pers->public[$i]->url; echo "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><input type=\"text\" name=\"inputPublic[publicFunktion]\" value=\"" . $pers->public[$i]->publicFunktion . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputPublic[bemerkungen]\">" . $pers->public[$i]->bemerkungen . "</textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"public\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputPublic[organisation]'].value != '' && form.elements['inputPublic[publicFunktion]'].value != '' ) { if( form.elements['inputPublic[datumBeginn]'].value && form.elements['inputPublic[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputPublic[datumEnde]'].value && form.elements['inputPublic[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=public'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>

<dl class="list">
<?php
    if ( $counterForSeperator != 0 ) {
        echo "<dd><div class=\"seperator\"></div></dd>";
    }
?>
    <dt>
<?php
    $datumEnde = substr( $pers->public[$i]->datumEnde, 0, 4 );
    $datumBeginn = substr( $pers->public[$i]->datumBeginn, 0, 4 );
    echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " " . str_replace( "9999", translate( "heute" ), $datumEnde ) );
?>
    </dt>
    <dd>
        <span class="bold">
<?php if( $pers->public[$i]->url ) { ?>
            <a href="<?php echo $pers->public[$i]->url; ?>" onclick="window.open(this.href); return false;">
<?php }
    echo $pers->public[$i]->organisation;
    if( $pers->public[$i]->url ) { ?>
            </a>
<?php } ?>
        </span>
        <a href="search.php?field=tblPublic.organisation&amp;expr=<?php echo urlencode( $pers->public[$i]->organisation ); ?>&amp;datumBeginn=<?php echo $pers->public[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->public[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum Mitglied waren bei" ) . " " . $pers->public[$i]->organisation; ?>" /></a>
    </dd>

<?php
    if( $_SESSION["rights"]["editProfile"] ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=public&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=public&amp;publicID={$pers->public[$i]->publicID}" ) );
        }
        echo "</dt>";
    }
?>
</dl>

<dl class="secondList">
<?php if( $pers->public[$i]->publicFunktion ) { ?>
    <dt></dt>
    <dd><?php echo $pers->public[$i]->publicFunktion; ?></dd>
<?php } ?>

<?php if( $pers->public[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->public[$i]->bemerkungen ); ?></dd>
<?php } ?>
</dl>

<?php
    }  // end if
    $e++;

    // first element needs to be identified in order to avoid div.seperator
    $counterForSeperator++;
    $i++;
    }  // end while

    if( $action !="ADD" && $_SESSION["rights"]["editProfile"] ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=public" ) );
    }
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

</div><!-- end #tabpublic -->



<!-- new tab -->

<div class="tabContent" id="tabVerein"<?php if( $context == "verein" || $context == "zahlungen" ) { echo " style=\"display:block\""; } ?>>


<div class="accordeon accordeonProfile show">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/expanded.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Funktion(en)" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $i = 0;
    while( $pers->verein[$i]->datumEnde >= date( "Y-m-d" ) ) {
?>

<?php
    if( $action == "EDIT" && $element == $e ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"inputVerein[vereinID]\" value=\"" . $pers->verein[$i]->vereinID . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputVerein[datumBeginn]\" value=\"" . $pers->verein[$i]->datumBeginn . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputVerein[datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $pers->verein[$i]->datumEnde ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Organisation") . "/" . translate( "Region" ) . " *</label><select name=\"inputVerein[organisationRegionID]\">"; makeOptions( "linkVereinOrganisation", $pers->verein[$i]->organisationRegionID ); echo "</select></td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><select name=\"inputVerein[funktionID]\">"; makeOptions( "linkVereinFunktion", $pers->verein[$i]->funktionID ); echo "</select></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputVerein[bemerkungen]\">" . $pers->verein[$i]->bemerkungen . "</textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"verein\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputVerein[organisationRegionID]'].value != '' && form.elements['inputVerein[funktionID]'].value != 0 ) { if( form.elements['inputVerein[datumBeginn]'].value && form.elements['inputVerein[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputVerein[datumEnde]'].value && form.elements['inputVerein[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=verein'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>

<dl class="list">
<?php
    if ( $i != 0 ) {
        echo "<dd><div class=\"seperator\"></div></dd>";
    }
?>
    <dt>
<?php
    $datumEnde = substr( $pers->verein[$i]->datumEnde, 0, 4 );
    $datumBeginn = substr( $pers->verein[$i]->datumBeginn, 0, 4 );
    if( $pers->verein[$i]->datumBeginn > date( "Y-m-d" ) ) {
        echo translate( "Ab" ) . " $datumBeginn";
    } elseif( $datumEnde == "9999" ) {
        echo translate( "Seit" ) . " $datumBeginn";
    } else {
        echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " $datumEnde" );
    }
?>
    </dt>
    <dd>
        <span class="bold"><?php echo $pers->verein[$i]->organisation; ?></span>
<?php if( $pers->verein[$i]->organisationRegionID > 1 && $pers->verein[$i]->organisationRegionID < 91 ) { ?>
        <a href="search.php?field=qryVerein.organisationRegionID&amp;expr=<?php echo urlencode( $pers->verein[$i]->organisationRegionID ); ?>&amp;datumBeginn=<?php echo $pers->verein[$i]->datumBeginn; ?>&amp;datumEnde=<?php echo $pers->verein[$i]->datumEnde; ?>&amp;hist=true" onclick="window.open(this.href); return false;">
        <img src="image/template/search2.png" alt="" title="<?php echo translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum Mitglied in der gleichen Organisation waren" ); ?>" /></a>
<?php } ?>
    </dd>

<?php //if( $_SESSION["rights"]["editProfile"] ) { ?>
<?php
    if( $_SESSION["rights"]["roleID"] >= 4 ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=verein&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=verein&amp;vereinID={$pers->verein[$i]->vereinID}" ) );
        }
        echo "</dt>";
    }
?>
</dl>

<dl class="secondList">
<?php if( $pers->verein[$i]->vereinFunktion ) { ?>
    <dt></dt>
    <dd><?php echo $pers->verein[$i]->vereinFunktion; ?></dd>
<?php } ?>

<?php if( $pers->verein[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->verein[$i]->bemerkungen ); ?></dd>
<?php } ?>
</dl>

<?php
    }  // end if
    $e++;
?>

<?php
    $i++;
    }  // end while
?>
<?php
    if( $action !="ADD" && $_SESSION["rights"]["roleID"] >= 4 ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=verein" ) );
    }
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php
    if( $action == "ADD" && $context == "verein" ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputVerein[datumBeginn]\" value=\"\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputVerein[datumEnde]\" value=\"\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Organisation") . "/" . translate( "Region" ) . " *</label><select name=\"inputVerein[organisationRegionID]\">"; makeOptions( "linkVereinOrganisation", 1 ); echo "</select></td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><select name=\"inputVerein[funktionID]\">"; makeOptions( "linkVereinFunktion", 99 ); echo "</select></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputVerein[bemerkungen]\"></textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"verein\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Hinzufügen" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputVerein[organisationRegionID]'].value != '' && form.elements['inputVerein[funktionID]'].value != 0 ) { if( form.elements['inputVerein[datumBeginn]'].value && form.elements['inputVerein[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputVerein[datumEnde]'].value && form.elements['inputVerein[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=verein'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    }
?>


<div class="accordeon accordeonProfile<?php if( $action == "EDIT" && $context == "verein" ) { echo " show"; } ?>">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Frühere Funktionen" ) . " (" . (count( $pers->verein )-$i) . ")"; ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    while( $pers->verein[$i]->datumBeginn ) {
?>

<?php
    if( $action == "EDIT" && $element == $e ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"inputVerein[vereinID]\" value=\"" . $pers->verein[$i]->vereinID . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputVerein[datumBeginn]\" value=\"" . $pers->verein[$i]->datumBeginn . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputVerein[datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $pers->verein[$i]->datumEnde ) . "\" /></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Organisation") . "/" . translate( "Region" ) . " *</label><select name=\"inputVerein[organisationRegionID]\">"; makeOptions( "linkVereinOrganisation", $pers->verein[$i]->organisationRegionID ); echo "</select></td></tr>";
        echo "<tr><td><label>" . translate( "Funktion" ) . " *</label><select name=\"inputVerein[funktionID]\">"; makeOptions( "linkVereinFunktion", $pers->verein[$i]->funktionID ); echo "</select></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputVerein[bemerkungen]\">" . $pers->verein[$i]->bemerkungen . "</textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td class=\"legend\">* = " . translate( "Minimaleinträge" ) . "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"verein\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputVerein[organisationRegionID]'].value != '' && form.elements['inputVerein[funktionID]'].value != 0 ) { if( form.elements['inputVerein[datumBeginn]'].value && form.elements['inputVerein[datumBeginn]'].value < '" . ( date( "Y" )-100 ) . "-01-01' || form.elements['inputVerein[datumEnde]'].value && form.elements['inputVerein[datumEnde]'].value < '" . ( date( "Y" )-100 ) . "-01-01' ) { return window.confirm('" . translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=verein'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>

<dl class="list">
<?php
    if ( $counterForSeperator != 0 ) {
        echo "<dd><div class=\"seperator\"></div></dd>";
    }
?>
    <dt>
<?php
    $datumEnde = substr( $pers->verein[$i]->datumEnde, 0, 4 );
    $datumBeginn = substr( $pers->verein[$i]->datumBeginn, 0, 4 );
    echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " " . str_replace( "9999", translate( "heute" ), $datumEnde ) );
?>
    </dt>
    <dd><span class="bold"><?php echo $pers->verein[$i]->organisation; ?></span></dd>

<?php
    if( $_SESSION["rights"]["roleID"] >= 4 ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=verein&amp;element=$e" ) );
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            $deleteButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=verein&amp;vereinID={$pers->verein[$i]->vereinID}" ) );
        }
        echo "</dt>";
    }
?>
</dl>

<dl class="secondList">
<?php if( $pers->verein[$i]->vereinFunktion ) { ?>
    <dt></dt>
    <dd><?php echo $pers->verein[$i]->vereinFunktion; ?></dd>
<?php } ?>

<?php if( $pers->verein[$i]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $pers->verein[$i]->bemerkungen ); ?></dd>
<?php } ?>
</dl>

<?php
    }  // end if
    $e++;

    $i++;
}  // end while
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<!-- Akkordeon "Mitgliederbeitrag" -->

<?php if( $_SESSION["rights"]["editPayment"] ) { ?>
<div class="accordeon accordeonProfile<?php if( $context == "zahlungen" ) { echo " show"; } ?>">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mitgliederbeitrag" );// . " (" . count( $pers->zahlungen ) . ")"; ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    if( $action == "EDIT" && $_SESSION["rights"]["editPayment"] ) {
        for( $i=0; $i<count( $pers->zahlungen ); $i++ ) {
            if( $pers->zahlungen[$i]->datumEinzahlung >= ( date("Y")-5 . "-01-01" ) || $pers->zahlungen[$i]->datumEinzahlung == "0000-00-00" || $pers->zahlungen[$i]->typID == 2 || $pers->zahlungen[$i]->typID == 3 ) {
                if( $element == $e ) {
                    echo "<fieldset class=\"fieldsetEditForm\">";
                    echo "<table>";
                    echo "<tr><td><input type=\"hidden\" name=\"inputZahlungen[zahlungID]\" value=\"" . $pers->zahlungen[$i]->zahlungID . "\" /></td></tr>";
                    echo "<tr><td><label>" . translate( "Typ" ) . "</label><select name=\"inputZahlungen[typID]\" disabled=\"disabled\">"; makeOptions( "linkZahlungenTyp", $pers->zahlungen[$i]->typID ); echo "</select></td></tr>";
                    if( isset( $pers->zahlungen[$i]->jahr ) ) {
                        echo "<tr><td><label>" . translate( "Jahr" ) . "</label><input type=\"text\" name=\"inputZahlungen[jahr]\" value=\"" . $pers->zahlungen[$i]->jahr . "\" /></td></tr>";
                    }
                    echo "<tr><td><label>" . translate( "Rechnungsdatum" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputZahlungen[datumRechnung]\" value=\"" . str_replace( "0000-00-00", "", $pers->zahlungen[$i]->datumRechnung ) . "\" /></td></tr>";
                    echo "<tr><td><label>" . translate( "Datum des Zahlungseingangs" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputZahlungen[datumEinzahlung]\" value=\"" . str_replace( "0000-00-00", "", $pers->zahlungen[$i]->datumEinzahlung ) . "\" /></td></tr>";
                    echo "<tr><td><label>" . translate( "Betrag" ) . " (CHF) *</label><input type=\"text\" name=\"inputZahlungen[betrag]\" value=\"" . $pers->zahlungen[$i]->betrag . "\" /></td></tr>";
                    echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputZahlungen[bemerkungen]\">" . $pers->zahlungen[$i]->bemerkungen . "</textarea></td></tr>";
                    echo "</table>";
                    echo "<table>";
                    echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
                    echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
                    echo "<input type=\"hidden\" name=\"context\" value=\"zahlungen\" />";
                    echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputZahlungen[betrag]'].value != 0 ) { if( form.elements['inputZahlungen[datumEinzahlung]'].value && form.elements['inputZahlungen[datumEinzahlung]'].value < '" . ( date( "Y" )-5 ) . "-01-01' ) { return window.confirm('" . translate( "Einzahlungsdatum liegt vor" ) . " " . ( date( "Y" )-5 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
                    echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=zahlungen'\" /></td></tr>";
                    echo "</table>";
                    echo "</fieldset>";
                } // if( $element == $e )
            } // if( $pers->zahlungen[$i]->datumEinzahlung >= ( date("Y") . "-01-01" ) ...
            $e++;
        } // for(..)
    } else { // if( $action == "EDIT" )
        if( count( $pers->zahlungen ) > 0 ) {
?>
<table class="tableProfileAccordeon">
    <tr>
        <td class="fixedWidthCell"><?php echo translate( "Typ" ); ?></td>
        <td><?php echo translate( "Jahr" ); ?></td>
        <td><?php echo translate( "Rechnungsdatum" ); ?></td>
        <td><?php echo translate( "Zahlungseingang" ); ?></td>
        <td class="fixedWidthCell"><?php echo translate( "Betrag" ); ?></td>
        <td class="fixedWidthCell"><?php echo translate( "Status" ); ?></td>
        <td><?php echo translate( "Bemerkungen" ); ?></td>
<?php if( $_SESSION["rights"]["editPayment"] ) { ?>
        <td>&nbsp;</td>
<?php } ?>
    </tr>
<?php
        } // if( count( $pers->zahlungen ) > 0 )

        for( $i=0; $i<count( $pers->zahlungen ); $i++ ) {
            if( $pers->zahlungen[$i]->datumEinzahlung >= ( date("Y")-5 . "-01-01" ) || $pers->zahlungen[$i]->datumEinzahlung == "0000-00-00" || $pers->zahlungen[$i]->typID == 2 || $pers->zahlungen[$i]->typID == 3 ) {
?>
    <tr>
        <td class="fixedWidthCell"><?php echo translate( $pers->zahlungen[$i]->typ ); ?></td>
        <td><?php echo isset( $pers->zahlungen[$i]->jahr ) ? $pers->zahlungen[$i]->jahr : "&nbsp;"; ?></td>
        <td><?php echo $pers->zahlungen[$i]->typID != 2 ? str_replace( "0000-00-00", "", $pers->zahlungen[$i]->datumRechnung ) : ""; ?></td>
        <td><?php echo str_replace( "0000-00-00", "", $pers->zahlungen[$i]->datumEinzahlung ); ?></td>
        <td class="fixedWidthCell">CHF <?php echo $pers->zahlungen[$i]->betrag; ?></td>
        <td class="fixedWidthCell">
<?php
    unset( $icon );
    if( $pers->zahlungen[$i]->datumRechnung != "0000-00-00" ) {
        $icon = "offen";
    }
    if( $pers->zahlungen[$i]->datumMahnung != "0000-00-00" ) {
        $icon = "gemahnt";
    }
    if( $pers->zahlungen[$i]->datumEinzahlung != "0000-00-00" ) {
        $icon = "einbezahlt";
    }
    echo translate( $icon );
?>
</td>
        <td><?php echo $pers->zahlungen[$i]->bemerkungen; ?></td>
<?php if( $_SESSION["rights"]["editPayment"] ) { ?>
        <td>
<?php
    $editButton->output( array( "class"=>"", "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=zahlungen&amp;element=$e" ) );
    $deleteButton->output( array( "class"=>"", "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=DELETE&amp;context=zahlungen&amp;zahlungID={$pers->zahlungen[$i]->zahlungID}" ) );
?>
        </td>
<?php } ?>
    </tr>
<?php
            } // if( $pers->zahlungen[$i]->datumEinzahlung >= ( date("Y") . "-01-01" ) ...
            $e++;
        } // for( $i=0; $i<count( $pers->zahlungen ); $i++ )

        if( count( $pers->zahlungen ) > 0 ) {
            echo "</table>";
        } // if( count( $pers->zahlungen ) > 0 )
    }  // if( $action == "EDIT" )

    if( $_SESSION["rights"]["editPayment"] && $action !="ADD" && $action != "EDIT" ) {
        $addButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=ADD&amp;context=zahlungen" ) );
    }
?>

<?php
    if( $action == "ADD" && $context == "zahlungen" && $_SESSION["rights"]["editPayment"] ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        echo "<tr><td><label>" . translate( "Typ" ) . "</label><select name=\"inputZahlungen[typID]\">"; makeOptions( "linkZahlungenTyp" ); echo "</select></td></tr>";
        echo "<tr><td><label>" . translate( "Jahr" ) . "</label><input type=\"text\" name=\"inputZahlungen[jahr]\" value=\"" . date( "Y" ) . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Rechnungsdatum" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputZahlungen[datumRechnung]\" value=\"\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Datum des Zahlungseingangs" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label><input type=\"text\" name=\"inputZahlungen[datumEinzahlung]\" value=\"" . date( "Y-m-d" ) . "\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Betrag" ) . " (CHF) *</label><input type=\"text\" name=\"inputZahlungen[betrag]\" value=\"\" /></td></tr>";
        echo "<tr><td><label>" . translate( "Bemerkungen" ) . "</label><textarea rows=\"5\" cols=\"5\" name=\"inputZahlungen[bemerkungen]\"></textarea></td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"zahlungen\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Hinzufügen" ) . "\" onclick=\"var form = document.forms['formMemberProfil']; if( form.elements['inputZahlungen[betrag]'].value != 0 ) { if( form.elements['inputZahlungen[datumEinzahlung]'].value && form.elements['inputZahlungen[datumEinzahlung]'].value < '" . ( date( "Y" )-5 ) . "-01-01' ) { return window.confirm('" . translate( "Einzahlungsdatum liegt vor" ) . " " . ( date( "Y" )-5 ) . " - " . translate( "Weiterfahren?" ) . "'); } } else { window.alert('" . translate( "Bitte alle mit einem * markierten Felder ausfüllen" ) . ".'); return false; }\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=zahlungen'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    }
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php } // if( $_SESSION["rights"]["editPayment"] ) ?>


<?php if( $_SESSION["rights"]["editProfile"] ) { ?>

<div class="accordeon accordeonProfile">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Logbuch" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<table class="tableProfileAccordeon">
<tr>
<td><?php echo translate( "Ereignis" ); ?></td>
<td><?php echo translate( "Datum" ) . "/" . translate( "Zeit" ); ?></td>
<td><?php echo translate( "IP-Adresse" ); ?></td>
<td>&nbsp;</td>
</tr>
<?php
    $i = 0;
    $j = 0;
    while( $pers->log[$i] && $i<100 && $j<20 ) {
        if( $pers->log[$i]->typID != 3 ) {
            if( $pers->log[$i]->typID == 10 ) {
                $result = searchMember( array( "tblPersonen.email"=>( $pers->log[$i]->userID == $pers->person->email ? $pers->log[$i]->param0 : $pers->log[$i]->userID ) ) );
            }

            echo "<tr>";
            if( $pers->log[$i]->typID == 1 && !$pers->log[$i]->success ) {
                echo "<td>" . translate( "Falsches Passwort" ) . "</td>";
            } elseif( $pers->log[$i]->typID == 10 ) {
                echo "<td>" . translate( $pers->log[$i]->typ ) . " " . translate( $pers->log[$i]->userID == $pers->person->email ? "versendet" : "erhalten" ) . "</td>";
            } else {
                echo "<td>" . translate( $pers->log[$i]->typ ) . "</td>";
            }
            echo "<td>{$pers->log[$i]->timestamp}</td>";
            echo "<td>";
            if( !in_array( $pers->log[$i]->typID, array( 6, 7, 8, 9 ) ) ) {
                echo "{$pers->log[$i]->ip}";
            } else {
                echo "&nbsp;";
            }
            echo "</td>";
            echo "<td>";
            if( $pers->log[$i]->typID == 5 ) {
                echo "{$pers->log[$i]->param0}";
            } elseif( $pers->log[$i]->typID == 10 ) {
                echo translate( $result[0]["qryDienstgrad.dienstgrad"], $result[0]["linkPersonSprache.sprache"] ) . " " . translate( $result[0]["qryDienstgrad.zusatzDg"], $result[0]["linkPersonSprache.sprache"] ) . " " .$result[0][".fullName"];
            } elseif( $pers->log[$i]->typID == 12 ) {
                echo "<img src=\"image/template/mail2.png\" alt=\"\" /> &laquo;{$pers->log[$i]->param1}&raquo;";
            } else {
                echo "&nbsp;";
            }
            echo "</td>";
            echo "</tr>";
            $j++;
        }
        $i++;
    }
?>
</table>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php } // if( $_SESSION["rights"]["editProfile"] ) ?>



<?php if( false ) { ?>
<div class="accordeon accordeonProfile<?php if( $action == "EDIT" && $context == "verein" ) { echo " show"; } ?>">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Unterstützung Vereinszwecke" ) ?></span></a>
<div class="groupChildren">
<div class="child">


<?php
    if( $action == "EDIT" && $element == $e ) {
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>";
        // echo "<tr><td><label>" . translate( "Support") . "</label>";
//         makeCheckboxes( "linkPersonSupport", "inputVerein2[support]", $pers->person->supportID );
/*    function makeCheckboxes( $linkTable, $name, $selected=0, $override=false ) {
        global $lang;

        $language = $override ? $override : $lang;

        $conn = dbconn::open();
        $query = "SELECT * FROM $linkTable";
        $result = $conn->query($query);

        dbconn::close( $conn );

        $i = 0;
        while( $row[$i] = $result->fetch(PDO::FETCH_NUM) ) {
            $i++;
        }

        $binstr = strrev( decbin( $selected ) );
        for( $j=0; $j<$i; $j++ ) {
            echo "<tr><td><label>" . translate( $row[$j][1], $language ) . "</label><input type=\"checkbox\" name=\"" . $name . "[$j]\" value=\"" . pow( 2, $row[$j][0]-1 ) . "\"";
            if( substr( $binstr, $j, 1 ) == 1 ) {
                echo " checked=\"checked\"";
            }
            echo "></td></tr>\n";
        }
    }*/
        echo "</td></tr>";
        echo "</table>";
        echo "<table>";
        echo "<tr><td><input type=\"hidden\" name=\"inputVerein2[support][dummy]\" value=\"1\" />";
        echo "<tr><td><input type=\"hidden\" name=\"member\" value=\"$pers->id\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "<input type=\"hidden\" name=\"context\" value=\"verein\" />";
        echo "<input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" />";
        echo "<input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"location='{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;context=verein'\" /></td></tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>

<?php if( $pers->person->support ) {  ?>
<dl class="list">
<?php // if( i$pers->person->support ) { ?>
    <dt><?php // echo translate( "Support" ); ?></dt>
    <dd>
<?php
    foreach( $pers->person->support as $val ) {
        $str .= "$val, ";
    }
    echo substr( $str, 0, -2 );
?>
    </dd>
<?php
    if( $_SESSION["rights"]["editProfile"] ) {
        echo "<dt>";
        $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?member={$pers->id}&amp;action=EDIT&amp;context=verein&amp;element=$e" ) );
        echo "</dt>";
    }
?>
</dl>
<?php
// end if( $pers->person->support )
//}


    // end if( $pers->person->support )
    }

    }  // end if
    $e++;
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php } // end if( false ) ?>

</div><!-- end #tabVerein -->



<!-- new tab -->

<div class="tabContent" id="tabBuddies"<?php if( $context == "buddies" ) { echo " style=\"display:block\""; } ?>>
<dl class="list">
    <dt></dt>
    <dd></dd>
</dl>
</div><!-- end #tabBuddies -->



<!-- new tab -->

<?php if( $_SESSION["rights"]["editProfile"] || $_SESSION["rights"]["editPayment"] ) { ?>

<div class="tabContent" id="tabEinstellungen"<?php if( $context == "einstellungen" ) { echo " style=\"display:block\""; } ?>>

<div class="accordeon accordeonProfile show">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Zustellungsoptionen" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<fieldset class="fieldsetEditFormPassword">
    <table>
<?php } ?>
<?php if( $_SESSION["rights"]["editProfile"] ) { ?>
        <tr><td><label><?php echo translate( "Bitte senden Sie mir Informationen primär an meine" ); ?></label><select name="inputEinstellungen[channelID]"><?php echo makeOptions( "linkPersonChannel", ( !$pers->person->channelID ? 98 : $pers->person->channelID ) ); ?></select></td></tr>
<?php } ?>
<?php if( $_SESSION["rights"]["roleID"] == 5 ) { ?>
        <tr><td><label><?php echo translate( "Bitte senden Sie mir postalische Nachrichten an" ); ?></label><select name="inputEinstellungen[postChannelID]"><?php echo makeOptions( "linkPersonChannelPost", ( !$pers->person->postChannelID ? 1 : $pers->person->postChannelID ) ); ?></select></td></tr>
        <tr><td><label><?php echo translate( "Bitte senden Sie mir elektronische Nachrichten an" ); ?></label><select name="inputEinstellungen[mailChannelID]"><?php echo makeOptions( "linkPersonChannelMail", ( !$pers->person->mailChannelID ? 1 : $pers->person->mailChannelID ) ); ?></select></td></tr>
<?php } ?>
<?php if( $_SESSION["rights"]["editProfile"] || $_SESSION["rights"]["editPayment"] ) { ?>
        <tr><td><label><?php echo translate( "Senden Sie mir bitte die Rechnung" ); ?></label><select name="inputEinstellungen[paymentChannelID]"><?php echo makeOptions( "linkZahlungenChannel", ( !isset( $pers->person->paymentChannelID ) ? 6 : $pers->person->paymentChannelID ) ); ?></select></td></tr>
<?php } ?>
<?php if( $_SESSION["rights"]["editProfile"] ) { ?>
        <tr><td><label><?php echo translate( "Hinweis auf neue Blog-Beiträge" ); ?></label><select name="inputEinstellungen[blogChannelID]"><?php echo makeOptions( "linkBlogChannel", ( !$pers->person->blogChannelID ? 2 : $pers->person->blogChannelID ) ); ?></select></td></tr>
<?php } ?>
<?php if( $_SESSION["rights"]["editProfile"] || $_SESSION["rights"]["editPayment"] ) { ?>
        <tr><td>
            <label></label>
            <input type="submit" class="formsSubmitButton" value="<?php echo translate( "Aktualisieren" ); ?>" />
            <input type="hidden" name="member" value="<?php echo $pers->id; ?>" />
            <input type="hidden" name="action" value="UPDATE" />
        </td></tr>
    </table>
</fieldset>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php } ?>

<?php if( $_SESSION["rights"]["editProfile"] ) { ?>

<div class="accordeon accordeonProfile">
<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Passwort ändern" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<fieldset class="fieldsetEditFormPassword">
    <table>
        <tr><td><label><?php echo translate( "Altes Passwort" ); ?>:</label><input type="password" name="oldPassword" onkeyup="checkPassword( document.forms['formMemberProfil'], document.images )" /></td></tr>
        <tr><td><label><?php echo translate( "Neues Passwort" ); ?>:</label><input type="password" name="newPassword" onkeyup="checkPassword( document.forms['formMemberProfil'], document.images )" /></td></tr>
        <tr><td><label><?php echo translate( "Neues Passwort wiederholen" ); ?>:</label><input type="password" name="newPassword2" onkeyup="checkPassword( document.forms['formMemberProfil'], document.images )" /></td></tr>
        <tr><td><label>
            <img id="pwok0" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 8 Zeichen" ); ?><br />
            <img id="pwok1" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 1 Kleinbuchstabe" ); ?> (a-z)<br />
            <img id="pwok2" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 1 Grossbuchstabe" ); ?> (A-Z)<br />
            <img id="pwok3" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 1 Ziffer" ); ?> (0-9)<br />
            <img id="pwok4" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 1 Sonderzeichen" ); ?> (. , : ; + - _ * ! @ $ % &amp;)<br />
            <img id="pwok5" src="image/template/delete.png" alt="" /> <?php echo translate( "Passwörter stimmen überein" ); ?>
        </label></td></tr>
        <tr><td><label></label><input name="submitPassword" class="formsSubmitButton" type="button" value="<?php echo translate( "Passwort ändern" ); ?>" disabled="disabled" onclick="sendXmlHttpRequest( 'setPassword.php', 'oldPassword=' + document.forms['formMemberProfil'].elements['oldPassword'].value + '&amp;newPassword=' + document.forms['formMemberProfil'].elements['newPassword'].value + '&amp;newPassword2=' + document.forms['formMemberProfil'].elements['newPassword2'].value ); document.forms['formMemberProfil'].elements['oldPassword'].value=''; document.forms['formMemberProfil'].elements['newPassword'].value=''; document.forms['formMemberProfil'].elements['newPassword2'].value=''; checkPassword( document.forms['formMemberProfil'], document.images )" /></td></tr>
    </table>
</fieldset>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->

<?php } ?>

<?php if( $_SESSION["rights"]["editProfile"] || $_SESSION["rights"]["editPayment"] ) { ?>

</div><!-- end #tabEinstellungen -->

<?php } ?>

</form>



<!-- new tab -->

<?php if( $_SESSION["rights"]["roleID"] == 5 ) { ?>
<form class="formUpdateProfil" id="formMemberProfilAdmin" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
<div class="tabContent" id="tabAdmin"<?php if( $context == "admin" ) { echo " style=\"display:block\""; } ?>>
<fieldset class="fieldsetEditForm">
    <table>
        <tr><td>
            <label><?php echo translate( "Mailversand" ); ?></label>
<?php
    $select = new select( "template" );
    $select->query( "SELECT emailID,titel FROM tblEmail ORDER BY titel" );
    $select->emptyrow = true;
    $select->selected = 0;
    $select->onchange = "document.forms['formMemberProfilAdmin'].elements['sendbutton'].disabled=(document.forms['formMemberProfilAdmin'].elements['template'].value==0)";
    $select->output();
?>
        </td></tr>
        <tr><td>
            <label></label>
            <input type="button" name="sendbutton" value="<?php echo translate( "Senden" ); ?>" disabled="disabled" onclick="obj=this; pic=document.getElementById( 'ajaxwait' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['formMemberProfilAdmin'].elements['template'].value+'&amp;personID=<?php echo $pers->id; ?>', 'obj.disabled=true; pic.src=\'image/template/ajax_wait.gif\';', 'pic.src=\'image/dummy.gif\'; obj.disabled=false;' ); return false;" />
            <img id="ajaxwait" src="image/dummy.gif" alt="" />
        </td></tr>
    </table>
    <table>
        <tr><td>
            <label><?php echo translate( "PDF-Generator" ); ?></label>
            <select name="pdftemplate" onchange="document.forms['formMemberProfilAdmin'].elements['pdfbutton'].disabled=(document.forms['formMemberProfilAdmin'].elements['pdftemplate'].value==0)">
                <option value="0"></option>
                <option value="pdfLetterInvoice"><?php echo translate( "Rechnung" ); ?></option>
                <option value="pdfLetterPaymentLastCall"><?php echo translate( "Last call" ); ?></option>
            </select>
        </td></tr>
        <tr><td>
            <label></label>
            <input type="button" name="pdfbutton" value="<?php echo translate( "PDF generieren" ); ?>" disabled="disabled" onclick="window.location='<?php echo $_SERVER["SCRIPT_NAME"]; ?>?member=<?php echo $pers->id; ?>&amp;action=MAKEPDF&amp;template='+document.forms['formMemberProfilAdmin'].elements['pdftemplate'].value; return false;" />
        </td></tr>
    </table>
    <table>
        <!--tr><td><label><?php echo translate( "Profil auf Plattform veröffentlichen" ) ?></label><input type="checkbox" name="inputAdmin[veroeffentlichungID]" value="1" <?php if( $pers->person->veroeffentlichungID == 1 ) { echo "checked=\"checked\""; } ?> /></td></tr-->
        <tr><td><label><?php echo translate( "Profil auf Plattform veröffentlichen" ) ?></label><?php $select = new select( "inputAdmin[veroeffentlichungID]", "linkPersonVeroeffentlichung", $pers->person->veroeffentlichungID ); $select->output(); ?></td></tr>
        <tr><td><label><?php echo translate( "Aktualisierung" ) ?></label><?php $select = new select( "inputAdmin[aktualisierungID]", "linkPersonAktualisierung", $pers->person->aktualisierungID ); $select->output(); ?></td></tr>
        <tr><td><label><?php echo translate( "Bemerkungen" ) ?></label><textarea name="inputAdmin[bemerkungen]" rows="5" cols="5"><?php echo $pers->person->bemerkungen; ?></textarea></td></tr>
        <tr><td><label><?php echo translate( "Rolle" ) ?></label><?php $select = new select( "inputAdmin[roleID]", "linkSysRoles", $pers->person->roleID ); $select->output(); ?></td></tr>
        <tr><td><label><?php echo translate( "Todesdatum" ) ?></label><input type="text" name="inputAdmin[todesdatum]" value="<?php echo str_replace( "0000-00-00", "", $pers->person->todesdatum ) ?>" /></td></tr>
        <tr><td>
            <label></label>
            <input type="submit" value="<?php echo translate( "Aktualisieren" ); ?>" onclick="var form = document.forms['formMemberProfilAdmin']; if( form.elements['inputAdmin[todesdatum]'].value && form.elements['inputAdmin[todesdatum]'].value < '<?php echo ( date( "Y" )-100 ); ?>-01-01' ) { return window.confirm('<?php echo translate( "Datum liegt vor" ) . " " . ( date( "Y" )-100 ) . " - " . translate( "Weiterfahren?" ); ?>'); }" />
            <input type="hidden" name="member" value="<?php echo $pers->id; ?>" />
            <input type="hidden" name="action" value="UPDATE" />
            <input type="hidden" name="context" value="admin" />
        </td></tr>
    </table>
</fieldset>

</div><!-- end #tabAdmin -->


</form>
<?php } ?>

<?php
    } //if( !$shortprofile )
?>

<?php
    } else {
        echo translate( "Unter dieser Nummer existiert kein Profil" );
    } // if( !$member && !$pers->person->personID || $pers->person->personID )
?>

</div><!-- end #content -->


<?php
    if( $_SESSION["rights"]["roleID"] == 5 ) {
        $i = 0;
        if(count($pers->log)){
            while( ( $pers->log[$i]->typID != 1 || !$pers->log[$i]->success ) && $i<100 ) {
                $i++;
            }
            if( $i < 100 ) {
                $login = preg_replace( "/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/", "$3.$2.$1, $4$5", $pers->log[$i]->timestamp ); //date( "d.m.Y, Hi", strtotime( $pers->log[$i]->timestamp ) );
            }
        } else {
            $login = '';
        }
    }
    $modified = array();
    $modified[] = preg_replace( "/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/", "$3.$2.$1", $pers->modDateOwner ); //date( "d.m.Y", strtotime( $pers->modDateOwner ) );
    $modified[] = $pers;
    $modified[] = preg_replace( "/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/", "$3.$2.$1", $pers->modDateAdmin ); //date( "d.m.Y", strtotime( $pers->modDateAdmin ) );
    $modified[] = ( $pers->modifier ) ? new person( $pers->modifier ) : NULL;
    include( "include/footer.inc.php" );
?>

</body>

</html>