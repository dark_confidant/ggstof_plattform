<?php
    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }


    $adressat = $_REQUEST["to"] ? new person( $_REQUEST["to"] ) : NULL;
    if( !isset( $adressat ) || ( !isset( $_REQUEST["emailID"] ) && !isset( $_GET["body"] ) ) ) {
        exit;
    }
    $adressat->getMilitaryData();
    $absender = new person( $_SESSION["personID"] );
    $absender->getMilitaryData();
    $absender->getJobData();

    $dienstgrad = translate( $absender->current->militaer->dienstgrad, $absender->person->sprache, false, false ) . ( $absender->current->militaer->zusatzDg ? " " . translate( $absender->current->militaer->zusatzDg, $absender->person->sprache, false, false ) : "" );
    $dienstgradText = translate( $adressat->current->militaer->dienstgradText, $adressat->person->sprache, false, false );
    $anrede = translate( $adressat->person->anrede, $adressat->person->sprache, false, false );
    $anredeText = translate( $adressat->person->anredeText, $adressat->person->sprache, false, false );

    $email = isset( $_GET["emailID"] ) ? getEmailContent( $_GET["emailID"], $adressat->person->sprache ) : array();
    $body = $_GET["body"] ? $_GET["body"] : $email["body"];
    $body = str_replace( array( "[NAME]", "[DG]", "[DG_TXT]", "[ANREDE]", "[ANREDE_TXT]", "[EMAIL]" ), array( $absender->person->name, $dienstgrad, $dienstgradText, $anrede, $anredeText, $adressat->person->email ), $body );

    if( $_POST["action"] == "SEND" ) {
        $from = $_POST["from"];
        $to = ( $_SERVER["SERVER_NAME"] == "ggstof" ) ? "juri@localhost" : $adressat->person->email;
        $sent = mail( $to, $_POST["subject"], $_POST["body"], "From: {$absender->person->fullName} <$from>\r\nBcc: plattform@ggstof.ch\r\nContent-type: text/plain; charset=iso-8859-1\r\nX-Mailer: PHP/" . phpversion(), "-f$from" );
        $feedback = sent ? translate( "Mail wurde erfolgreich verschickt.") : translate( "Ein Fehler ist aufgetreten." );
        logInvitation( $_SESSION["userID"], $adressat->person->email );
    }
?>

<?php
    include( "include/head.inc.php" );

    if( $sent ) {
        echo "<meta http-equiv=\"refresh\" content=\"2; URL=member.php\" />";
    }
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ); ?> (GGstOf)</title>
</head>

<body>

<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>

<div id="content">

<?php
    if( $_POST["action"] == "SEND" ) {
        echo "<p>$feedback</p>";
    } else {
?>

<form class="formUpdateProfil formEmailTemplate" name="formEmailTemplate" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <table>
            <tr><td><label><?php echo translate( "Empf&auml;nger" ); ?></label><?php echo "{$adressat->person->vorname} {$adressat->person->vorname2} {$adressat->person->name}"; ?></td></tr>
            <tr><td><label><?php echo translate( "Absender" ); ?></label><?php echo "{$absender->person->vorname} {$absender->person->vorname2} {$absender->person->name}"; ?>
                    <select name="from">
                        <option value="<?php echo $absender->person->email ?>" selected="selected"><?php echo $absender->person->email ?></option>
<?php
    if( $absender->beruf[0]->email ) {
?>
                        <option value="<?php echo $absender->beruf[0]->email ?>"><?php echo $absender->beruf[0]->email ?></option>
<?php
    }
?>
                    </select>
            </td></tr>
            <tr><td><label><?php echo translate( "Betreff" ); ?></label><?php if( $email["subject"] ) { echo $email["subject"]; } ?><input name="subject" type="<?php if( $email["subject"] ) { echo "hidden"; } else { echo "text"; } ?>" value="<?php if( $email["subject"] ) { echo $email["subject"]; } ?>" /></td></tr>
            <tr><td><label><?php echo translate( "Text" ); ?></label><textarea name="body" class="emailtemplate" rows="5" cols="5"><?php echo $body; ?></textarea></td></tr>
            <tr><td>
                <label>
                    <input type="hidden" name="emailID" value="<?php echo $_GET["emailID"]; ?>" />
                    <input type="hidden" name="to" value="<?php echo $_GET["to"]; ?>" />
                    <input type="hidden" name="action" value="SEND" />
                </label>
                <input type="submit" class="formsSubmitButton" value="<?php echo translate( "Senden" ); ?>" />
                <input type="reset" value="<?php echo translate( "Abbrechen" ); ?>" onclick="window.close()" />
            </td></tr>
        </table>
    </fieldset>
</form>

<?php
    } // if( $_POST["action"] == "SEND" )
?>

</div><!-- end #content -->

<?php
    include( "include/footer.inc.php" );
?>

</body>

</html>