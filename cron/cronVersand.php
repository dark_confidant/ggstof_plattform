<?php
    function __autoload( $cls ) {
        require_once( "../class/$cls.php" );
    }
    require_once( "../function/list.php" );

    $conn = dbconn::open();
    $query = "SELECT cronID,name,recipientsID,conditionID,emailID
              FROM sysCronVersand
              WHERE active=1
                  AND (month=MONTH(CURDATE()) OR month=0)
                  AND (day=DAYOFMONTH(CURDATE()) OR day=0)
                  AND (weekday=DAYOFWEEK(CURDATE()) OR weekday=0)";
    $result = $conn->query($query);

    dbconn::close( $conn );

    while( $cronjob = $result->fetch(PDO::FETCH_ASSOC) ) {
        $conn = dbconn::open();
        $query = "SELECT listID,param
                  FROM sysRecipients
                  WHERE recipientsID={$cronjob["recipientsID"]}";
        $result = $conn->query($query);

        dbconn::close( $conn );

        $row = $result->fetch(PDO::FETCH_ASSOC);
        list( $adressaten, $modDateOwner ) = memberList( $row[0], $row[1] );

        switch( $cronjob["conditionID"] ) {
            case 1:
                // "zuletzt bearbeitet" liegt l�nger als 1 Jahr zur�ck
                $n = count( $adressaten );
                for( $i=0; $i<$n; $i++ ) {
                    if( $modDateOwner[$i] > ( date( "Y" )-1 ) . date( "-m-d" ) ) {
                        unset( $adressaten[$i] );
                    }
                }
                break;
        }

        $email = new email( $cronjob["emailID"] );

        $n = count( $adressaten );
        for( $i=0; $i<$n; $i++ ) {
            $a = array_shift( $adressaten );
            $email->addRecipient( $a["tblPersonen.personID"] );
        }
        unset( $a, $adressaten, $n );

        $feedback = $email->send();

        if( $feedback ) {
            $conn = dbconn::open();
            $query = "UPDATE sysCronVersand
                      SET lastExecution=CURRENT_TIMESTAMP()
                      WHERE cronID={$cronjob["cronID"]}";
            $conn->query($query);

            dbconn::close( $conn );
        }
        echo "{$cronjob["name"]}: " . utf8_encode( $feedback[1] ) . "\n";
    }
?>