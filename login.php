<?php
    $_SERVER["DOCUMENT_ROOT"] = $_SERVER["DOCUMENT_ROOT"].'/public_html/plattform';
    session_start();

    function __autoload( $cls ) {
        require_once( "class/$cls.php" );
    }
    require_once( "function/lang.php" );
    require_once( "function/log.php" );

    if( isset( $_POST["userID"] ) && isset( $_POST["password"] ) ) {
        require_once( "function/userManagement.php" );
        $rights = userRights( $_POST["userID"] );
    }

    if( sha1( $_POST["password"] ) === $rights["password"] ) {
        $_SESSION["userID"]   = $rights["userID"];
        $_SESSION["personID"] = $rights["personID"];
        $_SESSION["role"]     = $rights["role"];
        $_SESSION["rights"]   = $rights;
        if( isset( $_SESSION["personID"] ) ) {
            $user = new person( $_SESSION["personID"] );
            $user->getMilitaryData();
            $_SESSION["lang"]       = $user->person->sprache;
            $_SESSION["userName"]   = "{$user->person->vorname} {$user->person->vorname2} {$user->person->name}";
            $_SESSION["dienstgrad"] = translate( $user->current->militaer->dienstgrad, $user->person->sprache ) . ( $user->current->militaer->zusatzDg ? " " . translate( $user->current->militaer->zusatzDg, $user->person->sprache ) : "" );
        } else {
            $_SESSION["lang"] = ( isset( $_COOKIE["lang"] ) ) ? $_COOKIE["lang"] : "de";
        }

        logLogin( $_POST["userID"], 1 );

        $location = ( isset( $_REQUEST["request"] ) ) ? $_REQUEST["request"] : "member.php";
        header( "Location: $location" );
    } else {
        logLogin( $_POST["userID"] );

        header( "Location: index.php" );
    }
?>