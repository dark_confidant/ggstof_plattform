<?php
    require_once('head.php');
    ggstof_head();
?>



<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ); ?> (GGstOf)</title>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</head>




<body id="index">




<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>




<div id="content" class="startIndex">



<div class="startItem">

<div class="startItemLeft">

<?php
    $content = getContent( 1 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );

    if( defined('$_SESSION["rights"]["editContent"]') && $_SESSION["rights"]["editContent"] ) {
?>
<a class="memberEditButton" href="fckeditor.php?action=EDIT&amp;item=1&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php
    }
?>

</div><!-- startItemLeft -->


<div class="startItemRight">

<?php
    $content = getContent( 2 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );

    if( defined('$_SESSION["rights"]["editContent"]') && $_SESSION["rights"]["editContent"] ) {
?>
<a class="memberEditButton" href="fckeditor.php?action=EDIT&amp;item=2&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php
    }
?>

</div><!-- startItemRight -->

</div><!-- startItem -->


<div class="startItem">

<div class="startItemLeft">

<p><strong><?php echo translate( "Blog" ); ?></strong></p>

<?php
    $blog = blogLastEntries( 5 );
    echo "<ul class=\"memberBlogList\">";
    for( $i=0; $i<count( $blog ); $i++ ) {
        $post_title = utf8_encode($blog[$i]["post_title"]);
        echo "<li><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">{$post_title}</a>";
        if( $blog[$i]["comment_count"] ) {
            echo " (+ {$blog[$i]["comment_count"]} <a href=\"{$blog[$i]["guid"]}#comments\" onclick=\"window.open( this.href ); return false;\">" . translate( "Kommentar" . ( $blog[$i]["comment_count"] > 1 ? "e" : "" ) ) . "</a>)";
        }
        echo "</li>";
    }
    echo "</ul>";
?>

</div><!-- startItemLeft -->


<div class="startItemRight">

<?php
    $content = getContent( 4 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );

    if( $_SESSION["rights"]["editContent"] ) {
?>
<a class="memberEditButton" href="fckeditor.php?action=EDIT&amp;item=4&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php
    }
?>

</div><!-- startItemRight -->

</div><!-- startItem -->


</div>
<!-- end #content -->




<?php
    $timestamp = date( "d.m.Y", max( $timestamp ) );
    include( "include/footer.inc.php" );
?>

</body>
<!-- end body -->

</html>