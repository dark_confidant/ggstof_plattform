<div id="warningOldIE">

<p><?php echo translate( "Sie verwenden einen alten und unsicheren Browser. Die korrekte Darstellung dieser Seite ist somit nicht gewährleistet. Wir empfehlen den Browser Firefox oder ansonsten einen Update Ihres Internet Explorers." ); ?></p>

</div>

<div id="warningNoJs">

<!--<p><?php echo translate( "Bitte aktivieren Sie" ); ?> <a href="http://www.activatejavascript.org/index.php?browser=ie" onclick="window.open('http://www.activatejavascript.org/index.php?browser=ie'); return false">JavaScript</a></p>-->

</div>




<div id="navigationTop">

<!-- start stripe and text in stripe -->
<div id="stripe">
<!-- .title to ensure that #title und #subtitle have a font-size of 1 and can be aligned with other elements -->
<a href="index.php"><span id="subtitle"><span class="title">GGstOf</span></span></a>
<span id="title"><span class="title"><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) ?></span></span>
</div>
<!-- end stripe and text in stripe -->

<?php
// display search form only on non public pages
if( isset( $_SESSION["userID"] ) ) {
if(!defined('hideSearch') || !$hideSearch ) {
    $expr = array_key_exists('expr', $_REQUEST) ? trim( stripslashes( urldecode( $_REQUEST["expr"] ) ) ): '';
    $field = array_key_exists('field', $_REQUEST) ? $_REQUEST["field"]: '';
    $hist  = array_key_exists('hist', $_REQUEST) ? $_REQUEST["hist"]: '';
?>
<!-- start search form -->
<form id="navigationTopSearchForm" action="search.php" method="post">
<div>
<select name="field" onchange="if(this.value=='qryBeruf.branche') { document.getElementById('searchText').className='invisible'; document.getElementById('searchText').name='dummy'; document.getElementById('searchBranche').className='inline'; document.getElementById('searchBranche').name='expr'; } else { document.getElementById('searchText').className='searchField'; document.getElementById('searchText').name='expr'; document.getElementById('searchBranche').className='invisible'; document.getElementById('searchBranche').name='dummy'; }">
    <option value="tblPersonen.name"<?php if( $field == "tblPersonen.name" ) { echo " selected"; } ?>><?php echo translate( "Name" ); ?></option>
    <option value="tblPersonen.vorname"<?php if( $field == "tblPersonen.vorname" ) { echo " selected"; } ?>><?php echo translate( "Vorname" ); ?></option>
    <option value="tblPersonen.geburtsdatum"<?php if( $field == "tblPersonen.geburtsdatum" ) { echo " selected"; } ?>><?php echo translate( "Jahrgang" ); ?></option>
    <option value="tblPersonen.plz"<?php if( $field == "tblPersonen.plz" ) { echo " selected"; } ?>><?php echo translate( "PLZ" ); ?></option>
    <option value="tblPersonen.ort"<?php if( $field == "tblPersonen.ort" ) { echo " selected"; } ?>><?php echo translate( "Ort" ); ?></option>
    <optgroup label="<?php echo translate( "Militär" ); ?>">
        <option value="qryDienstgrad.dienstgrad"<?php if( $field == "qryDienstgrad.dienstgrad" ) { echo " selected"; } ?>><?php echo translate( "Dienstgrad" ); ?></option>
        <option value="qryMilitaer.funktionMilitaerFull"<?php if( $field == "qryMilitaer.funktionMilitaerFull" ) { echo " selected"; } ?>><?php echo translate( "Militärische Funktion" ); ?></option>
        <option value="qryMilitaer.einteilung"<?php if( $field == "qryMilitaer.einteilung" ) { echo " selected"; } ?>><?php echo translate( "Militärische Einteilung" ); ?></option>
        <option value="brevetierungsdatum"<?php if( $field == "brevetierungsdatum" ) { echo " selected"; } ?>><?php echo translate( "Brevetierungsjahr" ); ?></option>
    </optgroup>
    <optgroup label="<?php echo translate( "Beruf" ); ?>">
        <option value="qryBeruf.branche"<?php if( $field == "qryBeruf.branche" ) { echo " selected"; } ?>><?php echo translate( "Branche" ); ?></option>
        <option value="qryBeruf.organisation"<?php if( $field == "qryBeruf.organisation" ) { echo " selected"; } ?>><?php echo translate( "Organisation" ); ?></option>
        <option value="qryBeruf.abteilung"<?php if( $field == "qryBeruf.abteilung" ) { echo " selected"; } ?>><?php echo translate( "Abteilung" ); ?></option>
        <option value="qryBeruf.funktion"<?php if( $field == "qryBeruf.funktion" ) { echo " selected"; } ?>><?php echo translate( "Funktion" ); ?></option>
        <option value="qryBeruf.ort"<?php if( $field == "qryBeruf.ort" ) { echo " selected"; } ?>><?php echo translate( "Ort" ); ?></option>
        <option value="qryBeruf.tags"<?php if( $field == "qryBeruf.tags" ) { echo " selected"; } ?>><?php echo translate( "Tags" ); ?></option>
    </optgroup>
    <optgroup label="<?php echo translate( "Abschlüsse" ); ?>">
        <option value="qryBerufAusbildung.titel"<?php if( $field == "qryBerufAusbildung.titel" ) { echo " selected"; } ?>><?php echo translate( "Titel" ); ?></option>
        <option value="qryBerufAusbildung.institution"<?php if( $field == "qryBerufAusbildung.institution" ) { echo " selected"; } ?>><?php echo translate( "Institution" ); ?></option>
    </optgroup>
    <optgroup label="<?php echo translate( "Vereine & Organisationen" ); ?>">
        <option value="tblPublic.organisation"<?php if( $field == "tblPublic.organisation" ) { echo " selected"; } ?>><?php echo translate( "Organisation" ); ?></option>
        <option value="tblPublic.publicFunktion"<?php if( $field == "tblPublic.publicFunktion" ) { echo " selected"; } ?>><?php echo translate( "Funktion" ); ?></option>
    </optgroup>
</select>
<span class="tooltipframe">
    <img src="image/template/question-mark-small.gif" alt="?" title="" onmouseover="document.getElementById( 'tooltip_search_'+document.forms['navigationTopSearchForm'].elements['field'].value.replace( /\./, '' ) ).className='tooltip'" onmouseout="document.getElementById( 'tooltip_search_'+document.forms['navigationTopSearchForm'].elements['field'].value.replace( /\./, '' ) ).className='invisible'" />
    <div id="tooltip_search_tblPersonenname" class="invisible"><?php echo translate( "Schreiben Sie nur den gesuchten Nachnamen in das Suchfeld (keine Kombination Name + Vorname). Sie können einen Stern * als Variable/Joker verwenden." ); ?></div>
    <div id="tooltip_search_tblPersonenvorname" class="invisible"><?php echo translate( "Schreiben Sie nur den gesuchten Vornamen in das Suchfeld (keine Kombination Name + Vorname). Sie können einen Stern * als Variable/Joker verwenden." ); ?></div>
    <div id="tooltip_search_tblPersonengeburtsdatum" class="invisible"><?php echo translate( 348 ); ?></div>
    <div id="tooltip_search_tblPersonenplz" class="invisible"><?php echo translate( 349 ); ?></div>
    <div id="tooltip_search_tblPersonenort" class="invisible"><?php echo translate( 350 ); ?></div>
    <div id="tooltip_search_qryDienstgraddienstgrad" class="invisible"><?php echo translate( 351 ); ?></div>
    <div id="tooltip_search_qryMilitaerfunktionMilitaerFull" class="invisible"><?php echo translate( 352 ); ?></div>
    <div id="tooltip_search_qryMilitaereinteilung" class="invisible"><?php echo translate( 353 ); ?></div>
    <div id="tooltip_search_brevetierungsdatum" class="invisible"><?php echo translate( 348 ); ?></div>
    <div id="tooltip_search_qryBerufbranche" class="invisible"><?php echo translate( 354 ); ?></div>
    <div id="tooltip_search_qryBeruforganisation" class="invisible"><?php echo translate( 355 ); ?></div>
    <div id="tooltip_search_qryBerufabteilung" class="invisible"><?php echo translate( 356 ); ?></div>
    <div id="tooltip_search_qryBeruffunktion" class="invisible"><?php echo translate( 357 ); ?></div>
    <div id="tooltip_search_qryBerufort" class="invisible"><?php echo translate( 350 ); ?></div>
    <div id="tooltip_search_qryBeruftags" class="invisible"><?php echo translate( 358 ); ?></div>
    <div id="tooltip_search_qryBerufAusbildungtitel" class="invisible"><?php echo translate( 359 ); ?></div>
    <div id="tooltip_search_qryBerufAusbildunginstitution" class="invisible"><?php echo translate( 360 ); ?></div>
    <div id="tooltip_search_tblPublicorganisation" class="invisible"><?php echo translate( 361 ); ?></div>
    <div id="tooltip_search_tblPublicpublicFunktion" class="invisible"><?php echo translate( 362 ); ?></div>
</span>
<input id="searchText" class="<?php echo $field == "qryBeruf.branche" ? "invisible" : "searchField"; ?>" name="<?php echo $field == "qryBeruf.branche" ? "" : "expr"; ?>" type="text" size="20" value="<?php echo $field == "qryBeruf.branche" ? "" : $expr; ?>" />
<?php
    $select = new select( $field == "qryBeruf.branche" ? "expr" : "" );
    $select->query( "SELECT DISTINCT branche,branche AS row1
                     FROM tblBeruf
                     WHERE datumEnde>=CURDATE() AND typID BETWEEN 0 AND 5 AND branche<>''
                     ORDER BY branche" );
    $select->id = "searchBranche";
    $select->selected = ( $field == "qryBeruf.branche" ) ? $expr : "";
    $select->class = ( $field == "qryBeruf.branche" ) ? "inline" : "invisible";
    $select->onchange = "document.forms['navigationTopSearchForm'].submit();";
    $select->output();
?>
<input id="submitLogin" type="submit" value=" <?php echo translate( "Suchen" ) ?> " />
</div>
</form>
<!-- end search form -->
<?php } ?>

<div id="navigationTopForm">
<?php
    if( isset( $_SESSION["userName"] ) ) {
        echo "<a href=\"member_profile.php?member={$_SESSION["personID"]}\" title=\"" . translate( "Zum Profil von" ) . " " . str_replace( "  ", " ", $_SESSION["userName"] ) . "\">{$_SESSION["dienstgrad"]} {$_SESSION["userName"]}</a>";
    } else {
        echo $_SESSION["userID"];
    }
?>
<br /><a href="logout.php" title="<?php echo translate( "Logout als" ) . " " . translate( $_SESSION["role"] ); ?>"><?php echo translate( "Logout als" ) . " " . translate( $_SESSION["role"] ); ?></a>
</div>

<?php } else { ?>

<!-- start login form -->
<form id="navigationTopForm" action="login.php" method="post">
<div>
<label class="labelTop"><?php echo translate( "E-Mail" ) ?></label><input name="userID" type="text" size="10"></input>
<label class="labelTop"><?php echo translate( "Passwort" ) ?></label><input name="password" type="password" size="10"></input>
<input id="submitLogin" type="submit" value=" <?php echo translate( "Login" ) ?> "></input>
<a href="password.php"><?php echo translate( "Passwort vergessen" ) ?>?</a>
</div>
</form>
<!-- end login form -->

<?php } ?>

<!-- start languages -->
<?php
    $uri = ( basename( $_SERVER["REQUEST_URI"] ) == "" ) ? "index.php" : $_SERVER["REQUEST_URI"];
    $uri = str_replace( "&", "&amp;", $uri );
    $uri .= strpos( $uri, "?" ) ? "&amp;" : "?";
    $memb = isset( $_POST["member"] ) ? "&amp;member=" . $_POST["member"] : "";
?>
<div id="language">
<a class="language" href="<?php echo $uri . "lang=de" . $memb; ?>">de</a>
<a class="language" href="<?php echo $uri . "lang=fr" . $memb; ?>">fr</a>
<a class="language" href="<?php echo $uri . "lang=it" . $memb; ?>">it</a>
<!--a class="language" href="<?php echo $uri . "lang=en" . $memb; ?>">en</a-->
</div>
<!-- end languages -->

<!-- start divider boxes in and below stripe -->
<div id="seperatorTop"></div>
<div id="seperatorBottom"></div>
<!-- end divider boxes in and below stripe -->

</div>