<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
<meta http-equiv="expires" content="0" />
<meta name="copyright" content="Juri Sarbach and Michael Imstepf (c) 2008" />
<meta name="author" content="Juri Sarbach and Michael Imstepf" />
<meta name="robots" content="noindex, nofollow" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="<?php if ($lang) { echo $lang; } else { echo "de"; } ?>" />
<link href="style/screen.css" media="screen" rel="stylesheet" type="text/css" />
<link href="style/print.css" media="print" rel="stylesheet" type="text/css" />
<!--[if lt IE 7]>
<link rel="stylesheet" media="screen" type="text/css" href="style/screen_ie6.css" />
<![endif]-->
<!--[if IE]>
<link rel="stylesheet" media="screen" type="text/css" href="style/screen_ie.css" />
<![endif]-->
<!-- by request of �!: -->
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://blog.ggstof.ch/?feed=rss2" />
<link rel="alternate" type="text/xml" title="RSS .92" href="http://blog.ggstof.ch/?feed=rss" />
<link rel="alternate" type="application/atom+xml" title="Atom 1.0" href="http://blog.ggstof.ch/?feed=atom" />
<link rel="pingback" href="http://www.ggstof.ch/blog/xmlrpc.php" />

<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/hideAlarmJSDeactivated.js"></script>
<script type="text/javascript" src="script/tools.js"></script>
<script type="text/javascript" src="script/escapeIframe.js"></script>

<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />