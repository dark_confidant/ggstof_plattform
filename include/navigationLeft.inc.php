<div>
<a href="index.php"><img id="logo" src="image/template/logo.png" alt="logo" /></a>

<?php
if( !defined('hideMenu') || !$hideMenu ) {
$currentPage = preg_replace( "/.php(.*)/", "", basename( $_SERVER["REQUEST_URI"] ) );
$context = array_key_exists('context', $_REQUEST) ? $_REQUEST["context"]: '';
?>

<ul id="navigationMenu">
<li class="navigationMenu"><a href="articles.php"<?php if( $currentPage=="articles" ) { echo " class=\"current\""; } ?>><?php echo translate( "Statuten" ); ?></a></li>
<li class="navigationMenu"><a href="organisation.php"<?php if( $currentPage=="organisation" ) { echo " class=\"current\""; } ?>><?php echo translate( "Organisation" ); ?></a></li>
<li class="navigationMenu<?php if( isset( $_SESSION["userID"] ) ) { ?> seperatorListElementNavigationLeft<?php } ?>"><a href="activities.php"<?php if( $currentPage=="activities" ) { echo " class=\"current\""; } ?>><?php echo translate( "Aktivitäten" ); ?></a></li>
<?php if( isset( $_SESSION["userID"] ) ) { ?>
<li class="navigationMenu"><a href="member.php"<?php if( in_array( $currentPage, array( "member", "member_profile", "howto" ) ) ) { echo " class=\"current\""; } ?>><?php echo translate( "Mitglieder" ); ?></a></li>
<li class="navigationMenu<?php if( $_SESSION["rights"]["roleID"] == 5 ) { ?> seperatorListElementNavigationLeft<?php } ?>"><a href="benefits.php"<?php if( $currentPage == "benefits" ) { echo " class=\"current\""; } ?>><?php echo translate( "Benefits" ); ?></a></li>

<?php if( $_SESSION["rights"]["roleID"] >= 3 ) { ?>
<li>
<form id="adminMenu" action="">
<fieldset>
<select name="context" class="navigationSelect" onchange="if( document.forms['adminMenu'].elements['context'].selectedIndex != 0 ) { top.location.href=document.forms['adminMenu'].elements['context'].options[document.forms['adminMenu'].elements['context'].selectedIndex].value; }">
    <option value="" <?php if( !in_array( $context, array( "kommunikation", "kassier", "service", "statistik", "admin" ) ) ) { echo " selected=\"selected\""; } ?> disabled="disabled"><?php echo translate( "Admin" ); ?></option>
    <option value="admin.php?context=kommunikation" <?php if( $context == "kommunikation" ) { echo " selected=\"selected\""; } ?>><?php echo translate( "Komm" ); ?></option>
    <option value="admin.php?context=kassier" <?php if( $context == "kassier" ) { echo " selected=\"selected\""; } ?>><?php echo translate( "Kassier" ); ?></option>
    <option value="admin.php?context=service" <?php if( $context == "service" ) { echo " selected=\"selected\""; } ?>><?php echo translate( "Service" ); ?></option>
    <option value="admin.php?context=statistik" <?php if( $context == "statistik" ) { echo " selected=\"selected\""; } ?>><?php echo translate( "Statistik" ); ?></option>
    <option value="admin.php?context=admin" <?php if( $context == "admin" ) { echo " selected=\"selected\""; } ?>><?php echo translate( "Admin" ); ?></option>
</select>
</fieldset>
</form>
</li>

<?php } ?>
<?php } ?>

</ul>
</div>

<?php } ?>