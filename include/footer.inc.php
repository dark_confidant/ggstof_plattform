<div class="lastClear"></div>

<div id="footerLeft">
GGstOf<br />
6000 Luzern<br />
<br />
<?php
    list( $coded, $key ) = encrypt_email( "info@ggstof.ch" );
    echo "<a href=\"#\" onclick=\"this.href=decrypt_email( '" . $coded . "','" . $key . "' )\">" . translate( "E-Mail schreiben" ) . "</a>";
?>
</div>

<div id="footerRight">
<?php
    if( isset( $login ) ) {
        echo translate( "letzter Login" ) . " " . translate( "von" ) . " {$pers->person->vorname} {$pers->person->vorname2} {$pers->person->name} " . translate( "am" ) . " $login<br />";
    }
    if( isset( $modified ) ) {
        if( $modified[0] != "00.00.0000" ) {
            echo translate( "zuletzt bearbeitet" ) . " " . translate( "von" ) . " " .  $modified[1]->person->vorname . " " . $modified[1]->person->vorname2 . " " . $modified[1]->person->name . " " . translate( "am" ) . " {$modified[0]}<br />";
        }
        if( $modified[2] != "00.00.0000" ) {
            echo translate( "zuletzt bearbeitet" ) . " " . translate( "von" ) . " " .  $modified[3]->person->vorname . " " . $modified[3]->person->vorname2 . " " . $modified[3]->person->name . " " . translate( "am" ) . " {$modified[2]}";
            list( $coded, $key ) = encrypt_email( $modified[3]->person->email );
            echo ", <a href=\"#\" onclick=\"this.href=decrypt_email( '" . $coded . "','" . $key . "' )\">" . translate( "E-Mail an Admin" ) . "</a>";
        }
    } else {
//         echo translate( "zuletzt bearbeitet" ) . " " . translate( "am" ) . " " . date( "d.m.Y", filemtime( __FILE__ ) );
        list( $coded, $key ) = encrypt_email( "netzwerk@ggstof.ch" );
        echo "<a href=\"#\" onclick=\"this.href=decrypt_email( '" . $coded . "','" . $key . "' )\">" . translate( "E-Mail an Webmaster" ) . "</a>";
    }
?>
</div>