<?php
    $imgName = ( file_exists( "../image/profile/" . $_GET["member"] . ".jpg" ) ) ? "../image/profile/" . $_GET["member"] . ".jpg" : "../image/template/nobody.jpg";
    $img = imagecreatefromjpeg( $imgName );
    if( $_GET["deceased"] ) {
//         imageantialias( $img, true );
        list( $width, $height ) = getimagesize( $imgName );

//         $linewidth = 20;
//         $scale = sqrt( pow( $linewidth, 2 )/(pow( $width, 2 )+pow( $height, 2 )) );
//
//         $coord = array( (1-$scale)*$width, 0,
//                         $width, 0,
//                         $width, $scale*$height,
//                         $scale*$width, $height,
//                         0, $height,
//                         0, (1-$scale)*$height );

        $dim = min( $width, $height );

        $coord = array( .38*$dim, 0,
                        .5*$dim, 0,
                        0, .5*$dim,
                        0, .38*$dim );

        $black = imagecolorallocate( $img, 0, 0, 0 );
        imagefilledpolygon( $img, $coord, 4, $black );
    }
    imagejpeg( $img );
    imagedestroy( $img );
?>