<?php

    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }



    $context = $_REQUEST["context"];
?>




<?php
include("include/head.inc.php");
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Projekte" ); ?></title>
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/accordeon.js"></script>
<script type="text/javascript" src="script/viewTabContent.js"></script>
</head>




<!-- start body -->
<body id="projects">




<!-- start #navigationLeft -->
<?php
include("include/navigationLeft.inc.php");
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
include("include/navigationTop.inc.php");
?>
<!-- end #navigationTop -->



<!-- start #content -->
<div id="content" class="partner">




<!-- start tabs, credits to facebook.com -->
<div class="home_main_item">
<div class="newsfeed_header">
<div id="newsfeed_tabs_wrapper">
<div id="newsfeed_tabs" class="Tabset_tabset">
<div class="HomeTabs">
<!-- spans to avoid href -->
<span id="One" class="HomeTabs_tab HomeTabs_first<?php if( empty( $context ) || $context == "netzwerk" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Netzwerk" ) ?></span>
<span id="Two" class="HomeTabs_tab<?php if( $context == "regionen" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Regionenvertreter" ) ?></span>
<span id="Five" class="HomeTabs_tab<?php if( $context == "kdogsts" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Kdo Gst S" ) ?></span>
<span id="Three" class="HomeTabs_tab<?php if( $context == "miliz" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Miliz Gst Of" ) ?></span>
<span id="Four" class="HomeTabs_tab<?php if( $context == "kommunikation" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Kommunikation" ) ?></span>
</div>
</div>
</div>
</div>
</div>
<!-- end tabs -->


<!-- start #tab1 -->
<div class="tabContent" id="tabOne"<?php if( empty( $context ) || $context == "netzwerk" ) { echo " style=\"display:block\""; } ?>>

<div class="tabContentHolder">

<div class="accordeon accordeonProfile show">
<div class="parent show"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Zielsetzung" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=11&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=netzwerk"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 11 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $today = date( "Y-m-d" );

    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID FROM tblVerein LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID WHERE organisationRegionID=14 AND datumBeginn<='$today' AND datumEnde>='$today' ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData();
        $n++;
    }
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ) ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>
<?php
    for( $i=0; $i<$n; $i++ ) {
        $j = 0;
        while( $team[$i]->verein[$j]->organisationRegionID != 14 && $j<count( $team[$i]->verein ) ) {
            $j++;
        }
        echo "<li><a href=\"member_profile.php?member=" .  $team[$i]->id . "\">" . translate( $team[$i]->militaer[0]->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->militaer[0]->zusatzDg . " " . $team[$i]->person->vorname . " ". $team[$i]->person->vorname2 . " ". $team[$i]->person->name . "</a>, " . $team[$i]->verein[$j]->vereinFunktion . "<br />" . nl2br( $team[$i]->verein[$j]->bemerkungen ) . "</li>";
    }
?>
</ul>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 12 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Projektkalender" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=12&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=netzwerk"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 13 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=13&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=netzwerk"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mitarbeit" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=14&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=netzwerk"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 14 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->

</div>
<!-- end .tabContentHolder -->

</div>
<!-- end #tab1 -->




<!-- start #tab2 -->
<div class="tabContent" id="tabTwo"<?php if( $context == "regionen" ) { echo " style=\"display:block\""; } ?>>

<div class="tabContentHolder">

<div class="accordeon accordeonProfile show">
<div class="parent show"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Zielsetzung" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=15&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=regionen"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 15 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $today = date( "Y-m-d" );

    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID FROM tblVerein LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID WHERE funktionID=13 AND datumBeginn<='$today' AND datumEnde>='$today' ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData();
        $n++;
    }
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ) ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>
<?php
    for( $i=0; $i<$n; $i++ ) {
        $j = 0;
        while( $team[$i]->verein[$j]->funktionID != 13 && $j<count( $team[$i]->verein ) ) {
            $j++;
        }
        echo "<li><a href=\"member_profile.php?member=" .  $team[$i]->id . "\">" . translate( $team[$i]->militaer[0]->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->militaer[0]->zusatzDg . " " . $team[$i]->person->vorname . " ". $team[$i]->person->vorname2 . " ". $team[$i]->person->name . "</a>, " . $team[$i]->verein[$j]->organisation . "<br />" . nl2br( $team[$i]->verein[$j]->bemerkungen ) . "</li>";
    }
?>
</ul>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 16 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Projektkalender" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=16&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=regionen"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 17 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=17&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=regionen"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mitarbeit" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=18&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=regionen"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 18 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->

</div>
<!-- end .tabContentHolder -->

</div>
<!-- end #tab2 -->




<!-- start #tab5 -->
<div class="tabContent" id="tabFive"<?php if( $context == "kdogsts" ) { echo " style=\"display:block\""; } ?>>

<div class="tabContentHolder">

<div class="accordeon accordeonProfile show">
<div class="parent show"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Zielsetzung" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=27&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kdogsts"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 27 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $today = date( "Y-m-d" );

    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID FROM tblVerein LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID WHERE funktionID=5 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE() ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData();
        $n++;
    }
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ) ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>
<?php
    for( $i=0; $i<$n; $i++ ) {
        $j = 0;
        while( $team[$i]->verein[$j]->funktionID != 5 && $j<count( $team[$i]->verein ) ) {
            $j++;
        }
        echo "<li><a href=\"member_profile.php?member=" .  $team[$i]->id . "\">" . translate( $team[$i]->militaer[0]->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->militaer[0]->zusatzDg . " " . $team[$i]->person->vorname . " ". $team[$i]->person->vorname2 . " ". $team[$i]->person->name . "</a>, " . $team[$i]->verein[$j]->vereinFunktion . "<br />" . nl2br( $team[$i]->verein[$j]->bemerkungen ) . "</li>";
    }
?>
</ul>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 28 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Projektkalender" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=28&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kdogsts"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 29 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=29&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kdogsts"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mitarbeit" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=30&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kdogsts"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 30 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->

</div>
<!-- end .tabContentHolder -->

</div>
<!-- end #tab5 -->




<!-- start #tab3 -->
<div class="tabContent" id="tabThree"<?php if( $context == "miliz" ) { echo " style=\"display:block\""; } ?>>

<div class="tabContentHolder">

<div class="accordeon accordeonProfile show">
<div class="parent show"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Zielsetzung" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=19&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=miliz"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 19 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $today = date( "Y-m-d" );

    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID FROM tblVerein LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID WHERE organisationRegionID=11 AND datumBeginn<='$today' AND datumEnde>='$today' ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData();
        $n++;
    }
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ) ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>
<?php
    for( $i=0; $i<$n; $i++ ) {
        $j = 0;
        while( $team[$i]->verein[$j]->organisationRegionID != 11 && $j<count( $team[$i]->verein ) ) {
            $j++;
        }
        echo "<li><a href=\"member_profile.php?member=" .  $team[$i]->id . "\">" . translate( $team[$i]->militaer[0]->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->militaer[0]->zusatzDg . " " . $team[$i]->person->vorname . " ". $team[$i]->person->vorname2 . " ". $team[$i]->person->name . "</a>, " . $team[$i]->verein[$j]->vereinFunktion . "<br />" . nl2br( $team[$i]->verein[$j]->bemerkungen ) . "</li>";
    }
?>
</ul>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 20 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Projektkalender" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=20&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=miliz"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 21 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=21&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=miliz"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mitarbeit" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=22&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=miliz"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 22 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->

</div>
<!-- end .tabContentHolder -->

</div>
<!-- end #tab3 -->




<!-- start #tab4 -->
<div class="tabContent" id="tabFour"<?php if( $context == "kommunikation" ) { echo " style=\"display:block\""; } ?>>

<div class="tabContentHolder">

<div class="accordeon accordeonProfile show">
<div class="parent show"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Zielsetzung" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=23&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kommunikation"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 23 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $today = date( "Y-m-d" );

    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID FROM tblVerein LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID WHERE organisationRegionID=12 AND datumBeginn<='$today' AND datumEnde>='$today' ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData();
        $n++;
    }
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ) ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>
<?php
    for( $i=0; $i<$n; $i++ ) {
        $j = 0;
        while( $team[$i]->verein[$j]->organisationRegionID != 12 && $j<count( $team[$i]->verein ) ) {
            $j++;
        }
        echo "<li><a href=\"member_profile.php?member=" .  $team[$i]->id . "\">" . translate( $team[$i]->militaer[0]->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->militaer[0]->zusatzDg . " " . $team[$i]->person->vorname . " ". $team[$i]->person->vorname2 . " ". $team[$i]->person->name . "</a>, " . $team[$i]->verein[$j]->vereinFunktion . "<br />" . nl2br( $team[$i]->verein[$j]->bemerkungen ) . "</li>";
    }
?>
</ul>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 24 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Projektkalender" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=24&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kommunikation"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<?php
    $content = getContent( 25 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ) ?> (<?php echo substr_count( $content["text"], "<li>" ); ?>)</span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=25&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kommunikation"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->



<div class="accordeon">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mitarbeit" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=26&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kommunikation"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 26 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->

</div>
<!-- end .tabContentHolder -->

</div>
<!-- end #tab4 -->




</div>
<!-- end #content -->



<?php
    $timestamp = date( "d.m.Y", max( $timestamp ) );
    include("include/footer.inc.php");
?>

</body>
</html>