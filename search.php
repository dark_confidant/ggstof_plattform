<?php
    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }

    date_default_timezone_set( "Europe/Zurich" );

    $keys = array(
        "tblPersonen.name"=>"Name",
        "tblPersonen.vorname"=>"Vorname",
        "tblPersonen.geburtsdatum"=>"Jahrgang",
        "tblPersonen.plz"=>"PLZ privat",
        "tblPersonen.ort"=>"Ort privat",
        "qryDienstgrad.dienstgrad"=>"Dienstgrad",
        "qryMilitaer.einteilung"=>"Einteilung",
        "qryMilitaer.funktionMilitaerFull"=>"Funktion Militär",
        "brevetierungsdatum"=>"Brevetierungsjahr",
        "qryBeruf.organisation"=>"Organisation Beruf",
        "qryBeruf.abteilung"=>"Abteilung Beruf",
        "qryBeruf.funktion"=>"Funktion Beruf",
        "qryBeruf.branche"=>"Branche",
        "qryBeruf.ort"=>"Ort geschäftlich",
        "qryBeruf.tags"=>"Tags",
        "qryBerufAusbildung.titel"=>"Titel",
        "qryBerufAusbildung.institution"=>"Institution",
        "tblPublic.organisation"=>"Organisation Öffentlichkeit",
        "tblPublic.publicFunktion"=>"Funktion Öffentlichkeit",
        "qryVerein.organisationRegionID"=>"Organisation",
        "linkMilitaerRS.rs"=>"RS",
        "linkMilitaerTruppengattung.truppengattung"=>"Truppengattung"
    );

    // process search
    $expr = trim( stripslashes( urldecode( $_REQUEST["expr"] ) ) );
    if( !empty( $expr ) && !preg_match( "/^[\*]+$/" , $expr ) && !preg_match( "/^[\?]+$/" , $expr ) && !preg_match( "/^[%]+$/" , $expr ) && !preg_match( "/^[_]+$/" , $expr ) ) {
        $field = array_key_exists('field', $_REQUEST) ? $_REQUEST["field"]: '';
        $hist  = array_key_exists('hist', $_REQUEST) ? $_REQUEST["hist"]: '';
        $datumBeginn = array_key_exists('datumBeginn', $_REQUEST) ?$_REQUEST["datumBeginn"]: '';
        $datumEnde = array_key_exists('datumEnde', $_REQUEST) ? $_REQUEST["datumEnde"]: '';
        if( $hist == "true" && !( $datumBeginn == "0000-00-00" && $datumEnde == "9999-12-31" ) ) {
            $timeframe = 1;  // Fr�here Eintr�ge
        } else if( $hist . $datumBeginn . $datumEnde == "" ) {
            $timeframe = 2;  // Aktuelle Eintr�ge
        } else if( $datumBeginn && $datumEnde && !( $datumBeginn == "0000-00-00" && $datumEnde == "9999-12-31" ) ) {
            $timeframe = 3;  // Eintr�ge zwischen x und y
        } else if( $datumBeginn == "0000-00-00" && $datumEnde == "9999-12-31" ) {
            $timeframe = 4;  // Alle Eintr�ge
        }

        switch( $field ) {
            case "qryDienstgrad.dienstgrad":
                $searchstr = translate( str_ireplace( array( " i Gst", " EMG", " SMG", "*", "%" ), "", trim( $expr ) ), false, true, false );
                if( stripos( "hptm maj oberstlt oberst", $searchstr ) !== false ) {
                    $searchstr .= " i Gst";
                }
                break;

            case "qryMilitaer.funktionMilitaerFull":
                $searchstr = str_ireplace( array( "Kdt", "Cdt" ), "?dt", $expr );
                break;

            case "qryMilitaer.einteilung":
                if( stripos( $expr, "Stab " ) === false && stripos( $expr, "EM " ) === false && stripos( $expr, "SM " ) === false ) {
                    $querystr = "(qryMilitaer.einteilung LIKE '$expr' OR qryMilitaer.vorgesVb LIKE '$expr')";
                    if( stripos( $expr, "*" ) === false ) {
                        $querystr .= " AND qryMilitaer.funktion LIKE '_dt'";
                    }
                } else {
                    $expr2 = str_ireplace( array( "Stab ", "EM ", "SM " ), "", $expr );
                    $querystr = "qryMilitaer.einteilung LIKE 'Stab $expr2' OR qryMilitaer.vorgesVb LIKE 'Stab $expr2' OR qryMilitaer.einteilung LIKE 'EM $expr2' OR qryMilitaer.vorgesVb LIKE 'EM $expr2' OR qryMilitaer.einteilung LIKE 'SM $expr2' OR qryMilitaer.vorgesVb LIKE 'SM $expr2'";
                }
                break;

            case "tblPersonen.geburtsdatum":
                $searchstr = array( "$expr-01-01", "$expr-12-31" );
                break;

            case "brevetierungsdatum":
                $searchstr = array( "$expr-01-01", "$expr-12-31" );
                break;

            case "qryBeruf.tags":
                $searchstr = "*$expr*";
                break;

            case "qryBerufAusbildung.titel":
                $hist = "true";
                $searchstr = $expr;
                break;

            case "qryBerufAusbildung.institution":
                $hist = "true";
                $searchstr = $expr;
                break;

            case "qryVerein.organisation":
                exit;
                break;

            case "qryVerein.organisationRegionID":
                $searchstr = ( $expr > 1 && $expr < 91 ) ? $expr : NULL;
                break;

            default:
                $searchstr = $expr;
                break;
        }
        $criteria = array( $field=>$searchstr );

        if(!defined('querystr')){
            $querystr = '';
        }

        $histquery = ( $hist == "true" );
        $table = strtok( $field, "." );
        if( $table == "qryMilitaer" || $table == "qryBeruf" || $table == "qryBerufAusbildung" || $table == "tblPublic" || $table == "qryVerein" ) {
            if( $hist == "true" && $field != "qryDienstgrad.dienstgrad" && !$datumBeginn && !$datumEnde ) {
                $criteria = array_merge( $criteria, array( "$table.datumEnde"=>array( "0000-00-00", date( "Y-m-d", time()-24*60*60 ) ) ) );
                if( $querystr ) {
                    $querystr = "($querystr) AND $table.datumEnde BETWEEN '0000-00-00' AND '" . date( "Y-m-d", time()-24*60*60 ) ."'";
                }
            } elseif( $datumBeginn && $datumEnde ) {
                $criteria = array_merge( $criteria, array( "$table.datumBeginn"=>array( "0000-00-00", $datumEnde ), "$table.datumEnde"=>array( $datumBeginn, "9999-12-31" ) ) );
                if( $querystr ) {
                    $querystr = "($querystr) AND $table.datumEnde BETWEEN '0000-00-00' AND '$datumEnde' AND $table.datumEnde BETWEEN '$datumBeginn' AND '9999-12-31'";
                }
            } else {
                $criteria = array_merge( $criteria, array( "$table.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "tblPersonen.todesdatum"=>"0000-00-00" ) );
                if( $querystr ) {
                    $querystr = "($querystr) AND $table.datumEnde BETWEEN '" . date( "Y-m-d" ) . "' AND '9999-12-31' AND tblPersonen.todesdatum='0000-00-00'";
                }
            }
        }

        $order = ( $timeframe == 4 ) ? "$table.datumEnde DESC,$table.datumBeginn DESC," : "";
        $order .= "name,vorname,vorname2,tblPersonen.ort";
        switch( $table ) {
            case "qryMilitaer":
                $group = "tblPersonen.personID,$table.militaerID";
                break;

            case "qryBeruf":
                $group = "tblPersonen.personID,$table.berufID";
                break;

            case "qryBerufAusbildung":
                $group = "tblPersonen.personID,$table.ausbildungID";
                break;

            case "tblPublic":
                $group = "tblPersonen.personID,$table.publicID";
                break;

            case "qryVerein":
                $group = "tblPersonen.personID,$table.vereinID";
                break;

            default:
                $group = false;
                break;
        }

         //print_r( $querystr );
        // die( $querystr );
        if(!defined('querystr')){
            $querystr = '';
        }
        $result = searchMember( $querystr ? $querystr : $criteria, $order, $group, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,tblPublic.*,qryVerein.*,qryZahlungen.*,qryBrevet.brevetierungsdatum,qryBeitritt.beitrittsdatum,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkMilitaerRS.rs", $histquery );
        if( !$_SESSION["rights"]["viewNonpublicProfile"] ) {
//             $result = searchMember( $querystr ? "($querystr) AND qryVerein.funktionID=1" : array_merge( $criteria, array( "qryVerein.funktionID"=>1 ) ), $order, $group, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,tblPublic.*,qryVerein.*,qryZahlungen.*,qryBrevet.brevetierungsdatum,qryBeitritt.beitrittsdatum,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkMilitaerRS.rs", $histquery );
//             $result2 = searchMember( $querystr ? "($querystr) AND qryVerein.funktionID BETWEEN 99 AND 1000" : array_merge( $criteria, array( "qryVerein.funktionID"=>array( 99, 1000 ) ) ), $order, $group, "tblPersonen.personID", $histquery );
            $result1 = $result2 = array();
            for( $i=0; $i<count( $result ); $i++ ) {
                if( $result[$i]["qryVerein.funktionID"] >= 99 ) {
                    $result2[] = $result[$i];
                } else {
                    $result1[] = $result[$i];
                }
            }
            $result = $result1;
        }
//         print_r( $result );
        logSearch( $_SESSION["userID"], "$field=$expr", min( count( $result ), 1 ) );

        if( $field == "qryVerein.organisationRegionID" ) {
            $expr = $result[0]["qryVerein.organisation"];
        }
        if( array_key_exists('action', $_GET) ? $_GET["action"]: '' == "EXPORT" && $_SESSION["rights"]["exportData"] ) {
            for( $i=0; $i<count( $result ); $i++ ) {
                $content[0][$i] = translate( $result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"] ) . " " . translate( $result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"] );
                $content[1][$i] = $result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"];
                $content[2][$i] = $result[$i]["tblPersonen.name"];
                $content[3][$i] = $result[$i]["tblPersonen.adresse"];
                $content[4][$i] = $result[$i]["tblPersonen.plz"];
                $content[5][$i] = $result[$i]["tblPersonen.ort"];
                $content[6][$i] = $result[$i]["tblPersonen.email"];

                switch( $table ) {
                    case "qryMilitaer":
                        $content[7][$i] = translate( $result[$i]["qryMilitaer.funktion"] ) . ( $result[$i]["qryMilitaer.zusatzFkt"] ? " " . translate( $result[$i]["qryMilitaer.zusatzFkt"] ) : "" );
                        $content[8][$i] = $result[$i]["qryMilitaer.einteilung"];
                        break;

                    case "qryBeruf":
                        $content[7][$i] = $result[$i]["qryBeruf.organisation"];
                        $content[8][$i] = $result[$i]["qryBeruf.funktion"];
                        $content[9][$i] = $result[$i]["qryBeruf.abteilung"];
                        break;

                    case "qryBerufAusbildung":
                        $content[7][$i] = $result[$i]["tblBerufAusbildung.institution"];
                        $content[8][$i] = $result[$i]["tblBerufAusbildung.titel"];
                        $content[9][$i] = $result[$i]["tblBerufAusbildung.abschlussjahr"];
                        break;

                    case "tblPublic":
                        $content[7][$i] = $result[$i]["tblPublic.publicFunktion"];
                        $content[8][$i] = $result[$i]["tblPublic.organisation"];
                        break;
                }
            }
            $headerrow = array(
                translate( "Dienstgrad" ),
                translate( "Vorname" ),
                translate( "Name" ),
                translate( "Adresse" ),
                translate( "PLZ" ),
                translate( "Ort" ),
                translate( "E-Mail" )
            );
            switch( $table ) {
                case "qryMilitaer":
                    array_push( $headerrow, "Funktion", "Einteilung" );
                    break;

                case "qryBeruf":
                    array_push( $headerrow, "Organisation", "Funktion", "Abteilung" );
                    break;

                case "qryBerufAusbildung":
                    array_push( $headerrow, "Institution", "Titel", "Jahr" );
                    break;

                case "tblPublic":
                    array_push( $headerrow, "Funktion", "Organisation" );
                    break;
            }

            switch( $_GET["format"] ) {
                case "xls":
                    export2xls( "Export", $content, $headerrow );
                    break;
            }
        }
    }

    $mailButton = new imgButton;
    $mailButton->src = "image/template/mail2.png";
    $xlsButton = new imgButton;
    $xlsButton->src = "image/template/excel-16x16.png";
    $xlsButton->class = "";
    $xlsButton->imgclass = "icon";
    $xlsButton->alt = "Excel-Liste exportieren";
    $xlsButton->title = "Excel-Liste exportieren";
    $xlsButton->caption = "Export";
?>




<!-- start head -->
<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Suche" ); ?></title>
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/accordeon.js"></script>
<script type="text/javascript" src="script/sorttable.js"></script>
</head>
<!-- end head -->



<body id="search">



<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>



<div id="content">
<?php

    if(!isset($result)) {
        $result = '';
    }
?>

<p><strong><?php echo translate( "Ihre Suche nach" ) . " " . translate( $keys[$field] ) . " = &laquo;$expr&raquo; " . translate( "hat" ) . " " .  count( $result ) . " " . translate( "Ergebnis" . ( count( $result ) == 1 ? "" : "se" ) ) . "."; ?></strong></p>
<p>
<?php
    $table = '';
    if( $table == "qryMilitaer" && $field != "qryDienstgrad.dienstgrad" || $table == "qryBeruf" || $table == "tblPublic" ) {
        echo ( $timeframe == 1 ) ? "<strong>" : "<a href=\"?field=$field&amp;expr=" . urlencode( $expr ) . "&amp;hist=true\">";
        echo translate( "Frühere Einträge" );
        echo ( $timeframe == 1 ) ? "</strong>" : "</a>";

        echo " &ndash; ";

        echo ( $timeframe == 4 ) ? "<strong>" : "<a href=\"?field=$field&amp;expr=" . urlencode( $expr ) . "&amp;hist=true&amp;datumBeginn=0000-00-00&amp;datumEnde=9999-12-31\">";
        echo translate( "Alle Einträge" );
        echo ( $timeframe == 4 ) ? "</strong>" : "</a>";

        echo " &ndash; ";

        echo ( $timeframe == 2 ) ? "<strong>" : "<a href=\"?field=$field&amp;expr=" . urlencode( $expr ) . "\">";
        echo translate( "Aktuelle Einträge" );
        echo ( $timeframe == 2 ) ? "</strong>" : "</a>";

//         echo ( in_array( $timeframe, array( 1, 3 ) ) ) ? " &ndash; <strong>". translate( "Eintr�ge zwischen" ) . " $datumBeginn " . translate( "und" ) . " " . str_replace( "9999-12-31", translate( "heute" ), $datumEnde ) . "</strong>" : "";
        echo ( $timeframe == 3 ) ? " &ndash; <strong>". translate( "Einträge zwischen" ) . " $datumBeginn " . translate( "und" ) . " " . str_replace( "9999-12-31", translate( "heute" ), $datumEnde ) . "</strong>" : "";
    }
?>
</p>

<div id="accordeon" class="accordeon accordeonProfile showAll">
<?php if( false ) { ?>
<div class="parent"><a class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Resultate in Mitgliederprofilen" ) ?> (<?php echo count( $result ); ?>):</span></a>
<div class="groupChildren">
<div class="child">
<?php } ?>

<?php if( count( $result ) > 0 ) { ?>
<table class="tableGrid tableMargin sortable">
<thead>
<tr>
<th class="fixWidthSearchResults firstColumn<?php if( $timeframe != 4 ) { echo " sorttable_sorted"; } ?>"><?php echo translate( "Name" ); ?><?php if( $timeframe != 4 ) { echo "<span id=\"sorttable_sortfwdind\">&nbsp;&#x25B4;</span>"; } ?></th>
<?php
    switch( $field ) {
        case "qryDienstgrad.dienstgrad":
            echo "<th>" . translate( "Funktion" ) . "</th>";
            echo "<th>" . translate( "Einteilung" ) . "</th>";
            break;

        case "qryMilitaer.funktionMilitaerFull":
            echo "<th>" . translate( "Einteilung" ) . "</th>";
            echo "<th>" . translate( "Funktion" ) . "</th>";
            break;

        case "qryMilitaer.einteilung":
            echo "<th>" . translate( "Funktion" ) . "</th>";
            break;

        case "qryBeruf.branche":
            echo "<th>" . translate( "Organisation" ) . "</th>";
            echo "<th>" . translate( "Funktion" ) . "</th>";
            echo "<th>" . translate( "Abteilung" ) . "</th>";
            break;

        case "qryBeruf.organisation":
            echo "<th>" . translate( "Abteilung" ) . "</th>";
            echo "<th>" . translate( "Funktion" ) . "</th>";
            break;

        case "qryBeruf.abteilung":
            echo "<th>" . translate( "Organisation" ) . "</th>";
            echo "<th>" . translate( "Funktion" ) . "</th>";
            break;

        case "qryBeruf.funktion":
            echo "<th>" . translate( "Organisation" ) . "</th>";
            echo "<th>" . translate( "Abteilung" ) . "</th>";
            break;

        case "qryBerufAusbildung.titel":
            echo "<th>" . translate( "Institution" ) . "</th>";
            echo "<th>" . translate( "Titel" ) . "</th>";
            echo "<th>" . translate( "Jahr" ) . "</th>";
            break;

        case "qryBerufAusbildung.institution":
            echo "<th>" . translate( "Institution" ) . "</th>";
            echo "<th>" . translate( "Titel" ) . "</th>";
            echo "<th>" . translate( "Jahr" ) . "</th>";
            break;

        case "tblPublic.organisation":
            echo "<th>" . translate( "Funktion" ) . "</th>";
            break;

        case "tblPublic.publicFunktion":
            echo "<th>" . translate( "Organisation" ) . "</th>";
            break;

        case "linkMilitaerTruppengattung.truppengattung":
            echo "<th>" . translate( "RS als" ) . "</th>";
            break;

        default:
//             echo "<th>" . translate( "Gefunden in" ) . "</th>";
            break;
    }
    if(!isset($timeframe)){
        $timeframe = '';
    }

    if( in_array( $timeframe, array( 1, 3, 4 ) ) && $table != "qryBerufAusbildung" ) {
        echo "<th" . ( $timeframe == 4 ? " class=\"sorttable_sorted\"" : "" ) . ">" . translate( "Periode" ) . ( $timeframe == 4 ? "<span id=\"sorttable_sortfwdind\">&nbsp;&#x25B4;</span>" : "" ) . "</th>";
    }

    if( in_array( $field, array( "qryDienstgrad.dienstgrad", "qryMilitaer.funktionMilitaerFull", "qryMilitaer.einteilung", "brevetierungsdatum" ) ) ) {
        echo "<th class=\"sorttable_nosort\">&nbsp;</th>";
    }

    if( $_SESSION["rights"]["roleID"] == 5 ) {
        if( $field == "brevetierungsdatum" ) {
            echo "<th>" . translate( "Beruf" ) . " " . translate( "Typ" ) . "</th>";
        }
        echo "<th>" . translate( "Mitgliederstatus" ) . "</th>";
    }
?>
</tr>
</thead>
<tbody>
<?php
//     $expr = str_replace( array( "?", "*", "/" ), array( ".", ".+", "\/" ), $expr );

    for( $i=0; $i<count( $result ); $i++ ) {
        echo "<tr>";
        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\">";
        echo "<a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\">";
        $table2 = ( strpos( $field, "qryMilitaer" ) !== false && $histquery ) ? "qryMilitaer" : "qryDienstgrad";
        echo translate( $result[$i]["$table2.dienstgrad"], $result[$i]["linkPersonSprache.sprache"] );
        if( $result[$i]["tblPersonen.todesdatum"] == "0000-00-00" ) {
            echo " " . translate( $result[$i]["$table2.zusatzDg"], $result[$i]["linkPersonSprache.sprache"] );
        }
        echo " {$result[$i][".fullName"]}";
        if( $result[$i]["tblPersonen.todesdatum"] != "0000-00-00" ) {
            echo " (&dagger;)";
        }
        echo "</a>";
        echo "</td>";

        switch( $field ) {
            case "qryDienstgrad.dienstgrad":
                echo "<td>" . ( $result[$i]["qryMilitaer.funktion"] ? translate( $result[$i]["qryMilitaer.funktion"] ) . " " . translate( $result[$i]["qryMilitaer.zusatzFkt"] ) . " <a href=\"?field=qryMilitaer.funktionMilitaerFull&amp;expr=" . urlencode( $result[$i]["qryMilitaer.funktion"] . " " . $result[$i]["qryMilitaer.zusatzFkt"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die die gleiche Funktion haben" ) . "\" /></a>" : "&nbsp;" ) . "</td>";
                echo "<td>" . ( $result[$i]["qryMilitaer.einteilung"] ? $result[$i]["qryMilitaer.einteilung"] . " <a href=\"?field=qryMilitaer.einteilung&amp;expr=" . urlencode( $result[$i]["qryMilitaer.einteilung"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=" . $_REQUEST["qdatumEnde"] . "\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die eingeteilt sind bei" ) . " {$result[$i]["qryMilitaer.einteilung"]}\" /></a>" : "&nbsp;" ) . "</td>";
                break;

            case "qryMilitaer.funktionMilitaerFull":
                echo "<td>" . ( $result[$i]["qryMilitaer.einteilung"] ? $result[$i]["qryMilitaer.einteilung"] . " <a href=\"?field=qryMilitaer.einteilung&amp;expr=" . urlencode( $result[$i]["qryMilitaer.einteilung"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die eingeteilt sind bei" ) . " {$result[$i]["qryMilitaer.einteilung"]}\" /></a>" : "&nbsp;" ) . "</td>";
                echo "<td>" . translate( $result[$i]["qryMilitaer.funktion"] ) . " " . translate( $result[$i]["qryMilitaer.zusatzFkt"] ) . "&nbsp;</td>";
                break;

            case "qryMilitaer.einteilung":
                echo "<td>" . ( $result[$i]["qryMilitaer.funktion"] ? translate( $result[$i]["qryMilitaer.funktion"] ) . " " . translate( $result[$i]["qryMilitaer.zusatzFkt"] ) . " {$result[$i]["qryMilitaer.einteilung"]} <a href=\"?field=qryMilitaer.funktionMilitaerFull&amp;expr=" . urlencode( "{$result[$i]["qryMilitaer.funktion"]} {$result[$i]["qryMilitaer.zusatzFkt"]}" ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die die gleiche Funktion haben" ) . "\" /></a>" : "&nbsp;" ) . "</td>";
                break;

            case "qryBeruf.branche":
                echo "<td>" . ( $result[$i]["qryBeruf.organisation"] ? $result[$i]["qryBeruf.organisation"] . " <a href=\"?field=qryBeruf.organisation&amp;expr=" . urlencode( $result[$i]["qryBeruf.organisation"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die angestellt sind bei" ) . " {$result[$i]["qryBeruf.organisation"]}\" /></a>" : "&nbsp;" ) . "</td>";
                echo "<td>" . ( $result[$i]["qryBeruf.funktion"] ? $result[$i]["qryBeruf.funktion"] . " <a href=\"?field=qryBeruf.funktion&amp;expr=" . urlencode( $result[$i]["qryBeruf.funktion"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die die gleiche Funktion haben" ) . "\" /></a>" : "&nbsp;" ) . "</td>";
                echo "<td>" . ( $result[$i]["qryBeruf.abteilung"] ? $result[$i]["qryBeruf.abteilung"] . " <a href=\"?field=qryBeruf.abteilung&amp;expr=" . urlencode( $result[$i]["qryBeruf.abteilung"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die in der gleichen Abteilung arbeiten" ) . "\" /></a>" : "&nbsp;" ) . "</td>";
                break;

            case "qryBeruf.organisation":
                echo "<td>" . ( $result[$i]["qryBeruf.abteilung"] ? $result[$i]["qryBeruf.abteilung"] . " <a href=\"?field=qryBeruf.abteilung&amp;expr=" . urlencode( $result[$i]["qryBeruf.abteilung"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die in der gleichen Abteilung arbeiten" ) . "\" /></a>" : "&nbsp;" ) . "</td>";
                echo "<td>" . ( $result[$i]["qryBeruf.funktion"] ? $result[$i]["qryBeruf.funktion"] . " <a href=\"?field=qryBeruf.funktion&amp;expr=" . urlencode( $result[$i]["qryBeruf.funktion"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die die gleiche Funktion haben" ) . "\" /></a>" : "&nbsp;" ) . "</td>";
                break;

            case "qryBeruf.abteilung":
                echo "<td>" . ( $result[$i]["qryBeruf.organisation"] ? $result[$i]["qryBeruf.organisation"] . " <a href=\"?field=qryBeruf.organisation&amp;expr=" . urlencode( $result[$i]["qryBeruf.organisation"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die angestellt sind bei" ) . " {$result[$i]["qryBeruf.organisation"]}\" /></a>" : "&nbsp;" ) . "</td>";
                echo "<td>" . ( $result[$i]["qryBeruf.funktion"] ? $result[$i]["qryBeruf.funktion"] . " <a href=\"?field=qryBeruf.funktion&amp;expr=" . urlencode( $result[$i]["qryBeruf.funktion"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die die gleiche Funktion haben" ) . "\" /></a>" : "&nbsp;" ) . "</td>";
                break;

            case "qryBeruf.funktion":
                echo "<td>" . ( $result[$i]["qryBeruf.organisation"] ? $result[$i]["qryBeruf.organisation"] . " <a href=\"?field=qryBeruf.organisation&amp;expr=" . urlencode( $result[$i]["qryBeruf.organisation"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die angestellt sind bei" ) . " {$result[$i]["qryBeruf.organisation"]}\" /></a>" : "&nbsp;" ) . "</td>";
                echo "<td>" . ( $result[$i]["qryBeruf.abteilung"] ? $result[$i]["qryBeruf.abteilung"] . " <a href=\"?field=qryBeruf.abteilung&amp;expr=" . urlencode( $result[$i]["qryBeruf.abteilung"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die in der gleichen Abteilung arbeiten" ) . "\" /></a>" : "&nbsp;" ) . "</td>";
                break;

            case "qryBerufAusbildung.titel":
                echo "<td>{$result[$i]["qryBerufAusbildung.institution"]} <a href=\"?field=qryBerufAusbildung.institution&amp;expr=" . urlencode( $result[$i]["qryBerufAusbildung.institution"] ) . "\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die einen Abschluss haben von" ) . " " . $result[$i]["qryBerufAusbildung.institution"] . "\" /></a>&nbsp;</td>";
                echo "<td>{$result[$i]["qryBerufAusbildung.titel"]}&nbsp;</td>";
                echo "<td>" . substr( $result[$i]["qryBerufAusbildung.abschlussjahr"], 0, 4 ) . "&nbsp;</td>";
                break;

            case "qryBerufAusbildung.institution":
                echo "<td>{$result[$i]["qryBerufAusbildung.institution"]}&nbsp;</td>";
                echo "<td>{$result[$i]["qryBerufAusbildung.titel"]} <a href=\"?field=qryBerufAusbildung.titel&amp;expr=" . urlencode( $result[$i]["qryBerufAusbildung.titel"] ) . "\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder mit diesem Abschluss" ) . "\" /></a>&nbsp;</td>";
                echo "<td>" . substr( $result[$i]["qryBerufAusbildung.abschlussjahr"], 0, 4 ) . "&nbsp;</td>";
                break;

            case "tblPublic.organisation":
                echo "<td>{$result[$i]["tblPublic.publicFunktion"]}&nbsp;</td>";
                break;

            case "tblPublic.publicFunktion":
                echo "<td>" . ( $result[$i]["tblPublic.organisation"] ? $result[$i]["tblPublic.organisation"] . " <a href=\"?field=tblPublic.organisation&amp;expr=" . urlencode( $result[$i]["tblPublic.organisation"] ) . "&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die Mitglied sind bei" ) . " {$result[$i]["tblPublic.organisation"]}\" /></a>" : "&nbsp;" ) . "</td>";
                break;

            case "linkMilitaerTruppengattung.truppengattung":
                echo "<td>{$result[$i]["linkMilitaerRS.rs"]}&nbsp;</td>";
                break;

            default:
                break;
        }

        if( in_array( $timeframe, array( 1, 3, 4 ) ) && $table != "qryBerufAusbildung" ) {
            $datumB = ( substr( $result[$i][$table.".datumBeginn"], 0, 4 ) == "0000" ) ? "????" : substr( $result[$i][$table.".datumBeginn"], 0, 4 );
            $datumE = ( substr( $result[$i][$table.".datumEnde"], 0, 4 ) == "9999" ) ? translate( "heute" ) : substr( $result[$i][$table.".datumEnde"], 0, 4 );
            echo "<td sorttable_customkey=\"$datumE$datumB\">" . translate( "Von" ) . " $datumB " . translate( "bis" ) . " $datumE</td>";
        }

        if( in_array( $field, array( "qryDienstgrad.dienstgrad", "qryMilitaer.funktionMilitaerFull", "qryMilitaer.einteilung" ) ) ) {
            $body = str_replace( array( "\n", "[Vorname]", "[Name]", "[Jahr]" ), array( "%0A", $result[$i]["tblPersonen.vorname"], $result[$i]["tblPersonen.name"], substr( $result[$i]["qryBrevet.brevetierungsdatum"], 0, 4 ) ), translate( 240 ) . "\n{$_SESSION["dienstgrad"]} {$_SESSION["userName"]}" );
            echo "<td><a href=\"mailto:plattform@ggstof.ch?subject=" . translate( "Fehler bei" ) . " " . strip_spaces( translate( $result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"] ) . " " . translate( $result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"] ) ) . " {$result[$i][".fullName"]}&amp;body=$body\">" . translate( "Fehler melden" ) . "</a></td>";
        }
        if( $field == "brevetierungsdatum" ) {
            $body = str_replace( array( "\n", "[Vorname]", "[Name]", "[Jahr]" ), array( "%0A", $result[$i]["tblPersonen.vorname"], $result[$i]["tblPersonen.name"], substr( $result[$i]["qryBrevet.brevetierungsdatum"], 0, 4 ) ), translate( 240 ) . "\n{$_SESSION["dienstgrad"]} {$_SESSION["userName"]}" );
            echo "<td><a href=\"mailto:plattform@ggstof.ch?subject=" . translate( "Fehler Brevetierungsjahr" ) . "&amp;body=$body\">" . translate( "Fehler melden" ) . "</a></td>";
        }

        if( $_SESSION["rights"]["roleID"] == 5 ) {
            if( $field == "brevetierungsdatum" ) {
                $criteria = array( "tblPersonen.personID"=>$result[$i]["tblPersonen.personID"], "qryBeruf.datumBeginn"=>array( "0000-00-00", $result[$i]["qryBrevet.brevetierungsdatum"] ), "qryBeruf.datumEnde"=>array( $result[$i]["qryBrevet.brevetierungsdatum"], "9999-12-31" ) );
                $result1 = searchMember( $criteria, false, false, "qryBeruf.typ" );
                echo "<td>{$result1[0]["qryBeruf.typ"]}</td>";
            }
            echo "<td>{$result[$i]["qryVerein.vereinFunktion"]}" . ( $result[$i]["tblPersonen.password"] ? " <img src=\"image/template/key.gif\" alt=\"pw\" />" : "" ) . "</td>";
        }

        echo "</tr>";
    }
?>
</tbody>
</table>
<?php } ?>

<?php if( false ) { ?>
</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
<?php } ?>

<?php
    if( isset( $result2 ) ) {
        echo "<p>... " . translate( "sowie" ) . " " . count( $result2 ) . " " . translate( "weitere Resultate von Gst Of, die nicht Mitglied der GGstOf sind" ) . ".</p>";
    }

    if( $_SESSION["rights"]["addProfile"] ) {
        echo "<p><a href=\"member_profile.php?action=EDIT&amp;element=0\">Neues Profil erstellen</a></p>";
    }

    if( $_SESSION["rights"]["exportData"] && $field != "brevetierungsdatum" && count( $result ) > 0 ) {
        echo "<p>";
        $xlsButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?field=$field&amp;expr=" . urlencode( $expr ) . "&amp;hist=$hist&amp;datumBeginn=$datumBeginn&amp;datumEnde=$datumEnde&amp;action=EXPORT&amp;format=xls" ) );
        echo "<p>";
    }
?>

<?php if( false ) { ?>
<div class="parent"><a class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Resultate in Events" ) ?> (2):</span></a>
<div class="groupChildren">
<div class="child">
<table class="tableGrid tableMargin">
<thead>
<tr>
<th class="fixWidthSearchResults firstColumn"><?php echo translate( "Event" ) ?></th>
<th class="fixWidthSearchResults"><?php echo translate( "Gefunden in" ) ?></th>
</tr>
</thead>
<tbody>
<tr>
<td class="firstColumn"><a href="events.php?event=">KORPSGEIST 09</a></td>
<td><?php echo translate( "Location" ) ?></td>
</tr>
</tbody>
</table>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->

<div class="parent"><a class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Resultate im Marktplatz" ) ?> (3):</span></a>
<div class="groupChildren">
<div class="child">
<table class="tableGrid tableMargin">
<thead>
<tr>
<th class="fixWidthSearchResults firstColumn"><?php echo translate( "Event" ) ?></th>
<th class="fixWidthSearchResults"><?php echo translate( "Gefunden in" ) ?></th>
</tr>
</thead>
<tbody>
<tr>
<td class="firstColumn"><a href="marktplatz.php?event=">Haus zu verkaufen</a></td>
<td><?php echo translate( "Titel" ) ?></td>
</tr>
</tbody>
</table>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
<?php } ?>

</div><!-- end .accordeon -->

</div>

<?php
    include( "include/footer.inc.php" );
?>


</body>
</html>