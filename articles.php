<?php
    require_once('head.php');
    ggstof_head();
?>

<?php
include("include/head.inc.php");
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Statuten" ); ?></title>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</head>



<!-- start body -->
<body>




<!-- start #navigationLeft -->
<?php
include("include/navigationLeft.inc.php");
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
$pagePublic = 1;
include("include/navigationTop.inc.php");
?>
<!-- end #navigationTop -->



<!-- start #content -->
<div id="content">

<div class="contentHolder">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="startEditButtonTop" href="fckeditor.php?action=EDIT&item=5&caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<?php
    $content = getContent( 5 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

</div>
<!-- end .contentHolder -->




</div>
<!-- end #content -->



<?php
    $timestamp = date( "d.m.Y", max( $timestamp ) );
    include( "include/footer.inc.php" );
?>

</body>
</html>