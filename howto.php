<?php

    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }


    $member = new person( $_SESSION["personID"] );
    $member->getMilitaryData();

    if(!isset($lang)){
        $lang = '';
    }

    $content = getEmailContent( 4, $lang );
    $text = $content["body"];

    $vars = new variableSet();
    $vars->MEMBER_ID = $member->id;
    $vars->NAME = $member->person->name;
    $vars->DG = translate( $member->current->militaer->dienstgrad ) . " " . translate( $member->current->militaer->zusatzDg );
    $vars->DG_TXT = translate( $member->current->militaer->dienstgradText );
    $vars->ANREDE = translate( $member->person->anrede ) . " " . translate( $member->current->militaer->dienstgradText );
    $vars->ANREDE_TXT = translate( $member->person->anredeText );
    $vars->EMAIL = $member->person->email;
    $vars->REC_INFO = translate( $member->person->channel );
    $vars->REC_INVOICE = translate( $member->person->paymentChannel );
    $vars->REC_BLOG = translate( $member->person->blogChannel );

    $text = $vars->replace( $content["body"] );
    $text = preg_replace( "/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i", "<a href=\"$1\" onclick=\"window.open( this.href ); return false;\">$1</a>", $text );
?>

<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Anleitung" ); ?></title>
</head>

<body>

<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>


<div id="content">

<p>
<?php
    echo nl2br( $text );
?>
</p>

</div><!-- content -->

<?php
    include( "include/footer.inc.php" );
?>

</body>

</html>