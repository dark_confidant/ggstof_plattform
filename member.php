<?php
    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }

//     $member = $_REQUEST["member"];

    date_default_timezone_set( "Europe/Zurich" );

    $conn = dbconn::open();
//     $query = "SELECT COUNT(*) FROM tblPersonen WHERE veroeffentlichungID=1 AND todesdatum='0000-00-00'";
//     $result = $conn->query($query);
//
//     $result = $result->fetch(PDO::FETCH_ASSOC);
//     $rnd = rand( 0, $result[0]-1 );

    $query = "SELECT personID
              FROM tblPersonen
              WHERE veroeffentlichungID>0 AND todesdatum='0000-00-00'
              ORDER BY RAND()";
//               LIMIT 1";
    $result = $conn->query($query);

    dbconn::close( $conn );

    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        if( file_exists( "image/profile/{$row[0]}.jpg" ) ) {
            break;
        }
    }
    $rndmemb = new person( $row[0] );
    $rndmemb->getMilitaryData();
    $rndmemb->getJobData();
    $rndmemb->getSocietyData();

    $user = new person( $_SESSION["personID"] );
    $user->getMilitaryData();
    $user->getJobData();
    $user->getSocietyData();
?>

<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Mitglieder" ); ?></title>
</head>

<body id="member">

<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>


<div id="content">


<div class="startItem">


<div id="startRndMember">

<p><strong><?php echo translate( "Wir stellen Ihnen vor" ); ?></strong></p>
<a href="member_profile.php?member=<?php echo $rndmemb->id; ?>"><img id="profilePhotoStart" src="include/profilePhoto.php?member=<?php echo $rndmemb->id; ?>" alt="<?php echo $rndmemb->person->fullName; ?>" title="<?php echo translate( "Profil anzeigen" ); ?>" /></a>

<!-- Dienstgrad -->
<?php
    if ( $rndmemb->current->militaer->dienstgrad ) {
?>
<p class="noMargin">
<?php
    echo translate( $rndmemb->current->militaer->dienstgrad, $rndmemb->person->sprache ) . " " . translate( $rndmemb->current->militaer->zusatzDg, $rndmemb->person->sprache );
?>
</p>
<?php } ?>

<!-- Vor- und Nachnamen -->
<p class="noMargin bold"><a href="member_profile.php?member=<?php echo $rndmemb->id; ?>" title="Profil anzeigen"><?php echo $rndmemb->person->fullName; ?></a></p>

<!-- Milit�r.Funktion, Milit�r.Einteilung -->
<p class="noMargin">
<?php
    if( $rndmemb->current->militaer->funktion ) {
        $funktion = $rndmemb->current->militaer->funktion . " " . $rndmemb->current->militaer->zusatzFkt;
        echo "{$funktion} <a href=\"search.php?field=qryMilitaer.funktionMilitaerFull&amp;expr=" . urlencode( $rndmemb->current->militaer->funktion . " " . $rndmemb->current->militaer->zusatzFkt ) . "&amp;datumBeginn={$rndmemb->current->militaer->datumBeginn}&amp;datumEnde={$rndmemb->current->militaer->datumEnde}&amp;hist=true\" onclick=\"window.open( this.href ); return false;\">
              <img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum die gleiche Funktion hatten" ) . "\" /></a>";
    }
    if( $rndmemb->current->militaer->funktion && $rndmemb->current->militaer->einteilung ) {
        echo ", ";
    }
    if( $rndmemb->current->militaer->einteilung ) {
        $einteilung = $rndmemb->current->militaer->einteilung;
        echo "{$einteilung} <a href=\"search.php?field=qryMilitaer.einteilung&amp;expr=" . urlencode( $rndmemb->current->militaer->einteilung ) . "&amp;datumBeginn={$rndmemb->current->militaer->datumBeginn}&amp;datumEnde={$rndmemb->current->militaer->datumEnde}&amp;hist=true\" onclick=\"window.open( this.href ); return false;\">
              <img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die im selben Zeitraum eingeteilt waren bei" ) . " {$rndmemb->current->militaer->einteilung}\" /></a>";
    }
?>
</p>

<!-- - - Abstand, Beruf.Funktion, Beruf.Abteilung -->
<p class="noMargin marginTop">
<?php
    echo $rndmemb->current->beruf->funktion;
    if ( $rndmemb->current->beruf->abteilung && $rndmemb->current->beruf->abteilung ) {
        echo ", ";
    }
    echo $rndmemb->current->beruf->abteilung;
?>
</p>

<!-- - - Beruf.Organisation -->
<?php
    if ( $rndmemb->current->beruf->organisation ) {
        echo "<p class=\"noMargin";
        // add extra space above if not done so before
        if ( !$rndmemb->current->beruf->funktion && !$rndmemb->current->beruf->abteilung ) {
            echo " marginTop";
        }
        echo "\">";
        echo $rndmemb->current->beruf->organisation;
        echo "</p>";
    }
?>



<?php /*
    $i = 0;
    while( $rndmemb->verein[0]->datumBeginn > date( "Y-m-d" ) ) {
        $i++;
    }
    echo translate( $rndmemb->verein[0]->funktion );
*/?>

</div><!-- startRndMember -->


<div id="startMember">

<p><strong><?php echo translate( "Mein Profil" ); ?></strong></p>
<a href="member_profile.php?member=<?php echo $user->id; ?>"><img id="profilePhotoStart" src="include/profilePhoto.php?member=<?php echo $user->id; ?>" alt="<?php echo $user->person->fullName; ?>" title="<?php echo translate( "Profil anzeigen" ); ?>" /></a>

<!-- Dienstgrad -->
<?php
    if ( $user->current->militaer->dienstgrad ) {
?>
<p class="noMargin">
<?php
    echo translate( $user->current->militaer->dienstgrad, $user->person->sprache ) . " " . translate( $user->current->militaer->zusatzDg, $user->person->sprache );
?>
</p>
<?php } ?>

<!-- Vor- und Nachnamen -->
<p class="noMargin bold"><a href="member_profile.php?member=<?php echo $user->id; ?>" title="<?php echo translate( "Profil anzeigen" ); ?>"><?php echo $user->person->fullName; ?></a></p>

<!-- Letzte Bearbeitung, letzter Login -->
<p class="noMargin">
<?php
    echo translate( "Zuletzt bearbeitet" ) . ": " . date( "d.m.Y", strtotime( max( $user->modDateOwner, $user->modDateAdmin ) ) ) . "<br />";

    $i = 0;
    $j = 0;
    while( $j<2 && $i<100 ) {
        $j += $user->log[$i]->typID == 1;
        $i++;
    }
    $i--;
    if( $user->log[$i]->success ) {
        echo translate( "letzter Login" ) . ": " . date( "d.m.Y, Hi", strtotime( $user->log[$i]->timestamp ) );
    } else {
        if( $user->log[$i]->timestamp ) {
            echo "<span class=\"warning\">" . translate( "Fehlgeschlagener Loginversuch am" ) . " " . date( "d.m.Y, Hi", strtotime( $user->log[$i]->timestamp ) ) . "</span>";
        }
    }
?>
</p>

<!-- Anleitung -->
<p class="noMargin"><br /><br /><a href="howto.php" title="<?php echo translate( "Anleitung" ); ?>"><?php echo translate( "Anleitung" ); ?></a></p>

</div><!-- startMember -->


</div><!-- startItem -->


<div class="startItem">


<div class="startItemLeft">

<?php
    $content = getContent( 4 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );

    if( $_SESSION["rights"]["editContent"] ) {
?>
<a class="memberEditButton" href="fckeditor.php?action=EDIT&amp;item=4&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php
    }
?>

</div><!-- startItemLeft -->


<div class="startItemRight">

<p><strong><?php echo translate( "Blog" ); ?></strong></p>

<?php
    $blog = blogLastEntries( 5 );
    
    echo "<ul class=\"memberBlogList\">";
    for( $i=0; $i<count( $blog ); $i++ ) {
        $title = utf8_encode($blog[$i]["post_title"]);
        echo "<li><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">{$title}</a>";
        if( $blog[$i]["comment_count"] ) {
            echo " (+ {$blog[$i]["comment_count"]} <a href=\"{$blog[$i]["guid"]}#comments\" onclick=\"window.open( this.href ); return false;\">" . translate( "Kommentar" . ( $blog[$i]["comment_count"] > 1 ? "e" : "" ) ) . "</a>)";
        }
        echo "</li>";
    }
    echo "</ul>";
?>

<?php if( false ) { ?>
<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<div class="editButtonHolder">
<a class="startEditButtonTop" href="fckeditor.php?action=EDIT&amp;item=3&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
</div>
<?php } ?>

<?php
    $content = getContent( 3 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
<?php } ?>

</div><!-- startItemRight -->


</div><!-- startItem -->


<div class="startItem">


<div class="startItemLeft">

<p><strong><?php echo translate( "Mitgliederbewegungen" ); ?></strong></p>
<ul class="memberBlogList">
    <li><a href="member_movements.php?context=neumitglieder"><?php echo translate( "Die letzten 20 Beitritte" ); ?></a></li>
    <li><a href="member_movements.php?context=neubrevetiert"><?php echo translate( "Neu brevetierte Gst Of" ); ?></a></li>
    <li><a href="member_movements.php?context=verstorben"><?php echo translate( "Verstorbene Gst Of" ) . " " . date( "Y" ); ?></a></li>
</ul>

</div><!-- startItemLeft -->


<div class="startItemRight">

<?php
//     $conn = dbconn::open();
//     $query = "SELECT param0 FROM sysStor ORDER BY storID";
//     $result = $conn->query($query);
//
//     dbconn::close( $conn );
// 
//     $row = array();
//     while( $row[] = $result->fetch(PDO::FETCH_ASSOC) ) { }

    $num = array();
    $conn = dbconn::open();
    $query = "SELECT COUNT(personID) FROM tblPersonen";
    $result = $conn->query($query);

    $num[] = $result->fetch(PDO::FETCH_NUM);

    $query = "SELECT COUNT(personID) FROM tblPersonen WHERE todesdatum='0000-00-00'";
    $result = $conn->query($query);

    $num[] = $result->fetch(PDO::FETCH_NUM);

    $result = glob( "image/profile/*.jpg" );
    $num[] = count( $result );

    $query = "SELECT COUNT(DISTINCT personID) FROM tblVerein WHERE organisationRegionID=1 AND funktionID=1 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()";
    $result = $conn->query($query);

    $num[] = $result->fetch(PDO::FETCH_NUM);

    $query = "SELECT COUNT(personID) FROM tblPersonen WHERE veroeffentlichungID=1";
    $result = $conn->query($query);

    $num[] = $result->fetch(PDO::FETCH_NUM);
    dbconn::close( $conn );
?>
<p><strong><?php echo translate( "Plattform Interna" ); ?></strong></p>
<p class="nomargin"><?php echo translate( "Erfasste Gst Of" ) . ": <strong>{$num[0][0]}</strong>"; ?></p>
<ul class="memberBlogList">
    <li><?php echo translate( "davon lebend" ) . ": <strong>{$num[1][0]}</strong>"; ?></li>
    <li><?php echo translate( "davon mit Bild" ) . ": <strong>{$num[2]}</strong>"; ?></li>
    <li><?php echo translate( "davon Mitglieder" ) . ": <strong>{$num[3][0]}</strong>"; ?></li>
    <li><?php echo translate( "davon veröffentlicht" ) . ": <strong>{$num[4][0]}</strong>"; ?></li>
</ul>

<?php
if( false ) {
    $content = getContent( 35 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );

    if( $_SESSION["rights"]["editContent"] ) {
?>
<a class="memberEditButton" href="fckeditor.php?action=EDIT&amp;item=35&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php
    }
}
?>

</div><!-- startItemRight -->


</div><!-- startItem -->


</div><!-- content -->

<?php
    include( "include/footer.inc.php" );
?>

</body>

</html>