<?php
    require_once('head.php');
    ggstof_head();

    $_SESSION["rights"] = userRights( $_SESSION["userID"] );

    date_default_timezone_set( "Europe/Zurich" );
    $context = array_key_exists('context', $_REQUEST) ? $_REQUEST["context"]: '';
?>




<?php
    include("include/head.inc.php");
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Aktivitäten" ); ?></title>
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/accordeon.js"></script>
<script type="text/javascript" src="script/viewTabContent.js"></script>
</head>


<body id="projects">


<?php
    include("include/navigationLeft.inc.php");
    include("include/navigationTop.inc.php");
?>


<div id="content" class="partner">


<!-- start tabs, credits to facebook.com -->
<div class="home_main_item">
<div class="newsfeed_header">
<div id="newsfeed_tabs_wrapper">
<div id="newsfeed_tabs" class="Tabset_tabset">
<div class="HomeTabs">
<!-- spans to avoid href -->
<span id="Laufbahn" class="HomeTabs_tab HomeTabs_first<?php if( empty( $context ) || $context == "laufbahn" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Laufbahn" ) ?></span>
<span id="Netzwerk" class="HomeTabs_tab<?php if( $context == "netzwerk" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Netzwerk" ) ?></span>
<span id="Miliz" class="HomeTabs_tab<?php if( $context == "miliz" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Miliz" ) ?></span>
<span id="Sicherheitspolitik" class="HomeTabs_tab<?php if( $context == "sicherheitspolitik" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Sicherheitspolitik" ) ?></span>
<span id="Kommunikation" class="HomeTabs_tab<?php if( $context == "kommunikation" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Kommunikation" ) ?></span>
</div>
</div>
</div>
</div>
</div>
<!-- end tabs -->


<div class="tabContent" id="tabLaufbahn"<?php if( empty( $context ) || $context == "laufbahn" ) { echo " style=\"display:block\""; } ?>>
<div class="tabContentHolder">

<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Themen und Ziele" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=36&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=laufbahn"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 36 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mittel und Wege" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=37&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=laufbahn"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 37 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Schwergewichte" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=38&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=laufbahn"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 38 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "News" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $blog = blogLastEntries( 5, 170 );
    echo "<ul class=\"memberBlogList\">";
    for( $i=0; $i<count( $blog ); $i++ ) {
        echo "<li><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">". utf8_encode($blog[$i]["post_title"]) ."</a></li>";
    }
    echo "</ul>";
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<?php if( isset( $_SESSION["userID"] ) ) { ?>
<div class="accordeon accordeonProfile">
<?php
    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID,tblVerein.vereinID
              FROM tblVerein
              LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID
              WHERE organisationRegionID BETWEEN 30 AND 39 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
              ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);
    //
    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData( $row[1] );
        $n++;
    }
?>
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ) ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>

    <?php
    for( $i=0; $i<$n; $i++ ) {
        echo "<li><a href=\"member_profile.php?member={$team[$i]->id}\">" . translate( $team[$i]->current->militaer->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->current->militaer->zusatzDg . " " . $team[$i]->person->vorname . " " . $team[$i]->person->vorname2 . " " . $team[$i]->person->name ."</a>, ". $team[$i]->verein[0]->vereinFunktion ."<br />" . nl2br( $team[$i]->verein[0]->bemerkungen ) . "</li>";
    }
    ?>
</ul>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ); ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $content = getContent( 39 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=39&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=laufbahn"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->
<?php } // if( isset( $_SESSION["userID"] ) ) ?>

</div><!-- end .tabContentHolder -->
</div><!-- end #tab -->


<div class="tabContent" id="tabNetzwerk"<?php if( $context == "netzwerk" ) { echo " style=\"display:block\""; } ?>>
<div class="tabContentHolder">

<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Themen und Ziele" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=40&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=netzwerk"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 40 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mittel und Wege" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=41&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=netzwerk"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 41 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Schwergewichte" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=42&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=netzwerk"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 42 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "News" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $blog = blogLastEntries( 5, 33 );
    echo "<ul class=\"memberBlogList\">";
    for( $i=0; $i<count( $blog ); $i++ ) {
        echo "<li><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">". utf8_encode($blog[$i]["post_title"]) ."</a></li>";
    }
    echo "</ul>";
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<?php if( isset( $_SESSION["userID"] ) ) { ?>
<div class="accordeon accordeonProfile">
<?php
    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID,tblVerein.vereinID
              FROM tblVerein
              LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID
              WHERE organisationRegionID BETWEEN 40 AND 49 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
              ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);
    //
    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData( $row[1] );
        $n++;
    }
?>
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ) ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>

    <?php
    for( $i=0; $i<$n; $i++ ) {
        echo "<li><a href=\"member_profile.php?member={$team[$i]->id}\">" . translate( $team[$i]->current->militaer->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->current->militaer->zusatzDg . " " . $team[$i]->person->vorname . " " . $team[$i]->person->vorname2 . " " . $team[$i]->person->name ."</a>, ". $team[$i]->verein[0]->vereinFunktion ."<br />" . nl2br( $team[$i]->verein[0]->bemerkungen ) . "</li>";
    }
    ?>
</ul>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ); ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $content = getContent( 43 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=43&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=netzwerk"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->
<?php } // if( isset( $_SESSION["userID"] ) ) ?>

</div><!-- end .tabContentHolder -->
</div><!-- end #tab -->


<div class="tabContent" id="tabMiliz"<?php if( $context == "miliz" ) { echo " style=\"display:block\""; } ?>>
<div class="tabContentHolder">

<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Themen und Ziele" ); ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=44&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=miliz"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 44 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mittel und Wege" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=45&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=miliz"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 45 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Schwergewichte" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=46&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=miliz"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 46 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "News" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $blog = blogLastEntries( 5, 44 );
    echo "<ul class=\"memberBlogList\">";
    for( $i=0; $i<count( $blog ); $i++ ) {
        echo "<li><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">". utf8_encode($blog[$i]["post_title"]) ."</a></li>";
    }
    echo "</ul>";
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<?php if( isset( $_SESSION["userID"] ) ) { ?>
<div class="accordeon accordeonProfile">
<?php
    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID,tblVerein.vereinID
              FROM tblVerein
              LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID
              WHERE organisationRegionID BETWEEN 50 AND 59 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
              ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData( $row[1] );
        $n++;
    }
?>
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ); ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>

    <?php
    for( $i=0; $i<$n; $i++ ) {
        echo "<li><a href=\"member_profile.php?member={$team[$i]->id}\">" . translate( $team[$i]->current->militaer->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->current->militaer->zusatzDg . " " . $team[$i]->person->vorname . " " . $team[$i]->person->vorname2 . " " . $team[$i]->person->name ."</a>, ". $team[$i]->verein[0]->vereinFunktion ."<br />" . nl2br( $team[$i]->verein[0]->bemerkungen ) . "</li>";
    }
    ?>
</ul>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ); ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $content = getContent( 47 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=47&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=miliz"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->
<?php } // if( isset( $_SESSION["userID"] ) ) ?>

</div><!-- end .tabContentHolder -->
</div><!-- end #tab -->


<div class="tabContent" id="tabSicherheitspolitik"<?php if( $context == "sicherheitspolitik" ) { echo " style=\"display:block\""; } ?>>
<div class="tabContentHolder">

<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Themen und Ziele" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=48&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=sicherheitspolitik"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 48 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mittel und Wege" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=49&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=sicherheitspolitik"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 49 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Schwergewichte" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=50&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=sicherheitspolitik"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 50 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "News" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $blog = blogLastEntries( 5, 153 );
    echo "<ul class=\"memberBlogList\">";
    for( $i=0; $i<count( $blog ); $i++ ) {
        echo "<li><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">". utf8_encode($blog[$i]["post_title"]) ."</a></li>";
    }
    echo "</ul>";
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<?php if( isset( $_SESSION["userID"] ) ) { ?>
<div class="accordeon accordeonProfile">
<?php
    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID,tblVerein.vereinID
              FROM tblVerein
              LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID
              WHERE organisationRegionID BETWEEN 60 AND 69 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
              ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData( $row[1] );
        $n++;
    }
?>
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ); ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>

    <?php
    for( $i=0; $i<$n; $i++ ) {
        echo "<li><a href=\"member_profile.php?member={$team[$i]->id}\">" . translate( $team[$i]->current->militaer->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->current->militaer->zusatzDg . " " . $team[$i]->person->vorname . " " . $team[$i]->person->vorname2 . " " . $team[$i]->person->name ."</a>, ". $team[$i]->verein[0]->vereinFunktion ."<br />" . nl2br( $team[$i]->verein[0]->bemerkungen ) . "</li>";
    }
    ?>
</ul>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ); ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $content = getContent( 51 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=51&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=sicherheitspolitik"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->
<?php } // if( isset( $_SESSION["userID"] ) ) ?>

</div><!-- end .tabContentHolder -->
</div><!-- end #tab -->


<div class="tabContent" id="tabKommunikation"<?php if( $context == "kommunikation" ) { echo " style=\"display:block\""; } ?>>
<div class="tabContentHolder">

<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Themen und Ziele" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=52&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kommunikation"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 52 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Mittel und Wege" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=53&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kommunikation"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 53 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Aktuelle Schwergewichte" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=54&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kommunikation"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 54 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "News" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $blog = blogLastEntries( 5, 32 );
    echo "<ul class=\"memberBlogList\">";
    for( $i=0; $i<count( $blog ); $i++ ) {
        echo "<li><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">". utf8_encode($blog[$i]["post_title"]) ."</a></li>";
    }
    echo "</ul>";
?>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<?php if( isset( $_SESSION["userID"] ) ) { ?>
<div class="accordeon accordeonProfile">
<?php
    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID,tblVerein.vereinID
              FROM tblVerein
              LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID
              WHERE organisationRegionID BETWEEN 12 AND 12 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
              ORDER BY funktionID DESC,name ASC";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $team[$n] = new person( $row[0] );
        $team[$n]->getMilitaryData();
        $team[$n]->getSocietyData( $row[1] );
        $n++;
    }
?>
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Team" ); ?> (<?php echo $n; ?>)</span></a>
<div class="groupChildren">
<div class="child">
<ul>

    <?php
    for( $i=0; $i<$n; $i++ ) {
        echo "<li><a href=\"member_profile.php?member={$team[$i]->id}\">" . translate( $team[$i]->current->militaer->dienstgrad, $team[$i]->person->sprache ) . " " . $team[$i]->current->militaer->zusatzDg . " " . $team[$i]->person->vorname . " " . $team[$i]->person->vorname2 . " " . $team[$i]->person->name ."</a>, ". $team[$i]->verein[0]->vereinFunktion ."<br />" . nl2br( $team[$i]->verein[0]->bemerkungen ) . "</li>";
    }
    ?>
</ul>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->


<div class="accordeon accordeonProfile">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Dokumente" ); ?></span></a>
<div class="groupChildren">
<div class="child">

<?php
    $content = getContent( 55 );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=55&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=kommunikation"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<?php } ?>

<div class="unorderedListAccordeon">
<?php
    echo stripslashes( $content["text"] );
?>
</div>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->
</div><!-- end .accordeon -->
<?php } // if( isset( $_SESSION["userID"] ) ) ?>

</div><!-- end .tabContentHolder -->
</div><!-- end #tab -->


</div><!-- end #content -->


<?php
    $timestamp = date( "d.m.Y", max( $timestamp ) );
    include("include/footer.inc.php");
?>

</body>
</html>