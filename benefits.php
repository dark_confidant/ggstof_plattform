<?php
    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }

    if( $_SESSION["rights"]["roleID"] == 5 ) {
        $benefitID = empty( $_REQUEST["benefitID"] ) ? NULL : $_REQUEST["benefitID"];
        $benefit = new benefit( $benefitID );
        $data = array_key_exists('data', $_POST) ? $_POST["data"]: '';
        $data["modifierID"] = $_SESSION["personID"];
        if( !isset( $data["live"] ) ) {
            $data = array_merge( $data, array( "live"=>0 ) );
        }

        switch( $action = array_key_exists('action', $_REQUEST) ? $_REQUEST["action"]: '' ) {
            case "SEARCH":
                $searchfield = array_key_exists('searchfield', $_POST) ? $_POST["searchfield"]: '';
                $searchexpr  = array_key_exists('searchexpr', $_POST) ? $_POST["searchexpr"]: '';
                if( $searchfield && $searchexpr ) {
                    $criteria = array( $searchfield=>$searchexpr, "tblPersonen.todesdatum"=>"0000-00-00" );
                    $result = searchMember( $criteria, false, false, "tblPersonen.personID", false );
                } else {
                    $result = array();
                }
                break;

            case "ADD":
                $berufID = $_GET["berufID"];
                $criteria = array( "qryBeruf.berufID"=>$berufID );
                $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache" );
                $member = new person( $result[0]["tblPersonen.personID"] );
                $member->getMilitaryData();
                $member->getJobData( $berufID );
                break;

            case "EDIT":
                $berufID = $benefit->data->berufID;
                $member = new person( $benefit->data->personID );
                $member->getMilitaryData();
                $member->getJobData( $berufID );
                break;

            case "UPDATE":
                if( $data["url"] == "http://" ) {
                    $data["url"] = "";
                }
                $benefit->setData( $data );
                if( !empty( $_FILES["bild"]["name"] ) ) {
                    $imgName = "image/benefits/{$benefit->id}.png";
                    move_uploaded_file( $_FILES["bild"]["tmp_name"], $imgName );

                    list( $width, $height, $type ) = getimagesize( $imgName );
                    switch( $type ) {
                        case 1: $imgOld = imagecreatefromgif( $imgName ); break;
                        case 2: $imgOld = imagecreatefromjpeg( $imgName ); break;
                        case 3: $imgOld = imagecreatefrompng( $imgName ); break;
                    }
                    imagealphablending( $imgOld, true );
                    imagesavealpha( $imgOld, true );
                    if( $width > 400 || $height > 400 ) {
                        $factor = min( 400/$width, 400/$height );
                        $imgNew = imagecreatetruecolor( floor( $width*$factor ), floor( $height*$factor ) );
                        imagecopyresampled( $imgNew, $imgOld, 0, 0, 0, 0, floor( $width*$factor ), floor( $height*$factor ), $width, $height );
                    } else {
                        $imgNew = $imgOld;
                    }
                    imagepng( $imgNew, $imgName );
                }
                header( "Location: {$_SERVER["SCRIPT_NAME"]}" );
                break;

            case "DELETE":
                $benefit->delete();
                unlink( "image/benefits/{$benefit->id}.png" );
                header( "Location: {$_SERVER["SCRIPT_NAME"]}" );
                break;
        }
    }

    // Instantiate site objects
    $addButton = new imgAddButton;
    $addButton->class = "";
    $addButton->href = "{$_SERVER["SCRIPT_NAME"]}?action=SEARCH";
    $editButton = new imgEditButton;
    $editButton->class = "";
?>

<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Benefits" ); ?></title>
</head>

<body>

<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>


<div id="content">

<?php
    if( $action == "ADD" || $action == "EDIT" ) {
        require_once("module/fckeditor/fckeditor.php");
        $names = array( "text1_de", "text2_de", "text1_fr", "text2_fr", "text1_it", "text2_it" );

        if(!defined('lang')){
            $lang = '';
        }
        foreach( $names as $n ) {
            $fck[$n] = new FCKeditor( "data[$n]" );
            $fck[$n]->BasePath = "module/fckeditor/";
            $fck[$n]->Config["CustomConfigurationsPath"] = "/public_html/plattform/module/myFCKconfig.js";
            $fck[$n]->Config["AutoDetectLanguage"] = false;
            $fck[$n]->Config["DefaultLanguage"] = $lang;
            $fck[$n]->Config["LinkUpload"] = false;
            $fck[$n]->Config["SkinPath"] = "skins/silver/";
            $fck[$n]->ToolbarSet = "GGstOf";
            $fck[$n]->Height = "300";
            $fck[$n]->Value = stripslashes( $benefit->data->$n );
        }
?>

<form id="formBenefits" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
<fieldset>


<div class="benefitsUploadWrapper">

<?php
    echo "<p class='benefitsParagraphMargin'><strong>" . translate( $member->current->militaer->dienstgrad, $member->person->sprache ) . " " . translate( $member->current->militaer->zusatzDg, $member->person->sprache ) . " {$member->person->fullName}</strong></p>";
    echo "<p class='benefitsParagraphMargin'>".substr( ( $member->beruf[0]->funktion ? "{$member->beruf[0]->funktion}, " : "" ) . ( $member->beruf[0]->abteilung ? "{$member->beruf[0]->abteilung}, " : "" ) . ( $member->beruf[0]->organisation ? "{$member->beruf[0]->organisation}, " : "" ), 0, -2 )."</p>";
?>
<p><label><?php echo translate( "Bild hochladen" ); ?> (<?php echo translate( "erlaubte Dateitypen" ) ?>: JPEG, JPG, PNG, GIF)</label><input name="bild" type="file" accept="image/jpeg image/png image/gif" /></p>
<p><label><?php echo translate( "URL" ); ?>: </label><input name="data[url]" type="text" value="<?php echo $benefit->data->url ? $benefit->data->url : "http://"; ?>" /></p>

</div>

<!-- deutsch -->
<div class="benefitsUploadWrapper">

<p class="benefitsParagraphMargin">DE</p>

<div class="benefitsUploadWrapperFirstBox">
<p class="benefitsParagraphSmallMargin"><label><?php echo translate( "Angebot" ); ?>:</label></p><p><?php $fck["text1_de"]->Create(); ?><!--textarea name="data[text1_de]" rows="5" cols="5"><?php echo $benefit->data->text1_de; ?></textarea--></p>
</div>

<div>
<p class="benefitsParagraphSmallMargin"><label><?php echo translate( "Weitere Informationen" ); ?>:</label></p><p><?php $fck["text2_de"]->Create(); ?><!--textarea name="data[text2_de]" rows="5" cols="5"><?php echo $benefit->data->text2_de; ?></textarea--></p>
</div>

<div class="clear"></div>

</div>

<!-- französisch -->
<div class="benefitsUploadWrapper">

<p class="benefitsParagraphMargin">FR</p>

<div class="benefitsUploadWrapperFirstBox">
<p class="benefitsParagraphSmallMargin"><label><?php echo translate( "Angebot" ); ?>:</label></p><p><?php $fck["text1_fr"]->Create(); ?><!--textarea name="data[text1_fr]" rows="5" cols="5"><?php echo $benefit->data->text1_fr; ?></textarea--></p>
</div>

<div>
<p class="benefitsParagraphSmallMargin"><label><?php echo translate( "Weitere Informationen" ); ?>:</label></p><p><?php $fck["text2_fr"]->Create(); ?><!--textarea name="data[text2_fr]" rows="5" cols="5"><?php echo $benefit->data->text2_fr; ?></textarea--></p>
</div>

<div class="clear"></div>

</div>

<!-- italienisch -->
<div class="benefitsUploadWrapper">

<p class="benefitsParagraphMargin">IT</p>

<div class="benefitsUploadWrapperFirstBox">
<p class="benefitsParagraphSmallMargin"><label><?php echo translate( "Angebot" ); ?>:</label></p><p><?php $fck["text1_it"]->Create(); ?><!--textarea name="data[text1_it]" rows="5" cols="5"><?php echo $benefit->data->text1_it; ?></textarea--></p>
</div>

<div>
<p class="benefitsParagraphSmallMargin"><label><?php echo translate( "Weitere Informationen" ); ?>:</label></p><p><?php $fck["text2_it"]->Create(); ?><!--textarea name="data[text2_it]" rows="5" cols="5"><?php echo $benefit->data->text2_it; ?></textarea--></p>
</div>

<div class="clear"></div>

</div>

<!-- checkbox -->
<div class="benefitsUploadWrapper">

<p><input id="live" type="checkbox" name="data[live]" value="1"<?php echo $benefit->data->live ? " checked=\"checked\"" : ""; ?> /><label for="live"><?php echo translate( "Angebot LIVE" ); ?></label></p>

</div>


<input type="hidden" name="data[berufID]" value="<?php echo $berufID; ?>" />
<input type="hidden" name="benefitID" value="<?php echo $benefit->id; ?>" />
<input type="hidden" name="action" value="UPDATE" />
<p class="benefitsParagraphMargin"><input type="submit" value="<?php echo translate( "Speichern" ) ?>" />
<input type="button" value="<?php echo translate( "Abbrechen" ) ?>" onClick="window.location='<?php echo $_SERVER["SCRIPT_NAME"]; ?>';" />
<?php
    if( $action == "EDIT" ) {
        echo "<input type=\"button\" value=\"" . translate( "Löschen" ) . "\" onclick=\"window.location='{$_SERVER["SCRIPT_NAME"]}?benefitID={$benefit->id}&amp;action=DELETE';\" />";
    }
?></p>

</fieldset>
</form>

<?php
    } elseif( $action == "SEARCH" ) {
?>

<form id="formBenefits" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
<fieldset>

<?php
        if( empty( $searchexpr ) ) {
?>

<div class="benefitsSearchWrapper">
    <p class="benefitsParagraphMargin"><?php echo translate( "Person suchen" ) ?>:</p>
    <p class="benefitsParagraphMargin"><input class="searchField" type="text" name="searchname" size="20" /></p>
    <p><input type="button" value=" <?php echo translate( "Suchen" ) ?> " onClick="var f = document.forms['formBenefits']; f.elements['searchfield'].value='tblPersonen.name'; f.elements['searchexpr'].value=f.elements['searchname'].value; f.submit();" /></p>
</div>

<div class="benefitsSearchWrapper">
    <p class="benefitsParagraphMargin"><?php echo translate( "Funktion suchen" ) ?>:</p>
    <p class="benefitsParagraphMargin"><input class="searchField" type="text" name="searchfunction" size="20" /></p>
    <p><input type="button" value=" <?php echo translate( "Suchen" ) ?> " onClick="var f = document.forms['formBenefits']; f.elements['searchfield'].value='qryBeruf.funktion'; f.elements['searchexpr'].value=f.elements['searchfunction'].value; f.submit();" /></p>
</div>
<input type="hidden" name="searchfield" />
<input type="hidden" name="searchexpr" />
<input type="hidden" name="action" value="SEARCH" />

<?php
        } else {
            if( count( $result ) > 0 ) {
?>

<div id="accordeon" class="accordeon accordeonProfile showAll">
<table class="tableGrid tableMargin">
    <thead>
        <tr>
            <th class="fixWidthSearchResults firstColumn"><?php echo translate( "Name" ); ?></th>
            <th><?php echo translate( "Position" ); ?></th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
<?php
    for( $i=0; $i<count( $result ); $i++ ) {
        $member = new person( $result[$i]["tblPersonen.personID"] );
        $member->getMilitaryData();
        $member->getJobData();
        echo "<tr>
                  <td class=\"firstColumn\">
                      <a href=\"member_profile.php?member={$member->id}\" onclick=\"window.open( this.href ); return false;\">". translate( $member->current->militaer->dienstgrad, $member->person->sprache ) . " " . translate( $member->current->militaer->zusatzDg, $member->person->sprache ) . " {$member->person->fullName}</a>
                  </td>
                  <td>
                      <select name=\"berufID$i\">";
        $b = 0;
        while( $member->beruf[$b]->datumEnde >= date( "Y-m-d" ) ) {
            echo "<option value=\"{$member->beruf[$b]->berufID}\">". substr( ( $member->beruf[$b]->funktion ? "{$member->beruf[$b]->funktion}, " : "" ) . ( $member->beruf[$b]->abteilung ? "{$member->beruf[$b]->abteilung}, " : "" ) . ( $member->beruf[$b]->organisation ? "{$member->beruf[$b]->organisation}, " : "" ), 0, -2 ) . "</option>";
            $b++;
        }

        echo          "</select>
                  </td>
                  <td>
                      <input type=\"button\" value=\"" . translate( "Weiter" ) . "\" onclick=\"window.location='{$_SERVER["SCRIPT_NAME"]}?action=ADD&amp;berufID='+document.forms['formBenefits'].elements['berufID$i'].value;\" />
                  </td>
              </tr>";
    } // for( $i=0; $i<count( $result ); $i++ )
?>
    </tbody>
</table>
</div>

<?php
            } else {
                echo translate( "Kein Eintrag gefunden" );
            } // if( count( $result ) > 0 )
        } // if( empty( $searchexpr ) )
?>

</fieldset>
</form>

<?php
    } else {
?>

<p><strong><?php echo translate( "Spezielle Angebote von Mitgliedern für Mitglieder" );

    if( $action !="ADD" && $_SESSION["rights"]["roleID"] == 5 ) {
        echo "<span style='margin-left: 8px'>";
        $addButton->output();
        echo "</span>";
    }


?></strong></p>

<?php
    $conn = dbconn::open();

    $query = "SELECT * FROM tblBenefits
              LEFT JOIN (
                  SELECT berufID,personID
                  FROM tblBeruf
              ) AS qryBeruf ON tblBenefits.berufID=qryBeruf.berufID";
    $query .= $_SESSION["rights"]["roleID"] < 5 ? " WHERE live=1" : "";
    $result = $conn->query($query);

    dbconn::close( $conn );

    while( $entry = $result->fetch(PDO::FETCH_ASSOC) ) {
        $member = new person( $entry["personID"] );
        $member->getMilitaryData();
        $member->getJobData( $entry["berufID"] );

        // border
        echo "<div class='benefitsWrapper'>
        ";

        // profile pic
        echo "<div class='benefitsColumnImg'>
        ";
        echo "<a href=\"member_profile.php?member={$member->id}\"><img class='profilePhoto' src=\"include/profilePhoto.php?member={$member->id}&amp;deceased=" . ( $member->person->todesdatum != "0000-00-00" ) . "\" alt=\"{$member->person->fullName}\" /></a>";

        // edit button for admin
        if( $_SESSION["rights"]["roleID"] == 5 ) {
            echo "<div class='benefitsColumnEdit'>
            ";
            $editButton->output( array( "href"=>"{$_SERVER["SCRIPT_NAME"]}?benefitID={$entry["benefitID"]}&amp;action=EDIT" ) );
            echo "
            </div>
            ";
        }

        echo "
        </div>
        ";

        // text and logo
        // create two sub columns to veritcally align the two benefitsColumnText boxes
        echo "<div class='benefitsColumnTextFirst'>
        <div class='benefitsColumnTextLeft'>
        ";

        echo "<p><strong>" . translate( $member->current->militaer->dienstgrad, $member->person->sprache ) . ( $member->person->todesdatum == "0000-00-00" ? " " . translate( $member->current->militaer->zusatzDg, $member->person->sprache ) : "" ) . $member->person->fullName ."</strong></p>";
        echo "<p>" . $member->beruf[0]->funktion. " " . $member->beruf[0]->abteilung. "<br />" . $member->beruf[0]->organisation."</p>";

        echo "
        </div>
        ";

        echo "<div class='benefitsColumnTextRight'>
        ";

        echo "<p>" . ( $entry["url"] ? "<a href=\"{$entry["url"]}\" onclick=\"window.open( this.href ); return false;\">" : "" ) . "<img src=\"image/benefits/{$entry["benefitID"]}.png\" alt=\"\" />" . ( $entry["url"] ? "</a>" : "" ) . "</p>";
        if( $_SESSION["rights"]["roleID"] == 5 && $entry["live"] ) {
            echo "<p><strong>LIVE</strong></p>";
        }

        echo "
        </div>
        </div>
        ";

        echo "
        <div class='benefitsColumnTextLast'>
        <div class='benefitsColumnTextLeft'>
        ";
        if(!defined('lang')){
            $lang = '';
        }


        if(!isset($entry["text1_$lang"])){
            $entry["text1_$lang"] = '';
        }
        if(!isset($entry["text2_$lang"])){
            $entry["text2_$lang"] = '';
        }

        echo "<p>{$entry["text1_$lang"]}</p>";

        echo "
        </div>
        ";

        echo "<div class='benefitsColumnTextRight'>
        ";

        echo "<p>{$entry["text2_$lang"]}</p>";

        echo "
        </div>
        </div>
        ";



        echo "<div class='clear'></div>";

        echo "
        </div>

        ";
    }
?>
                        

<?php
    }
?>

</div><!-- #content -->

<?php
    include( "include/footer.inc.php" );
?>

</body>

</html>