<?php

    require_once('head.php');
    ggstof_head();

    require_once( "include/prepend.inc.php" );

    if( $_SERVER["REQUEST_METHOD"] == "POST" ) {
        if( isset( $_POST["phrase"] ) && is_string( $_POST["phrase"] ) && isset( $_SESSION["phrase"] ) && strlen( $_POST["phrase"] ) > 0 && strlen( $_SESSION["phrase"] ) > 0 && $_POST["phrase"] == $_SESSION["phrase"] ) {
            unset( $_SESSION["phrase"] );

            $body = "Anrede: " . $_POST["inputPrivat"]["anrede"];
            $body .= "\nVorname: " . $_POST["inputPrivat"]["vorname"];
            $body .= "\nNachname: " . $_POST["inputPrivat"]["name"];
            $body .= "\nGeburtsdatum: " . $_POST["inputPrivat"]["geburtsdatum"];
            $body .= "\nSprache: " . $_POST["inputEinstellungen"]["sprache"];
            $body .= "\nWohnadresse: " . $_POST["inputPrivat"]["adresse"];
            $body .= "\nPostleitzahl: " . $_POST["inputPrivat"]["plz"];
            $body .= "\nOrt: " . $_POST["inputPrivat"]["ort"];
            $body .= "\nE-Mail privat: " . $_POST["inputPrivat"]["email"];
            $body .= "\nAktueller Dienstgrad: " . $_POST["inputMilitaer"]["dienstgrad"]; // . " " . $_POST["inputMilitaer"]["zusatzDg"];
            $body .= "\nAktuelle Einteilung: " . $_POST["inputMilitaer"]["einteilung"];
            $body .= "\nAktuelle Funktion: " . $_POST["inputMilitaer"]["funktion"];
            $body .= "\nWer/Was gab den Ausschlag f�r Ihr Aufnahmegesuch?: " . $_POST["inputPrivat"]["reference"];

            $success = mail( "aufnahmegesuch@ggstof.ch", "Aufnahmegesuch {$_POST["inputPrivat"]["vorname"]} {$_POST["inputPrivat"]["name"]}", $body, "From: plattform@ggstof.ch\r\nX-Mailer: PHP/" . phpversion() );
            $msg = $success ? "Aufnahmegesuch wurde erfolgreich verschickt." : "Ein Fehler ist aufgetreten.";
        } else {
            $msg = "Falscher Code.";
        }
        unlink( "temp/" . md5( session_id()) . ".png" );
    }
?>




<?php
include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf)"; ?></title>
<link rel="stylesheet" type="text/css" href="style/lightbox.css" media="screen" />
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/xmlHttp.js"></script>
<?php if( $success ) { ?>
<meta http-equiv="refresh" content="2; URL=index.php">
<?php } ?>
</head>



<!-- start body -->
<body>




<!-- start #navigationLeft -->
<?php
include( "include/navigationLeft.inc.php" );
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
include( "include/navigationTop.inc.php" );
?>
<!-- end #navigationTop -->


<!-- start #content -->
<div id="content">

<p><strong><?php echo translate( "Aufnahmegesuch" ); ?></strong></p>

<?php
    if( $msg ) { echo "<p><strong>" . translate( $msg ) . "</strong></p>"; }
    if( !$success ) {
?>
<form id="formMemberNewProfil" class="formUpdateProfil" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
<fieldset class="fieldsetEditForm">
<table>
<tr><td><label><?php echo translate( "Anrede" ); ?></label><input type="text" name="inputPrivat[anrede]" value="<?php echo $_POST["inputPrivat"]["anrede"]; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Vorname" ); ?></label><input type="text" name="inputPrivat[vorname]" value="<?php echo $_POST["inputPrivat"]["vorname"]; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Nachname" ); ?></label><input type="text" name="inputPrivat[name]" value="<?php echo $_POST["inputPrivat"]["name"]; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Geburtsdatum" ); ?> (<?php echo translate( "Format" ) ?>: <?php echo translate( "JJJJ-MM-TT" ) ?>):</label><input type="text" name="inputPrivat[geburtsdatum]" value="<?php echo $_POST["inputPrivat"]["geburtsdatum"]; ?>" /></td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "Sprache" ); ?></label><input type="text" name="inputEinstellungen[sprache]" value="<?php echo $_POST["inputEinstellungen"]["sprache"]; ?>" /></td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "Wohnadresse" ); ?></label><input type="text" name="inputPrivat[adresse]" value="<?php echo $_POST["inputPrivat"]["adresse"]; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Postleitzahl" ); ?></label><input type="text" name="inputPrivat[plz]" value="<?php echo $_POST["inputPrivat"]["plz"]; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Ort" ); ?></label><input type="text" name="inputPrivat[ort]" value="<?php echo $_POST["inputPrivat"]["ort"]; ?>" /></td></tr>
</td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "E-Mail privat" ); ?></label><input type="text" name="inputPrivat[email]" value="<?php echo $_POST["inputPrivat"]["email"]; ?>" /></td></tr>
</table>
<table>
</table>
<table>
<?php if( false ) { ?>
<tr><td><label><?php echo translate( "Aktueller Dienstgrad" ); ?></label><select name="inputMilitaer[dienstgrad]"><?php makeOptions( "linkMilitaerDienstgrad", $_POST["inputMilitaer"]["dienstgradID"], false, $lang ); ?></select> <select name="inputMilitaer[zusatzDgID]"><?php makeOptions( "linkMilitaerZusatz", $_POST["inputMilitaer"]["zusatzDgID"], false, $lang ); ?></select></td></tr>
<tr><td><label><?php echo translate( "Aktuelle Einteilung" ); ?></label><input type="text" name="inputMilitaer[einteilung]" value="<?php echo $_POST["inputMilitaer"]["einteilung"]; ?>" /></td></tr>
<?php } ?>
<tr><td><label><?php echo translate( "Aktueller Dienstgrad" ); ?></label><input type="text" name="inputMilitaer[dienstgrad]" value="<?php echo $_POST["inputMilitaer"]["dienstgrad"]; ?>" /> <!--input type="text" name="inputMilitaer[zusatzDg]" value="<?php echo $_POST["inputMilitaer"]["zusatzDg"]; ?>" /--></td></tr>
<tr><td><label><?php echo translate( "Aktuelle Einteilung" ); ?></label><input type="text" name="inputMilitaer[einteilung]" value="<?php echo $_POST["inputMilitaer"]["einteilung"]; ?>" /></td></tr>
<tr><td><label><?php echo translate( "Aktuelle Funktion" ); ?></label><input type="text" name="inputMilitaer[funktion]" value="<?php echo $_POST["inputMilitaer"]["funktion"]; ?>" /></td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "Ich akzeptiere" ); ?> <a href="articles.php" onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=600,left=50,top=50,status'); return false"><?php echo translate( "die Statuten" ); ?></a></label><input type="checkbox" name="inputEinstellungen[statuten]" value="1" <?php if( $_POST["inputEinstellungen"]["statuten"] ) { echo "checked=\"checked\" "; } ?>/></td></tr>
<tr><td><label><?php echo translate( "Ich akzeptiere" ); ?> <a href="privacy.php" onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=600,left=50,top=50,status'); return false"><?php echo translate( "die Datenschutzbestimmungen" ); ?></a></label><input type="checkbox" name="inputEinstellungen[datenschutz]" value="1" <?php if( $_POST["inputEinstellungen"]["datenschutz"] ) { echo "checked=\"checked\" "; } ?>/></td></tr>
<tr><td><label><?php echo translate( "Ich akzeptiere" ); ?> <a href="codex.php" onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=600,left=50,top=50,status'); return false"><?php echo translate( "den Codex" ); ?></a></label><input type="checkbox" name="inputEinstellungen[codex]" value="1" <?php if( $_POST["inputEinstellungen"]["codex"] ) { echo "checked=\"checked\" "; } ?>/></td></tr>
</table>
<table>
<tr><td><label><?php echo translate( "Wer/Was gab den Ausschlag f�r Ihr Aufnahmegesuch?" ); ?></label><input type="text" name="inputPrivat[reference]" value="<?php echo $_POST["inputPrivat"]["reference"]; ?>" /></td></tr>
</table>
<table>
<tr><td class="legend"><?php echo translate( "Alle Felder sind Pflicht" ); ?></td></tr>
</table>
<table>
<tr><td class="legend"><?php echo translate( "Bitte schreiben Sie die Zeichenfolge im folgenden Bild in das leere Feld unterhalb des Bildes. Wenn Sie die Zeichenkette nicht lesen k�nnen, klicken Sie auf" ) . " &laquo;" . translate( "Neues Captcha" ) . "&raquo;."; ?></td></tr>
</table>
<?php
    captcha();
?>
<table>
<tr><td><img id="c" src="temp/<?php echo md5( session_id() ) . ".png?" . time(); ?>" width="200" height="80" alt="" /></td></tr>
<tr><td><input type="text" name="phrase" /><input type="button" value="<?php echo translate( "Neues Captcha" ); ?>" onclick="document.getElementById('c').src = 'image/dummy.gif'; sendXmlHttpRequest( 'captcha.php', '', '', '' ); window.setTimeout( 'var d = new Date(); document.getElementById(\'c\').src = \'temp/<?php echo md5( session_id() ); ?>.png?\'+d.getTime()', 1000 )" /></td></tr>
</table>
<table>
<tr><td>
<input type="submit" class="formsSubmitButton" value="<?php echo translate( "Absenden" ); ?>" onclick="var form = document.forms['formMemberNewProfil']; if( form.elements['inputPrivat[anrede]'].value != '' && form.elements['inputPrivat[vorname]'].value != '' && form.elements['inputPrivat[name]'].value != '' && form.elements['inputEinstellungen[sprache]'].value != '' && form.elements['inputPrivat[adresse]'].value != '' && form.elements['inputPrivat[plz]'].value != '' && form.elements['inputPrivat[ort]'].value != '' && form.elements['inputPrivat[email]'].value != '' && form.elements['inputMilitaer[dienstgrad]'].value != '' && form.elements['inputMilitaer[einteilung]'].value != '' && form.elements['inputMilitaer[funktion]'].value != '' && form.elements['inputEinstellungen[statuten]'].checked && form.elements['inputEinstellungen[datenschutz]'].checked && form.elements['inputEinstellungen[codex]'].checked && form.elements['inputPrivat[reference]'].value != '' && form.elements['phrase'].value != '' ) { return true; } else { window.alert('<?php echo translate( "Bitte alle Felder ausf�llen" ); ?>.'); return false; }" />
<input type="reset" value="<?php echo translate( "Abbrechen" ); ?>" onclick="location='index.php'" />
</td></tr>
</table>
</fieldset>
</form>
<?php } ?>


</div>
<!-- end #content -->


<?php
    $timestamp = date( "d.m.Y", strtotime( $pers->timestamp ) );
    include( "include/footer.inc.php" );
?>

</body>
<!-- end body -->

</html>