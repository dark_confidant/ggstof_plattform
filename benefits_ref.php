<?php
    require_once('head.php');
    ggstof_head(true);

    $benefitID = $_REQUEST["benefitID"];
    if( !isset( $benefitID ) ) {
        exit;
    }

    $member = new person( $_SESSION["personID"] );
//     $member->getJobData();
    $lang = array( "de"=>"0", "fr"=>"2", "it"=>"3" );
    $sex = array( "1"=>"m�nnlich", "2"=>"weiblich" );
?>

<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Benefits" ); ?></title>
</head>

<body onload="document.forms['form_ref'].submit();">

<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>

<div id="content">

<h1><?php echo translate( "Sie werden gleich weitergeleitet" ); ?>...</h1>

<?php
    switch( $benefitID ) {
        case 5:
?>
<form name="form_ref" action="http://sanitas-corporate.ch/form.php?L=<?php echo $lang[$member->person->sprache]; ?>" method="post">
<input type="hidden" value="<?php echo "{$member->person->vorname} {$member->person->vorname2}" ; ?>" name="firstname" />
<input type="hidden" value="<?php echo $member->person->name ; ?>" name="lastname" />
<input type="hidden" value="<?php echo preg_replace( "/(\d{4})-(\d{2})-(\d{2})/", "$3.$2.$1", $member->person->geburtsdatum ); ?>" name="birthday" />
<input type="hidden" value="<?php echo $member->person->adresse ; ?>" name="street_no" />
<input type="hidden" value="<?php echo $member->person->plz ; ?>" name="zip" />
<input type="hidden" value="<?php echo $member->person->ort ; ?>" name="city" />
<input type="hidden" value="<?php echo $member->person->land ; ?>" name="country" />
<input type="hidden" value="<?php echo $member->person->mobil ? $member->person->mobil : $member->person->tf ; ?>" name="phone" />
<input type="hidden" value="<?php echo $member->person->email ; ?>" name="email" />
<input type="hidden" value="GGstOf" name="employer" />
<input type="hidden" value="Luzern" name="workspace" />
<input type="hidden" value="<?php echo $sex[$member->person->anredeID] ; ?>" name="insurant[sex]" />
<input type="hidden" value="Basic" name="insurant[systemchoice][]" />
</form>
<?php
        break;
    } // switch( $benefitID )
?>

</div><!-- #content -->

<?php
    include( "include/footer.inc.php" );
?>

</body>

</html>