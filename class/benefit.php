<?php
    class benefit {

        function __construct( $id=NULL ) {
            if( isset( $id ) ) {
                $this->id = $id;
                $this->getData();
            }
        }

        function getData() {
            $conn = dbconn::open();

            $query = "SELECT * FROM tblBenefits
                      LEFT JOIN (
                          SELECT berufID,personID
                          FROM tblBeruf
                      ) AS qryBeruf ON tblBenefits.berufID=qryBeruf.berufID
                      WHERE tblBenefits.benefitID={$this->id}";
            $result = $conn->query($query);

            $this->data = $result->fetch(PDO::FETCH_OBJ);

            dbconn::close( $conn );
        }

        function setData( $data=array() ) {
            $conn = dbconn::open();

//             $mod = array_key_exists( "modifierID", $data ) ? "Admin" : "Owner";
            if( isset( $this->id ) ) {
                $set = '';
                foreach( $data as $key => $value ) {
                    $set .= "$key='" . $conn->quote($value) . "',";
                }
                $set = substr( $set, 0, -1 );
                $query = "UPDATE tblBenefits SET $set WHERE benefitID={$this->id}";
            } else {
                $keys = '';
                $values = '';
                foreach( $data as $key => $value ) {

                    $keys .= "$key,";
                    $values .= "'" . $conn->quote($value) . "',";
                }
                $keys = substr( $keys, 0, -1 );
                $values = substr( $values, 0, -1 );
                $query = "INSERT INTO tblBenefits ($keys) VALUES ($values)";
            }

            $result = $conn->query($query);

            $insid = $conn->lastInsertId();
            if( $insid ) {
                $this->id = $insid;
            }

            dbconn::close( $conn );

            $this->getData();
        }

        function delete() {
            $conn = dbconn::open();

            $query = "DELETE FROM tblBenefits WHERE benefitID={$this->id}";
            $conn->query($query);


            dbconn::close( $conn );
        }

    }
?>