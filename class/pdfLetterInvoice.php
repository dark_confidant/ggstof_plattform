<?php
    class pdfLetterInvoice extends pdfLetter {

        public $contentID = 29;

        function Template() {
            $conn = dbconn::open();
            $query = "SELECT personID
                      FROM tblVerein
                      WHERE funktionID=4 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()";
            $result = $conn->query($query);

            dbconn::close( $conn );
            $row = $result->fetch(PDO::FETCH_NUM);
            $sender = new person( $row[0] );
            $sender->getMilitaryData();
            $this->sender = str_replace( "  ", " ", translate( $sender->current->militaer->dienstgrad, $sender->person->sprache ) . " " . translate( $sender->current->militaer->zusatzDg, $sender->person->sprache ) . " " . $sender->person->fullName );
            $this->senderFunction = "Kassier";
            $this->senderFunction2 = "Der Kassier";
            $this->senderEmail = "kassier@ggstof.ch";
        }

        function SetVars() {
            $conn = dbconn::open();
            $query = "SELECT betrag
                      FROM tblZahlungenJahresbeitrag
                      WHERE jahr=" . date( "Y" );
            $result = $conn->query($query);

            dbconn::close( $conn );
            $row = $result->fetch(PDO::FETCH_NUM);
            if( $row[0] ) {
                $this->vars->ZAHLUNG_JAHRESBEITRAG = "CHF $row[0]";
            }

            $conn = dbconn::open();
            $query = "SELECT SUM(betrag) AS summe
                      FROM tblZahlungen
                      WHERE personID={$this->recipient->id} AND typID=1 AND jahr<" . date( "Y" ) . " AND datumEinzahlung='0000-00-00'";
            $result = $conn->query($query);

            dbconn::close( $conn );
            $row = $result->fetch(PDO::FETCH_NUM);
            if( $row[0] ) {
                $this->vars->ZAHLUNG_AUSSTEHEND = translate( 334, $this->recipient->person->sprache, false, false ) . " CHF $row[0].";
            }

            $conn = dbconn::open();
            $query = "SELECT SUM(betrag) AS summe
                      FROM tblZahlungen
                      WHERE personID={$this->recipient->id} AND typID=1 AND datumEinzahlung='0000-00-00'";
            $result = $conn->query($query);

            dbconn::close( $conn );
            $row = $result->fetch(PDO::FETCH_NUM);
            if( $row[0] ) {
                $this->vars->ZAHLUNG_TOTAL = "CHF $row[0]";
            }

        }

    }
?>