<?php
    class dbconn {

        private static $instance;

        private function __construct() { }

        public static function open( $db=false ) {
            if( !isset( self::$instance ) ) {
                $host = "localhost";
                switch ($db) {
                    case "ggstofch_blog":
                        $user = "root";
                        $pw = "root";
                        $dsn = "mysql:dbname=ggstofch_blog;host=localhost";
                        break;
                    default:
                        $user = "root";
                        $pw = "root";
                        $dsn = "mysql:dbname=ggstofch_pform;host=localhost;charset=utf8";
                        break;
                }
                try {
                    self::$instance = new PDO($dsn, $user, $pw);
                } catch(PDOException $e) {
                    echo 'Connection failed: ' . $e->getMessage();
                }
            }
            return self::$instance;
        }

        public static function close( $conn = NULL ) {

            self::$instance = NULL;
        }

    }
?>