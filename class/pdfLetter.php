<?php
    class pdfLetter extends FPDF {

        function Init() {
            $this->x0 = 10;
            $this->x1 = 39;
            $this->y0 = 10;
            $this->y1 = 58;
            $this->lineHeight = 4;
            $this->ColWidth = 210-($this->x1)-16;

            $this->SetMargins( $this->x1, $this->y0 );
            $this->SetTextColor( 0 );

            $this->contentType = "application/pdf";

            $this->Template();
        }

        function Template() {}

        function SetVars() {}

        function Header() {
            $this->Image( $_SERVER["DOCUMENT_ROOT"] . "/image/template/logoPDF.png", $this->x0, $this->y0, 0, 25 );

            $this->SetFillColor( 127, 127, 127 );
            $this->Rect( $this->x1, $this->y0, $this->ColWidth, 9.5, "F" );
            $this->SetFillColor( 63, 63, 63 );
            $this->Rect( 163, $this->y0, 5, 9.5, "F" );
            $this->SetFillColor( 191, 191, 191 );
            $this->Rect( 163, ($this->y0)+9.5, 5, 9.5, "F" );

            $this->SetXY( $this->x1, ($this->y0)+3 );
            $this->SetFont( "Helvetica", "B", 12 );
            $this->Cell( 122, $this->lineHeight, translate( "Gesellschaft der Generalstabsoffiziere", $this->recipient->person->sprache, false, false ), 0, 0, "R" );
            $this->SetX( -41 );
            $this->Cell( 0, $this->lineHeight, "GGstOf" );

            $this->SetXY( $this->x1, $this->y0+11 );
            $this->SetFont( "Helvetica", "", 8 );
            $this->MultiCell( 122, $this->lineHeight, utf8_encode( "{$this->sender}, " . translate( $this->senderFunction, $this->recipient->person->sprache, false, false ) . "\n{$this->senderEmail}" ), 0, "R" );
            $this->SetXY( -41, $this->y0+11 );
            $this->MultiCell( 20, $this->lineHeight, "\n" . date( "d.m.Y" ) );
        }

        function Footer() {
            $this->SetXY( $this->x0, -38 );
            $this->SetFont( "Helvetica", "", 10 );
            $this->MultiCell( 0, $this->lineHeight, "GGstOf\n6000 Luzern\n\ninfo@ggstof.ch" );
        }

        function Compose( $recipientID, $channelID=NULL ) {
            $this->vars = new variableSet();
            $this->recipient = new person( $recipientID );
            $this->channelID = isset( $channelID ) ? $channelID : $this->recipient->person->channelID;

            $this->recipient->getMilitaryData();
            $this->recipient->getJobData();

            $m = 0;
            while( $this->recipient->militaer[$m]->datumBeginn > date( "Y-m-d" ) ) {
                $m++;
            }
            $b = 0;
            while( $this->recipient->beruf[$b]->datumBeginn > date( "Y-m-d" ) ) {
                $b++;
            }

            $dienstgrad = strip_spaces( translate( $this->recipient->militaer[$m]->dienstgrad, $this->recipient->person->sprache, false, false ) . " " . translate( $this->recipient->militaer[$m]->zusatzDg, $this->recipient->person->sprache, false, false ) );
            $vorname  = strip_spaces( "{$this->recipient->person->vorname} {$this->recipient->person->vorname2}" );
            $vorname  = utf8_encode($vorname);
            $name     = $this->recipient->person->name;
            $name     = utf8_encode($name);
            $adresse  = $this->channelID == 4 ? "{$this->recipient->beruf[$b]->organisation}\n{$this->recipient->beruf[$b]->adresse}" : $this->recipient->person->adresse;
            $postfach = $this->channelID == 4 ? $this->recipient->beruf[$b]->postfach : $this->recipient->person->postfach;
            $adresse .= $postfach ? "\n$postfach" : "";
            $adresse  = utf8_encode($adresse);
            $ort      = $this->channelID == 4 ? "{$this->recipient->beruf[$b]->plz} {$this->recipient->beruf[$b]->ort}" : "{$this->recipient->person->plz} {$this->recipient->person->ort}";
            $ort      = utf8_encode($ort);
            $land = str_replace( array( "Schweiz", "Suisse", "Svizzera", "Switzerland" ), "", ( $this->channelID == 4 ? $this->recipient->beruf[$b]->land : $this->recipient->person->land ) );
            $anrede = translate( $this->recipient->person->anrede, $this->recipient->person->sprache, false, false ) . " " . translate( $this->recipient->militaer[$m]->dienstgradText, $this->recipient->person->sprache, false, false );
            $anredeText = translate( $this->recipient->person->anredeText, $this->recipient->person->sprache, false, false );
            $dienstgradText = translate( $this->recipient->militaer[$m]->dienstgradText, $this->recipient->person->sprache, false, false );
            $this->recipient->fullname = "$dienstgrad $vorname $name";

            $this->vars->MEMBER_ID = $this->recipient->id;
            $this->vars->NAME = $name;
            $this->vars->DG = $dienstgrad;
            $this->vars->DG_TXT = $dienstgradText;
            $this->vars->ANREDE = $anred;
            $this->vars->ANREDE_TXT = $anredeText;
            $this->vars->EMAIL = $this->recipient->person->email;
            $this->vars->REC_INVOICE = translate( $this->recipient->person->paymentChannel, $this->recipient->person->sprache, false, false );
            $this->SetVars();

            require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/contentManagement.php" );
            $content = getEmailContent( $this->contentID, $this->recipient->person->sprache );
            $this->subject = $this->vars->replace( $content["subject"] );
            $this->body    = $this->vars->replace( $content["body"] );

            $this->AddPage();

            $this->SetFont( "Helvetica", "", 10 );
            $this->SetY( $this->y1 );
            $this->MultiCell( $this->ColWidth, $this->lineHeight, "$dienstgrad\n$vorname $name\n$adresse\n$ort\n$land" );

            $this->SetFont( "Helvetica", "B", 10 );
            $this->SetY( $this->y1+35 );
            $this->Cell( 0, 0, utf8_encode( $this->subject ) );

            $this->SetFont( "Helvetica", "", 10 );
            $this->SetY( $this->y1+48 );
            $this->MultiCell( $this->ColWidth, $this->lineHeight, utf8_encode( $this->body ) );
            $this->Ln();
            $this->MultiCell( $this->ColWidth, $this->lineHeight, str_replace('é', 'É', strtoupper( translate( "Gesellschaft der Generalstabsoffiziere", $this->recipient->person->sprache, false, false ) ) . "\n" . translate( $this->senderFunction2, $this->recipient->person->sprache, false, false ) ) );
            $this->Ln();
            $this->Ln();
            $this->Cell( $this->ColWidth, $this->lineHeight, utf8_encode( $this->sender ) );

            $this->filename = "{$this->subject} - {$this->recipient->fullname}.pdf";
        }

        function Download( $filename=NULL ) {
            $this->filename = isset( $filename ) ? $filename : $this->filename;
            $this->Output( $this->filename, "D" );
        }

    }
?>