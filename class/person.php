<?php
    class person {

        public $log = array();

        function __construct( $id=NULL ) {
            if( isset( $id ) ) {
                $this->id = $id;
                $this->getPersonData();
            }
        }


        function getPersonData() {
            $conn = dbconn::open();

            $query = "SELECT * FROM tblPersonen
                      LEFT JOIN linkMilitaerRS ON tblPersonen.rsID=linkMilitaerRS.linkRsID
                      LEFT JOIN linkPersonStatus ON tblPersonen.statusID=linkPersonStatus.linkStatusID
                      LEFT JOIN linkPersonAnrede ON tblPersonen.anredeID=linkPersonAnrede.linkAnredeID
                      LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID
                      LEFT JOIN linkPersonVeroeffentlichung ON tblPersonen.veroeffentlichungID=linkPersonVeroeffentlichung.veroeffentlichungID
                      LEFT JOIN linkMilitaerTruppengattung ON tblPersonen.truppengattungID=linkMilitaerTruppengattung.linkTruppengattungID
                      LEFT JOIN linkPersonChannel ON tblPersonen.channelID=linkPersonChannel.linkChannelID
                      LEFT JOIN linkPersonChannelPost ON tblPersonen.postChannelID=linkPersonChannelPost.linkChannelID
                      LEFT JOIN linkPersonChannelMail ON tblPersonen.mailChannelID=linkPersonChannelMail.linkChannelID
                      LEFT JOIN linkZahlungenChannel ON tblPersonen.paymentChannelID=linkZahlungenChannel.linkChannelID
                      LEFT JOIN linkBlogChannel ON tblPersonen.blogChannelID=linkBlogChannel.linkChannelID
                      LEFT JOIN linkSysRoles ON tblPersonen.roleID=linkSysRoles.linkRoleID
                      WHERE tblPersonen.personID={$this->id}";
            $result = $conn->query($query);

            $this->person = $result->fetch(PDO::FETCH_OBJ) ;
            $this->person->fullName = str_replace( "  ", " ", "{$this->person->vorname} {$this->person->vorname2} {$this->person->name}" );

            $query = "SELECT * FROM linkPersonSupport";
            $result = $conn->query($query);

            $i = 0;
            while( $row[$i] = $result->fetch(PDO::FETCH_NUM) ) {
                $i++;
            }
            $binstr = strrev( decbin( $this->person->supportID ) );
            for( $j=0; $j<$i; $j++ ) {
                if( substr( $binstr, $j, 1 ) == 1 ) {
                    $this->person->support[] = $row[$j][1];
                }
            }

            $query = "SELECT MIN(datumBeginn) AS brevetierungsjahr FROM tblMilitaer
                      WHERE personID={$this->id} AND dienstgradID BETWEEN 1 AND 2";
//                          AND funktion LIKE 'Gst Of%'";
            $result = $conn->query($query);

            $result = $result->fetch(PDO::FETCH_NUM);
            $this->person->brevetierungsjahr = $result[0];

            $query = "SELECT MIN(datumBeginn) AS beitrittsdatum FROM tblVerein
                      WHERE personID={$this->id} AND funktionID BETWEEN 1 AND 13";
            $result = $conn->query($query);

            $result = $result->fetch(PDO::FETCH_NUM);
            $this->person->beitrittsdatum = $result[0];

            $query = "SELECT qry.modDateOwner FROM (
                          SELECT tblPersonen.personID,tblPersonen.modDateOwner FROM tblPersonen WHERE tblPersonen.personID={$this->id}
                          UNION (SELECT personID,modDateOwner FROM tblMilitaer WHERE personID={$this->id} ORDER BY modDateOwner DESC LIMIT 0,1)
                          UNION (SELECT personID,modDateOwner FROM tblBeruf WHERE personID={$this->id} ORDER BY modDateOwner DESC LIMIT 0,1)
                          UNION (SELECT personID,modDateOwner FROM tblBerufAusbildung WHERE personID={$this->id} ORDER BY modDateOwner DESC LIMIT 0,1)
                          UNION (SELECT personID,modDateOwner FROM tblPublic WHERE personID={$this->id} ORDER BY modDateOwner DESC LIMIT 0,1)
                          UNION (SELECT personID,modDateOwner FROM tblVerein WHERE personID={$this->id} ORDER BY modDateOwner DESC LIMIT 0,1)
                      ) AS qry
                      ORDER BY qry.modDateOwner DESC
                      LIMIT 0,1";
            $result = $conn->query($query);

            $row = $result->fetch(PDO::FETCH_NUM);
            if( $row !== false ) {
                $this->modDateOwner = $row[0];
            } else {
                $this->modDateOwner = "0000-00-00 00:00:00";
            }
            $query = "SELECT qry.modDateAdmin,qry.modifierID FROM (
                          SELECT tblPersonen.personID,tblPersonen.modDateAdmin,tblPersonen.modifierID FROM tblPersonen WHERE tblPersonen.personID={$this->id}
                          UNION (SELECT personID,modDateAdmin,modifierID FROM tblMilitaer WHERE personID={$this->id} ORDER BY modDateAdmin DESC LIMIT 0,1)
                          UNION (SELECT personID,modDateAdmin,modifierID FROM tblBeruf WHERE personID={$this->id} ORDER BY modDateAdmin DESC LIMIT 0,1)
                          UNION (SELECT personID,modDateAdmin,modifierID FROM tblBerufAusbildung WHERE personID={$this->id} ORDER BY modDateAdmin DESC LIMIT 0,1)
                          UNION (SELECT personID,modDateAdmin,modifierID FROM tblPublic WHERE personID={$this->id} ORDER BY modDateAdmin DESC LIMIT 0,1)
                          UNION (SELECT personID,modDateAdmin,modifierID FROM tblVerein WHERE personID={$this->id} ORDER BY modDateAdmin DESC LIMIT 0,1)
                      ) AS qry
                      ORDER BY qry.modDateAdmin DESC
                      LIMIT 0,1";
            $result = $conn->query($query);

            $row = $result->fetch(PDO::FETCH_NUM);
            if( $row !== false ) {
                $this->modDateAdmin = $row[0];
                $this->modifier = $row[1];
            } else {
                $this->modDateAdmin = "0000-00-00 00:00:00";
            }

            $query = "SELECT sysLog.*,linkSysLogTyp.typ FROM sysLog
                      LEFT JOIN linkSysLogTyp ON linkSysLogTyp.typID=sysLog.typID
                      WHERE userID<>'' AND userID='{$this->person->email}' OR sysLog.typID=10 AND param0='{$this->person->email}'
                      ORDER BY timestamp DESC
                      LIMIT 0,100";
            $result = $conn->query($query);

            $i = 0;
            while( $obj = $result->fetch(PDO::FETCH_ASSOC) ) {
                $this->log[$i] = $obj;
                $i++;
            }
            dbconn::close( $conn );
        }

        function setPersonData( $data=array() ) {
            $conn = dbconn::open();

            $mod = array_key_exists( "modifierID", $data ) ? "Admin" : "Owner";
            if( isset( $this->id ) ) {
                $set = '';
                foreach( $data as $key => $value ) {
                    $set .= "$key='" . $conn->quote($value) . "',";
                }
                $query = "UPDATE tblPersonen SET {$set}modDate$mod=CURRENT_TIMESTAMP() WHERE personID={$this->id}";
            } else {
                $keys = '';
                $values = '';
                foreach( $data as $key => $value ) {
                    $keys .= "$key,";
                    $values .= "'" . $conn->quote($value) . "',";
                }
                $keys = substr( $keys, 0, -1 );
                $values = substr( $values, 0, -1 );
                $query = "INSERT INTO tblPersonen ($keys,modDate$mod) VALUES ($values,CURRENT_TIMESTAMP())";
            }

            $result = $conn->query($query);

            $insid = $conn->lastInsertId();
            if( $insid ) {
                $this->id = $insid;
            }

            dbconn::close( $conn );

            $this->getPersonData();
        }


        function getMilitaryData( $id=NULL ) {
            $conn = dbconn::open();

/*            if( $id == "current" ) {
                $query = "SELECT militaerID FROM tblMilitaer
                          WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                          ORDER BY datumEnde DESC,datumBeginn DESC
                          LIMIT 0,1";
                $result = $conn->query($query);


                $row = $result->fetch(PDO::FETCH_NUM);
                $id = $row[0];
            }*/
            $query = "SELECT militaerID FROM tblMilitaer
                      WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                      ORDER BY datumEnde DESC,datumBeginn DESC
                      LIMIT 0,1";
            $result = $conn->query($query);


            $row = $result->fetch(PDO::FETCH_NUM);
            $current = $row[0];

            $query = "SELECT *,zusatzFkt.zusatz AS zusatzFkt,zusatzDg.zusatz AS zusatzDg FROM tblMilitaer
                      LEFT JOIN linkMilitaerZusatz AS zusatzFkt ON tblMilitaer.zusatzFktID=zusatzFkt.linkZusatzID
                      LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                      LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                      WHERE personID={$this->id}" . ( $id ? " AND militaerID=$id" : "" ) . "
                      ORDER BY datumEnde DESC,datumBeginn DESC";
            $result = $conn->query($query);


            dbconn::close( $conn );

            $i = 0;
            while( $obj = $result->fetch(PDO::FETCH_OBJ) ) {
                $this->militaer[$i] = $obj;
                if( $this->militaer[$i]->militaerID == $current ) {
                    $this->current->militaer =& $this->militaer[$i];
                }
                $i++;
            }
            if( !$this->current->militaer->dienstgradID && count( $this->militaer[0] ) ) {
                $this->current->militaer->dienstgradID =& $this->militaer[0]->dienstgradID;
                $this->current->militaer->dienstgrad =& $this->militaer[0]->dienstgrad;
                $this->current->militaer->zusatzDgID =& $this->militaer[0]->zusatzDgID;
                $this->current->militaer->zusatzDg =& $this->militaer[0]->zusatzDg;
            }
        }

        function setMilitaryData( $data, $id=NULL ) {
            $conn = dbconn::open();

            $mod = array_key_exists( "modifierID", $data ) ? "Admin" : "Owner";
            if( isset( $id ) ) {
                foreach( $data as $key => $value ) {
                    $set .= "$key='" . $conn->quote($value) . "',";
                }
                $query = "UPDATE tblMilitaer SET {$set}modDate$mod=CURRENT_TIMESTAMP() WHERE militaerID=$id";
            } else {
                foreach( $data as $key => $value ) {
                    $keys .= "$key,";
                    $values .= "'" . $conn->quote($value) . "',";
                }
                $keys = substr( $keys, 0, -1 );
                $values = substr( $values, 0, -1 );
                $query = "INSERT INTO tblMilitaer (personID,$keys,modDate$mod) VALUES ({$this->id},$values,CURRENT_TIMESTAMP())";
            }

            $result = $conn->query($query);
            $insid = $conn->lastInsertId();


            dbconn::close( $conn );

            if( $insid ) {
                return $insid;
            }

            $this->getMilitaryData();
        }

        function deleteMilitaryData( $id ) {
            $conn = dbconn::open();

            $query = "DELETE FROM tblMilitaer WHERE militaerID=$id";
            $conn->query($query);


            dbconn::close( $conn );
        }


        function getJobData( $id=NULL ) {
            $conn = dbconn::open();

/*            if( $id == "current" ) {
                $query = "SELECT berufID FROM tblBeruf
                          WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                          ORDER BY datumEnde DESC,datumBeginn DESC
                          LIMIT 0,1";
                $result = $conn->query($query);


                $row = $result->fetch(PDO::FETCH_NUM);
                $id = $row[0];
            }*/
            $query = "SELECT berufID FROM tblBeruf
                      WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                      ORDER BY datumEnde DESC,datumBeginn DESC
                      LIMIT 0,1";
            $result = $conn->query($query);


            $row = $result->fetch(PDO::FETCH_NUM);
            $current = $row[0];

            $query = "SELECT * FROM tblBeruf
                      LEFT JOIN linkBerufPosition ON tblBeruf.positionID=linkBerufPosition.linkPositionID
                      LEFT JOIN linkBerufTyp ON tblBeruf.typID=linkBerufTyp.linkTypID
                      WHERE personID={$this->id}" . ( $id ? " AND berufID=$id" : "" ) . "
                      ORDER BY datumEnde DESC,datumBeginn DESC";
            $result = $conn->query($query);


            dbconn::close( $conn );

            $i = 0;
            while( $obj = $result->fetch(PDO::FETCH_OBJ) ) {
                $this->beruf[$i] = $obj;
                if( $this->beruf[$i]->berufID == $current ) {
                    $this->current->beruf =& $this->beruf[$i];
                }
                $i++;
            }
        }

        function setJobData( $data, $id=NULL ) {
            $conn = dbconn::open();

            $mod = array_key_exists( "modifierID", $data ) ? "Admin" : "Owner";
            if( isset( $id ) ) {
                foreach( $data as $key => $value ) {
                    $set .= "$key='" . $conn->quote($value) . "',";
                }
                $query = "UPDATE tblBeruf SET {$set}modDate$mod=CURRENT_TIMESTAMP() WHERE berufID=$id";
            } else {
                foreach( $data as $key => $value ) {
                    $keys .= "$key,";
                    $values .= "'" . $conn->quote($value) . "',";
                }
                $keys = substr( $keys, 0, -1 );
                $values = substr( $values, 0, -1 );
                $query = "INSERT INTO tblBeruf (personID,$keys,modDate$mod) VALUES ({$this->id},$values,CURRENT_TIMESTAMP())";
            }

            $conn->query($query);


            dbconn::close( $conn );

            $this->getJobData();
        }

        function deleteJobData( $id ) {
            $conn = dbconn::open();

            $query = "DELETE FROM tblBeruf WHERE berufID=$id";
            $conn->query($query);


            dbconn::close( $conn );
        }


        function getEducationData( $id=NULL ) {
            $conn = dbconn::open();

/*            if( $id == "current" ) {
                $query = "SELECT ausbildungID FROM tblBerufAusbildung
                          WHERE personID={$this->id} AND datumEnde<=CURDATE()
                          ORDER BY datumEnde DESC
                          LIMIT 0,1";
                $result = $conn->query($query);


                $row = $result->fetch(PDO::FETCH_NUM);
                $id = $row[0];
            }*/
            $query = "SELECT ausbildungID FROM tblBerufAusbildung
                      WHERE personID={$this->id} AND datumEnde<=CURDATE()
                      ORDER BY datumEnde DESC
                      LIMIT 0,1";
            $result = $conn->query($query);


            $row = $result->fetch(PDO::FETCH_NUM);
            $current = $row[0];

            $query = "SELECT * FROM tblBerufAusbildung
                      LEFT JOIN linkBerufAusbildungTyp ON tblBerufAusbildung.typID=linkBerufAusbildungTyp.linkTypID
                      WHERE personID={$this->id}" . ( $id ? " AND ausbildungID=$id" : "" ) . "
                      ORDER BY datumEnde DESC";
            $result = $conn->query($query);


            dbconn::close( $conn );

            $i = 0;
            while( $obj = $result->fetch(PDO::FETCH_OBJ) ) {
                $this->ausbildung[$i] = $obj;
                if( $this->ausbildung[$i]->ausbildungID == $current ) {
                    $this->current->ausbildung =& $this->ausbildung[$i];
                }
                $i++;
            }
        }

        function setEducationData( $data, $id=NULL ) {
            $conn = dbconn::open();

            $mod = array_key_exists( "modifierID", $data ) ? "Admin" : "Owner";
            if( isset( $id ) ) {
                foreach( $data as $key => $value ) {
                    $set .= "$key='" . $conn->quote($value) . "',";
                }
                $query = "UPDATE tblBerufAusbildung SET {$set}modDate$mod=CURRENT_TIMESTAMP() WHERE ausbildungID=$id";
            } else {
                foreach( $data as $key => $value ) {
                    $keys .= "$key,";
                    $values .= "'" . $conn->quote($value) . "',";
                }
                $keys = substr( $keys, 0, -1 );
                $values = substr( $values, 0, -1 );
                $query = "INSERT INTO tblBerufAusbildung (personID,$keys,modDate$mod) VALUES ({$this->id},$values,CURRENT_TIMESTAMP())";
            }

            $conn->query($query);


            dbconn::close( $conn );

            $this->getEducationData();
        }

        function deleteEducationData( $id ) {
            $conn = dbconn::open();

            $query = "DELETE FROM tblBerufAusbildung WHERE ausbildungID=$id";
            $conn->query($query);


            dbconn::close( $conn );
        }


        function getPublicData( $id=NULL ) {
            $conn = dbconn::open();

/*            if( $id == "current" ) {
                $query = "SELECT publicID FROM tblPublic
                          WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                          ORDER BY datumEnde DESC,datumBeginn DESC
                          LIMIT 0,1";
                $result = $conn->query($query);


                $row = $result->fetch(PDO::FETCH_NUM);
                $id = $row[0];
            }*/
            $query = "SELECT publicID FROM tblPublic
                      WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                      ORDER BY datumEnde DESC,datumBeginn DESC
                      LIMIT 0,1";
            $result = $conn->query($query);


            $row = $result->fetch(PDO::FETCH_NUM);
            $current = $row[0];

            $query = "SELECT * FROM tblPublic
                      WHERE personID={$this->id}" . ( $id ? " AND publicID=$id" : "" ) . "
                      ORDER BY datumEnde DESC,datumBeginn DESC";
            $result = $conn->query($query);


            dbconn::close( $conn );

            $i = 0;
            while( $obj = $result->fetch(PDO::FETCH_OBJ) ) {
                $this->public[$i] = $obj;
                if( $this->public[$i]->publicID == $current ) {
                    $this->current->public =& $this->public[$i];
                }
                $i++;
            }
        }

        function setPublicData( $data, $id=NULL ) {
            $conn = dbconn::open();

            $mod = array_key_exists( "modifierID", $data ) ? "Admin" : "Owner";
            if( isset( $id ) ) {
                foreach( $data as $key => $value ) {
                    $set .= "$key='" . $conn->quote($value) . "',";
                }
                $query = "UPDATE tblPublic SET {$set}modDate$mod=CURRENT_TIMESTAMP() WHERE publicID=$id";
            } else {
                foreach( $data as $key => $value ) {
                    $keys .= "$key,";
                    $values .= "'" . $conn->quote($value) . "',";
                }
                $keys = substr( $keys, 0, -1 );
                $values = substr( $values, 0, -1 );
                $query = "INSERT INTO tblPublic (personID,$keys,modDate$mod) VALUES ({$this->id},$values,CURRENT_TIMESTAMP())";
            }

            $conn->query($query);


            dbconn::close( $conn );

            $this->getPublicData();
        }

        function deletePublicData( $id ) {
            $conn = dbconn::open();

            $query = "DELETE FROM tblPublic WHERE publicID=$id";
            $conn->query($query);


            dbconn::close( $conn );
        }


        function getSocietyData( $id=NULL ) {
            $conn = dbconn::open();

/*            if( $id == "current" ) {
                $query = "SELECT vereinID FROM tblVerein
                          WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                          ORDER BY datumEnde DESC,datumBeginn DESC
                          LIMIT 0,1";
                $result = $conn->query($query);


                $row = $result->fetch(PDO::FETCH_NUM);
                $id = $row[0];
            }*/
            $query = "SELECT vereinID FROM tblVerein
                      WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                      ORDER BY datumEnde DESC,datumBeginn DESC
                      LIMIT 0,1";
            $result = $conn->query($query);


            $row = $result->fetch(PDO::FETCH_NUM);
            $current = $row[0];

            $query = "SELECT * FROM tblVerein
                      LEFT JOIN linkVereinOrganisation ON tblVerein.organisationRegionID=linkVereinOrganisation.linkOrganisationID
                      LEFT JOIN linkVereinFunktion ON tblVerein.funktionID=linkVereinFunktion.linkVereinFunktionID
                      WHERE personID={$this->id}" . ( $id ? " AND vereinID=$id" : "" ) . "
                      ORDER BY datumEnde DESC,datumBeginn DESC";
            $result = $conn->query($query);

            dbconn::close( $conn );

            $i = 0;
            while( $obj = $result->fetch(PDO::FETCH_OBJ) ) {
                $this->verein[$i] = $obj;
                if( $this->verein[$i]->vereinID == $current ) {
                    $this->current->verein =& $this->verein[$i];
                }
                $i++;
            }
        }

        function setSocietyData( $data, $id=NULL ) {
            $conn = dbconn::open();

            $mod = array_key_exists( "modifierID", $data ) ? "Admin" : "Owner";
            if( isset( $id ) ) {
                foreach( $data as $key => $value ) {
                    $set .= "$key='" . $conn->quote($value) . "',";
                }
                $query = "UPDATE tblVerein SET {$set}modDate$mod=CURRENT_TIMESTAMP() WHERE vereinID=$id";
            } else {
                foreach( $data as $key => $value ) {
                    $keys .= "$key,";
                    $values .= "'" . $conn->quote($value) . "',";
                }
                $keys = substr( $keys, 0, -1 );
                $values = substr( $values, 0, -1 );
                $query = "INSERT INTO tblVerein (personID,$keys,modDate$mod) VALUES ({$this->id},$values,CURRENT_TIMESTAMP())";
            }

            $conn->query($query);


            dbconn::close( $conn );

            $this->getSocietyData();
        }

        function deleteSocietyData( $id ) {
            $conn = dbconn::open();

            $query = "DELETE FROM tblVerein WHERE vereinID=$id";
            $conn->query($query);


            dbconn::close( $conn );
        }


        function getPaymentData( $id=NULL ) {
            $conn = dbconn::open();

            $query = "SELECT *,IF(datumRechnung>IF(datumMahnung>datumEinzahlung,datumMahnung,datumEinzahlung),datumRechnung,IF(datumMahnung>datumEinzahlung,datumMahnung,datumEinzahlung)) AS maxDate
                      FROM tblZahlungen
                      LEFT JOIN linkZahlungenTyp ON tblZahlungen.typID=linkZahlungenTyp.linkZahlungID
                      WHERE personID={$this->id}" . ( $id ? " AND zahlungID=$id" : "" ) . "
                      ORDER BY jahr DESC,maxDate DESC";
            $result = $conn->query($query);

            dbconn::close( $conn );

            $i = 0;
            while( $obj = $result->fetch(PDO::FETCH_OBJ) ) {
                $this->zahlungen[$i] = $obj;
                $i++;
            }
        }

        function setPaymentData( $data, $id=NULL ) {
            $conn = dbconn::open();

            if( isset( $id ) ) {
                foreach( $data as $key => $value ) {
                    $set .= "$key='" . $conn->quote($value) . "',";
                }
                $set = substr( $set, 0, -1 );
                $query = "UPDATE tblZahlungen SET $set WHERE zahlungID=$id";
            } else {
                foreach( $data as $key => $value ) {
                    $keys .= "$key,";
                    $values .= "'" . $conn->quote($value) . "',";
                }
                $keys = substr( $keys, 0, -1 );
                $values = substr( $values, 0, -1 );
                $query = "INSERT INTO tblZahlungen (personID,$keys) VALUES ({$this->id},$values)";
            }

            $conn->query($query);


            dbconn::close( $conn );

            $this->getPaymentData();
        }

        function deletePaymentData( $id ) {
            $conn = dbconn::open();

            $query = "DELETE FROM tblZahlungen WHERE zahlungID=$id";
            $conn->query($query);


            dbconn::close( $conn );
        }


        function counter() {
            $conn = dbconn::open();

            $query = "UPDATE tblPersonen SET counter=counter+1 WHERE personID={$this->id}";
            $conn->query($query);


            dbconn::close( $conn );
        }


        function closeFile( $date ) {
            $conn = dbconn::open();

            $query = "UPDATE tblPersonen SET password='',aktualisierungID=1,channelID=98,paymentChannelID=0,blogChannelID=1 WHERE personID={$this->id}";
            $conn->query($query);

            $query = "UPDATE tblMilitaer SET datumEnde='$date' WHERE personID={$this->id} AND datumEnde>CURDATE()";
            $conn->query($query);

            $query = "UPDATE tblBeruf SET datumEnde='$date' WHERE personID={$this->id} AND datumEnde>CURDATE()";
            $conn->query($query);

            $query = "UPDATE tblPublic SET datumEnde='$date' WHERE personID={$this->id} AND datumEnde>CURDATE()";
            $conn->query($query);

            $query = "UPDATE tblVerein SET datumEnde='$date' WHERE personID={$this->id} AND datumEnde>CURDATE()";
            $conn->query($query);

            $query = "INSERT INTO tblVerein (personID,datumBeginn,organisationRegionID,funktionID) VALUES ({$this->id},'$date',91,199)";
            $conn->query($query);


            dbconn::close( $conn );
        }


        function invoice( $jahr=false, $datumRechnung=false, $betrag=false, $paymentChannelID=false ) {
            $jahr = $jahr ? $jahr : date( "Y" );
            $datumRechnung = $datumRechnung ? $datumRechnung : date( "Y-m-d" );
            if( !$betrag ) {
                $conn = dbconn::open();
                $query = "SELECT betrag FROM tblZahlungenJahresbeitrag WHERE jahr=$jahr";
                $result = $conn->query($query);

                dbconn::close( $conn );
                $row = $result->fetch(PDO::FETCH_NUM);
                $betrag = empty( $row ) ? 0 : $row[0];
            }
            $paymentChannelID = $paymentChannelID ? $paymentChannelID : $this->person->paymentChannelID;

            $conn = dbconn::open();
            $query = "INSERT INTO tblZahlungen (personID,typID,jahr,datumRechnung,paymentChannelID,betrag,modifierID) VALUES ({$this->id},1,$jahr,'$datumRechnung',$paymentChannelID,$betrag,{$_SESSION["personID"]})";
            $conn->query($query);

            dbconn::close( $conn );

            return empty( $retval ) ? false : $retval;
        }


        function isMember() {
            $conn = dbconn::open();
            $query = "SELECT COUNT(personID)
                      FROM tblVerein
                      WHERE personID={$this->id} AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE() AND organisationRegionID=1 AND funktionID=1";
            $result = $conn->query($query);

            dbconn::close( $conn );

            $row = $result->fetch(PDO::FETCH_NUM);
            return $row[0] > 0;
        }


        function printPDF() {
            //ob_clean();
            //ob_flush();
            require_once( "{$_SERVER["DOCUMENT_ROOT"]}/class/pdfMemberProfile.php" );
            require_once( "{$_SERVER["DOCUMENT_ROOT"]}/function/export.php" );

            $pdf = new pdfMemberProfile( "L", "mm", "A4" );

            $pdf->Init();

            personPDF( $pdf, $this );

            $pdf->Output( "{$this->person->vorname}{$this->person->vorname2}{$this->person->name}.pdf", "D" );
        }

//         function printInvoicePDF() {
//             require_once( "class/pdfInvoice.php" );
//             require_once( "function/export.php" );
// 
//             $pdf = new pdfInvoice( "P", "mm", "A4" );
// 
//             $pdf->Init();
// 
//             invoicePDF( $pdf, $this );
// 
//             $pdf->Output( str_replace( " ", "", $this->person->vorname . $this->person->vorname2 . $this->person->name ) . ".pdf", "D" );
//         }

        function buildVcard() {
            set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER["DOCUMENT_ROOT"] . "/module/pear/PEAR" );
            require_once( "module/pear/PEAR/Contact_Vcard_Build.php" );

            $m = 0;
            while( $this->militaer[$m]->datumBeginn >= date( "Y-m-d" ) && $m < 100 ) {
                $m++;
            }
            $b = 0;
            while( $this->beruf[$b]->datumBeginn >= date( "Y-m-d" ) && $b < 100 ) {
                $b++;
            }
            $a = 0;
            while( $this->ausbildung[$a]->datumEnde >= date( "Y-m-d" ) && $a < 100 ) {
                $a++;
            }

            $zusatzDg  = ( empty( $this->militaer[$m]->zusatzDg ) ) ? "" : " " . translate( $this->militaer[$m]->zusatzDg );
            $zusatzFkt = ( empty( $this->militaer[$m]->zusatzFkt ) ) ? "" : " " . translate( $this->militaer[$m]->zusatzFkt );

            // instantiate vcard
            $vcard = new Contact_Vcard_Build( "3.0" );

            // set private data
            $vcard->setFormattedName( ( $this->person->vorname ) . " " . ( $this->person->name ) );
            $vcard->setName( ( $this->person->name ), ( $this->person->vorname ), ( $this->person->vorname2 ), ( $this->ausbildung[$a]->titel ), "" );
            if( $this->person->geburtsdatum != "0000-00-00" ) {
                $vcard->setBirthday( $this->person->geburtsdatum );
            }
            $vcard->setURL( $this->person->url );
            $vcard->setPhoto( "http://plattform.ggstof.ch/image/profile/{$this->id}.jpg" );
            $vcard->addParam( "VALUE", "uri" );
            $vcard->setNote( ( translate( $this->militaer[$m]->dienstgrad, $this->person->sprache ) . $zusatzDg . ", " . translate( $this->militaer[$m]->funktion ) . $zusatzFkt . ", " . translate( $this->militaer[$m]->einteilung ) . "; Quelle: plattform.ggstof.ch, " . date( "d.m.Y" ) ) );

            // add private address
            $vcard->addEmail( $this->person->email );
            $vcard->addParam( "TYPE", "HOME" );
            $vcard->addParam( "TYPE", "PREF" );
            $vcard->addAddress( ( $this->person->postfach ), "", ( $this->person->adresse ), ( $this->person->ort ), "", $this->person->plz, ( $this->person->land ) );
            $vcard->addParam( "TYPE", "HOME" );
            $vcard->addTelephone( $this->person->tf );
            $vcard->addParam( "TYPE", "HOME" );
            $vcard->addTelephone( $this->person->mobil );
            $vcard->addParam( "TYPE", "CELL" );

            // add job address
            if( $this->beruf[$b]->datumEnde >= date( "Y-m-d" ) ) {
                $vcard->setTitle( ( $this->beruf[$b]->funktion ) );
                $vcard->addOrganization( array( ( $this->beruf[$b]->organisation ), ( $this->beruf[$b]->abteilung ) ) );
                $vcard->addEmail( $this->beruf[$b]->email );
                $vcard->addParam( "TYPE", "WORK" );
                $vcard->addAddress( ( $this->beruf[$b]->postfach ), "", ( $this->beruf[$b]->adresse ), ( $this->beruf[$b]->ort ), "", $this->beruf[$b]->plz, ( $this->beruf[$b]->land ) );
                $vcard->addParam( "TYPE", "WORK" );
                $vcard->addTelephone( $this->beruf[$b]->tf );
                $vcard->addParam( "TYPE", "WORK" );
            }

            // dispatch
//             $vcard->send( str_replace( " ", "", $this->person->vorname . $this->person->vorname2 . $this->person->name ) . ".vcf", "attachment", "UTF-8" );
            $vcard->send( str_replace( " ", "", $this->person->vorname . $this->person->vorname2 . $this->person->name ) . ".vcf", "attachment", "ISO-8859-1" );
        }

    }
?>