<?php
    class pdfInvoice extends FPDF {

        function Init() {
            $this->x0 = 10;
            $this->x1 = 39;
            $this->y0 = 10;
            $this->y1 = 58;
            $this->lineHeight = 4;
            $this->ColWidth = 210-($this->x1)-16;

            $this->SetMargins( $this->x1, $this->y0 );
            $this->SetTextColor( 0 );

            $conn = dbconn::open();
            $query = "SELECT personID
                      FROM tblVerein
                      WHERE funktionID=4 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()";
            $result = $conn->query($query);

            dbconn::close( $conn );
            $row = $result->fetch(PDO::FETCH_NUM);
            $kassier = new person( $row[0] );
            $kassier->getMilitaryData( "current" );
            $this->kassier = str_replace( "  ", " ", translate( $kassier->militaer[0]->dienstgrad, $kassier->person->sprache ) . " " . translate( $kassier->militaer[0]->zusatzDg, $kassier->person->sprache ) . "{$kassier->person->vorname} {$kassier->person->vorname2} {$kassier->person->name}" );
        }

        function Header() {
            $this->Image( "image/template/logoPDF.png", $this->x0, $this->y0, 0, 25 );

            $this->SetFillColor( 127, 127, 127 );
            $this->Rect( $this->x1, $this->y0, $this->ColWidth, 9.5, "F" );
            $this->SetFillColor( 63, 63, 63 );
            $this->Rect( 163, $this->y0, 5, 9.5, "F" );
            $this->SetFillColor( 191, 191, 191 );
            $this->Rect( 163, ($this->y0)+9.5, 5, 9.5, "F" );

//             $this->SetXY( $this->x1, ($this->y0)+3 );
//             $this->SetFont( "Helvetica", "B", 12 );
//             $this->Cell( 120, $this->lineHeight, utf8_encode( translate( "Gesellschaft der Generalstabsoffiziere", $person->person->sprache, false, false ) ), 0, 0, "R" );
//             $this->SetX( -41 );
//             $this->Cell( 0, $this->lineHeight, "GGstOf" );

//             $this->SetXY( $this->x1, $this->x0+10 );
//             $this->SetFont( "Helvetica", "", 8 );
//             $this->MultiCell( 120, $this->lineHeight, utf8_encode( "{$this->kassier}, " . translate( "Kassier" ) . "\nkassier@ggstof.ch" ), 0, "R" );
//             $this->SetXY( -41, $this->x0+10 );
//             $this->MultiCell( 20, $this->lineHeight, "\n" . date( "d.m.Y" ) );
        }

        function Footer() {
            $this->SetXY( $this->x0, -46 );
            $this->SetFont( "Helvetica", "", 10 );
            $this->MultiCell( 0, $this->lineHeight, "GGstOf\nc/o Kdo Gst S\nAAL\n6000 Luzern 30\n\ninfo@ggstof.ch" );
        }

    }
?>