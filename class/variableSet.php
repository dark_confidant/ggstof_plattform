<?php
    class variableSet {

        public $MOD_DATE = "";
        public $MEMBER_ID = "";
        public $NAME = "";
        public $DG = "";
        public $DG_TXT = "";
        public $ANREDE = "";
        public $ANREDE_TXT = "";
        public $EMAIL = "";
        public $BLOG_TITLE = "";
        public $BLOG_CONTENT = "";
        public $BLOG_URL = "";
        public $PW_LINK = "";
        public $REC_INFO = "";
        public $REC_INVOICE = "";
        public $REC_BLOG = "";
        public $ZAHLUNG_JAHRESBEITRAG = "";
        public $ZAHLUNG_AUSSTEHEND = "";
        public $ZAHLUNG_TOTAL = "";
        public $ANZ_MITGLIEDER = "";
        public $BREVET_JAHR = "";
        public $BREVET_LISTE = "";
        public $STAB_AKTUELL = "";
        public $STAB_LISTE = "";
        public $JAHR = "";
        public $MONAT = "";
        public $AKT_ADRESSE = "";
        public $AKT_MIL = "";
        public $AKT_BIZ = "";
        public $PDF_PROFIL = "";

        public $description = array(
            "MOD_DATE"=>"Datum der letzten Änderung",
            "MEMBER_ID"=>"Member ID",
            "NAME"=>"Name",
            "DG"=>"Dienstgrad",
            "DG_TXT"=>"Dienstgrad",
            "ANREDE"=>"Anrede",
            "ANREDE_TXT"=>"Anrede lang",
            "EMAIL"=>"E-Mail",
            "VEROEFFENTLICHUNG"=>"Veröffentlichung des Profils",
            "BLOG_TITLE"=>"Blog-Titel",
            "BLOG_CONTENT"=>"Blog-Eintrag",
            "BLOG_URL"=>"Blog-Link",
            "PW_LINK"=>"Passwort-Link",
            "REC_INFO"=>"Info-Adresse",
            "REC_INVOICE"=>"Rechnungsadresse",
            "REC_BLOG"=>"Blog-Adresse",
            "ZAHLUNG_JAHRESBEITRAG"=>"Jahresbeitrag",
            "ZAHLUNG_AUSSTEHEND"=>"Ausstehende Zahlungen Vorjahre",
            "ZAHLUNG_TOTAL"=>"Ausstehende Zahlungen total",
            "ANZ_MITGLIEDER"=>"Anzahl Mitglieder",
            "BREVET_JAHR"=>"Brevetjahr",
            "BREVET_LISTE"=>"Liste der Brevetierten",
            "STAB_AKTUELL"=>"Aktuelle Einteilung",
            "STAB_LISTE"=>"Liste aktuelle Einteilung",
            "JAHR"=>"Aktuelles Jahr",
            "MONAT"=>"Aktueller Monat",
            "AKT_ADRESSE"=>"Aktuelle Adresse",
            "AKT_MIL"=>"Aktueller Eintrag Militär",
            "AKT_BIZ"=>"Aktueller Eintrag Beruf",
            "PDF_PROFIL"=>"PDF Profil"
        );

        function __construct() {
            $this->JAHR = date( "Y" );
            $month = array( "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember" );
            $this->MONAT = $month[date( "n" )-1];
        }

        function replace( $text ) {
            foreach( $this as $var=>$value ) {
                $text = str_replace( "[$var]", $value, $text );
            }
            return $text;
        }

        function output() {
            foreach( $this->description as $key=>$value ) {
                echo "[$key]: " . translate( $value ) . "<br />";
            }
        }

    }
?>