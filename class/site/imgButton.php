<?php
    class imgButton {

        public $class    = "imgButton";
        public $href     = "#";
        public $style    = "";
        public $id       = NULL;
        public $imgclass = "imgButtonImg";
        public $src      = "image/dummy.gif";

        public function output( $override=array() ) {
            foreach( $this as $key=>$value ) {
                $$key = $value;
            }
            foreach( $override as $key=>$value ) {
                $$key = $value;
            }
            $id = $id ? $id : uniqid( "img_" );
            $alt = defined('alt') ? translate( $alt ) : "";
            $title = defined('title') ? translate( $title ) : "";
            $caption = defined('caption') ? " " . translate( $caption ) : "";

            if(!defined('onclick')){
                $onclick = '';
            }

            echo "<a class=\"$class\" href=\"$href\" style=\"$style\" onclick=\"this.blur();$onclick\"><img id=\"$id\" class=\"$imgclass\" src=\"$src\" alt=\"$alt\" title=\"$title\" />$caption</a>";
        }

    }

    class imgAddButton extends imgButton {

        public $class = "addButton";
        public $src   = "image/template/add.png";
        public $alt   = "Eintrag hinzufügen";
        public $title = "Eintrag hinzufügen";

    }

    class imgDeleteButton extends imgButton {

        public $class = "deleteButton";
        public $src   = "image/template/delete.png";
        public $alt   = "Löschen";
        public $title = "Löschen";

        function __construct() {
            $this->onclick = "noScrollingToTop(); return confirm( '" . translate( "Löschen" ) . "?' );";
        }

    }

    class imgEditButton extends imgButton {

        public $class = "editButton";
        public $src   = "image/template/edit.gif";
        public $alt   = "Bearbeiten";
        public $title = "Bearbeiten";

    }
?>