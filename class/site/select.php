<?php
    class select extends _select {

        protected function init() {
            $this->lang = $_SESSION["lang"];
        }

        protected function translate( $str ) {
            return translate( $str, $this->lang );
        }

    }
?>