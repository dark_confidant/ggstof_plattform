<?php
// Juri Sarbach 2011

    class _select {

        public $class    = "";
        public $emptyrow = false;
        public $id       = NULL;
        public $onchange = "";
        public $style    = "";

        function __construct( $name=NULL, $linkTable=NULL, $selected=1, $options=array() ) {
            $this->name      = $name;
            $this->linkTable = $linkTable;
            $this->selected  = $selected;
            foreach( $options as $key=>$value ) {
                $this->$key = $value;
            }
            $this->options = array();
            if( isset( $linkTable ) ) {
                $this->query( "SELECT * FROM $linkTable" );
            }
            $this->init();
        }

        public function addOption( $option, $value=NULL ) {
            if( isset( $value ) ) {
                $this->options["$value"] = $option;
            } else {
                $this->options[] = $option;
            }
        }

        protected function init() {}

        public function output( $override=array() ) {
            foreach( $override as $key=>$value ) {
                $this->$key = $value;
            }
            foreach( $this as $key=>$value ) {
                $$key = $value;
            }
            $id = $id ? $id : uniqid( "select" );
            $name = $name ? $name : $id;
            echo "<select id=\"$id\" name=\"$name\" class=\"$class\" style=\"$style\" onchange=\"$onchange\">\n";
            if( $emptyrow ) {
                echo "<option value=\"0\"";
                if( $selected == 0 ) {
                    echo " selected=\"selected\"";
                }
                echo "></option>\n";
            }
            foreach( $options as $key=>$value ) {
                echo "<option value=\"$key\"";
                if( $selected == $key ) {
                    echo " selected=\"selected\"";
                }
                echo ">" . $this->translate( $value ) . "</option>\n";
            }
            echo "</select>\n";
        }

        public function query( $query ) {
            $conn = dbconn::open();
            $result = $conn->query($query);

            dbconn::close( $conn );

            while( $row = $result->fetch(PDO::FETCH_NUM) ) {
                $this->options["$row[0]"] = $row[1];
            }
        }

        protected function translate( $str ) {
            return $str;
        }

    }
?>