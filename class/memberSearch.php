<?php
    class memberSearch {

        function __construct( $criteria=NULL, $fields=NULL, $order=NULL, $group=NULL, $hist=NULL ) {
            $this->include = array();
            if( isset( $criteria ) ) {
                $this->addInclude( $criteria );
            }
            $this->exclude = array();

            $this->select = array();
            $this->addFields( array( "tblPersonen"=>array( "personID", "name", "vorname", "vorname2", "ort", "spracheID" ), "qryDienstgrad"=>array( "*" ), "linkPersonSprache"=>array( "sprache" ) ) );
            if( isset( $fields ) ) {
                $this->addFields( $fields );
                $linktables = array( "linkPersonAktualisierung"=>"aktualisierungID", "linkPersonAnrede"=>"anredeID", "linkPersonChannel"=>"channelID", "linkPersonChannelPost"=>"postChannelID", "linkPersonChannelMail"=>"mailChannelID", "linkBlogChannel"=>"blogChannelID", "linkZahlungenChannel"=>"paymentChannelID", "linkMilitaerRS"=>"rsID", "linkMilitaerTruppengattung"=>"truppengattungID" );
                if( count( $inter = array_intersect_key( $linktables, $fields ) ) ) {
                    $this->addFields( array( "tblPersonen"=>array_values( $inter ) ) );
                }
            }

            if( isset( $order ) ) {
                $this->order = $order;
            } else {
                $this->order = "tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
            }

            if( isset( $group ) ) {
                $this->group = $group;
            } else {
                $this->group = "tblPersonen.personID";
            }

            if( isset( $hist ) ) {
                $this->hist = $hist;
            } else {
                $this->hist = true;
            }
        }

        function addInclude( $criteria ) {
            $this->include[] = $criteria;
        }

        function addExclude( $criteria ) {
            $this->exclude[] = $criteria;
        }

        function addFields( $fields ) {
            if( is_array( $fields) && count( $fields ) ) {
                $keys = array_keys( array_intersect_key( $this->select, $fields ) );
                foreach( $keys as $key ) {
                    $this->select[$key] = array_unique( array_merge( $this->select[$key], $fields[$key] ) );
                }
                $this->select = array_merge( $this->select, array_diff_key( $fields, $this->select ) );
            }
        }

        function buildQuery() {
            $tables = $this->select;
            foreach( $this->include as $criteria ) {
                $keys = array_keys( array_intersect_key( $criteria, $tables ) );
                foreach( $keys as $key ) {
                    $tables[$key] = array_unique( array_merge( array_keys( $criteria[$key] ), array_values( $tables[$key] ) ) );
                }
                $keys = array_keys( array_diff_key( $criteria, $tables ) );
                foreach( $keys as $key ) {
                    $tables[$key] = array_keys( $criteria[$key] );
                }
                $keys = array_keys( array_diff_key( $tables, $criteria ) );
                foreach( $keys as $key ) {
                    $tables[$key] = array_values( $tables[$key] );
                }
            }
            foreach( $this->exclude as $criteria ) {
                $keys = array_keys( array_intersect_key( $criteria, $tables ) );
                foreach( $keys as $key ) {
                    $tables[$key] = array_unique( array_merge( array_keys( $criteria[$key] ), array_values( $tables[$key] ) ) );
                }
                $keys = array_keys( array_diff_key( $criteria, $tables ) );
                foreach( $keys as $key ) {
                    $tables[$key] = array_keys( $criteria[$key] );
                }
                $keys = array_keys( array_diff_key( $tables, $criteria ) );
                foreach( $keys as $key ) {
                    $tables[$key] = array_values( $tables[$key] );
                }
            }

            $this->query = "SELECT ";
            foreach( $this->select as $table=>$columns ) {
                foreach( $columns as $column ) {
                    $this->query .= "$table.$column,";
                }
            }
            $this->query = substr( $this->query, 0, -1 );
            $this->query .= "
                    FROM tblPersonen";
            if( array_key_exists( "linkPersonSprache", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID";
            }
            if( array_key_exists( "linkPersonVeroeffentlichung", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkPersonVeroeffentlichung ON tblPersonen.veroeffentlichungID=linkPersonVeroeffentlichung.veroeffentlichungID";
            }
            if( array_key_exists( "linkPersonAktualisierung", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkPersonAktualisierung ON tblPersonen.aktualisierungID=linkPersonAktualisierung.linkAktualisierungID";
            }
            if( array_key_exists( "linkPersonAnrede", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkPersonAnrede ON tblPersonen.anredeID=linkPersonAnrede.linkAnredeID";
            }
            if( array_key_exists( "linkPersonChannel", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkPersonChannel ON tblPersonen.channelID=linkPersonChannel.linkChannelID";
            }
            if( array_key_exists( "linkPersonChannelPost", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkPersonChannelPost ON tblPersonen.mailChannelID=linkPersonChannelPost.linkChannelID";
            }
            if( array_key_exists( "linkPersonChannelMail", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkPersonChannelMail ON tblPersonen.postChannelID=linkPersonChannelMail.linkChannelID";
            }
            if( array_key_exists( "linkBlogChannel", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkBlogChannel ON tblPersonen.blogChannelID=linkBlogChannel.linkChannelID";
            }
            if( array_key_exists( "linkZahlungenChannel", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkZahlungenChannel ON tblPersonen.paymentChannelID=linkZahlungenChannel.linkChannelID";
            }
            if( array_key_exists( "linkMilitaerRS", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkMilitaerRS ON tblPersonen.rsID=linkMilitaerRS.linkRsID";
            }
            if( array_key_exists( "linkMilitaerTruppengattung", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN linkMilitaerTruppengattung ON tblPersonen.truppengattungID=linkMilitaerTruppengattung.linkTruppengattungID";
            }
            if( array_key_exists( "qryDienstgrad", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN (
                        SELECT personID,dienstgradID,dienstgrad,dienstgradText,zusatzDgID,zusatzDg.zusatz AS zusatzDg
                        FROM tblMilitaer
                        LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                        LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                        WHERE datumBeginn<=CURDATE()" . ( in_array( "dienstgrad", $tables["qryDienstgrad"] ) ? " AND datumEnde>=CURDATE()" : "" ) . "
                        ORDER BY datumBeginn DESC
                    ) AS qryDienstgrad ON tblPersonen.personID=qryDienstgrad.personID";
            }
            if( array_key_exists( "qryMilitaer", $tables ) !== false ) {
                $columns = array_unique( array_merge( array( "personID", "militaerID", "dienstgradID", "zusatzDgID", "zusatzFktID" ), $tables["qryMilitaer"] ) );
                $this->query .= "
                    LEFT JOIN (
                        SELECT " . preg_replace( "/\bzusatzDg\b/", "zusatzDg.zusatz AS zusatzDg", preg_replace( "/\bzusatzFkt\b/", "zusatzFkt.zusatz AS zusatzFkt", implode( ",", $columns ) ) ) . "
                        FROM tblMilitaer
                        LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                        LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                        LEFT JOIN linkMilitaerZusatz AS zusatzFkt ON tblMilitaer.zusatzFktID=zusatzFkt.linkZusatzID
                        WHERE datumBeginn<=CURDATE()" . ( $this->hist ? "" : " AND datumEnde>=CURDATE()" ) . "
                        ORDER BY datumEnde DESC,datumBeginn DESC
                    ) AS qryMilitaer ON tblPersonen.personID=qryMilitaer.personID";
            }
            if( array_key_exists( "qryBrevet", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN (
                        SELECT personID,MIN(datumBeginn) AS brevetierungsdatum
                        FROM tblMilitaer
                        WHERE dienstgradID BETWEEN 1 AND 2
                        GROUP BY personID
                    ) AS qryBrevet ON tblPersonen.personID=qryBrevet.personID";
            }
            if( array_key_exists( "qryBeruf", $tables ) !== false ) {
                $columns = array_unique( array_merge( array( "personID", "berufID", "positionID", "typID" ), $tables["qryBeruf"] ) );
                $this->query .= "
                    LEFT JOIN (
                        SELECT " . implode( ",", $columns ) . "
                        FROM tblBeruf
                        LEFT JOIN linkBerufPosition ON tblBeruf.positionID=linkBerufPosition.linkPositionID
                        LEFT JOIN linkBerufTyp ON tblBeruf.typID=linkBerufTyp.linkTypID
                        WHERE datumBeginn<=CURDATE()" . ( $this->hist ? "" : " AND datumEnde>=CURDATE()" ) . "
                        ORDER BY email DESC,datumEnde DESC,datumBeginn DESC
                    ) AS qryBeruf ON tblPersonen.personID=qryBeruf.personID";
            }
            if( array_key_exists( "qryBerufAusbildung", $tables ) !== false ) {
                $columns = array_unique( array_merge( array( "personID", "ausbildungID", "typID" ), $tables["qryBerufAusbildung"] ) );
                $this->query .= "
                    LEFT JOIN (
                        SELECT " . preg_replace( "/\babschlussjahr\b/", "YEAR(datumEnde) AS abschlussjahr", implode( ",", $columns ) ) . "
                        FROM tblBerufAusbildung
                        LEFT JOIN linkBerufAusbildungTyp ON tblBerufAusbildung.typID=linkBerufAusbildungTyp.linkTypID
                        WHERE datumEnde<=CURDATE()
                        ORDER BY datumEnde DESC
                    ) AS qryBerufAusbildung ON tblPersonen.personID=qryBerufAusbildung.personID";
            }
            if( array_key_exists( "tblPublic", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN tblPublic ON tblPersonen.personID=tblPublic.personID";
            }
            if( array_key_exists( "qryVerein", $tables ) !== false ) {
                $columns = array_unique( array_merge( array( "personID", "vereinID", "funktionID", "organisationRegionID" ), $tables["qryVerein"] ) );
                $this->query .= "
                    LEFT JOIN (
                        SELECT " . implode( ",", $columns ) . "
                        FROM tblVerein
                        LEFT JOIN linkVereinFunktion ON tblVerein.funktionID=linkVereinFunktion.linkVereinFunktionID
                        LEFT JOIN linkVereinOrganisation ON tblVerein.organisationRegionID=linkVereinOrganisation.linkOrganisationID
                        WHERE datumBeginn<=CURDATE()" . ( $this->hist ? "" : " AND datumEnde>=CURDATE()" ) . "
                        ORDER BY datumEnde DESC,datumBeginn DESC
                    ) AS qryVerein ON tblPersonen.personID=qryVerein.personID";
            }
            if( array_key_exists( "qryBeitritt", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN (
                        SELECT personID,MIN(datumBeginn) AS beitrittsdatum
                        FROM tblVerein
                        WHERE funktionID BETWEEN 1 AND 13 AND organisationRegionID=1
                        GROUP BY personID
                    ) AS qryBeitritt ON tblPersonen.personID=qryBeitritt.personID";
            }
            if( array_key_exists( "qryAustritt", $tables ) !== false ) {
                $this->query .= "
                    LEFT JOIN (
                        SELECT personID,MAX(datumEnde) AS austrittsdatum
                        FROM tblVerein
                        WHERE funktionID BETWEEN 1 AND 13 AND organisationRegionID=1
                        GROUP BY personID
                    ) AS qryAustritt ON tblPersonen.personID=qryAustritt.personID";
            }
            if( array_key_exists( "qryZahlungen", $tables ) !== false ) {
                $columns = array_unique( array_merge( array( "personID", "zahlungID", "paymentChannelID" ), $tables["qryZahlungen"] ) );
                $this->query .= "
                    LEFT JOIN (
                        SELECT " . implode( ",", $columns ) . "
                        FROM tblZahlungen
                        LEFT JOIN linkZahlungenChannel ON tblZahlungen.paymentChannelID=linkZahlungenChannel.linkChannelID
                        ORDER BY datumRechnung DESC
                    ) AS qryZahlungen ON tblPersonen.personID=qryZahlungen.personID";
            }
//             if( array_key_exists( "qryLogin", $tables ) !== false ) {
//                 $this->query .= "
//                     LEFT JOIN (
//                         SELECT userID,max(timestamp) AS login
//                         FROM sysLog
//                         WHERE typID=1 AND success=1
//                         GROUP BY userID
//                     ) AS qryLogin ON tblPersonen.email=qryLogin.userID";
//             }
            $this->query .= "
                    WHERE ";
            foreach( $this->include as $criteria ) {
                $this->query .= "(";
                foreach( $criteria as $table=>$columns ) {
                    foreach( $columns as $column=>$value ) {
                        if( is_array( $value ) ) {
                            $this->query .= "$table.$column BETWEEN '$value[0]' AND '$value[1]'";
                        } else {
                            if( strpos( $value, "NULL" ) !== false ) {
                                $this->query .= "$table.$column IS NULL";
                            } else {
                                $value = str_replace( array( "?", "*" ), array( "_", "%" ), addslashes( $value ) );
                                $op = ( strpos( $value, "%" ) !== false || strpos( $value, "_" ) !== false ) ? " LIKE " : "=";

                                $this->query .= "$table.$column$op'$value'";
                            }
                        }
                        $this->query .= " AND ";
                    }
                }
                $this->query = substr( $this->query, 0, -4 ) . ") OR ";
            }
            $this->query = substr( $this->query, 0, -3 );
            if( count( $this->exclude )  ) {
                $this->query .= "AND (";
                foreach( $this->exclude as $criteria ) {
                    $this->query .= "(";
                    foreach( $criteria as $table=>$columns ) {
                        foreach( $columns as $column=>$value ) {
                            if( is_array( $value ) ) {
                                $this->query .= "$table.$column NOT BETWEEN '$value[0]' AND '$value[1]'";
                            } else {
                                if( strpos( $value, "NULL" ) !== false ) {
                                    $this->query .= "$table.$column IS NOT NULL";
                                } else {
                                    $value = str_replace( array( "?", "*" ), array( "_", "%" ), addslashes( $value ) );
                                    $op = ( strpos( $value, "%" ) !== false || strpos( $value, "_" ) !== false ) ? " NOT LIKE " : "<>";

                                    $this->query .= "$table.$column$op'$value'";
                                }
                            }
                            $this->query .= " AND ";
                        }
                    }
                    $this->query = substr( $this->query, 0, -4 ) . ") OR ";
                }
                $this->query = substr( $this->query, 0, -3 ) . ") ";
            }
            $this->query .= "
                    GROUP BY {$this->group}
                    ORDER BY {$this->order}";
        }

        function execute( $count=false ) {
            if(!defined('query')){
                $query = '';
            }
            $this->buildQuery();
            $conn = dbconn::open();
            $result = $conn->query($query); // mysql_query( $this->query, $conn );

            dbconn::close( $conn );
            if( $count ) {
                return mysql_num_rows( $result );
            }
            $retval = array();
            while( $row = mysql_fetch_alias_assoc( $result ) ) {
                $retval[] = $row;
            }
            return is_array( $retval ) ? $retval : array();
        }

        function printQuery() {
            $this->buildQuery();
            echo "{$this->query}<br />";
        }

        function count() {
            return $this->execute( true );
        }

    }
?>