<?php
    class email {

        public $lang;

        function __construct( $id, $recipient=NULL, $channelID=NULL ) {
            require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/contentManagement.php" );
            require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/lang.php" );

            $this->id = $id;
            foreach( array( "de", "fr", "it" ) as $lang ) {
                $content = getEmailContent( $this->id, $lang );

                $this->subject[$lang] = $content["subject"];
                $this->body[$lang] = $content["body"];
            }
            $this->subject["fr"] = $this->subject["fr"] ? $this->subject["fr"] : $this->subject["de"];
            $this->body["fr"] = $this->body["fr"] ? $this->body["fr"] : $this->body["de"];
            $this->subject["it"] = $this->subject["it"] ? $this->subject["it"] : $this->subject["de"];
            $this->body["it"] = $this->body["it"] ? $this->body["it"] : $this->body["de"];

            $sender = new person( $_SESSION["personID"] );
            if( $content["absenderID"] == 1 ) {
                $this->senderEmail = $sender->person->email;
                $this->senderName = "{$sender->person->vorname} {$sender->person->name}";
            } else {
                $this->senderEmail = $content["absender"];
                $this->senderName = $content["absenderText"];
            }

            $this->recipients = array();
            if( isset( $recipient ) ) {
                if( is_array( $recipient ) ) {
                    foreach( $recipient as $r ) {
                        $this->addRecipient( $r, $channelID );
                    }
                } else {
                    $this->addRecipient( $recipient == 0 ? $sender->id : $recipient, $channelID );
                }
            }
            $this->cc = array();
            $this->bcc = array();
            $this->attachments = array();
            $this->boundary = md5( time() );
        }

        function addRecipient( $id, $channelID=NULL ) {
            $i = count( $this->recipients );
            $this->recipients[$i] = new person( $id );
//             $this->recipients[$i]->getMilitaryData();
//             $this->recipients[$i]->getJobData();
            $this->recipients[$i]->channelID = $channelID;
        }

        function addCC( $email ) {
            $this->cc[] = $email;
//             $this->recipients[$i] = new person( $id );
//             $this->recipients[$i]->getMilitaryData();
//             $this->recipients[$i]->getJobData();
//             $this->recipients[$i]->channelID = $channelID;
        }

//         function addBCC( $email ) {
//             $i = count( $this->bcc );
//             $this->bcc[$i] = $email;
//         }

        function addAttachment( &$file, $filename=NULL, $type=NULL ) {
            $i = count( $this->attachments );
            if( is_string( $file ) ) {
                $this->attachments[$i]["file"] = $file;
                $this->attachments[$i]["filename"] = isset( $filename ) ? $filename : basename( $file );
                $this->attachments[$i]["contentType"] = isset( $type ) ? $type : mime_content_type( $file );
            } else {
                $this->attachments[$i]["pointer"] = $file;
                $this->attachments[$i]["filename"] = isset( $filename ) ? $filename : $file->filename;
                $this->attachments[$i]["contentType"] = isset( $type ) ? $type : $file->contentType;
            }
        }

        function deleteAttachments() {
            $this->attachments = array();
        }

        function send( $param=NULL ) {
            require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/log.php" );

            $this->vars = new variableSet();

            if( strpos( $this->body["de"], "[BLOG_" ) !== false ) {
                require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/blog.php" );
                $blog = $param["blogID"] ? blogEntry( $param["blogID"] ) : blogLastEntries();
                $blog_title = $blog[0]["post_title"];
                $blog_content = strip_tags( $blog[0]["post_content"] );
                $blog_content = strlen( $blog_content ) > 500 ? substr( $blog_content, 0, 500 ) . "..." : $blog_content;
                $blog_guid = $blog[0]["guid"];

                $this->vars->BLOG_TITLE = $blog_title;
                $this->vars->BLOG_CONTENT = $blog_content;
                $this->vars->BLOG_URL = $blog_guid;
            } else if( strpos( $this->body["de"], "[BREVET_" ) !== false ) {
                require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/list.php" );
                $param = $param ? $param : substr( $this->recipients[0]->person->brevetierungsjahr, 0, 4 );
                $this->vars->BREVET_JAHR = $param;
                $result = memberList( 24, $param );
                for( $i=0; $i<count( $result ); $i++ ) {
                    if( $result[$i]["tblPersonen.veroeffentlichungID"] ) {
                        $this->vars->BREVET_LISTE .= ( $result[$i]["qryVerein.funktionID"] <= 13 ) ? strip_spaces( translate( $result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false ) . " " . translate( $result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false ) ) . " {$result[$i][".fullName"]}, {$result[$i]["qryMilitaer.funktion"]} {$result[$i]["qryMilitaer.zusatzFkt"]}\n" : "";
                    }
                }
//                 for( $i=0; $i<count( $this->recipients ); $i++ ) {
//                     $this->vars->BREVET_LISTE .= strip_spaces( translate( $this->recipients[$i]->current->militaer->dienstgrad, $this->recipients[$i]->person->sprache, false, false ) . " " . translate( $this->recipients[$i]->current->militaer->zusatzDg, $this->recipients[$i]->person->sprache, false, false ) ) . " {$this->recipients[$i]->person->fullName}\n";
//                 }
            } else if( strpos( $this->body["de"], "[STAB_" ) !== false ) {
                require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/list.php" );
                $this->recipients[0]->getMilitaryData();
                $param = $param ? $param : $this->recipients[0]->current->militaer->einteilung;
                $this->vars->STAB_AKTUELL = $param;
                $result = memberList( 46, $param );
                for( $i=0; $i<count( $result ); $i++ ) {
                    if( $result[$i]["tblPersonen.veroeffentlichungID"] ) {
                        $this->vars->STAB_LISTE .= ( $result[$i]["qryVerein.funktionID"] <= 13 ) ? strip_spaces( translate( $result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false ) . " " . translate( $result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false ) ) . " {$result[$i][".fullName"]}, {$result[$i]["qryMilitaer.funktion"]} {$result[$i]["qryMilitaer.zusatzFkt"]}" . ( preg_match( "/^[kc]dt/i", $result[$i]["qryMilitaer.funktion"] ) ? " {$result[$i]["qryMilitaer.einteilung"]}" : "" ) . "\n" : "";
                    }
                }
            }
            if( strpos( $this->body["de"], "[ANZ_MITGLIEDER]" ) !== false ) {
                $conn = dbconn::open();
                $query = "SELECT COUNT(DISTINCT personID) FROM tblVerein WHERE organisationRegionID=1 AND funktionID=1 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()";
                $result = $conn->query($query);
                $row = $result->fetch(PDO::FETCH_NUM);
                $this->vars->ANZ_MITGLIEDER = $row[0];
                dbconn::close( $conn );
            }

//             $n = 0;
            $success = 0;
            $failure = array();
            $n = count( $this->recipients );
// die( "$n" );
            for( $i=0; $i<$n; $i++ ) {
                $recipient = $n > 1 ? array_shift( $this->recipients ) : $this->recipients[0];
                $recipient->getMilitaryData();
                $recipient->getJobData();

                $lang = $this->lang ? $this->lang : $recipient->person->sprache;

                if( strpos( $this->body[$lang], "[BLOG_" ) !== false ) {
                    $channelID = isset( $recipient->channelID ) ? $recipient->channelID : $recipient->person->blogChannelID;
                    $email = ( $channelID == 3 ) ? $recipient->current->beruf->email : $recipient->person->email;
                } else if( strpos( $this->body[$lang], "[ZAHLUNG_" ) !== false ) {
                    $conn = dbconn::open();
                    $query = "SELECT betrag
                              FROM tblZahlungenJahresbeitrag
                              WHERE jahr=" . date( "Y" );
                    $result = $conn->query($query);

                    dbconn::close( $conn );
                    $row = $result->fetch(PDO::FETCH_NUM);
                    if( $row[0] ) {
                        $this->vars->ZAHLUNG_JAHRESBEITRAG = "CHF $row[0]";
                    }

                    $conn = dbconn::open();
                    $query = "SELECT SUM(betrag) AS summe
                              FROM tblZahlungen
                              WHERE personID={$recipient->id} AND typID=1 AND jahr<" . date( "Y" ) . " AND datumEinzahlung='0000-00-00'";
                    $result = $conn->query($query);

                    dbconn::close( $conn );
                    $row = $result->fetch(PDO::FETCH_NUM);
                    if( $row[0] ) {
//                         $this->vars->ZAHLUNG_AUSSTEHEND = translate( 334, $adressat[$i]["linkPersonSprache.sprache"], false, false ) . " CHF $row[0].";
                        $this->vars->ZAHLUNG_AUSSTEHEND = translate( 334, $lang, false, false ) . " CHF $row[0].";
                    }

                    $conn = dbconn::open();
                    $query = "SELECT SUM(betrag) AS summe
                              FROM tblZahlungen
                              WHERE personID={$recipient->id} AND typID=1 AND jahr<=" . date( "Y" ) . " AND datumEinzahlung='0000-00-00'";
                    $result = $conn->query($query);

                    dbconn::close( $conn );
                    $row = $result->fetch(PDO::FETCH_NUM);
                    if( $row[0] ) {
                        $this->vars->ZAHLUNG_TOTAL = "CHF $row[0]";
                    }

                    $channelID = isset( $recipient->channelID ) ? $recipient->channelID : $recipient->person->paymentChannelID;
                    $email = ( $channelID == 1 ) ? $recipient->current->beruf->email : $recipient->person->email;
                } else {
                    $channelID = isset( $recipient->channelID ) ? $recipient->channelID : $recipient->person->channelID;
                    $email = ( $channelID == 100 ) ? $recipient->current->beruf->email : $recipient->person->email;
                }

                if( strpos( $this->body[$lang], "[AKT_ADRESSE]" ) !== false ) {
                    $this->vars->AKT_ADRESSE = "{$recipient->person->adresse}\n{$recipient->person->postfach}\n{$recipient->person->plz} {$recipient->person->ort}\n{$recipient->person->land}";
                    $this->vars->AKT_ADRESSE = str_replace( "\n\n", "\n", $this->vars->AKT_ADRESSE );
                }

                if( strpos( $this->body[$lang], "[PW_LINK]" ) !== false ) {
                    require_once( "{$_SERVER["DOCUMENT_ROOT"]}/function/userManagement.php" );
                    $hash = $param["hash"] ? $param["hash"] : reset_password( $recipient->person->email, 60*60*24 );
                    $this->vars->PW_LINK = "http://{$_SERVER["SERVER_NAME"]}/password.php?$hash";
                    $email = $recipient->person->email;
                }

                if( strpos( $this->body[$lang], "[PDF_PROFIL]" ) !== false ) {
//                 if( strpos( $this->body["de"], "[PDF_PROFIL]" ) !== false ) {
                    require_once( "{$_SERVER["DOCUMENT_ROOT"]}/class/pdfMemberProfile.php" );
                    require_once( "{$_SERVER["DOCUMENT_ROOT"]}/function/export.php" );

                    $recipient->getEducationData();
                    $recipient->getPublicData();
                    $recipient->getSocietyData();

                    $pdf = new pdfMemberProfile( "L", "mm", "A4" );
                    $pdf->Init();
                    personPDF( $pdf, $recipient );

//                     $this->addAttachment( &$pdf, "{$recipient->person->vorname}{$recipient->person->vorname2}{$recipient->person->name}.pdf", "application/pdf" );
                    $this->addAttachment( $pdf, "{$recipient->person->vorname}{$recipient->person->vorname2}{$recipient->person->name}.pdf", "application/pdf" );
                    $this->vars->PDF_PROFIL = "";
                }

                $this->vars->MEMBER_ID = $recipient->id;
                $this->vars->NAME = $recipient->person->name;
                $this->vars->VEROEFFENTLICHUNG = translate( $recipient->person->veroeffentlichung, $lang, false, false );
                $this->vars->DG = strip_spaces( translate( $recipient->current->militaer->dienstgrad, $lang, false, false ) . " " . translate( $recipient->current->militaer->zusatzDg, $lang, false, false ) );
                $this->vars->DG_TXT = translate( $recipient->current->militaer->dienstgradText, $lang, false, false );
                $this->vars->ANREDE = translate( $recipient->person->anrede, $lang, false, false ) . " " . translate( $recipient->current->militaer->dienstgradText, $lang, false, false );
                $this->vars->ANREDE_TXT = translate( $recipient->person->anredeText, $lang, false, false );
                $this->vars->EMAIL = $recipient->person->email;
                $this->vars->MOD_DATE = substr( max( $recipient->modDateUser, $recipient->modDateAdmin ), 0, 10 );
                $this->vars->REC_INFO = translate( $recipient->person->channel, $lang, false, false );
                $this->vars->REC_INVOICE = translate( $recipient->person->paymentChannel, $lang, false, false );
                $this->vars->REC_BLOG = translate( $recipient->person->blogChannel, $lang, false, false );
                $this->vars->AKT_MIL = translate( "seit", $lang, false, false ) . " {$recipient->current->militaer->datumBeginn}\n{$recipient->current->militaer->funktion} {$recipient->current->militaer->zusatzFkt}\n{$recipient->current->militaer->einteilung}\n{$this->vars->DG}\n{$recipient->current->militaer->bemerkungen}";
                $this->vars->AKT_BIZ = translate( "seit", $lang, false, false ) . " {$recipient->current->beruf->datumBeginn}\n{$recipient->current->beruf->funktion}, {$recipient->current->beruf->abteilung}\n{$recipient->current->beruf->organisation}\n" . translate( $recipient->current->beruf->position, $lang, false, false ) . ", " . translate( $recipient->current->beruf->typ, $lang, false, false ) . "\n{$recipient->current->beruf->ort}\n{$recipient->current->beruf->email}";
                $this->vars->MONAT = translate( $this->vars->MONAT, $lang, false, false );

                $subject = $this->vars->replace( $this->subject[$lang] ) ;
                $body    = $this->vars->replace( $this->body[$lang] ) ;

                if( empty( $email ) ) {
                    $failure[] = $recipient->person->fullName;
                } else {
// die( $email );
                    $email = ( $_SERVER["SERVER_NAME"] == "ggstof" ) ? "juri@localhost" : $email;

                    $headers = "From: {$this->senderName} <{$this->senderEmail}>\r\n";
                    if( count( $this->cc ) ) {
                        $headers .= "Cc: ";
                        foreach( $this->cc as $cc ) {
                            $headers .= ( $_SERVER["SERVER_NAME"] == "ggstof" ) ? "juri@localhost, " : "$cc, ";
                        }
                        $headers = substr( $headers, 0, -2 ) . "\r\n";
                    }
                    $headers .= "MIME-Version: 1.0\r\nContent-type: multipart/mixed; boundary=\"{$this->boundary}\"\r\nX-Mailer: PHP/" . phpversion();
//                     $body = "--{$this->boundary}\nContent-Type: text/plain; charset=iso-8859-1\n\n$body";
                    $body = "--{$this->boundary}\nContent-Type: text/plain; charset=utf-8\n\n$body";
                    for( $j=0; $j<count( $this->attachments ); $j++ ) {
                        $body .= "\n\n--{$this->boundary}";
                        $body .= "\nContent-Type: {$this->attachments[$j]["contentType"]}; name=\"{$this->attachments[$j]["filename"]}\"\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment; filename=\"{$this->attachments[$j]["filename"]}\"\n\n";
                        if( $this->attachments[$j]["file"] ) {
                        } else {
                            $body .= chunk_split( base64_encode( $this->attachments[$j]["pointer"]->Output( "", "S" ) ) );
                        }
                    }
                    $body .= "\n\n--{$this->boundary}--\n";
// die( $body );

                    $sent = mail( $email, $subject, $body, $headers, "-f{$this->senderEmail}" );
                    logEmail( $recipient->person->email, $sent, $this->id, utf8_decode( $subject ) );
                    if( $sent ) {
                        $success++;
                        if( in_array( $this->id, array( 1, 10, 14, 24, 32, 46 ) ) ) {
                            $conn = dbconn::open();
                            $query = "UPDATE tblPersonen SET remindDate=CURRENT_TIMESTAMP() WHERE personID={$recipient->id}";
                            $conn->query($query);

                            dbconn::close( $conn );
                        }
                    } else {
                        $failure[] = "{$recipient->person->fullName} <$email>";
                    }
                }

                if( strpos( $this->body[$lang], "[PDF_PROFIL]" ) !== false ) {
                    $this->deleteAttachments();
                }
//                 $n++;
            }

            $feedback = "$success " . translate( "von", false, false, false ) . " $n " . translate( "Mails wurden erfolgreich verschickt", false, false, false ) . ".";
            if( $f = count( $failure ) ) {
                $feedback .= "\n\n" . translate( "Bei folgenden Adressaten ist ein Fehler aufgetreten", false, false, false ) . ":";
                for( $i=0; $i<$f; $i++ ) {
                    $feedback .= "\n" . $failure[$i];
                }
            }

            return array( ( $n ? $success/$n : 0 ), $feedback );
        }

    }
?>