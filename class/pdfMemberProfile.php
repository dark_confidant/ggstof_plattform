<?php
    class pdfMemberProfile extends FPDF {

        function Init() {
            $this->x0 = 15;
            $this->y0 = 5;
            $this->y1 = 18;
            $this->lineHeight = 4;
            $this->colWidth = (297 - 4 * $this->x0) / 3;

            $this->SetMargins( $this->x0, $this->y0 );
            $this->SetTextColor( 0 );
        }

        function Header() {
            $this->SetXY( $this->x0, $this->y0 );
            $this->SetFont( "Helvetica", "B", 10 );
            $this->Cell( 0, $this->lineHeight, translate( "Erfasste Informationen auf der Plattform der GGstOf", false, false, false ) );

            $this->SetX( -($this->x0) );
            $this->Cell( 0, $this->lineHeight, date( "d.m.Y H:i" ), 0, 0, "R" );

            $this->Line( $this->x0, 12, 297-($this->x0), 12 );
        }

        function Footer() {
            $this->SetXY( -($this->x0), -8 );
            $this->SetFont( "Helvetica", "", 10 );
            $this->Cell( 0, $this->lineHeight, translate( "bitte wenden", false, false, false ), 0, 0, "R" );
        }

        function SetCol( $col ) {
            $x = $this->x0 + $col * ($this->x0 + $this->colWidth);
            $this->SetX( $x );
            $this->SetLeftMargin( $x );
            $this->SetY( $this->y1 );
            $this->col = $col;
        }

        function NewCol( $colMax ) {
            if( $this->GetY() > 210-15-6*$this->lineHeight && $this->col < $colMax ) {
                $this->SetCol( $this->col+1 );
            }
            return $this->col <= $colMax;
        }

        function AcceptPageBreak() {
            return false;
        }

        function MakeCell( $type, $condition, $true, $false ) {
            if( $condition ) {
                $text = $true;
            } else {
                $text = $false;
                $this->SetTextColor( 127 );
            }
            $this->$type( $this->colWidth, $this->lineHeight, $text, 0, 1 );
            $this->SetTextColor( 0 );
        }

    }
?>