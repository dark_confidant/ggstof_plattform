<?php
    class pdfLetterPaymentLastCall extends pdfLetterInvoice {

        public $contentID = 54;

        function Template() {
            $conn = dbconn::open();
            $query = "SELECT personID
                      FROM tblVerein
                      WHERE funktionID=2 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()";
            $result = $conn->query($query);

            dbconn::close( $conn );
            $row = $result->fetch(PDO::FETCH_NUM);
            $sender = new person( $row[0] );
            $sender->getMilitaryData();
            $this->sender = str_replace( "  ", " ", translate( $sender->current->militaer->dienstgrad, $sender->person->sprache ) . " " . translate( $sender->current->militaer->zusatzDg, $sender->person->sprache ) . " " . $sender->person->fullName );
            $this->senderFunction = "Präsident";
            $this->senderFunction2 = "Der Präsident";
            $this->senderEmail = "praesident@ggstof.ch";
        }

    }
?>