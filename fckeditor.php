<?php

    require_once('head.php');
    ggstof_head(true);

    require_once( "module/fckeditor/fckeditor.php" );

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }



    if( !$_SESSION["rights"]["editContent"] && $_SESSION["rights"]["roleID"] != 4 ) {
        die( translate( "Keine Berechtigung." ) );
    }

    $action = $_REQUEST["action"];
    $contentID = empty( $_REQUEST["item"] ) ? NULL : $_REQUEST["item"];
    $content = empty( $_POST["content"] ) ? NULL : $_POST["content"];
    $caller = array_key_exists('caller', $_REQUEST) ? $_REQUEST["caller"]: '';
    $context = array_key_exists('context', $_REQUEST) ? $_REQUEST["context"]: '';
    $extended = isset( $_REQUEST["extended"] );

    if( $action == "UPDATE" && isset( $content ) && $_SESSION["rights"]["editContent"] ) {
        if( isset( $_POST["title"] ) ) {
            if( empty( $contentID ) ) {
                setContentExt( array( $_POST["title"], $_POST["content"], $_POST["access"] ) );
            } else {
                setContentExt( array( $_POST["title"], $_POST["content"], $_POST["access"] ), $contentID );
            }
        } else {
            setContent( $contentID, $content );
        }
        header( "Location: $caller?context=$context" );
    }
?>


<!-- start head -->
<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ); ?> (GGstOf)</title>
</head>
<!-- end head -->




<!-- start body -->
<body>




<!-- start #navigationLeft -->
<?php
    include( "include/navigationLeft.inc.php" );
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
include("include/navigationTop.inc.php");
?>
<!-- end #navigationTop -->




<!-- start #content -->
<div id="content" class="start">

<?php if( $action == "EDIT" || $action == "ADD" ) { ?>

<form class="formUpdateProfil" name="formMemberProfil" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
<fieldset>
<?php

    if( $extended ) {
        if( $contentID ) {
            $content = getContentExt( $contentID );
            $content = $content[0];
        }
        echo translate( "Titel" );
?>
<input type="text" name="title" value="<?php echo $content["title"]; ?>" />
<?php
    } else {
        $content = getContent( $contentID );
    }
    if(!defined('lang')){
        $lang = '';
    }

    $fck = new FCKeditor( "content" );
    $fck->BasePath = "module/fckeditor/";
    $fck->Config["CustomConfigurationsPath"] = "/public_html/plattform/module/myFCKconfig.js";
    $fck->Config["AutoDetectLanguage"] = false;
    $fck->Config["DefaultLanguage"] = $lang;
    $fck->Config["LinkUpload"] = ( $_SESSION["rights"]["uploadDocs"] == 1 );
    $fck->Config["SkinPath"] = "skins/silver/";
    $fck->ToolbarSet = "GGstOf";
    $fck->Height = "500";
    $fck->Value = stripslashes( $content["text"] );
    $fck->Create();

    if( $extended ) {
        echo translate( "Sichtbar ab Stufe" );
?>
<select name="access"><?php makeOptions( "linkSysRoles", $content["access"], true ); ?></select><br /><br />
<?php
    }
?>
<input type="hidden" name="item" value="<?php echo $contentID; ?>" />
<input type="hidden" name="caller" value="<?php echo $caller; ?>" />
<input type="hidden" name="context" value="<?php echo $context; ?>" />
<input type="hidden" name="action" value="UPDATE" />
<input type="submit" class="formsSubmitButton" value="<?php echo translate( "Aktualisieren" ); ?>" />
<input type="reset" value="<?php echo translate( "Abbrechen" ); ?>" onclick="location='<?php echo $caller . "?context=" . $context; ?>'" />
</fieldset>
</form>
Credits to <a href="http://www.fckeditor.net/" onclick="window.open('http://www.fckeditor.net/'); return false;">Frederico Caldeira Knabben</a>

<?php } ?>

</div>
<!-- end #content -->


<?php
//include("include/footer.inc.php");
?>

</body>
<!-- end body -->

</html>
