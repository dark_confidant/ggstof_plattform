<?php
    function memberList( $id, $param=NULL, $count=false ) {
        require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/search.php" );
//         global $order;

        switch( $id ) {
            case 1:
                // Ver�ffentlichte Profile, Mitglieder
//                 $criteria = array( "tblPersonen.veroeffentlichungID"=>1, "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache" );
                $criteria = array( "tblPersonen"=>array( "veroeffentlichungID"=>1, "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "remindDate", "email", "veroeffentlichungID" ), "qryBeruf"=>array( "email" ), "linkPersonVeroeffentlichung"=>array( "veroeffentlichung" ) );
                $search = new memberSearch( $criteria, $fields );
                $include = $search->execute();

                $result = array();
                $varargout = array();
                for( $i=0; $i<count( $include ); $i++ ) {
                    $person = new person( $include[$i]["tblPersonen.personID"] );
                    $modDateOwner = substr( $person->modDateOwner, 0, 10 );
                    $j = 0;
                    while( ( $person->log[$j]->typID != 1 || !$person->log[$j]->success ) && $j<100 ) {
                        $j++;
                    }
                    if( $j < 100 ) {
                        $login = substr( $person->log[$j]->timestamp, 0, 10 );
                    } else {
                        $login = "0000-00-00";
                    }

                    if( $login != "0000-00-00" || $modDateOwner != "0000-00-00" ) {
                        $include[$i]["login"] = $login;
                        $result[] = $include[$i];
                        $varargout[] = $modDateOwner;
                    }
                }
//                 $conn = dbconn::open();
//                 $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=4";
//                 $conn->query($query);
//
//                 dbconn::close( $conn );

                break;

            case 2:
                // Aktuelle Liste GGstOf Mitglieder
                $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "qryBeruf"=>array( "typ" ), "linkPersonAnrede"=>array( "anrede", "anredeText" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 3:
                // Aktuelle Liste GGstOf Mitglieder ohne Neumitglieder und ohne neu-brevetierte Gst Of
                $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", ( date( "Y" )-1 ) . "-12-31" ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
                $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" ); // = case 2
                $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) );
                $exclude = searchMember( $criteria, false, false, "tblPersonen.personID" );
                $result = array_values( array_udiff( $include, $exclude, "fncMemberListUdiff" ) );

//                 $search = new memberSearch();
//                 $search->addInclude( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", ( date( "Y" )-1 ) . "-12-31" ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
//                 $search->addExclude( "qryVerein"=>array( "organisationRegionID"=>1, "funktionID"=>1 ), "qryBrevet"=>array( "brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) ) );
//                 $search->addFields( "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
//                 $result = $search->execute();
                break;

            case 4:
                // Liste der Beitritte in diesem Jahr
                $criteria = array( "qryBeitritt"=>array( "beitrittsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) ), "qryVerein"=>array( "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "qryBeitritt"=>array( "beitrittsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 5:
                // Liste der Beitritte im Vorjahr
//                 $criteria = array( "qryBeitritt.beitrittsdatum"=>array( ( date( "Y" )-1 ) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = searchMember( $criteria );
                $criteria = array( "qryBeitritt"=>array( "beitrittsdatum"=>array( ( date( "Y" )-1 ) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ) ), "qryVerein"=>array( "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "qryBeitritt"=>array( "beitrittsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 6:
                // Liste der Austritte + Verstorbenen in diesem Jahr
//                 $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumEnde"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryVerein.*,qryAustritt.*,linkPersonSprache.sprache" );
//                 $criteria = array( "tblPersonen.todesdatum"=>array( date( "Y" ) . "-01-01", date( "Y-m-d" ) ) );
//                 $result = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryVerein.*,qryAustritt.*,linkPersonSprache.sprache" ) );

//                 $search = new memberSearch();
//                 $search->addInclude( array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumEnde"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) ) );
//                 $search->addInclude( array( "tblPersonen"=>array( "todesdatum"=>array( date( "Y" ) . "-01-01", date( "Y-m-d" ) ) ) ) );
//                 $search->addFields( array( "tblPersonen"=>array( "todesdatum" ), "qryAustritt"=>array( "austrittsdatum" ) ) );
//                 $result = $search->execute();

                $search = new memberSearch();
                $search->addInclude( array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumEnde"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) ) );
                $search->addFields( array( "tblPersonen"=>array( "todesdatum" ), "qryAustritt"=>array( "austrittsdatum" ) ) );
                $include = $search->execute();

                $search = new memberSearch();
                $search->addInclude( array( "tblPersonen"=>array( "todesdatum"=>array( date( "Y" ) . "-01-01", date( "Y-m-d" ) ) ) ) );
                $search->addFields( array( "tblPersonen"=>array( "todesdatum" ), "qryAustritt"=>array( "austrittsdatum" ) ) );
                $result = array_merge( $include, $search->execute() );
                break;

            case 7:
                // Liste der Austritte im Vorjahr
//                 $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumEnde"=>array( ( date( "Y" )-1 ) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryAustritt.*,linkPersonSprache.sprache" );
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryAustritt"=>array( "austrittsdatum"=>array( ( date( "Y" )-1 ) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ) ) );
                $fields = array( "qryAustritt"=>array( "austrittsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 8:
                // Liste der Verstorbenen
//                 $criteria = array( "tblPersonen.todesdatum"=>array( "0000-00-01", date( "Y-m-d" ) ) );
//                 $result = searchMember( $criteria );
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>array( "0000-00-01", date( "Y-m-d" ) ) ) );
                $fields = array( "tblPersonen"=>array( "todesdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 9:
                // Aktuelle Liste NICHT-Mitglieder
                $param = ( $param == "" ) ? "*" : $param;
                if( $param == "*" ) {
                    $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>99 );
                } elseif( $param == 0 ) {
                    $criteria = "tblPersonen.todesdatum='0000-00-00' AND (qryBeruf.typID=$param OR qryBeruf.typID IS NULL) AND qryVerein.organisationRegionID=1 AND qryVerein.funktionID=99";
                } else {
                    $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryBeruf.typID"=>$param, "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>99 );
                }
                $result = searchMember( $criteria, "qryBeruf.typ,tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort", false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                break;

            case 10:
                // Kein Interesse an GGstOf
//                 $criteria = array( "qryVerein.organisationRegionID"=>91, "qryVerein.funktionID"=>100 );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache" );
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "organisationRegionID"=>91, "funktionID"=>100 ) );
                $fields = array( "qryBeruf"=>array( "typ" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 11:
                // Neu brevetierte Gst Of
//                 $criteria = array( "qryBeitritt"=>array( "beitrittsdatum"=>"NULL" ), "qryBrevet"=>array( "brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) ), "qryVerein"=>array( "organisationRegionID"=>1, "funktionID"=>99 ) ); // Nicht-Mitglieder
//                 $criteria = array( "qryBeitritt"=>array( "beitrittsdatum"=>"NULL" ), "qryBrevet"=>array( "brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) ) );
                $fields = array( "tblPersonen"=>array( "geburtsdatum", "remindDate" ), "qryMilitaer"=>array( "einteilung" ), "qryBeruf"=>array( "funktion", "organisation", "typ" ), "qryBerufAusbildung"=>array( "titel", "institution" ), "qryBeitritt"=>array( "beitrittsdatum" ), "qryVerein"=>array( "funktionID", "vereinFunktion", "timestamp" ) );
//                 $search = new memberSearch( $criteria, $fields );
//                 $include = $search->execute();

//                 $criteria = array( "qryBrevet"=>array( "brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) ), "qryVerein"=>array( "organisationRegionID"=>1, "funktionID"=>1 ) ); // Mitglieder
                $criteria = array( "qryBrevet"=>array( "brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) ) );
                $search = new memberSearch( $criteria, $fields );
                $search->order = "qryBeitritt.beitrittsdatum DESC";
//                 $result = array_merge( $include, $search->execute() );
                $result = $search->execute();
                break;

            case 12:
                // Export Gst S
                $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryVerein.datumEnde"=>"9999-12-31" );
                $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonChannel.channel,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>99, "qryVerein.datumEnde"=>"9999-12-31" );
                $include = array_merge( $include, array_values( array_udiff( searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonChannel.channel,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" ), $include, "fncMemberListUdiff" ) ) );
                $criteria = array( "qryVerein.organisationRegionID"=>91, "qryVerein.funktionID"=>100, "qryVerein.datumEnde"=>"9999-12-31" );
                $include = array_merge( $include, array_values( array_udiff( searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonChannel.channel,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" ), $include, "fncMemberListUdiff" ) ) );
                $criteria = array( "tblPersonen.todesdatum"=>array( "0000-00-01", date( "Y-m-d" ) ) ); // = case 8
                $exclude = searchMember( $criteria, false, false, "tblPersonen.personID" );
                $result = array_values( array_udiff( $include, $exclude, "fncMemberListUdiff" ) );
                usort( $result, "fncMemberListUsort" );

                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=7";
                $conn->query($query);

                dbconn::close( $conn );

                break;

            case 13:
                // Kein Interesse an Armee
//                 $criteria = array( "qryVerein.organisationRegionID"=>91, "qryVerein.funktionID"=>101 );
//                 $result = searchMember( $criteria );
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "organisationRegionID"=>91, "funktionID"=>101 ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();
                break;

            case 14:
                // Beitritte im Q1 ohne neu-brevetierte Gst Of
                $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-03-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                $result = searchMember( $criteria );
                break;

            case 15:
                // Beitritte im Q2 ohne neu-brevetierte Gst Of
                $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-04-01", date( "Y" ) . "-06-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                $result = searchMember( $criteria );
                break;

            case 16:
                // Beitritte im Q3 ohne neu-brevetierte Gst Of
                $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-07-01", date( "Y" ) . "-09-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                $result = searchMember( $criteria );
                break;

            case 17:
                // Ausstehende Beitr�ge
                $criteria = array( "typID"=>1, "datumEinzahlung"=>"0000-00-00" );
                $result = searchPayment( $criteria );
                break;

            case 18:
                // Spenden
                $criteria = array( "typID"=>2 );
                $result = searchPayment( $criteria );
                break;

            case 19:
                // Typ - undefined
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ) );
                $fields = array( "tblPersonen"=>array( "email" ), "qryBeruf"=>array( "email", "organisation", "funktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $search->hist = false;
                $include = $search->execute();

                $criteria = array( "qryBeruf"=>array( "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ) );
                $search = new memberSearch( $criteria );
                $exclude = $search->execute();
                $result = array_values( array_udiff( $include, $exclude, "fncMemberListUdiff" ) );
                break;

            case 19.1:
                // Typ - undefined, Mitglieder
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "email" ), "qryBeruf"=>array( "email", "organisation", "funktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $search->hist = false;
                $include = $search->execute();

                $criteria = array( "qryBeruf"=>array( "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ) );
                $search = new memberSearch( $criteria );
                $exclude = $search->execute();
                $result = array_values( array_udiff( $include, $exclude, "fncMemberListUdiff" ) );
                break;

            case 20:
                // Beruf Typ
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryBeruf"=>array( "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "typID"=>$param ) );
                $fields = array( "tblPersonen"=>array( "email" ), "qryBeruf"=>array( "email", "organisation", "funktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 20.1:
                // Beruf Typ, Mitglieder
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryBeruf"=>array( "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "typID"=>$param ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "email" ), "qryBeruf"=>array( "email", "organisation", "funktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 21:
                // Verstorben im aktuellen Jahr
//                 $criteria = array( "tblPersonen.todesdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) );
//                 $result = searchMember( $criteria );
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) ) );
                $fields = array( "tblPersonen"=>array( "todesdatum" ), "qryBeitritt"=>array( "beitrittsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 22:
                // Neumitglieder
//                 $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = searchMember( $criteria, "qryBeitritt.beitrittsdatum DESC", false, "tblPersonen.*,qryDienstgrad.*,qryBeitritt.beitrittsdatum,linkPersonSprache.sprache" );
                $criteria = array( "qryVerein"=>array( "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "todesdatum" ), "qryBeitritt"=>array( "beitrittsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $search->order = "qryBeitritt.beitrittsdatum DESC";
                $result = $search->execute();
                break;

            case 23:
                // Aktuelle Liste GGstOf Mitglieder f�r Zustelloptionen
//                 $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 ); // = case 2
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache,linkPersonChannel.channel,linkPersonChannelPost.postChannel,linkPersonChannelMail.mailChannel,linkZahlungenChannel.paymentChannel,linkBlogChannel.blogChannel" );
                $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "linkPersonChannel"=>array( "channel" ), "linkPersonChannel"=>array( "channel" ), "linkPersonChannelPost"=>array( "postChannel" ), "linkPersonChannelMail"=>array( "mailChannel" ), "linkZahlungenChannel"=>array( "paymentChannel" ), "linkBlogChannel"=>array( "blogChannel" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 24:
                // Brevetierungsjahr
//                 $criteria = array( "qryBrevet.brevetierungsdatum"=>array( "$param-01-01", "$param-12-31" ) );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBrevet.brevetierungsdatum,qryVerein.*,linkPersonSprache.sprache" );
                $criteria = array( "qryBrevet"=>array( "brevetierungsdatum"=>array( "$param-01-01", "$param-12-31" ) ) );
                $fields = array( "tblPersonen"=>array( "email", "geburtsdatum", "todesdatum", "veroeffentlichungID" ), "qryBeruf"=>array( "typID", "typ" ), "qryBrevet"=>array( "brevetierungsdatum" ), "qryVerein"=>array( "vereinFunktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 25:
                // Brevetierungsjahr - Typ
//                 $criteria = array( "qryBrevet.brevetierungsdatum"=>array( "$param-01-01", "$param-12-31" ), "qryBeruf.datumBeginn"=>array( "0001-00-00", "$param-12-31" ), "qryBeruf.datumEnde"=>array( "$param-01-01", "9999-12-31" ) );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBrevet.brevetierungsdatum,linkPersonSprache.sprache" );
                $criteria = array( "qryBrevet"=>array( "brevetierungsdatum"=>array( "$param-01-01", "$param-12-31" ) ), "qryBeruf"=>array( "datumBeginn"=>array( "0001-00-00", "$param-12-31" ), "datumEnde"=>array( "$param-01-01", "9999-12-31" ) ) );
                $fields = array( "tblPersonen"=>array( "email", "geburtsdatum", "todesdatum" ), "qryBeruf"=>array( "typID", "typ" ), "qryBrevet"=>array( "brevetierungsdatum" ), "qryVerein"=>array( "vereinFunktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 26:
                // Brevetierungsjahr - Fehler
//                 $criteria = array( "qryBrevet.brevetierungsdatum"=>"0000-00-00" );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBrevet.brevetierungsdatum,linkPersonSprache.sprache" );
                $criteria = array( "qryBrevet"=>array( "brevetierungsdatum"=>"0000-00-00" ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();
                break;

            case 27:
                // Branchen
//                 $criteria = array( "qryBeruf.branche"=>$param, "qryBeruf., "qryBeruf.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache" );
                $criteria = array( "qryBeruf"=>array( "branche"=>$param, "typID"=>array( 0, 5 ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ) );
                $fields = array( "qryBeruf"=>array( "berufID", "positionID", "position", "funktion", "branche", "organisation" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 28:
                // Blog-Empf�nger
//                 $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "tblPersonen.blogChannelID"=>array( 2, 3 ) );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache" );
                $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ), "tblPersonen"=>array( "blogChannelID"=>array( 2, 3 ) ) );
                $fields = array( "tblPersonen"=>array( "email", "blogChannelID" ), "qryBeruf"=>array( "email" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 29:
                // kein Blog-Mail
//                 $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "tblPersonen.blogChannelID"=>1 );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache" );
                $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ), "tblPersonen"=>array( "blogChannelID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "email", "blogChannelID" ), "qryBeruf"=>array( "email" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 30:
                // Regionen
//                 $criteria = array( "qryVerein.organisationRegionID"=>$param, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryVerein.*,linkPersonSprache.sprache" );
                $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>$param ) );
                $fields = array( "tblPersonen"=>array( "ort", "land", "plz" ), "qryVerein"=>array( "vereinID", "organisationRegionID" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 31:
                // Sprachen - Mitglieder
//                 $criteria = array( "spracheID"=>$param, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonChannel.channel,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                $criteria = array( "tblPersonen"=>array( "spracheID"=>$param ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
//                 $fields = array( "tblPersonen"=>array( "ort", "land", "plz" ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();
                break;

            case 32:
                // Sprachen - Nicht-Mitglieder, kein Interesse... ohne Verstorbene
//                 $criteria = array( "spracheID"=>$param, "tblPersonen.todesdatum"=>"0000-00-00" );
//                 $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonChannel.channel,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
//                 $criteria = array( "spracheID"=>$param, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 ); // =31
//                 $exclude = searchMember( $criteria, false, false, "tblPersonen.personID" );
//                 $result = array_values( array_udiff( $include, $exclude, "fncMemberListUdiff" ) );
                $criteria = array( "tblPersonen"=>array( "spracheID"=>$param, "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "funktionID"=>array( 99, 101 ) ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();
                break;

            case 33:
                // Nicht-publizierte GGstOf Mitglieder
//                 $criteria = array( "tblPersonen.veroeffentlichungID"=>0, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.personID" );
                $criteria = array( "tblPersonen"=>array( "veroeffentlichungID"=>0 ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();

                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=8";
                $conn->query($query);

                dbconn::close( $conn );

                break;

            case 34:
                // Alter Mitglieder
//                 $criteria = array( "tblPersonen.geburtsdatum"=>array( ( date( "Y" )-$param[1] ) . date( "-m-d" ), ( date( "Y" )-$param[0] ) . date( "-m-d" ) ), "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBrevet.brevetierungsdatum,linkPersonSprache.sprache" );
                $criteria = array( "tblPersonen"=>array( "geburtsdatum"=>array( ( date( "Y" )-$param-4 ) . date( "-m-d" ), ( date( "Y" )-( $param == 31 ? 30 : $param ) ) . date( "-m-d" ) ), "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "geburtsdatum" ), "qryBrevet"=>array( "brevetierungsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 35:
                // Alter Mitglieder plus aD
//                 $criteria = array( "tblPersonen.geburtsdatum"=>array( "0000-01-01", (date( "Y" )-66) . date( "-m-d" ) ), "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBrevet.brevetierungsdatum,linkPersonSprache.sprache" );
//                 $criteria = array( "qryDienstgrad.zusatzDgID"=>4, "tblPersonen.geburtsdatum"=>"0000-01-00", "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBrevet.brevetierungsdatum,linkPersonSprache.sprache" ) );

                $criteria = array( "tblPersonen"=>array( "geburtsdatum"=>array( "0000-01-01", ( date( "Y" )-66 ) . date( "-m-d" ) ), "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "geburtsdatum" ), "qryBrevet"=>array( "brevetierungsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $include = $search->execute();

                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryDienstgrad"=>array( "zusatzDgID"=>4 ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $search = new memberSearch( $criteria, $fields );
                $result = array_merge( $include, $search->execute() );
                break;

            case 36:
                // Alter Nicht-Mitglieder
//                 $criteria = array( "tblPersonen.geburtsdatum"=>array( (date( "Y" )-$param[1]) . date( "-m-d" ), (date( "Y" )-$param[0]) . date( "-m-d" ) ), "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>99 );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBrevet.brevetierungsdatum,linkPersonSprache.sprache" );
                $criteria = array( "tblPersonen"=>array( "geburtsdatum"=>array( ( date( "Y" )-$param-4 ) . date( "-m-d" ), ( date( "Y" )-( $param == 31 ? 30 : $param ) ) . date( "-m-d" ) ), "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>99 ) );
                $fields = array( "tblPersonen"=>array( "geburtsdatum" ), "qryBrevet"=>array( "brevetierungsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 37:
                // Alter Nicht-Mitglieder plus aD
//                 $criteria = array( "tblPersonen.geburtsdatum"=>array( "0000-01-01", (date( "Y" )-66) . date( "-m-d" ) ), "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>99 );
//                 $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBrevet.brevetierungsdatum,linkPersonSprache.sprache" );
//                 $criteria = array( "qryDienstgrad.zusatzDgID"=>4, "tblPersonen.geburtsdatum"=>"0000-01-00", "tblPersonen.todesdatum"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>99 );
//                 $result = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBrevet.brevetierungsdatum,linkPersonSprache.sprache" ) );

                $criteria = array( "tblPersonen"=>array( "geburtsdatum"=>array( "0000-01-01", ( date( "Y" )-66 ) . date( "-m-d" ) ), "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>99 ) );
                $fields = array( "tblPersonen"=>array( "geburtsdatum" ), "qryBrevet"=>array( "brevetierungsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $include = $search->execute();

                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryDienstgrad"=>array( "zusatzDgID"=>4 ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>99 ) );
                $search = new memberSearch( $criteria, $fields );
                $result = array_merge( $include, $search->execute() );
                break;

            case 38:
                // Neue Mitglieder, nie eingeloggt/nie bearbeitet/unvollst�ndig
//                 $criteria = array( "tblPersonen"=>array( "aktualisierungID"=>array( 2, 3 ), "veroeffentlichungID"=>array( 2, 3 ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $criteria = array( "tblPersonen"=>array( "aktualisierungID"=>array( 2, 3 ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "email", "aktualisierungID", "veroeffentlichungID", "remindDate", "channelID" ), "qryBeruf"=>array( "email" ), "qryBeitritt"=>array( "beitrittsdatum" ), "linkPersonVeroeffentlichung"=>array( "veroeffentlichung" ) );
                $search = new memberSearch( $criteria, $fields );
                $search->order = "qryBeitritt.beitrittsdatum DESC";
                $include = $search->execute();

                $result = array();
                $varargout = $param > 0 ? array() : NULL;
                for( $i=0; $i<count( $include ); $i++ ) {
                    $person = new person( $include[$i]["tblPersonen.personID"] );
                    $include[$i]["modDateOwner"] = substr( $person->modDateOwner, 0, 10 );
                    $j = 0;
                    while( ( $person->log[$j]->typID != 1 || !$person->log[$j]->success ) && $j<100 ) {
                        $j++;
                    }
                    if( $j < 100 ) {
                        $login = substr( $person->log[$j]->timestamp, 0, 10 );
                    } else {
                        $login = "0000-00-00";
                    }

                    if( $param == 0 && $login == "0000-00-00" && $include[$i]["modDateOwner"] == "0000-00-00" ) {
                        $result[] = $include[$i];
                    }
                    if( $param == 1 && $login != "0000-00-00" && $include[$i]["modDateOwner"] == "0000-00-00" && $include[$i]["tblPersonen.aktualisierungID"] == 3 ) {
                        $result[] = $include[$i];
                        $varargout[] = $login;
                    }
                    if( $param == 2 && $login != "0000-00-00" && $include[$i]["modDateOwner"] != "0000-00-00" && $include[$i]["tblPersonen.aktualisierungID"] == 3 && $include[$i]["tblPersonen.veroeffentlichungID"] == 2 ) {
                        $result[] = $include[$i];
                        $varargout[] = $login;
                    }
                }
                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=" . ( $param + 1 );
                $conn->query($query);

                dbconn::close( $conn );

                break;

            case 39:
                // Geburtstage
                $criteria = array( "MONTH(tblPersonen.geburtsdatum)"=>$param, "tblPersonen.geburtsdatum"=>array( "0000-00-01", date( "Y-m-d" ) ), "tblPersonen.todesdatum"=>"0000-00-00" );
                $result = searchMember( $criteria, "DAY(tblPersonen.geburtsdatum),tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort", false, "tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache" );
                break;

            case 40:
                // Liste der Verstorbenen im Vorjahr
//                 $criteria = array( "tblPersonen.todesdatum"=>array( ( date( "Y" )-1 ) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ) );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryAustritt.*,linkPersonSprache.sprache" );
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>array( ( date( "Y" )-1 ) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ) ) );
                $fields = array( "qryAustritt"=>array( "austrittsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 41:
                // Neu brevetierte Gst Of im Vorjahr
//                 $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>99, "qryBeitritt.beitrittsdatum"=>"NULL", "qryBrevet.brevetierungsdatum"=>array( (date( "Y" )-1) . "-01-01", (date( "Y" )-1) . "-12-31" ) );
//                 $include = searchMember( $criteria ); // Nicht-Mitglieder
//                 $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( (date( "Y" )-1) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ) );
//                 $result = array_merge( $include, searchMember( $criteria, "qryBeitritt.beitrittsdatum DESC", false, "tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache,qryBeitritt.beitrittsdatum" ) ); // = case 3

                $search = new memberSearch();
                $search->addInclude( array( "qryBeitritt"=>array( "beitrittsdatum"=>"NULL" ), "qryBrevet"=>array( "brevetierungsdatum"=>array( ( date( "Y" )-1 ) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ) ), "qryVerein"=>array( "organisationRegionID"=>1, "funktionID"=>99 ) ) ); // Nicht-Mitglieder
                $search->addFields( array( "tblPersonen"=>array( "geburtsdatum", "remindDate" ), "qryMilitaer"=>array( "einteilung" ), "qryBeruf"=>array( "funktion", "organisation", "typ" ), "qryBerufAusbildung"=>array( "titel", "institution" ), "qryBeitritt"=>array( "beitrittsdatum" ) ) );
                $include = $search->execute();

                $search = new memberSearch();
                $search->addInclude( array( "qryBrevet"=>array( "brevetierungsdatum"=>array( ( date( "Y" )-1 ) . "-01-01", ( date( "Y" )-1 ) . "-12-31" ) ), "qryVerein"=>array( "organisationRegionID"=>1, "funktionID"=>1 ) ) ); // Mitglieder
                $search->addFields( array( "tblPersonen"=>array( "geburtsdatum", "remindDate" ), "qryMilitaer"=>array( "einteilung" ), "qryBeruf"=>array( "funktion", "organisation", "typ" ), "qryBerufAusbildung"=>array( "titel", "institution" ), "qryBeitritt"=>array( "beitrittsdatum" ) ) );
                $search->order = "qryBeitritt.beitrittsdatum DESC";
                $result = array_merge( $include, $search->execute() );
                break;

            case 42:
                // Almost lost
//                 $criteria = array( "tblPersonen.geburtsdatum"=>array( (date( "Y" )-66) . date( "-m-d" ), (date( "Y" )-57) . date( "-m-d" ) ), "tblPersonen.channelID"=>100 );
//                 $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache" );
                $criteria = array( "tblPersonen"=>array( "geburtsdatum"=>array( ( date( "Y" )-66 ) . date( "-m-d" ), ( date( "Y" )-57 ) . date( "-m-d" ) ), "channelID"=>100 ) );
                $fields = array( "tblPersonen"=>array( "geburtsdatum", "email", "adresse", "postfach", "plz", "ort" ), "qryBeruf"=>array( "email", "adresse", "postfach", "plz", "ort" ) );
                $search = new memberSearch( $criteria, $fields );
                $include = $search->execute();

                $result = array();
                for( $i=0; $i<count( $include ); $i++ ) {
                    if( $include[$i]["tblPersonen.email"] == $include[$i]["qryBeruf.email"] || empty( $include[$i]["tblPersonen.email"] ) ) {
                        $result[] = $include[$i];
                    }
                }
                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=6";
                $conn->query($query);

                dbconn::close( $conn );

                break;

            case 43:
                // Unver�ffentlichte GGstOf Mitglieder
//                 $criteria = array( "tblPersonen.veroeffentlichungID"=>0, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                $criteria = array( "tblPersonen"=>array( "veroeffentlichungID"=>0 ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "tblPersonen"=>array( "email", "veroeffentlichungID", "remindDate", "channelID" ), "qryBeruf"=>array( "email" ), "qryBeitritt"=>array( "beitrittsdatum" ) );
                $search = new memberSearch( $criteria, $fields );
                $include = $search->execute();

                $result = array();
                $varargout = array();
                for( $i=0; $i<count( $include ); $i++ ) {
                    $person = new person( $include[$i]["tblPersonen.personID"] );
                    $modDateOwner = substr( $person->modDateOwner, 0, 10 );
                    $j = 0;
                    while( ( $person->log[$j]->typID != 1 || !$person->log[$j]->success ) && $j<100 ) {
                        $j++;
                    }
                    if( $j < 100 ) {
                        $login = substr( $person->log[$j]->timestamp, 0, 10 );
                    } else {
                        $login = "0000-00-00";
                    }

                    if( $modDateOwner != "0000-00-00" && $login == "0000-00-00" ) {
                        $include[$i]["modDateOwner"] = $modDateOwner;
                        $result[] = $include[$i];
                        $varargout[] = substr( $person->modDateAdmin, 0, 10 );
                    }
                }

                break;

            case 44:
                // Kdt, Mitglieder
//                 $criteria = array( "qryMilitaer.funktion"=>"?dt", "qryMilitaer.zusatzFktID"=>1, "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,linkPersonSprache.sprache" );
                $criteria = array( "qryMilitaer"=>array( "funktion"=>"?dt", "zusatzFktID"=>1, "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "qryMilitaer"=>array( "einteilung", "funktion", "datumBeginn" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 45:
                // Mitglieder, keine Aktualisierung
//                 $criteria = array( "tblPersonen.aktualisierungID"=>1, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//                 $result = searchMember( $criteria );
                $criteria = array( "tblPersonen"=>array( "aktualisierungID"=>1 ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();

                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=9";
                $conn->query($query);

                dbconn::close( $conn );

                break;

            case 46:
                // Aktuelle Einteilung
                $param = empty( $param ) ? $param : "*$param";
//                 $criteria = array( "qryMilitaer.einteilung"=>$param, "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//                 $include = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryVerein.*,linkPersonSprache.sprache" );
//                 $criteria = array( "qryMilitaer.vorgesVb"=>$param, "qryMilitaer.funktion"=>"?dt", "qryMilitaer.zusatzFktID"=>array( 1, 3 ), "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//                 $include = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryVerein.*,linkPersonSprache.sprache" ) );
//                 $criteria = array( "qryDienstgrad.zusatzDgID"=>4 );
//                 $exclude = searchMember( $criteria, false, false, "tblPersonen.personID" );
//                 $result = array_values( array_udiff( $include, $exclude, "fncMemberListUdiff" ) );

                $criteria = array( "qryMilitaer"=>array( "einteilung"=>$param, "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ) );
                $fields = array( "tblPersonen"=>array( "email", "veroeffentlichungID" ), "qryMilitaer"=>array( "militaerID", "einteilung", "funktion", "zusatzFkt" ), "qryVerein"=>array( "funktionID", "vereinFunktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $include = $search->execute();

                if( !empty( $param ) ) {
                    $criteria = array( "qryMilitaer"=>array( "vorgesVb"=>$param, "funktion"=>"?dt", "zusatzFktID"=>array( 1, 3 ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ) );
                    $search = new memberSearch( $criteria, $fields );
                    $include = array_merge( $include, $search->execute() );
                }

                $criteria = array( "qryDienstgrad"=>array( "zusatzDgID"=>4 ) );
                $search = new memberSearch( $criteria );
                $result = array_values( array_udiff( $include, $search->execute(), "fncMemberListUdiff" ) );

                usort( $result, "fncMemberListUsort" );
                break;

            case 47:
                // Abschl�sse/Titel
//                 $criteria = array( "qryBerufAusbildung.titel"=>$param );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBerufAusbildung.*,qryVerein.vereinFunktion,linkPersonSprache.sprache" );
                $criteria = array( "qryBerufAusbildung"=>array( "titel"=>$param ) );
                $fields = array( "qryBerufAusbildung"=>array( "ausbildungID", "titel", "typID", "institution", "abschlussjahr" ), "qryVerein"=>array( "vereinFunktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 48:
                // Abschl�sse/Institution
//                 $criteria = array( "qryBerufAusbildung.institution"=>$param );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBerufAusbildung.*,qryVerein.vereinFunktion,linkPersonSprache.sprache" );
                $criteria = array( "qryBerufAusbildung"=>array( "institution"=>$param ) );
                $fields = array( "qryBerufAusbildung"=>array( "ausbildungID", "titel", "typID", "institution", "abschlussjahr" ), "qryVerein"=>array( "vereinFunktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 49:
                // Abschl�sse/Typ
//                 $criteria = array( "qryBerufAusbildung.typID"=>$param );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBerufAusbildung.*,qryVerein.vereinFunktion,linkPersonSprache.sprache" );
                $criteria = array( "qryBerufAusbildung"=>array( "typID"=>$param ) );
                $fields = array( "qryBerufAusbildung"=>array( "ausbildungID", "titel", "typID", "institution", "abschlussjahr" ), "qryVerein"=>array( "vereinFunktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 50:
                // Abschl�sse/Typ, Beruf/Typ
//                 $criteria = array( "qryBerufAusbildung.typID"=>$param[0], "qryBeruf.typID"=>$param[1], "qryBeruf.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//                 $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBerufAusbildung.*,qryVerein.vereinFunktion,linkPersonSprache.sprache" );
                $criteria = array( "qryBerufAusbildung"=>array( "typID"=>$param[0] ), "qryBeruf"=>array( "typID"=>$param[1], "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ) );
                $fields = array( "qryBerufAusbildung"=>array( "ausbildungID", "titel", "typID", "institution", "abschlussjahr" ), "qryVerein"=>array( "vereinFunktion" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 51:
                // Vorges Vb
                $param = empty( $param ) ? "NULL" : $param;

//                 $criteria = array( "qryMilitaer"=>array( "vorgesVb"=>$param, "funktion"=>"?dt", "zusatzFktID"=>array( 1, 3 ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ) );
//                 $fields = array( "qryMilitaer"=>array( "militaerID", "einteilung", "funktion", "zusatzFkt", "vorgesVb" ) );
//                 $search = new memberSearch( $criteria, $fields );
//                 $search->order = "qryMilitaer.einteilung,tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
//                 $include = $search->execute();

//                 $criteria = array( "qryDienstgrad"=>array( "zusatzDgID"=>4 ) );
//                 $search = new memberSearch( $criteria );
//                 $result = array_values( array_udiff( $include, $search->execute(), "fncMemberListUdiff" ) );
                $criteria = array( "qryMilitaer"=>array( "vorgesVb"=>$param, "funktion"=>"?dt", "zusatzFktID"=>array( 1, 3 ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ) );
                $fields = array( "qryMilitaer"=>array( "militaerID", "einteilung", "funktion", "zusatzFkt", "vorgesVb" ) );
                $search = new memberSearch( $criteria, $fields );
                $search->addExclude( array( "qryDienstgrad"=>array( "zusatzDgID"=>4 ) ) );
                $search->order = "qryMilitaer.einteilung,tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
                $result = $search->execute();
                break;

            case 52:
                // Branchen; siehe auch case 27
                $criteria = array( "qryBeruf"=>array( "branche"=>$param, "typID"=>array( 0, 5 ) ) );
                $fields = array( "qryBeruf"=>array( "berufID", "positionID", "position", "funktion", "branche", "organisation" ) );
                $search = new memberSearch( $criteria, $fields );
                $search->group = "qryBeruf.berufID";
                $result = $search->execute();
                break;

            case 53:
                // SC, Mitglieder

//                 $criteria = array( "qryMilitaer"=>array( "funktion"=>"SC", "zusatzFktID"=>1, "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
//                 $fields = array( "qryMilitaer"=>array( "einteilung", "funktion", "datumBeginn" ) );
//                 $search = new memberSearch( $criteria, $fields );
//                 $include = $search->execute();
// 
//                 $criteria = array( "qryMilitaer"=>array( "funktion"=>"CEM", "zusatzFktID"=>1, "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
//                 $search = new memberSearch( $criteria, $fields );
//                 $include = array_merge( $include, $search->execute() );
// 
//                 $criteria = array( "qryMilitaer"=>array( "funktion"=>"CSM", "zusatzFktID"=>1, "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
//                 $search = new memberSearch( $criteria, $fields );
//                 $result = array_merge( $include, $search->execute() );

                $search = new memberSearch();
                $search->addInclude( array( "qryMilitaer"=>array( "funktion"=>"SC", "zusatzFktID"=>1, "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) ) );
                $search->addInclude( array( "qryMilitaer"=>array( "funktion"=>"CEM", "zusatzFktID"=>1, "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) ) );
                $search->addInclude( array( "qryMilitaer"=>array( "funktion"=>"CSM", "zusatzFktID"=>1, "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) ) );
                $search->addFields( array( "qryMilitaer"=>array( "einteilung", "funktion", "datumBeginn" ) ) );
                $result = $search->execute();
                break;

            case 54:
                // Alle lebenden Gst Of
//                 $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00" );
//                 $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();

                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=12";
                $conn->query($query);

                dbconn::close( $conn );
                break;

            case 55:
                // Team Golf
//                 $criteria = array( "qryVerein.organisationRegionID"=>20, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//                 $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
                $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>20 ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();
                break;

            case 56:
                // Zu best�tigende Profile
//                 $criteria = array( "tblPersonen.codex"=>1, "tblPersonen.datenschutz"=>1, "tblPersonen.veroeffentlichungID"=>0, "tblPersonen.todesdatum"=>"0000-00-00" );
//                 $result = searchMember( $criteria );
                $criteria = array( "tblPersonen"=>array( "codex"=>1, "datenschutz"=>1, "veroeffentlichungID"=>0, "todesdatum"=>"0000-00-00" ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();
                break;

            case 57:
                // Alle erfassten Gst Of
                $criteria = array( "tblPersonen"=>array( "personID"=>"*" ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();

                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=10";
                $conn->query($query);

                dbconn::close( $conn );
                break;

            case 58:
                // Profile mit Bild
                $img = glob( "image/profile/*.jpg" );
                $result = array();
                foreach( $img as $i ) {
                    $criteria = array( "tblPersonen"=>array( "personID"=>substr( $i, 14, -4 ) ) );
                    $search = new memberSearch( $criteria );
                    $result = array_merge( $result, $search->execute() );
                }
                usort( $result, "fncMemberListUsort" );

                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=11";
                $conn->query($query);

                dbconn::close( $conn );
                break;

            case 59:
                // Ver�ffentlichte Profile
                $criteria = array( "tblPersonen"=>array( "veroeffentlichungID"=>1 ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();

                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=4";
                $conn->query($query);

                dbconn::close( $conn );
                break;

            case 60:
                // Vorges Vb Alle
                $param = empty( $param ) ? "NULL" : $param;

                $search = new memberSearch();
                $search->addInclude( array( "qryMilitaer"=>array( "vorgesVb"=>$param, "funktion"=>"?dt", "zusatzFktID"=>array( 1, 3 ) ) ) );
                $search->addInclude( array( "qryMilitaer"=>array( "vorgesVb"=>$param, "funktion"=>"Chef", "zusatzFktID"=>array( 1, 3 ) ) ) );
                $search->addFields( array( "qryMilitaer"=>array( "militaerID", "datumBeginn", "datumEnde", "einteilung", "funktion", "zusatzFkt", "vorgesVb", "dienstgrad", "zusatzDg" ) ) );
                $search->order = "qryMilitaer.einteilung,tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
                $result = $search->execute();
                break;

            case 61:
                // Ver�ffentlichung GGstOf Mitglieder
                $criteria = array( "tblPersonen"=>array( "veroeffentlichungID"=>$param ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "linkPersonVeroeffentlichung"=>array( "veroeffentlichung" ) );
                $search = new memberSearch( $criteria, $fields );
                $include = $search->execute();

                for( $i=0; $i<count( $include ); $i++ ) {
                    $person = new person( $include[$i]["tblPersonen.personID"] );
                    $j = 0;
                    while( ( $person->log[$j]->typID != 1 || !$person->log[$j]->success ) && $j<100 ) {
                        $j++;
                    }
                    if( $j < 100 ) {
                        $login = substr( $person->log[$j]->timestamp, 0, 10 );
                    } else {
                        $login = "0000-00-00";
                    }

                    if( $login != "0000-00-00" ) {
                        $include[$i]["login"] = $login;
                        $result[] = $include[$i];
                    }
                }
                break;

            case 62:
                // Mitglieder, Offline-Aktualisierung
                $criteria = array( "tblPersonen"=>array( "aktualisierungID"=>2 ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $search = new memberSearch( $criteria );
                $include = $search->execute();

                $result = array();
                $varargout = array();
                for( $i=0; $i<count( $include ); $i++ ) {
                    $person = new person( $include[$i]["tblPersonen.personID"] );
                    $result[] = $include[$i];
                    $varargout[] = substr( $person->modDateAdmin, 0, 10 );
                }
                $conn = dbconn::open();
                $query = "UPDATE sysStor SET param0=" . count( $result ) . " WHERE storID=5";
                $conn->query($query);

                dbconn::close( $conn );

                break;

            case 63:
                // Kein Interesse
                $criteria = array( "tblPersonen"=>array( "todesdatum"=>"0000-00-00" ), "qryVerein"=>array( "organisationRegionID"=>91, "funktionID"=>array( 100, 101 ) ) );
                $search = new memberSearch( $criteria );
                $result = $search->execute();
                break;

            case 64:
                // Aktuelle Liste GGstOf Mitglieder A-M (siehe case 2)
                $criteria = array( "tblPersonen"=>array( "name"=>array( "a", "mzz" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "qryBeruf"=>array( "typ" ), "linkPersonAnrede"=>array( "anrede", "anredeText" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 65:
                // Aktuelle Liste GGstOf Mitglieder N-Z (siehe case 2)
                $criteria = array( "tblPersonen"=>array( "name"=>array( "n", "zzz" ) ), "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
                $fields = array( "qryBeruf"=>array( "typ" ), "linkPersonAnrede"=>array( "anrede", "anredeText" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 66:
                // Blog-Empf�nger A-M (siehe case 28)
                $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ), "tblPersonen"=>array( "blogChannelID"=>array( 2, 3 ), "name"=>array( "a", "mzz" ) ) );
                $fields = array( "tblPersonen"=>array( "email", "blogChannelID" ), "qryBeruf"=>array( "email" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 67:
                // Blog-Empf�nger N-Z (siehe case 28)
                $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ), "tblPersonen"=>array( "blogChannelID"=>array( 2, 3 ), "name"=>array( "n", "zzz" ) ) );
                $fields = array( "tblPersonen"=>array( "email", "blogChannelID" ), "qryBeruf"=>array( "email" ) );
                $search = new memberSearch( $criteria, $fields );
                $result = $search->execute();
                break;

            case 68:
                // Vorges Vb Stab
                $param = empty( $param ) ? "NULL" : $param;

                $search = new memberSearch();
                $search->addInclude( array( "qryMilitaer"=>array( "vorgesVb"=>$param, "zusatzFktID"=>array( 1, 3 ) ) ) );
                $search->addExclude( array( "qryMilitaer"=>array( "funktion"=>"?dt" ), "qryDienstgrad"=>array( "zusatzDgID"=>4 ) ) ); // ohne Kdt, a D
                $search->addFields( array( "qryMilitaer"=>array( "militaerID", "datumBeginn", "datumEnde", "einteilung", "funktion", "zusatzFkt", "vorgesVb" ) ) );
                $search->order = "qryMilitaer.einteilung,tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
                $result = $search->execute();
                break;

            default:
                die( "Liste $id existiert nicht!" );
                break;
        }
// echo $search->query;

        if( isset( $varargout ) ) {
            return array( $result, $varargout );
        } else {
            return $result;
        }
    }

    function fncMemberListUdiff( $a, $b ) {
        if( $a["tblPersonen.personID"] > $b["tblPersonen.personID"] ) {
            return 1;
        } else if( $a["tblPersonen.personID"] < $b["tblPersonen.personID"] ) {
            return -1;
        } else {
            return 0;
        }
    }

    function fncMemberListUsort( $a, $b ) {
        return strcasecmp( $a["tblPersonen.name"], $b["tblPersonen.name"] );
    }
?>