<?php
    function captcha() {
        // Uses the PEAR CAPTCHA classes

        set_include_path( $_SERVER["DOCUMENT_ROOT"] . "/module/pear/PEAR" );
        require_once( "Text/CAPTCHA.php" );

        // Set CAPTCHA image options (font must exist!)
        $imageOptions = array(
            "font_size"        => 24,
            "font_path"        => $_SERVER["DOCUMENT_ROOT"] . "/font",
            "font_file"        => "Georgia.ttf",
            "text_color"       => "#737373",
            "lines_color"      => "#212121",
            "background_color" => "#CECECE",
            "antialias"        => true
        );

        // Set CAPTCHA options
        $options = array(
            "width"        => 200,
            "height"       => 80,
            "output"       => "png",
            "imageOptions" => $imageOptions
        );

        // Generate a new Text_CAPTCHA object, Image driver
        $c = Text_CAPTCHA::factory( "Image" );
        $retval = $c->init( $options );
        if( PEAR::isError( $retval ) ) {
            printf( "Error initializing CAPTCHA: %s!", $retval->getMessage() );
        }

        // Get CAPTCHA secret passphrase
        $_SESSION["phrase"] = $c->getPhrase();

        // Get CAPTCHA image (as PNG)
        $png = $c->getCAPTCHA();
        if( PEAR::isError( $png ) ) {
            printf( "Error generating CAPTCHA: %s!", $png->getMessage() );
        }
        file_put_contents( $_SERVER["DOCUMENT_ROOT"] . "/temp/" . md5( session_id() ) . ".png", $png );

        set_include_path( $_SERVER["DOCUMENT_ROOT"] );
    }

    function encrypt_email( $email ) {
        // Port of Tim Williams' and Andrew Moulden's email obfuscator JavaScript 2.1; http://www.jottings.com/obfuscator/

        $email = strtolower( $email );
        $shift = strlen( $email );
        $coded = "";

        $unmixedkey = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.@";
        $inprogresskey = $unmixedkey;
        $mixedkey = "";
        $unshuffled = strlen( $unmixedkey );

        for( $i=0; $i<=strlen( $unmixedkey ); $i++ ) {
            $ranpos = rand( 0, $unshuffled-1 );
            $nextchar = $inprogresskey{$ranpos};
            $mixedkey .= $nextchar;
            $inprogresskey = substr( $inprogresskey, 0, $ranpos ) . substr( $inprogresskey, $ranpos+1, $unshuffled );
            $unshuffled--;
        }
        $key = $mixedkey;

        for( $j=0; $j<$shift; $j++ ) {
            if( strpos( $key, $email{$j} ) === false ) {
                $chr = $email{$j};
                $coded .= $email{$j};
            } else {
                $chr = (strpos( $key, $email{$j} ) + $shift) % strlen( $key );
                $coded .= $key{$chr};
            }
        }

        return array( $coded, $key );
    }

    function mysql_fetch_alias_assoc( $result ) {
        if( !( $row = $result->fetch(PDO::FETCH_BOTH) ) ) {
            return NULL;
        }

        $assoc = array();
        for( $i=0; $i < $result->columnCount(); $i++ ) {
            $colMeta = $result->getColumnMeta($i);
            $table = $colMeta['table'];
            $field = $colMeta['name'];
            $assoc["$table.$field"] = $row[$i];
        }
        $assoc[".fullName"] = str_replace( "  ", " ", " {$assoc["tblPersonen.vorname"]}  {$assoc["tblPersonen.vorname2"]} {$assoc["tblPersonen.name"]}" );

        return $assoc;
    }

    function sendmail( $emailID, $adressat, $param=NULL ) {
    // DEPRECATED - USE email CLASS INSTEAD

        require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/blog.php" );
        require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/contentManagement.php" );
        require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/lang.php" );
        require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/search.php" );
        require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/userManagement.php" );

        foreach( array( "de", "fr", "it" ) as $lng ) {
            $content = getEmailContent( $emailID, $lng );

            $bod[$lng] = $content["body"];
            $sub[$lng] = $content["subject"];
        }

        switch( $adressat ) {
            case "0":
                $criteria = array( "tblPersonen.personID"=>$_SESSION["personID"] );
                $adressat = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,tblPublic.*,qryVerein.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                break;
            case "L1":
                // Alle Mitglieder
//                 $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "tblPersonen.channelID"=>1 );
//                 $adressat = searchMember( $criteria );
//                 $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "tblPersonen.channelID"=>100 );
//                 $adressat = array_merge( $adressat, searchMember( $criteria ) );
                $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
                $adressat = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,tblPublic.*,qryVerein.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                break;
            case "L2":
                // Blog-Empf�nger
                $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "tblPersonen.blogChannelID"=>array( 2, 3 ) );
                $adressat = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,tblPublic.*,qryVerein.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                break;
            case "L3":
                // Alle lebenden Gst Of
//                 $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "tblPersonen.channelID"=>1 );
//                 $adressat = searchMember( $criteria );
//                 $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "tblPersonen.channelID"=>100 );
//                 $adressat = array_merge( $adressat, searchMember( $criteria ) );
                $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00" );
                $adressat = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,tblPublic.*,qryVerein.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                break;
            default:
                $criteria = array( "tblPersonen.personID"=>$adressat );
                $adressat = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,tblPublic.*,qryVerein.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText" );
                break;
        }

        $absender = ( $content["absenderID"] == 1 ) ? $_SESSION["userID"] : $content["absender"];
        $absenderText = ( $content["absenderID"] == 1 ) ? "{$adressat[0]["tblPersonen.vorname"]} {$adressat[0]["tblPersonen.name"]}" : $content["absenderText"];

        if( $emailID == 2 ) {
            $blog = $param["blogID"] ? blogEntry( $param["blogID"] ) : blogLastEntries();
            $blog_title = $blog[0]["post_title"];
            $blog_content = strip_tags( $blog[0]["post_content"] );
            $blog_content = strlen( $blog_content ) > 500 ? substr( $blog_content, 0, 500 ) . "..." : $blog_content;
            $blog_guid = $blog[0]["guid"];
        }

        $n = 0;
        $success = 0;
        $failure = array();
        for( $i=0; $i<count( $adressat ); $i++ ) {
//             $pers = new person( $adressat[$i]["tblPersonen.personID"] );
//             $lang = $adressat[$i]["linkPersonSprache.sprache"];
            $body = empty( $bod[$adressat[$i]["linkPersonSprache.sprache"]] ) ? $bod["de"] : $bod[$adressat[$i]["linkPersonSprache.sprache"]];
            $subject = empty( $sub[$adressat[$i]["linkPersonSprache.sprache"]] ) ? $sub["de"] : $sub[$adressat[$i]["linkPersonSprache.sprache"]];

            $dienstgrad = translate( $adressat[$i]["qryDienstgrad.dienstgrad"], $adressat[$i]["linkPersonSprache.sprache"], false, false ) . ( $adressat[$i]["qryDienstgrad.zusatzDg"] ? " " . translate( $adressat[$i]["qryDienstgrad.zusatzDg"], $adressat[$i]["linkPersonSprache.sprache"], false, false ) : "" );
            $dienstgradText = translate( $adressat[$i]["qryDienstgrad.dienstgradText"], $adressat[$i]["linkPersonSprache.sprache"], false, false );
            $anrede = translate( $adressat[$i]["linkPersonAnrede.anrede"], $adressat[$i]["linkPersonSprache.sprache"], false, false );
            $anredeText = translate( $adressat[$i]["linkPersonAnrede.anredeText"], $adressat[$i]["linkPersonSprache.sprache"], false, false );
            $moddate = array();
            foreach( array( "tblPersonen", "qryBeruf", "qryBerufAusbildung", "qryMilitaer", "qryPublic", "qryVerein" ) as $tbl ) {
                $moddate[] = $adressat[$i]["$tbl.modDateUser"];
                $moddate[] = $adressat[$i]["$tbl.modDateAdmin"];
            }
            $moddate = substr( max( $moddate ), 0, 10 );
            if( strpos( $body, "[PW_LINK]" ) !== false ) {
                $hash = $param["hash"] ? $param["hash"] : reset_password( $adressat[$i]["tblPersonen.email"], 60*60*24 );
                $pwlink = "http://{$_SERVER["SERVER_NAME"]}/password.php?$hash";
            }

            $body = str_replace( array( "[MEMBER_ID]", "[NAME]", "[DG]", "[DG_TXT]", "[ANREDE]", "[ANREDE_TXT]", "[EMAIL]", "[MOD_DATE]", "[PW_LINK]", "[REC_INFO]", "[REC_INVOICE]", "[REC_BLOG]" ), array( $adressat[$i]["tblPersonen.personID"], $adressat[$i]["tblPersonen.name"], $dienstgrad, $dienstgradText, $anrede, $anredeText, $adressat[$i]["tblPersonen.email"], $moddate, $pwlink, translate( $adressat[$i]["linkPersonChannel.channel"], $adressat[$i]["linkPersonSprache.sprache"], false, false ), translate( $adressat[$i]["linkZahlungenChannel.paymentChannel"], $adressat[$i]["linkPersonSprache.sprache"], false, false ), translate( $adressat[$i]["linkBlogChannel.blogChannel"], $adressat[$i]["linkPersonSprache.sprache"], false, false ) ), $body );
            $subject = str_replace( array( "[MEMBER_ID]", "[NAME]", "[DG]", "[DG_TXT]", "[ANREDE]", "[ANREDE_TXT]", "[EMAIL]", "[MOD_DATE]", "[PW_LINK]", "[REC_INFO]", "[REC_INVOICE]", "[REC_BLOG]" ), array( $adressat[$i]["tblPersonen.personID"], $adressat[$i]["tblPersonen.name"], $dienstgrad, $dienstgradText, $anrede, $anredeText, $adressat[$i]["tblPersonen.email"], $moddate, $pwlink, translate( $adressat[$i]["linkPersonChannel.channel"], $adressat[$i]["linkPersonSprache.sprache"], false, false ), translate( $adressat[$i]["linkZahlungenChannel.paymentChannel"], $adressat[$i]["linkPersonSprache.sprache"], false, false ), translate( $adressat[$i]["linkBlogChannel.blogChannel"], $adressat[$i]["linkPersonSprache.sprache"], false, false ) ), $subject );

            if( $emailID == 2 ) {
                $body = str_replace( array( "[BLOG_TITLE]", "[BLOG_CONTENT]", "[BLOG_URL]" ), array( $blog_title, $blog_content, $blog_guid ), $body );
                $subject = str_replace( array( "[BLOG_TITLE]", "[BLOG_CONTENT]", "[BLOG_URL]" ), array( $blog_title, $blog_content, $blog_guid ), $subject );

                $channel = array( "2"=>$adressat[$i]["tblPersonen.email"], "3"=>$adressat[$i]["qryBeruf.email"] );
                $email = $channel[$adressat[$i]["tblPersonen.blogChannelID"]];
            } else if( in_array( $emailID, array( 20, 21, 22 ) ) ) {
                $conn = dbconn::open();
                $query = "SELECT SUM(betrag) AS summe
                          FROM tblZahlungen
                          WHERE personID={$adressat[$i]["tblPersonen.personID"]} AND typID=1 AND jahr<" . date( "Y" ) . " AND datumEinzahlung='0000-00-00'";
                $result = $conn->query($query);

                dbconn::close( $conn );
                $row = $result->fetch(PDO::FETCH_ASSOC);
                if( $row[0] ) {
                    $zahlung_ausstehend = translate( 334, $adressat[$i]["linkPersonSprache.sprache"], false, false ) . " CHF $row[0].";
                }
                $conn = dbconn::open();
                $query = "SELECT SUM(betrag) AS summe
                          FROM tblZahlungen
                          WHERE personID={$adressat[$i]["tblPersonen.personID"]} AND typID=1 AND jahr<=" . date( "Y" ) . " AND datumEinzahlung='0000-00-00'";
                $result = $conn->query($query);

                dbconn::close( $conn );
                $row = $result->fetch(PDO::FETCH_ASSOC);
                if( $row[0] ) {
                    $zahlung_total = "CHF $row[0]";
                }

                $body = str_replace( array( "[ZAHLUNG_AUSSTEHEND]", "[ZAHLUNG_TOTAL]" ), array( $zahlung_ausstehend, $zahlung_total ), $body );
                $subject = str_replace( array( "[ZAHLUNG_AUSSTEHEND]", "[ZAHLUNG_TOTAL]" ), array( $zahlung_ausstehend, $zahlung_total ), $subject );

                $email = ( $adressat[$i]["tblPersonen.paymentChannelID"] == 1 ) ? $adressat[$i]["qryBeruf.email"] : $adressat[$i]["tblPersonen.email"];
            } else {
//                 $channel = array( "1"=>$adressat[$i]["tblPersonen.email"], "100"=>$adressat[$i]["qryBeruf.email"] );
//                 $email = $channel[$adressat[$i]["tblPersonen.channelID"]];
                $email = ( $adressat[$i]["tblPersonen.channelID"] == 100 ) ? $adressat[$i]["qryBeruf.email"] : $adressat[$i]["tblPersonen.email"];
            }

            if( !empty( $email ) ) {
                $n++;
                $email = ( $_SERVER["SERVER_NAME"] == "ggstof" ) ? "juri@localhost" : $email;
                $sent = mail( $email, $subject, $body, "From: $absenderText <$absender>\r\nContent-type: text/plain; charset=iso-8859-1\r\nX-Mailer: PHP/" . phpversion(), "-f$absender" );
                if( $sent ) {
                    $success++;
                } else {
                    $failure[] = $email;
                }
            }
        }

        $feedback = "$success von $n " . translate( "Mails wurden erfolgreich verschickt", false, false, false ) . ".";
        if( $f = count( $failure ) ) {
            $feedback .= "\n\n" . translate( "Bei folgenden Adressaten ist ein Fehler aufgetreten", false, false, false ) . ":";
            for( $i=0; $i<$f; $i++ ) {
                $feedback .= "\n" . $failure[$i];
            }
        }

        return array( ( $n ? $success/$n : 0 ), $feedback );
    }

    function strip_spaces( $str ) {
        return preg_replace( "/^[ \t]+|[ \t]+$/", "", $str );
    }
?>