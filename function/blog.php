<?php
    function blogLastEntries( $limit=1, $cat=NULL ) {
        $conn = dbconn::open( "ggstofch_blog" );
        $query = "SELECT ID,post_date,post_title,post_content,guid,comment_count
                  FROM wp_posts
                  LEFT JOIN wp_term_relationships ON wp_term_relationships.object_id=wp_posts.ID
                  WHERE post_status='publish' AND post_type='post'" . ( $cat ? " AND term_taxonomy_id=$cat" : "" ) . "
                  GROUP BY ID
                  ORDER BY post_date DESC LIMIT 0,$limit";
        $result = $conn->query($query);

        while( $row = $result->fetch(PDO::FETCH_ASSOC) ) {
            $entry[] = $row;
        }
        dbconn::close( $conn );

        return $entry;
    }

    function blogEntry( $id ) {
        $conn = dbconn::open( "ggstofch_blog" );
        $query = "SELECT ID,post_date,post_title,post_content,guid,comment_count
                  FROM wp_posts
                  WHERE ID=$id";
        $result = $conn->query($query);

        while( $row = $result->fetch(PDO::FETCH_ASSOC) ) {
            $entry[] = $row;
        }
        dbconn::close( $conn );

        return $entry;
    }
?>
