<?php
    function reset_password( $userID, $expiry=900 ) {
        $hash = sha1( $userID . microtime( true ) );
        $expiry += time();

        $conn = dbconn::open();

        $query = "INSERT INTO sysAuth (email,hash,expiry) VALUES ('$userID','$hash',$expiry)";
        $conn->query($query);


        dbconn::close( $conn );

        return $hash;
    }

    function userRights( $userID, $field=false, $value=false ) {
        $conn = dbconn::open();

        if( $userID ) {
            if( $field && $value ) {
                $query = "UPDATE tblPersonen SET $field='$value' WHERE email='$userID'";
                $result = $conn->query($query);

                $retval = !mysql_error();
            } else {
                $query = "SELECT personID,email AS userID,password,datenschutz,codex,roleID,role,organisationRegionID AS regionID,editProfile,viewNonpublicProfile,addProfile,closeProfile,confirmProfile,createList,exportData,editContent,uploadDocs,deleteDocs,viewPayment,editPayment
                          FROM tblPersonen
                          LEFT JOIN linkSysRoles ON tblPersonen.roleID=linkSysRoles.linkRoleID
                          LEFT JOIN (SELECT personID AS pID,organisationRegionID FROM tblVerein WHERE organisationRegionID BETWEEN 3 AND 10 AND datumEnde>=CURDATE()) AS qryVerein ON tblPersonen.personID=qryVerein.pID
                          WHERE email='$userID'";
                $result = $conn->query($query);
                if( mysql_error() ) {
//                     die( mysql_error() );
                    die( "Keine Berechtigung." );
                }
                $retval = $result->fetch(PDO::FETCH_ASSOC);
            }
        } else {
            $retval = array();
        }

        dbconn::close( $conn );

        return $retval;
    }

    function validate_password( $pw1, $pw2 ) {
        return strlen( $pw1 ) >= 8 &&
               preg_match( "/[a-z]+/", $pw1 ) > 0 &&
               preg_match( "/[A-Z]+/", $pw1 ) > 0 &&
               preg_match( "/[0-9]+/", $pw1 ) > 0 &&
               preg_match( "/[.,:;+\-_*!@$%&]+/", $pw1 ) > 0 &&
               $pw1 == $pw2;
    }
?>