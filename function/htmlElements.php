<?php
    function makeOptions( $linkTable, $selected=1, $inclEmpty=false, $override=false ) {
    // DEPRECATED - USE select CLASS INSTEAD
        global $lang;

        $language = $override ? $override : $lang;

        $conn = dbconn::open();
        $query = "SELECT * FROM $linkTable";
        $result = $conn->query($query);

        dbconn::close( $conn );

        if( $inclEmpty ) {
            echo "<option value=\"0\"";
            if( $selected == 0 ) {
                echo " selected=\"selected\"";
            }
            echo "></option>\n";
        }
        while( $row = $result->fetch(PDO::FETCH_ASSOC) ) {
            echo "<option value=\"$row[0]\"";
            if( $selected == $row[0] ) {
                echo " selected=\"selected\"";
            }
            echo ">" . translate( $row[1], $language ) . "</option>\n";
        }
    }
?>