<?php
    require_once( "tools.php" );

    function searchMember2( $criteria, $fields=array(), $order="tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort", $group="tblPersonen.personID", $hist=true, $count=false ) {
        $default = array( "tblPersonen"=>array( "personID", "name", "vorname", "vorname2", "ort", "spracheID", "aktualisierungID", "anredeID", "channelID", "mailChannelID", "postChannelID", "blogChannelID", "paymentChannelID", "rsID", "truppengattungID" ), "qryDienstgrad"=>array( "*" ), "linkPersonSprache"=>array( "sprache" ) );

        $select = array();
        $keys = array_keys( array_intersect_key( $default, $fields ) );
        foreach( $keys as $key ) {
            $select[$key] = array_unique( array_merge( $default[$key], $fields[$key] ) );
        }
        $select = array_merge( $select, array_diff_key( $default, $fields ), array_diff_key( $fields, $default ) );

        $include = array();
        $keys = array_keys( array_intersect_key( $criteria, $select ) );
        foreach( $keys as $key ) {
            $include[$key] = array_unique( array_merge( array_keys( $criteria[$key] ), array_values( $select[$key] ) ) );
        }
        $keys = array_keys( array_diff_key( $criteria, $select ) );
        foreach( $keys as $key ) {
            $include[$key] = array_keys( $criteria[$key] );
        }
        $keys = array_keys( array_diff_key( $select, $criteria ) );
        foreach( $keys as $key ) {
            $include[$key] = array_values( $select[$key] );
        }

        $query = "SELECT ";
        foreach( $select as $table=>$columns ) {
            foreach( $columns as $column ) {
                $query .= "$table.$column,";
            }
        }
        $query = substr( $query, 0, -1 );
        $query .= "
                FROM tblPersonen";
        if( array_key_exists( "linkPersonSprache", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID";
        }
        if( array_key_exists( "linkPersonVeroeffentlichung", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonVeroeffentlichung ON tblPersonen.veroeffentlichungID=linkPersonVeroeffentlichung.veroeffentlichungID";
        }
        if( array_key_exists( "linkPersonAktualisierung", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonAktualisierung ON tblPersonen.aktualisierungID=linkPersonAktualisierung.linkAktualisierungID";
        }
        if( array_key_exists( "linkPersonAnrede", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonAnrede ON tblPersonen.anredeID=linkPersonAnrede.linkAnredeID";
        }
        if( array_key_exists( "linkPersonChannel", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonChannel ON tblPersonen.channelID=linkPersonChannel.linkChannelID";
        }
        if( array_key_exists( "linkPersonChannelPost", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonChannelPost ON tblPersonen.postChannelID=linkPersonChannelPost.linkChannelID";
        }
        if( array_key_exists( "linkPersonChannelMail", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonChannelMail ON tblPersonen.mailChannelID=linkPersonChannelMail.linkChannelID";
        }
        if( array_key_exists( "linkBlogChannel", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkBlogChannel ON tblPersonen.blogChannelID=linkBlogChannel.linkChannelID";
        }
        if( array_key_exists( "linkZahlungenChannel", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkZahlungenChannel ON tblPersonen.paymentChannelID=linkZahlungenChannel.linkChannelID";
        }
        if( array_key_exists( "linkMilitaerRS", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkMilitaerRS ON tblPersonen.rsID=linkMilitaerRS.linkRsID";
        }
        if( array_key_exists( "linkMilitaerTruppengattung", $include ) !== false ) {
            $query .= "
                LEFT JOIN linkMilitaerTruppengattung ON tblPersonen.truppengattungID=linkMilitaerTruppengattung.linkTruppengattungID";
        }
        if( array_key_exists( "qryDienstgrad", $include ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,dienstgradID,dienstgrad,dienstgradText,zusatzDgID,zusatzDg.zusatz AS zusatzDg
                    FROM tblMilitaer
                    LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                    LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                    WHERE datumBeginn<=CURDATE() " . ( strpos( $checkstr, "qryDienstgrad.dienstgrad" ) !== false ? "AND datumEnde>=CURDATE()" : "" ) . "
                    ORDER BY datumBeginn DESC
                ) AS qryDienstgrad ON tblPersonen.personID=qryDienstgrad.personID";
        }
        if( array_key_exists( "qryMilitaer", $include ) !== false ) {
            $columns = array_unique( array_merge( array( "personID", "militaerID", "datumBeginn", "datumEnde", "dienstgradID", "zusatzFktID" ), $include["qryMilitaer"] ) );
            $query .= "
                LEFT JOIN (
                    SELECT " . implode( ",", $columns ) . "
                    FROM tblMilitaer
                    LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                    LEFT JOIN linkMilitaerZusatz AS zusatzFkt ON tblMilitaer.zusatzFktID=zusatzFkt.linkZusatzID
                    WHERE datumBeginn<=CURDATE() " . ( $hist ? "" : "AND datumEnde>=CURDATE()" ) . "
                    ORDER BY datumEnde DESC,datumBeginn DESC
                ) AS qryMilitaer ON tblPersonen.personID=qryMilitaer.personID";
        }
        if( array_key_exists( "qryBrevet", $include ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,MIN(datumBeginn) AS brevetierungsdatum
                    FROM tblMilitaer
                    WHERE dienstgradID BETWEEN 1 AND 2
                    GROUP BY personID
                ) AS qryBrevet ON tblPersonen.personID=qryBrevet.personID";
        }
        if( array_key_exists( "qryBeruf", $include ) !== false ) {
            $columns = array_unique( array_merge( array( "personID", "berufID", "datumBeginn", "datumEnde", "positionID", "typID" ), $include["qryBeruf"] ) );
            $query .= "
                LEFT JOIN (
                    SELECT " . implode( ",", $columns ) . "
                    FROM tblBeruf
                    LEFT JOIN linkBerufPosition ON tblBeruf.positionID=linkBerufPosition.linkPositionID
                    LEFT JOIN linkBerufTyp ON tblBeruf.typID=linkBerufTyp.linkTypID
                    WHERE datumBeginn<=CURDATE() " . ( $hist ? "" : "AND datumEnde>=CURDATE()" ) . "
                    ORDER BY email DESC,datumEnde DESC,datumBeginn DESC
                ) AS qryBeruf ON tblPersonen.personID=qryBeruf.personID";
        }
        if( array_key_exists( "qryBerufAusbildung", $include ) !== false ) {
            $columns = array_unique( array_merge( array( "personID", "ausbildungID", "datumBeginn", "datumEnde", "typID" ), $include["qryBerufAusbildung"] ) );
            $query .= "
                LEFT JOIN (
                    SELECT " . implode( ",", $columns ) . ",YEAR(datumEnde) AS abschlussjahr
                    FROM tblBerufAusbildung
                    LEFT JOIN linkBerufAusbildungTyp ON tblBerufAusbildung.typID=linkBerufAusbildungTyp.linkTypID
                    WHERE datumEnde<=CURDATE()
                    ORDER BY datumEnde DESC
                ) AS qryBerufAusbildung ON tblPersonen.personID=qryBerufAusbildung.personID";
        }
        if( array_key_exists( "tblPublic", $include ) !== false ) {
            $query .= "
                LEFT JOIN tblPublic ON tblPersonen.personID=tblPublic.personID";
        }
        if( array_key_exists( "qryVerein", $include ) !== false ) {
            $columns = array_unique( array_merge( array( "personID", "vereinID", "datumBeginn", "datumEnde", "funktionID", "organisationRegionID" ), $include["qryVerein"] ) );
            $query .= "
                LEFT JOIN (
                    SELECT " . implode( ",", $columns ) . "
                    FROM tblVerein
                    LEFT JOIN linkVereinFunktion ON tblVerein.funktionID=linkVereinFunktion.linkVereinFunktionID
                    LEFT JOIN linkVereinOrganisation ON tblVerein.organisationRegionID=linkVereinOrganisation.linkOrganisationID
                    WHERE datumBeginn<=CURDATE() " . ( $hist ? "" : "AND datumEnde>=CURDATE()" ) . "
                    ORDER BY datumEnde DESC,datumBeginn DESC
                ) AS qryVerein ON tblPersonen.personID=qryVerein.personID";
        }
        if( array_key_exists( "qryBeitritt", $include ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,MIN(datumBeginn) AS beitrittsdatum
                    FROM tblVerein
                    WHERE funktionID BETWEEN 1 AND 13 AND organisationRegionID=1
                    GROUP BY personID
                ) AS qryBeitritt ON tblPersonen.personID=qryBeitritt.personID";
        }
        if( array_key_exists( "qryAustritt", $include ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,MAX(datumEnde) AS austrittsdatum
                    FROM tblVerein
                    WHERE funktionID BETWEEN 1 AND 13 AND organisationRegionID=1
                    GROUP BY personID
                ) AS qryAustritt ON tblPersonen.personID=qryAustritt.personID";
        }
        if( array_key_exists( "qryZahlungen", $include ) !== false ) {
            $columns = array_unique( array_merge( array( "personID", "zahlungID", "datumBeginn", "datumEnde", "paymentChannelID" ), $include["qryZahlungen"] ) );
            $query .= "
                LEFT JOIN (
                    SELECT " . implode( ",", $columns ) . "
                    FROM tblZahlungen
                    LEFT JOIN linkZahlungenChannel ON tblZahlungen.paymentChannelID=linkZahlungenChannel.linkChannelID
                    ORDER BY datumRechnung DESC
                ) AS qryZahlungen ON tblPersonen.personID=qryZahlungen.personID";
        }
        $query .= "
                WHERE ";
        if( is_array( $criteria ) ) {
            foreach( $criteria as $table=>$columns ) {
                foreach( $columns as $column=>$value ) {
                    if( is_array( $value ) ) {
                        $query .= "$table.$column>='$value[0]' AND $table.$column<='$value[1]' AND ";
                    } else {
                        if( strpos( $value, "NULL" ) !== false ) {
                            $query .= "$table.$column IS NULL AND ";
                        } else {
                            $value = str_replace( array( "?", "*" ), array( "_", "%" ), addslashes( $value ) );
                            $op = ( strpos( $value, "%" ) !== false || strpos( $value, "_" ) !== false ) ? " LIKE " : "=";

                            $query .= "$table.$column$op'$value' AND ";
                        }
                    }
                }
            }
            $query = substr( $query, 0, -4 );
        } else {
            $query .= str_replace( array( "?", "*" ), array( "_", "%" ), $criteria ) . " ";
        }
        $query .= "GROUP BY $group
                   ORDER BY $order";
//         echo $query;
        $conn = dbconn::open();
        $result = $conn->query($query);

        dbconn::close( $conn );
//         unset( $criteria, $order, $group, $query, $key, $value, $op, $conn );
        if( $count ) {
            return mysql_num_rows( $result );
        }
        $retval = array();
        while( $row = mysql_fetch_alias_assoc( $result ) ) {
            $retval[] = $row;
        }
        return is_array( $retval ) ? $retval : array();
    }

    function searchMember( $criteria, $order=false, $group=false, $fields="tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache", $hist=true, $count=false ) {
        $checkstr = is_array( $criteria ) ? implode( array_keys( $criteria ) ) . " $order $group $fields" : "$criteria $order $group $fields";

        $query = "SELECT $fields
                  FROM tblPersonen";
        if( strpos( $checkstr, "linkPersonSprache" ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID";
        }
        if( strpos( $checkstr, "linkPersonVeroeffentlichung" ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonVeroeffentlichung ON tblPersonen.veroeffentlichungID=linkPersonVeroeffentlichung.veroeffentlichungID";
        }
        if( strpos( $checkstr, "linkPersonAktualisierung" ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonAktualisierung ON tblPersonen.aktualisierungID=linkPersonAktualisierung.linkAktualisierungID";
        }
        if( strpos( $checkstr, "linkPersonAnrede" ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonAnrede ON tblPersonen.anredeID=linkPersonAnrede.linkAnredeID";
        }
        if( strpos( $checkstr, "linkPersonChannel" ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonChannel ON tblPersonen.channelID=linkPersonChannel.linkChannelID";
        }
        if( strpos( $checkstr, "linkPersonChannelPost" ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonChannelPost ON tblPersonen.postChannelID=linkPersonChannelPost.linkChannelID";
        }
        if( strpos( $checkstr, "linkPersonChannelMail" ) !== false ) {
            $query .= "
                LEFT JOIN linkPersonChannelMail ON tblPersonen.mailChannelID=linkPersonChannelMail.linkChannelID";
        }
        if( strpos( $checkstr, "linkBlogChannel" ) !== false ) {
            $query .= "
                LEFT JOIN linkBlogChannel ON tblPersonen.blogChannelID=linkBlogChannel.linkChannelID";
        }
        if( strpos( $checkstr, "linkZahlungenChannel" ) !== false ) {
            $query .= "
                LEFT JOIN linkZahlungenChannel ON tblPersonen.paymentChannelID=linkZahlungenChannel.linkChannelID";
        }
        if( strpos( $checkstr, "linkMilitaerRS" ) !== false ) {
            $query .= "
                LEFT JOIN linkMilitaerRS ON tblPersonen.rsID=linkMilitaerRS.linkRsID";
        }
        if( strpos( $checkstr, "linkMilitaerTruppengattung" ) !== false ) {
            $query .= "
                LEFT JOIN linkMilitaerTruppengattung ON tblPersonen.truppengattungID=linkMilitaerTruppengattung.linkTruppengattungID";
        }
        if( strpos( $checkstr, "qryDienstgrad" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,dienstgradID,dienstgrad,dienstgradText,zusatzDgID,zusatzDg.zusatz AS zusatzDg
                    FROM tblMilitaer
                    LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                    LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                    WHERE datumBeginn<=CURDATE() " . ( strpos( $checkstr, "qryDienstgrad.dienstgrad" ) !== false ? "AND datumEnde>=CURDATE()" : "" ) . "
                    ORDER BY datumBeginn DESC
                ) AS qryDienstgrad ON tblPersonen.personID=qryDienstgrad.personID";
        }
        if( strpos( $checkstr, "qryMilitaer" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,militaerID,datumBeginn,datumEnde,dienstgradID,dienstgrad,dienstgradText,zusatzDgID,zusatzDg.zusatz AS zusatzDg,einteilung,funktion,vorgesVb,bemerkungen,zusatzFktID,zusatzFkt.zusatz AS zusatzFkt,RTRIM(CONCAT(funktion,' ',IFNULL(zusatzFkt.zusatz,''))) AS funktionMilitaerFull
                    FROM tblMilitaer
                    LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                    LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                    LEFT JOIN linkMilitaerZusatz AS zusatzFkt ON tblMilitaer.zusatzFktID=zusatzFkt.linkZusatzID
                    WHERE datumBeginn<=CURDATE() " . ( $hist ? "" : "AND datumEnde>=CURDATE()" ) . "
                    ORDER BY datumEnde DESC,datumBeginn DESC
                ) AS qryMilitaer ON tblPersonen.personID=qryMilitaer.personID";
        }
        if( strpos( $checkstr, "qryBrevet" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,MIN(datumBeginn) AS brevetierungsdatum
                    FROM tblMilitaer
                    WHERE dienstgradID BETWEEN 1 AND 2
                    GROUP BY personID
                ) AS qryBrevet ON tblPersonen.personID=qryBrevet.personID";
        }
        if( strpos( $checkstr, "qryBeruf" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT berufID,datumBeginn,datumEnde,organisation,abteilung,funktion,branche,bemerkungen,adresse,postfach,plz,ort,land,url,tf,email,tags,positionID,position,typID,typ,personID,titelBezeichnung,timestamp,modDateOwner,modDateAdmin,modifierID
                    FROM tblBeruf
                    LEFT JOIN linkBerufPosition ON tblBeruf.positionID=linkBerufPosition.linkPositionID
                    LEFT JOIN linkBerufTyp ON tblBeruf.typID=linkBerufTyp.linkTypID
                    WHERE datumBeginn<=CURDATE() " . ( $hist ? "" : "AND datumEnde>=CURDATE()" ) . "
                    ORDER BY email DESC,datumEnde DESC,datumBeginn DESC
                ) AS qryBeruf ON tblPersonen.personID=qryBeruf.personID";
        }
        if( strpos( $checkstr, "qryBerufAusbildung" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT *,YEAR(datumEnde) AS abschlussjahr
                    FROM tblBerufAusbildung
                    LEFT JOIN linkBerufAusbildungTyp ON tblBerufAusbildung.typID=linkBerufAusbildungTyp.linkTypID
                    WHERE datumEnde<=CURDATE()
                    ORDER BY datumEnde DESC
                ) AS qryBerufAusbildung ON tblPersonen.personID=qryBerufAusbildung.personID";
        }
        if( strpos( $checkstr, "tblPublic" ) !== false ) {
            $query .= "
                LEFT JOIN tblPublic ON tblPersonen.personID=tblPublic.personID";
        }
        if( strpos( $checkstr, "qryVerein" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT *
                    FROM tblVerein
                    LEFT JOIN linkVereinFunktion ON tblVerein.funktionID=linkVereinFunktion.linkVereinFunktionID
                    LEFT JOIN linkVereinOrganisation ON tblVerein.organisationRegionID=linkVereinOrganisation.linkOrganisationID
                    WHERE datumBeginn<=CURDATE() " . ( $hist ? "" : "AND datumEnde>=CURDATE()" ) . "
                    ORDER BY datumEnde DESC,datumBeginn DESC
                ) AS qryVerein ON tblPersonen.personID=qryVerein.personID";
        }
        if( strpos( $checkstr, "qryBeitritt" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,MIN(datumBeginn) AS beitrittsdatum
                    FROM tblVerein
                    WHERE funktionID BETWEEN 1 AND 13 AND organisationRegionID=1
                    GROUP BY personID
                ) AS qryBeitritt ON tblPersonen.personID=qryBeitritt.personID";
        }
        if( strpos( $checkstr, "qryAustritt" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT personID,MAX(datumEnde) AS austrittsdatum
                    FROM tblVerein
                    WHERE funktionID BETWEEN 1 AND 13 AND organisationRegionID=1
                    GROUP BY personID
                ) AS qryAustritt ON tblPersonen.personID=qryAustritt.personID";
        }
        if( strpos( $checkstr, "qryZahlungen" ) !== false ) {
            $query .= "
                LEFT JOIN (
                    SELECT *
                    FROM tblZahlungen
                    LEFT JOIN linkZahlungenChannel ON tblZahlungen.paymentChannelID=linkZahlungenChannel.linkChannelID
                    ORDER BY datumRechnung DESC
                ) AS qryZahlungen ON tblPersonen.personID=qryZahlungen.personID";
        }
        $query .= " WHERE ";
        if( is_array( $criteria ) ) {
            foreach( $criteria as $key => $value ) {
                if( is_array( $value ) ) {
                    $query .= "$key>='$value[0]' AND $key<='$value[1]' AND ";
                } else {
                    if( strpos( $value, "NULL" ) !== false ) {
                        $query .= "$key IS NULL AND ";
                    } else {
                        $value = str_replace( array( "?", "*" ), array( "_", "%" ), addslashes( $value ) );
                        $op = ( strpos( $value, "%" ) !== false || strpos( $value, "_" ) !== false ) ? " LIKE " : "=";

                        $query .= "$key$op'$value' AND ";
                    }
                }
            }
            $query = substr( $query, 0, -4 );
        } else {
            $query .= str_replace( array( "?", "*" ), array( "_", "%" ), $criteria ) . " ";
        }
        $query .= "GROUP BY " . ( $group ? $group : "tblPersonen.personID" );
        $query .= " ORDER BY " . ( $order ? $order : "tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort" );
//         echo $query;
        $conn = dbconn::open();
        $result = $conn->query($query);

        dbconn::close( $conn );
        unset( $criteria, $order, $group, $query, $key, $value, $op, $conn );
        if( $count ) {
            return mysql_num_rows( $result );
        }
        $retval = array();
        while( $row = mysql_fetch_alias_assoc( $result ) ) {
            $retval[] = $row;
        }
        return is_array( $retval ) ? $retval : array();
    }

    function searchPayment( $criteria, $order=false ) {
        $query = "SELECT tblZahlungen.*,tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache,linkZahlungenChannel.paymentChannel
                  FROM tblZahlungen
                  LEFT JOIN linkZahlungenChannel ON tblZahlungen.paymentChannelID=linkZahlungenChannel.linkChannelID
                  LEFT JOIN tblPersonen ON tblZahlungen.personID=tblPersonen.personID
                  LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID
                  LEFT JOIN (
                      SELECT personID,dienstgradID,dienstgrad,dienstgradText,zusatzDgID,zusatzDg.zusatz AS zusatzDg
                      FROM tblMilitaer
                      LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                      LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                      WHERE datumBeginn<=CURDATE()
                      ORDER BY datumBeginn DESC
                  ) AS qryDienstgrad ON tblZahlungen.personID=qryDienstgrad.personID
                  LEFT JOIN (
                      SELECT *
                      FROM tblVerein
                      LEFT JOIN linkVereinFunktion ON tblVerein.funktionID=linkVereinFunktion.linkVereinFunktionID
                      LEFT JOIN linkVereinOrganisation ON tblVerein.organisationRegionID=linkVereinOrganisation.linkOrganisationID
                      WHERE datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                      ORDER BY datumEnde DESC,datumBeginn DESC
                  ) AS qryVerein ON tblPersonen.personID=qryVerein.personID
                  WHERE ";
        foreach( $criteria as $key => $value ) {
            if( is_array( $value ) ) {
                $query .= "$key>='$value[0]' AND $key<='$value[1]' AND ";
            } else {
                if( strpos( $value, "NULL" ) !== false ) {
                    $query .= "$key IS $value AND ";
                } else {
                    $value = addslashes( $value );
                    $value = str_replace( array( "?", "*" ), array( "_", "%" ), $value );
                    $op = ( strpos( $value, "%" ) !== false || strpos( $value, "_" ) !== false ) ? " LIKE " : "=";

                    $query .= "$key$op'$value' AND ";
                }
            }
        }
        $query = substr( $query, 0, -4 ) . "GROUP BY tblZahlungen.personID,tblZahlungen.zahlungID ORDER BY ";
        $query .= $order ? $order : "tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
        $conn = dbconn::open();
        $result = $conn->query($query);

        dbconn::close( $conn );

        $retval = array();
        while( $row = mysql_fetch_alias_assoc( $result ) ) {
            $retval[] = $row;
        }

//         return is_array( $retval ) ? $retval : array();
        return $retval;
    }
?>