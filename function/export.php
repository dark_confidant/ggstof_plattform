<?php
    function export2pdf( $filename, $content ) {
        require_once( "class/pdfMemberProfile.php" );

        $pdf = new pdfMemberProfile( "L", "mm", "A4" );

        $pdf->Init();

        foreach( $content as $id ) {
            $pers = new person( $id );
            $pers->getMilitaryData();
            $pers->getJobData();
            $pers->getEducationData();
            $pers->getPublicData();
            $pers->getSocietyData();

            personPDF( $pdf, $pers );
        }

        $pdf->Output( "$filename.pdf", "D" );
    }

    function export2vcf( $pers ) {
        $pers->buildVcard();
    }

    function export2xls( $filename, $content, $headerrow=false ) {

        set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER["DOCUMENT_ROOT"] . "/module/pear/PEAR" );
        require_once( "Spreadsheet/Excel/Writer.php" );

        // Creating a workbook
        $workbook = new Spreadsheet_Excel_Writer();
        $workbook->SetVersion( 8 );
        // Creating a worksheet
        $worksheet =& $workbook->addWorksheet( $filename );

        // Creating the format
        $format_header =& $workbook->addFormat();
        $format_header->setBold();
        $format_header->setFgColor( "black" );
        $format_header->setPattern( 1 );
        $format_header->setColor( "white" );
        $format_header->setAlign( "center" );

        // The actual data
        $i = 0;
        if( $headerrow ) {
            for( $col=0; $col<count( $headerrow ); $col++ ) {
                $worksheet->write( 0, $col, $headerrow[$col], $format_header );
            }
            $i++;
        }
        
        for( $col=0; $col<count( $content ); $col++ ) {
            for( $row=$i; $row<count( $content[$col] )+$i; $row++ ) {
                $worksheet->write( $row, $col, $content[$col][$row-$i] );
            }
        }

        // sending HTTP headers
        $workbook->send( "$filename.xls" );

        // Let's send the file
        $workbook->close();
    }

    function personPDF( &$pdf, &$person ) {
        $pdf->AddPage();
            $pdf->SetCol( 0 );
                // Pers�nliche Informationen
                $pdf->SetFont( "Helvetica", "B", 10 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Persönliche Informationen", $person->person->sprache, false, false ) , 0, 1 );
                $pdf->Ln();
                if( file_exists( "{$_SERVER["DOCUMENT_ROOT"]}/image/profile/{$person->id}.jpg" ) ) {
                    $pdf->Image( "{$_SERVER["DOCUMENT_ROOT"]}/image/profile/{$person->id}.jpg", NULL, NULL, 0, 50 );
                } else {
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Bild", $person->person->sprache, false, false )  . "]" );
                    $pdf->Ln( 50 );
                }
                $pdf->Ln();
                $pdf->SetFont( "Helvetica", "", 10 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( $person->militaer[0]->dienstgrad, $person->person->sprache, false, false ) . " " . translate( $person->militaer[0]->zusatzDg, $person->person->sprache, false, false ) , 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  $person->person->vorname . " " . $person->person->vorname2 , 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  $person->person->name , 0, 1 );
                $pdf->Ln();
                $pdf->MakeCell( "Cell", $person->person->geburtsdatum,  translate( "Geburtsdatum", $person->person->sprache, false, false  ) . ": " . $person->person->geburtsdatum, "[" .  translate( "Geburtsdatum", $person->person->sprache, false, false  ) . "]" );
                $pdf->Ln();
                $pdf->SetFont( "Helvetica", "U", 10 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Wohnadresse", $person->person->sprache, false, false  ) . ":", 0, 1 );
                $pdf->SetFont( "Helvetica", "", 10 );
                $pdf->MakeCell( "Cell", $person->person->adresse,  $person->person->adresse , "[" .  translate( "Strasse und Hausnummer", $person->person->sprache, false, false  ) . "]" );
                $pdf->MakeCell( "Cell", $person->person->postfach, $person->person->postfach , "[" .  translate( "Postfach", $person->person->sprache, false, false  ) . "]" );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, ( $person->person->plz ? $person->person->plz : "[" . translate( "Postleitzahl", $person->person->sprache, false, false ) . "]" ) . " " . ( $person->person->ort ?  $person->person->ort  : "[" .  translate( "Ort", $person->person->sprache, false, false  ) . "]" ) . ( !in_array( $person->person->land, array( "Schweiz", "Suisse", "Svizzera", "Switzerland" ) ) ?  ", " . $person->person->land  : "" ), 0, 1 );
                $pdf->MakeCell( "Cell", $person->person->tf, $person->person->tf, "[" .  translate( "Telefon Festnetz", $person->person->sprache, false, false  ) . "]" );
                $pdf->MakeCell( "Cell", $person->person->mobil, $person->person->mobil, "[" .  translate( "Telefon mobil", $person->person->sprache, false, false  ) . "]" );
                $pdf->Ln();
                $pdf->MakeCell( "Cell", $person->person->email, $person->person->email . " (=" .  translate( "Username", $person->person->sprache, false, false  ) . ")", "[" .  translate( "E-Mail privat", $person->person->sprache, false, false  ) . "] (=" .  translate( "Username", $person->person->sprache, false, false  ) . ")" );
                $pdf->MakeCell( "Cell", $person->person->url,  $person->person->url , "[" .  translate( "Website privat", $person->person->sprache, false, false  ) . "]" );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Xing-Profil", $person->person->sprache, false, false  ) . ": " .  translate( ( $person->person->xing ? "Ja" : "Nein" ), $person->person->sprache, false, false ) , 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "LinkedIn-Profil", $person->person->sprache, false, false  ) . ": " .  translate( ( $person->person->linkedin ? "Ja" : "Nein" ), $person->person->sprache, false, false ) , 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "facebook-Profil", $person->person->sprache, false, false  ) . ": " .  translate( ( $person->person->facebook ? "Ja" : "Nein" ), $person->person->sprache, false, false  ), 0, 1 );
                $pdf->Ln();
//                 $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, ( translate( "Bitte senden Sie mir Informationen prim�r an meine", $person->person->sprache, false, false  ) . ": " . ( translate( $person->person->channel, $person->person->sprache, false, false  ), 0, 1 );
//                 $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, ( translate( "Senden Sie mir bitte die Rechnung", $person->person->sprache, false, false  ) . ": " . ( translate( $person->person->paymentChannel, $person->person->sprache, false, false  ), 0, 1 );
//                 $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, ( translate( "Hinweis auf neue Blog-Beitr�ge", $person->person->sprache, false, false  ) . ": " . ( translate( $person->person->blogChannel, $person->person->sprache, false, false  ), 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Bitte senden Sie mir Informationen primär an meine", $person->person->sprache, false, false  ) . ": ", 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "    " .  translate( $person->person->channel, $person->person->sprache, false, false  ), 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Senden Sie mir bitte die Rechnung", $person->person->sprache, false, false  ) . ": ", 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "    " .  translate( $person->person->paymentChannel, $person->person->sprache, false, false  ), 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Hinweis auf neue Blog-Beiträge", $person->person->sprache, false, false  ) . ": ", 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "    " .  translate( $person->person->blogChannel, $person->person->sprache, false, false  ), 0, 1 );
//                 $pdf->Ln();
                $pdf->Ln();
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Mitglied der GGstOf seit", $person->person->sprache, false, false  ) . ": " . $person->person->beitrittsdatum, 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Letzte Profilaktualisierung", $person->person->sprache, false, false  ) . ": " . $person->person->modDateOwner, 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Anzeige", $person->person->sprache, false, false ) . ": " . translate( $person->person->veroeffentlichung, $person->person->sprache, false, false ) , 0, 1 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Passwort", $person->person->sprache, false, false ) . " " . translate( ( $person->person->password ? "vergeben" : "nicht definiert" ), $person->person->sprache, false, false )  );

            $pdf->SetCol( 1 );
                // Milit�r
                $pdf->SetFont( "Helvetica", "B", 10 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Militärische Laufbahn", $person->person->sprache, false, false  ), 0, 1 );
                $pdf->Ln();
                $pdf->SetFont( "Helvetica", "", 10 );
                $i = 0;
                $next = true;
                //while( $person->militaer[$i]->datumBeginn && $next ) {
                while($i<count($person->militaer)){
                    if( $person->militaer[$i]->datumBeginn > date( "Y-m-d" ) ) {
                        $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Ab", $person->person->sprache, false, false  ) . " " . $person->militaer[$i]->datumBeginn, 0, 1 );
                    } else {
                        $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, $person->militaer[$i]->datumBeginn . " " .  translate( "bis", $person->person->sprache, false, false  ) . " " . str_replace( "9999-12-31",  translate( "heute", $person->person->sprache, false, false  ), $person->militaer[$i]->datumEnde ), 0, 1 );
                    }
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( $person->militaer[$i]->dienstgrad, $person->person->sprache, false, false  ) . " " .  translate( $person->militaer[$i]->zusatzDg, $person->person->sprache, false, false  ), 0, 1 );
                    $pdf->MakeCell( "Cell", $person->militaer[$i]->funktion,  $person->militaer[$i]->funktion . " " . $person->militaer[$i]->zusatzFkt , "[" .  translate( "Funktion", $person->person->sprache, false, false ) . "]" );
                    $pdf->MakeCell( "Cell", $person->militaer[$i]->einteilung,  $person->militaer[$i]->einteilung , "[" .  translate( "Einteilung", $person->person->sprache, false, false  ) . "]" );
                    $pdf->MakeCell( "MultiCell", $person->militaer[$i]->bemerkungen,  $person->militaer[$i]->bemerkungen , "[" .  translate( "Bemerkungen", $person->person->sprache, false, false  ) . "]" );
                    $pdf->Ln();
                    $next = $pdf->NewCol( 2 );
                    $i++;
                }
                if( $pdf->NewCol( 2 ) ) {
                    $pdf->SetTextColor( 127 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Zeitraum", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Dienstgrad", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Funktion", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Einteilung", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Bemerkungen", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->SetTextColor( 0 );
                    $pdf->Ln();
                }
                $pdf->Ln();

                // Milit�rische Heimat
                if( $pdf->NewCol( 2 ) ) {
                    $pdf->SetFont( "Helvetica", "U", 10 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Militärische Heimat", $person->person->sprache, false, false ) . ":", 0, 1 );
                    $pdf->SetFont( "Helvetica", "", 10 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Truppengattung", $person->person->sprache, false, false ) . ": " .  $person->person->truppengattung , 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "RS als", $person->person->sprache, false, false ) . ": " .  $person->person->rs , 0, 1 );
                }

        $pdf->AddPage();
            $pdf->SetCol( 0 );
                // Beruf
                $pdf->SetFont( "Helvetica", "B", 10 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Berufliche Laufbahn", $person->person->sprache, false, false ), 0, 1 );
                $pdf->Ln();
                $pdf->SetFont( "Helvetica", "", 10 );
                $i = 0;
                $next = true;

                while($i<count($person->beruf)) {
                    if( $person->beruf[$i]->datumBeginn > date( "Y-m-d" ) ) {
                        $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Ab", $person->person->sprache, false, false ) . " " . $person->beruf[$i]->datumBeginn, 0, 1 );
                    } else {
                        $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, $person->beruf[$i]->datumBeginn . " " .  translate( "bis", $person->person->sprache, false, false ) . " " . str_replace( "9999-12-31",  translate( "heute", $person->person->sprache, false, false ), $person->beruf[$i]->datumEnde ), 0, 1 );
                    }
                    $pdf->MakeCell( "Cell", $person->beruf[$i]->typ,  $person->beruf[$i]->typ , "[" .  translate( "Typ", $person->person->sprache, false, false ) . " (" . translate( "Privatwirtschaft", $person->person->sprache, false, false ) . ", " . translate( "Staatsangestellter", $person->person->sprache, false, false ) . ", " . translate( "Berufsoffizier", $person->person->sprache, false, false ) . ", " . translate( "Pensionär", $person->person->sprache, false, false ) . ")]" );
                    $pdf->MakeCell( "Cell", $person->beruf[$i]->branche,  $person->beruf[$i]->branche , "[" .  translate( "Branche", $person->person->sprache, false, false ) . "]" );
                    $pdf->MakeCell( "Cell", $person->beruf[$i]->organisation,  $person->beruf[$i]->organisation , "[" .  translate( "Organisation", $person->person->sprache, false, false ) . "]" );
                    $pdf->MakeCell( "Cell", $person->beruf[$i]->abteilung,  $person->beruf[$i]->abteilung , "[" .  translate( "Abteilung", $person->person->sprache, false, false ) . "]" );
                    $pdf->MakeCell( "Cell", $person->beruf[$i]->funktion,  $person->beruf[$i]->funktion , "[" .  translate( "Funktion", $person->person->sprache, false, false ) . "]" );
                    $pdf->MakeCell( "Cell", $person->beruf[$i]->position,  $person->beruf[$i]->position , "[" .  translate( "Position", $person->person->sprache, false, false ) . "(" . translate( "Führungsposition", $person->person->sprache, false, false ) . ", " . translate( "Mitarbeiter", $person->person->sprache, false, false ) . "...)]" );
                    if( $person->beruf[$i]->datumEnde == "9999-12-31" && $person->beruf[$i]->typID != 6 ) {
                        $pdf->MakeCell( "Cell", $person->beruf[$i]->adresse,  $person->beruf[$i]->adresse , "[" .  translate( "Adresse", $person->person->sprache, false, false ) . "]" );
                        $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, ( $person->beruf[$i]->plz ? $person->beruf[$i]->plz : "[" .  translate( "Postleitzahl", $person->person->sprache, false, false  ) . "]" ) . " " . ( $person->beruf[$i]->ort ?  $person->beruf[$i]->ort  : "[" .  translate( "Ort", $person->person->sprache, false, false  ) . "]" ) . ( !in_array( $person->beruf[$i]->land, array( "Schweiz", "Suisse", "Svizzera", "Switzerland" ) ) ?  ", " . $person->beruf[$i]->land  : "" ), 0, 1 );
                        $pdf->MakeCell( "Cell", $person->beruf[$i]->tf, $person->beruf[$i]->tf, "[" .  translate( "Telefon geschäftlich", $person->person->sprache, false, false ) . "]" );
                        $pdf->MakeCell( "Cell", $person->beruf[$i]->email, $person->beruf[$i]->email, "[" .  translate( "E-Mail geschäftlich", $person->person->sprache, false, false ) . "]" );
                    }
                    $pdf->Ln();
                    $next = $pdf->NewCol( 1 );
                    $i++;
                }
                if( $pdf->NewCol( 1 ) ) {
                    $pdf->SetTextColor( 127 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Zeitraum", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Typ", $person->person->sprache, false, false ) . " (" . translate( "Privatwirtschaft", $person->person->sprache, false, false ) . ", " . translate( "Staatsangestellter", $person->person->sprache, false, false ) . ", " . translate( "Berufsoffizier", $person->person->sprache, false, false ) . ", " . translate( "Pensionär", $person->person->sprache, false, false ) . ")]", 0, 1 ) ;
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Branche", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Organisation", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Abteilung", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Funktion", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Position", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->SetTextColor( 0 );
                    $pdf->Ln();
                }
                $pdf->Ln();

                // Abschl�sse
                $pdf->NewCol( 1 );
                $pdf->SetFont( "Helvetica", "B", 10 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Abschlüsse", $person->person->sprache, false, false ), 0, 1 );
                $pdf->Ln();
                $pdf->SetFont( "Helvetica", "", 10 );
                $i = 0;
                $next = true;
                while( $i<count($person->ausbildung)) {
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, $person->ausbildung[$i]->datumEnde, 0, 1 );
                    $pdf->MakeCell( "Cell", $person->ausbildung[$i]->institution,  $person->ausbildung[$i]->institution , "[" . translate( "Institution", $person->person->sprache, false, false ) . "]" );
                    $pdf->MakeCell( "Cell", $person->ausbildung[$i]->titel,  $person->ausbildung[$i]->titel , "[" .  translate( "Titel", $person->person->sprache, false, false ) . "]" );
                    $pdf->Ln();
                    $next = $pdf->NewCol( 1 );
                    $i++;
                }
                if( $pdf->NewCol( 1 ) ) {
                    $pdf->SetTextColor( 127 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Datum", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Institution", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Titel", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->SetTextColor( 0 );
                }

            $pdf->SetCol( 2 );
                // Vereine
                $pdf->SetFont( "Helvetica", "B", 10 );
                $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Vereine & Organisationen", $person->person->sprache, false, false ), 0, 1 );
                $pdf->Ln();
                $pdf->SetFont( "Helvetica", "", 10 );
                $i = 0;
                $next = true;
                while( $i<count($person->public)) {
                    if( $person->public[$i]->datumBeginn > date( "Y-m-d" ) ) {
                        $pdf->Cell( $pdf->colWidth, $pdf->lineHeight,  translate( "Ab", $person->person->sprache, false, false ) . " " . $person->public[$i]->datumBeginn, 0, 1 );
                    } else {
                        $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, $person->public[$i]->datumBeginn . " " .  translate( "bis", $person->person->sprache, false, false ) . " " . str_replace( "9999-12-31",  translate( "heute", $person->person->sprache, false, false ), $person->public[$i]->datumEnde ), 0, 1 );
                    }
                    $pdf->MakeCell( "Cell", $person->public[$i]->publicFunktion,  $person->public[$i]->publicFunktion , "[" .  translate( "Funktion", $person->person->sprache, false, false ) . "]" );
                    $pdf->MakeCell( "Cell", $person->public[$i]->organisation,  $person->public[$i]->organisation , "[" .  translate( "Organisation", $person->person->sprache, false, false ) . "]" );
                    $pdf->Ln();
                    $next = $pdf->NewCol( 2 );
                    $i++;
                }
                if( $pdf->NewCol( 2 ) ) {
                    $pdf->SetTextColor( 127 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Zeitraum", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Funktion", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->Cell( $pdf->colWidth, $pdf->lineHeight, "[" .  translate( "Organisation", $person->person->sprache, false, false ) . "]", 0, 1 );
                    $pdf->SetTextColor( 0 );
                }
    }

    function invoicePDF( &$pdf, &$person, $channel ) {
    // DEPRECATED - USE pdfLetterInvoice OBJECT INSTEAD
        /*require_once( $_SERVER["DOCUMENT_ROOT"] . "/function/contentManagement.php" );

        $person->getMilitaryData();
        $person->getJobData();

        $pdf->AddPage();

        $m = 0;
        while( $person->militaer[$m]->datumBeginn > date( "Y-m-d" ) {
            $m++;
        }
        $b = 0;
        while( $person->beruf[$b]->datumBeginn > date( "Y-m-d" ) {
            $b++;
        }

        $dienstgrad = strip_spaces( translate( $person->militaer[$m]->dienstgrad, $person->person->sprache, false, false ) . " " . translate( $person->militaer[$m]->zusatzDg, $person->person->sprache, false, false );
        $vorname  = strip_spaces( "{$person->person->vorname} {$person->person->vorname2}" );
        $name     = $person->person->name;
        $adresse  = $channel == 4 ? "{$person->beruf[$b]->organisation}\n{$person->beruf[$b]->adresse}" : $person->person->adresse;
        $postfach = $channel == 4 ? $person->beruf[$b]->postfach : $person->person->postfach;
        $adresse .= $postfach ? "\n$postfach" : "";
        $ort      = $channel == 4 ? "{$person->beruf[$b]->plz} {$person->beruf[$b]->ort}" : "{$person->person->plz} {$person->person->ort}";
        $land = str_replace( array( "Schweiz", "Suisse", "Svizzera", "Switzerland" ), "", ( $channel == 4 ? $person->beruf[$b]->land : $person->person->land );
        $anrede = translate( $person->person->anrede, $person->person->sprache, false, false ) . " " . translate( $person->militaer[$m]->dienstgradText, $person->person->sprache, false, false );
        $anredeText = translate( $person->person->anredeText, $person->person->sprache, false, false );
        $dienstgradText = translate( $person->militaer[$m]->dienstgradText, $person->person->sprache, false, false );

        $conn = dbconn::open();
        $query = "SELECT SUM(betrag) AS summe
                  FROM tblZahlungen
                  WHERE personID={$person->id} AND typID=1 AND jahr<" . date( "Y" ) . " AND datumEinzahlung='0000-00-00'";
        $result = $conn->query($query);

        dbconn::close( $conn );
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if( $row[0] ) {
            $zahlung_ausstehend = translate( 334, $person->person->sprache, false, false ) . " CHF $row[0].";
        }
        $conn = dbconn::open();
        $query = "SELECT SUM(betrag) AS summe
                  FROM tblZahlungen
                  WHERE personID={$person->id} AND typID=1 AND jahr<=" . date( "Y" ) . " AND datumEinzahlung='0000-00-00'";
        $result = $conn->query($query);

        dbconn::close( $conn );
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if( $row[0] ) {
            $zahlung_total = "CHF $row[0]";
        }
        $content = getEmailContent( 29, $person->person->sprache );
        $subject = str_replace( array( "[MEMBER_ID]", "[NAME]", "[DG]", "[DG_TXT]", "[ANREDE]", "[ANREDE_TXT]", "[EMAIL]", "[REC_INVOICE]", "[ZAHLUNG_AUSSTEHEND]", "[ZAHLUNG_TOTAL]" ), array( $person->person->personID, $name, $dienstgrad, $dienstgradText, $anrede, $anredeText, $person->person->email, translate( $person->person->paymentChannel, $person->person->sprache, false, false ), $zahlung_ausstehend, $zahlung_total ), $content["subject"] );
        $body = str_replace( array( "[MEMBER_ID]", "[NAME]", "[DG]", "[DG_TXT]", "[ANREDE]", "[ANREDE_TXT]", "[EMAIL]", "[REC_INVOICE]", "[ZAHLUNG_AUSSTEHEND]", "[ZAHLUNG_TOTAL]" ), array( $person->person->personID, $name, $dienstgrad, $dienstgradText, $anrede, $anredeText, $person->person->email, translate( $person->person->paymentChannel, $person->person->sprache, false, false ), $zahlung_ausstehend, $zahlung_total ), $content["body"] );

        $pdf->SetFont( "Helvetica", "", 10 );
        $pdf->SetY( $pdf->y1 );
        $pdf->MultiCell( $pdf->ColWidth, $pdf->lineHeight,  "$dienstgrad\n$vorname $name\n$adresse\n$ort\n$land" );

        $pdf->SetFont( "Helvetica", "B", 10 );
        $pdf->SetY( $pdf->y1+35 );
        $pdf->Cell( 0, 0,  $subject );

        $pdf->SetFont( "Helvetica", "", 10 );
        $pdf->SetY( $pdf->y1+48 );
        $pdf->MultiCell( $pdf->ColWidth, $pdf->lineHeight,  $body );
        $pdf->Ln();
        $pdf->MultiCell( $pdf->ColWidth, $pdf->lineHeight,  str_replace( "�", "�", strtoupper( translate( "Gesellschaft der Generalstabsoffiziere", $person->person->sprache, false, false ) ) . "\n" . translate( "Der Kassier", $person->person->sprache, false, false ) );
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell( $pdf->ColWidth, $pdf->lineHeight,  $pdf->kassier );

        $pdf->SetXY( $pdf->x1, ($pdf->y0)+3 );
        $pdf->SetFont( "Helvetica", "B", 12 );
        $pdf->Cell( 122, $pdf->lineHeight,  translate( "Gesellschaft der Generalstabsoffiziere", $person->person->sprache, false, false ), 0, 0, "R" );
        $pdf->SetX( -41 );
        $pdf->Cell( 0, $pdf->lineHeight, "GGstOf" );

        $pdf->SetXY( $pdf->x1, $pdf->y0+11 );
        $pdf->SetFont( "Helvetica", "", 8 );
        $pdf->MultiCell( 122, $pdf->lineHeight,  "{$pdf->kassier}, " . translate( "Kassier", $person->person->sprache, false, false ) . "\nkassier@ggstof.ch" ), 0, "R" );
        $pdf->SetXY( -41, $pdf->y0+11 );
        $pdf->MultiCell( 20, $pdf->lineHeight, "\n" . date( "d.m.Y" ) );*/
    }
?>