<?php
    function getMembershipfee( $jahr ) {
        $conn = dbconn::open();
        $query = "SELECT betrag FROM tblZahlungenJahresbeitrag WHERE jahr=$jahr";
        $result = $conn->query($query);

        dbconn::close( $conn );

        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row[0];
    }

    function setMembershipfee( $jahr, $betrag ) {
        $conn = dbconn::open();

        $query = "INSERT INTO tblZahlungenJahresbeitrag (jahr,betrag) VALUES ($jahr,$betrag)";
        $conn->query($query);


        dbconn::close( $conn );
    }

    function invoice( $personID, $jahr, $datumRechnung, $paymentChannelID ) {
        $conn = dbconn::open();

        $modifierID = $_SESSION["personID"];
        for( $i=0; $i<count( $personID ); $i++ ) {
            $query = "INSERT INTO tblZahlungen (personID,typID,jahr,datumRechnung,paymentChannelID,betrag,modifierID) VALUES ({$personID[$i]},1,{$jahr[$i]},'{$datumRechnung[$i]}',{$paymentChannelID[$i]},(SELECT betrag FROM tblZahlungenJahresbeitrag WHERE jahr={$jahr[$i]}),$modifierID)";
            $conn->query($query);
            $retval .= mysql_error();
        }

        dbconn::close( $conn );

        return empty( $retval ) ? false : $retval;
    }

    function payment( $zahlungID, $datumEinzahlung ) {
        $conn = dbconn::open();

        $modifierID = $_SESSION["personID"];
        for( $i=0; $i<count( $zahlungID ); $i++ ) {
            if( !empty( $datumEinzahlung[$i] ) ) {
                $query = "UPDATE tblZahlungen SET datumEinzahlung='{$datumEinzahlung[$i]}',modifierID=$modifierID WHERE zahlungID={$zahlungID[$i]}";
// echo "$query<br />";
                $result = $conn->query($query);
                $retval .= mysql_error();
            }
        }

        dbconn::close( $conn );

        return empty( $retval ) ? false : $retval;
    }
?>