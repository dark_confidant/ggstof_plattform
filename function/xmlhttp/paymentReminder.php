<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }
    require_once( "../lang.php" );
    require_once( "../search.php" );

    if( isset( $_POST["zahlungID"] ) ) {
        $criteria = array( "tblZahlungen.zahlungID"=>$_POST["zahlungID"] );
        $result = searchPayment( $criteria );

        $conn = dbconn::open();
        $query = "UPDATE tblZahlungen SET anzahlMahnungen=anzahlMahnungen+1,datumMahnung=CURDATE() WHERE zahlungID={$_POST["zahlungID"]}";
        $conn->query($query);

        dbconn::close( $conn );

        $paymentChannelID = $_POST["paymentChannelID"] ? $_POST["paymentChannelID"] : $result[0]["tblZahlungen.paymentChannelID"];

        if( in_array( $paymentChannelID, array( 1, 3 ) ) ) {
            $email = new email( 20, $result[0]["tblZahlungen.personID"], $paymentChannelID );
            $email->addCC( "kassier@ggstof.ch" );
            $email->send();
        }
        if( in_array( $paymentChannelID, array( 2, 4 ) ) ) {
            $pdf = new pdfLetterPaymentReminder( "P", "mm", "A4" );
            $pdf->Init();
            $pdf->Compose( $result[0]["tblZahlungen.personID"], 2 );
            $emailPDF = new email( 41, $result[0]["tblZahlungen.personID"], $paymentChannelID );
            $emailPDF->addAttachment( &$pdf );
            $emailPDF->addCC( "kassier@ggstof.ch" );
            $emailPDF->send();
        }
        if( in_array( $paymentChannelID, array( 5, 6 ) ) ) {
            $pdfPost = new pdfLetterPaymentReminder( "P", "mm", "A4" );
            $pdfPost->Init();
            $pdfPost->Compose( $result[0]["tblZahlungen.personID"], $paymentChannelID );
            $pdfPost->Download();
//             $pdfPost->Download( "Mahnung-{$_POST["zahlungID"]}-" . date( "Y-m-d" ) . ".pdf" );
        }

//         echo utf8_encode( translate( "Mahnung verschickt.", false, false, false ) );
    }
?>