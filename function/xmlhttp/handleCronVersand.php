<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }

    $conn = dbconn::open();
    if( $_POST["action"] == "UPDATE" && isset( $_POST["cronID"] ) && isset( $_POST["name"] ) && isset( $_POST["conditionID"] ) && isset( $_POST["active"] ) ) {
        $query = "UPDATE sysCronVersand
                  SET name='" . utf8_decode( $_POST["name"] ) . "',month={$_POST["month"]},day={$_POST["day"]},weekday={$_POST["weekday"]},recipientsID={$_POST["recipientsID"]},conditionID={$_POST["conditionID"]},emailID={$_POST["emailID"]},active={$_POST["active"]}
                  WHERE cronID={$_POST["cronID"]}";
    } elseif( $_POST["action"] == "INSERT" && isset( $_POST["name"] ) && isset( $_POST["conditionID"] ) && isset( $_POST["active"] ) ) {
        $query = "INSERT INTO sysCronVersand
                  (name,month,day,weekday,recipientsID,conditionID,emailID,active) VALUES ('" . utf8_decode( $_POST["name"] ) . "',{$_POST["month"]},{$_POST["day"]},{$_POST["weekday"]},{$_POST["recipientsID"]},{$_POST["conditionID"]},{$_POST["emailID"]},{$_POST["active"]})";
    } elseif( $_POST["action"] == "DELETE" && isset( $_POST["cronID"] ) ) {
        $query = "DELETE FROM sysCronVersand
                  WHERE cronID={$_POST["cronID"]}";
    }
    $conn->query($query);

    dbconn::close( $conn );
?>