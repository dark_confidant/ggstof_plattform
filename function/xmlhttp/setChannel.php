<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }
    require_once( "../lang.php" );

    if( isset( $_POST["member"] ) && isset( $_POST["channelID"] ) && isset( $_POST["postChannelID"] ) && isset( $_POST["mailChannelID"] ) && isset( $_POST["paymentChannelID"] ) && isset( $_POST["blogChannelID"] ) ) {
        $conn = dbconn::open();
        $query = "UPDATE tblPersonen SET channelID={$_POST["channelID"]},postChannelID={$_POST["postChannelID"]},mailChannelID={$_POST["mailChannelID"]},paymentChannelID={$_POST["paymentChannelID"]},blogChannelID={$_POST["blogChannelID"]} WHERE personID={$_POST["member"]}";
        $result = $conn->query($query);

        dbconn::close( $conn );
    }
?>