<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }
    $_SERVER["DOCUMENT_ROOT"] = $_SERVER["DOCUMENT_ROOT"].'/plattform';
    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }
    foreach( glob( "../../class/site/*.php" ) as $cls ) {
        require_once( $cls );
    }
    foreach( glob( "../*.php" ) as $fnc ) {
        require_once( $fnc );
    }
    //require_once( "../list.php" );
    //require_once( "../search.php" );

    $email = new email( $_POST["id"] );

    if( isset( $_POST["recipientsID"] ) ) {
        if( $_POST["recipientsID"] ){
            $conn = dbconn::open();
            $query = "SELECT listID,param
                      FROM sysRecipients
                      WHERE recipientsID={$_POST["recipientsID"]}";
            $result = $conn->query($query);

            dbconn::close( $conn );

            $row = $result->fetch(PDO::FETCH_NUM);

            if( $row[0] ) {
                $adressaten = memberList( intval($row[0]), $row[1] );
            }
        } else {
            $adressaten = array( array( "tblPersonen.personID"=>$_SESSION["personID"] ) );
        }

//         case "0":
//             $criteria = array( "tblPersonen.personID"=>$_SESSION["personID"] );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L1":
//             // Alle Mitglieder
// //             $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
// //             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             $criteria = array( "qryVerein"=>array( "datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "organisationRegionID"=>1, "funktionID"=>1 ) );
//             $search = new memberSearch( $criteria );
//             $adressaten = $search->execute();
//             break;
// 
//         case "L2":
//             // Blog-Empf�nger
//             $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "tblPersonen.blogChannelID"=>array( 2, 3 ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L3":
//             // Alle lebenden Gst Of
//             $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00" );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L4":
//             // Region Rh�ne-L�man
//             $criteria = array( "qryVerein.organisationRegionID"=>3, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L5":
//             // Region Jura
//             $criteria = array( "qryVerein.organisationRegionID"=>4, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L6":
//             // Region Ticino
//             $criteria = array( "qryVerein.organisationRegionID"=>5, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L7":
//             // Region Bern
//             $criteria = array( "qryVerein.organisationRegionID"=>6, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L8":
//             // Region Basel
//             $criteria = array( "qryVerein.organisationRegionID"=>7, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L9":
//             // Region Luzern
//             $criteria = array( "qryVerein.organisationRegionID"=>8, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L10":
//             // Region Z�rich
//             $criteria = array( "qryVerein.organisationRegionID"=>9, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L11":
//             // Region Ostschweiz
//             $criteria = array( "qryVerein.organisationRegionID"=>10, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L12":
//             // Typ Privatwirtschaft
//             $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryBeruf.typID"=>1 );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID", false );
//             break;
// 
//         case "L13":
//             // Typ Berufsoffiziere
//             $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryBeruf.typID"=>3 );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID", false );
//             break;
// 
//         case "L14":
//             // Typ Verwaltung
//             $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryBeruf.typID"=>2 );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID", false );
//             break;
// 
//         case "L15":
//             // Typ Pension�re
//             $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryBeruf.typID"=>6 );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID", false );
//             break;
// 
//         case "L16":
//             // Typ Verband/Organisation
//             $criteria = array( "tblPersonen.todesdatum"=>"0000-00-00", "qryBeruf.typID"=>4 );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID", false );
//             break;
// 
//         case "L17":
//             // Kdt, Mitglieder
//             $criteria = array( "qryMilitaer.funktion"=>"?dt", "qryMilitaer.zusatzFktID"=>1, "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
// //             $include = searchMember( $criteria, false, false, "tblPersonen.personID" );
// //             $criteria = array( "qryMilitaer.funktion"=>"Cdt", "qryMilitaer.zusatzFktID"=>1, "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
// //             $adressaten = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.personID" ) );
//             break;
// 
//         case "L19":
//             // Team Golf
//             $criteria = array( "qryVerein.organisationRegionID"=>20, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
//             $adressaten = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             break;
// 
//         case "L21":
//             // SC, Mitglieder
//             $criteria = array( "qryMilitaer.funktion"=>"SC", "qryMilitaer.zusatzFktID"=>1, "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//             $include = searchMember( $criteria, false, false, "tblPersonen.personID" );
//             $criteria = array( "qryMilitaer.funktion"=>"CEM", "qryMilitaer.zusatzFktID"=>1, "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//             $include = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.personID" ) );
//             $criteria = array( "qryMilitaer.funktion"=>"CSM", "qryMilitaer.zusatzFktID"=>1, "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
//             $adressaten = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.personID" ) );
//             break;
    }

    if( isset( $_POST["list"] ) ) {
        switch( $_POST["list"] ) {
            case "L18":
                // Brevetierungsjahr
                $criteria = array( "qryBrevet.brevetierungsdatum"=>array( "{$_POST["param"]}-01-01", "{$_POST["param"]}-12-31" ), "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
                $include = searchMember( $criteria, false, false, "tblPersonen.personID" );
                $criteria = array( "tblPersonen.personID"=>$_SESSION["personID"] );
                $adressaten = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.personID" ) );
                break;

            case "L20":
                // Mil Einteilung
                $param = urldecode( $_POST["param"] );
                $param = empty( $param ) ? $param : "*$param";
                $criteria = array( "qryMilitaer.einteilung"=>$param, "qryMilitaer.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
                $include = searchMember( $criteria, false, false, "tblPersonen.personID" );
                $criteria = array( "qryDienstgrad.zusatzDgID"=>4 );
                $exclude = searchMember( $criteria, false, false, "tblPersonen.personID" );
                $include = array_values( array_udiff( $include, $exclude, "fncMemberListUdiff" ) );
                $criteria = array( "tblPersonen.personID"=>$_SESSION["personID"] );
                $adressaten = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.personID" ) );
                break;
        }
    }

    if( isset( $_POST["personID"] ) ) {
        $adressaten = array( array( "tblPersonen.personID"=>$_POST["personID"] ) );
    }
    $i = 0;
    foreach($adressaten as $a) {
        $email->addRecipient(intval($a["tblPersonen.personID"]));
        $i++;

    }

    unset( $a, $adressaten, $n );

    if( $_POST["recipientsID"] == "0" ) {
        $email->lang = "de";
        $feedback = $email->send( $_POST["param"] );
        $email->lang = "fr";
        $feedback = $email->send( $_POST["param"] );
        $email->lang = "it";
        $feedback = $email->send( $_POST["param"] );

        if( $feedback[0] ) {
            $feedback[1] = translate( "Mails wurden erfolgreich verschickt" ) . ".";
        } else {
            $feedback[1] = translate( "Ein Fehler ist aufgetreten" ) . ".";
        }
    } else {
         $feedback = $email->send( $_POST["param"] );

//         if( $feedback[0] && in_array( $_POST["id"], array( 1, 10, 14, 24, 32, 46 ) ) ) {
//             $conn = dbconn::open();
//             $query = "UPDATE tblPersonen SET remindDate=CURRENT_TIMESTAMP() WHERE personID={$_POST["adressaten"]}";
//             $conn->query($query);
//
//             dbconn::close( $conn );
//         }
    }

    if( !$_POST["nofeedback"] ) {
        echo utf8_encode( $feedback[1] );
    }
?>