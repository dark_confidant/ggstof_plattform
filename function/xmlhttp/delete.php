<?php

session_start();
if (!isset($_SESSION["userID"])) {
    die("Zugriff verweigert.");
}

function __autoload($cls) {
    require_once( "../../class/$cls.php" );
}

require_once( "../list.php" );
require_once( "../search.php" );

if ($_POST["loginID"] != "" && $_POST["typID"] != "" && $_POST["personID"] != "") {
    $conn = dbconn::open();
    $query = "SELECT sysLog.*,linkSysLogTyp.typ,qryPersonen.*
              FROM sysLog
              LEFT JOIN linkSysLogTyp ON sysLog.typID=linkSysLogTyp.typID
              LEFT JOIN (
                  SELECT personID,email FROM tblPersonen
                  WHERE email<>''
              ) AS qryPersonen ON sysLog.userID=qryPersonen.email
              WHERE (linkSysLogTyp.typID = 1 AND sysLog.success = 0) AND personID={$_POST["personID"]} AND loginID={$_POST["loginID"]}
              GROUP BY sysLog.loginID
              ORDER BY sysLog.timestamp DESC";
    $result = $conn->query($query);
    dbconn::close($conn);

    if ($result->fetch(PDO::FETCH_NUM)) {
        // Delete row
        $conn = dbconn::open();
        $query = "DELETE FROM sysLog WHERE loginID={$_POST["loginID"]}";
        $result = $conn->query($query);
        if (!$result) {

        }
        echo "Eintrag {$_POST["loginID"]} wurde gelöscht.";
    } else {

        echo "Ein Fehler ist aufgetreten.";
    }
    dbconn::close($conn);
}