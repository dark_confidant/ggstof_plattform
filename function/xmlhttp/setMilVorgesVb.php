<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }

    if( $_POST["action"] == "UPDATE" && isset( $_POST["personID"] ) && isset( $_POST["militaerID"] ) && isset( $_POST["vorgesVb"] ) ) {
        $pers = new person( $_POST["personID"] );

        $data["vorgesVb"] = utf8_decode( $_POST["vorgesVb"] );
        $data["modifierID"] = $_SESSION["personID"];

        $pers->setMilitaryData( $data, $_POST["militaerID"] );
    }
?>