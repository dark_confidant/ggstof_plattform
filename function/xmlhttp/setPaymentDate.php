<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }

    if( isset( $_POST["zahlungID"] ) && isset( $_POST["datumEinzahlung"] ) ) {
        $conn = dbconn::open();
        $query = "UPDATE tblZahlungen SET datumEinzahlung='{$_POST["datumEinzahlung"]}',modifierID={$_SESSION["personID"]} WHERE zahlungID={$_POST["zahlungID"]}";
        $result = $conn->query($query);

        dbconn::close( $conn );
    }
?>