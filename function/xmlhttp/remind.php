<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $class_name ) {
        require_once( "../../class/" . $class_name . ".php" );
    }
    require_once( "../contentManagement.php" );
    require_once( "../lang.php" );

    $pers = new person( $_POST["personID"] );
    $lang = $pers->person->sprache;
    $pers->getMilitaryData();
    $i = 0;
    while( $pers->militaer[$i]->datumBeginn > date( "Y-m-d" ) ) {
        $i++;
    }
    $dienstgrad = translate( $pers->militaer[$i]->dienstgrad ) . ( $pers->militaer[$i]->zusatzDg ? " " . translate( $pers->militaer[$i]->zusatzDg ) : "" );

    $content = getContent( 31 );
    $body = str_replace( array( "[MODDATE]", "[MEMBER_ID]", "[NAME]", "[DG]", "[EMAIL]" ), array( $pers->timestamp, $pers->id, $pers->person->name, $dienstgrad, $pers->person->email ), stripslashes( strip_tags( html_entity_decode( $content["text"] ) ) ) );
    $content = getContent( 34 );
    $subject = str_replace( array( "[MODDATE]", "[MEMBER_ID]", "[NAME]", "[DG]", "[EMAIL]" ), array( $pers->timestamp, $pers->id, $pers->person->name, $dienstgrad, $pers->person->email ), stripslashes( strip_tags( html_entity_decode( $content["text"] ) ) ) );

    $success = mail( $pers->person->email, $subject, $body, "From: plattform@ggstof.ch\r\nX-Mailer: PHP/" . phpversion() );

    $conn = dbconn::open();
    $query = "UPDATE tblPersonen SET remindDate=CURRENT_TIMESTAMP() WHERE personID=$pers->id";
    $result = $conn->query($query);

    dbconn::close( $conn );

    if( !$success ) {
        echo utf8_encode( translate( "Ein Fehler ist aufgetreten.", $_SESSION["lang"], false, false ) );
    }
?>