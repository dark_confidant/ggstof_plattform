<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }
    require_once( "../lang.php" );

    if( $_POST["action"] == "UPDATE" && isset( $_POST["personID"] ) && isset( $_POST["militaerID"] ) && isset( $_POST["zusatzDgID"] ) ) {
        $pers = new person( $_POST["personID"] );

        $data["zusatzDgID"] = $_POST["zusatzDgID"];
        $data["modifierID"] = $_SESSION["personID"];

        $pers->setMilitaryData( $data, $_POST["militaerID"] );
    }
?>