<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }
    require_once( "../lang.php" );

    if( $_POST["action"] == "UPDATE" && isset( $_POST["personID"] ) && isset( $_POST["typID"] ) ) {
        $pers = new person( $_POST["personID"] );

        if( $_POST["typID"] == 3 ) {  // Berufsoffizier
            $data["branche"] = translate( "Verteidigung", $pers->sprache );
            $data["organisation"] = translate( "VBS", $pers->sprache );
            $data["funktion"] = translate( "Berufsoffizier", $pers->sprache );
        }
        $data["typID"] = $_POST["typID"];
        if( isset( $_POST["year"] ) ) {
            $data["datumBeginn"] = $_POST["year"] . "-01-01";
            $data["datumEnde"]   = $_POST["year"] . "-12-31";
        } else {
            $data["datumBeginn"] = date( "Y" ) . "-01-01";
        }
        $data["bemerkungen"] = "defined by admin @ " . date( "Y-m-d" );
        $data["modifierID"]  = $_SESSION["personID"];

        $pers->setJobData( $data );

//         $conn = dbconn::open();
//         $query = "DELETE FROM tblBeruf WHERE personID=" . $data["personID"] . " AND typID=0 AND datumEnde>='" . $data["datumBeginn"] . "'";
//         $result = $conn->query($query);
//
//         dbconn::close( $conn );
    }
?>