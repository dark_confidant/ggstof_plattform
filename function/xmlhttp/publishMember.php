<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }

    $member = new person( $_POST["id"] );
    $member->setPersonData( array( "veroeffentlichung"=>1, "modifierID"=>$_SESSION["personID"] ) );
?>