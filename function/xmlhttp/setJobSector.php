<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }

    if( $_POST["action"] == "UPDATE" && isset( $_POST["personID"] ) && isset( $_POST["berufID"] ) && isset( $_POST["branche"] ) ) {
        $pers = new person( $_POST["personID"] );

        $data["branche"]    = utf8_decode( $_POST["branche"] );
        $data["modifierID"] = $_SESSION["personID"];

        $pers->setJobData( $data, $_POST["berufID"] );
    }
?>