<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }

    if( $_POST["action"] == "UPDATE" && isset( $_POST["personID"] ) && isset( $_POST["berufID"] ) && isset( $_POST["positionID"] ) ) {
        $pers = new person( $_POST["personID"] );

        $data["positionID"] = $_POST["positionID"];
        $data["modifierID"] = $_SESSION["personID"];

        $pers->setJobData( $data, $_POST["berufID"] );
    }
?>