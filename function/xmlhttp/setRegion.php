<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }

    if( $_POST["action"] == "UPDATE" && isset( $_POST["personID"] ) && isset( $_POST["vereinID"] ) && isset( $_POST["organisationRegionID"] ) ) {
        $pers = new person( $_POST["personID"] );

        $data["organisationRegionID"] = $_POST["organisationRegionID"];
        $data["funktionID"] = 1;
        $data["modifierID"] = $_SESSION["personID"];
        if( $_POST["vereinID"] == "NULL" ) {
            $data["datumBeginn"] = date( "Y" ) . "-01-01";
            $vereinID = NULL;
        } else {
            $vereinID = $_POST["vereinID"];
        }

        $pers->setSocietyData( $data, $vereinID );
    }
?>