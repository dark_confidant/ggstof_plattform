<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }

    if( $_POST["action"] == "UPDATE" && isset( $_POST["personID"] ) && isset( $_POST["ausbildungID"] ) && isset( $_POST["typID"] ) ) {
        $pers = new person( $_POST["personID"] );

        $data["typID"] = utf8_decode( $_POST["typID"] );
        $data["modifierID"] = $_SESSION["personID"];

        $pers->setEducationData( $data, $_POST["ausbildungID"] );
    }
?>