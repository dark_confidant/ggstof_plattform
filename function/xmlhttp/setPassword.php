<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $class_name ) {
        require_once( "../../class/" . $class_name . ".php" );
    }
    require_once( "../lang.php" );
    require_once( "../userManagement.php" );

    $userID = $_SESSION["userID"];
    $rights = userRights( $userID );

    if( sha1( $_POST["oldPassword"] ) === $rights["password"] ) {
        if( validate_password( $_POST["newPassword"], $_POST["newPassword2"] ) ) {
            $success = userRights( $userID, "password", sha1( $_POST["newPassword"] ) );
            $feedback = $success ? "Ihr Passwort wurde ge�ndert." : "Ein Fehler ist aufgetreten.";
        }
        $success = userRights( $userID, "password", sha1( $_POST["newPassword"] ) );
    } else {
        $feedback = "Altes Passwort falsch. Passwort wurde nicht geändert.";
    }

    echo translate( $feedback, false, false, false ) ;
?>