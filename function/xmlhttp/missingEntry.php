<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    require_once( "../lang.php" );

    $success = mail( "plattform@ggstof.ch", translate( "Fehlender Eintrag f�r Truppengattung oder RS" ), var_export( $_POST, true ) . "\n\nhttp://" . $_SERVER["SERVER_NAME"] . "/member_profile.php?member=" . $_POST["personID"], "From: plattform@ggstof.ch\r\nX-Mailer: PHP/" . phpversion() );
    if( $success ) {
        echo utf8_encode( translate( "Mail wurde gesendet.", false, false, false ) );
    } else {
        echo utf8_encode( translate( "Ein Fehler ist aufgetreten. Bitte plattform@ggstof.ch benachrichtigen.", false, false, false ) );
    }
?>