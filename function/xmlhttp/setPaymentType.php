<?php
    session_start();
    if( !isset( $_SESSION["userID"] ) ) {
        die( "Zugriff verweigert." );
    }

    function __autoload( $cls ) {
        require_once( "../../class/$cls.php" );
    }
    require_once( "../lang.php" );

    if( isset( $_POST["id"] ) && isset( $_POST["typ"] ) ) {
        $conn = dbconn::open();
        $query = "UPDATE tblZahlungen SET typID={$_POST["typ"]} WHERE zahlungID={$_POST["id"]}";
        $result = $conn->query($query);
        echo mysql_error() ? mysql_error() : utf8_encode( translate( "Vorschuss wurde in Beitrag umgewandelt.", false, false, false ) );
        dbconn::close( $conn );
    }
?>