<?php
    function set_language() {
        if( !isset( $_SESSION["lang"] ) ) {
            if( isset( $_COOKIE["lang"] ) ) {
                $_SESSION["lang"] = $_COOKIE["lang"];
            } else {
                $hal = substr( $_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2 );
                $_SESSION["lang"] = ( in_array( $hal, array( "de", "fr", "it", "en" ) ) ) ? $hal : "de";
                setcookie( "lang", $_SESSION["lang"] );
            }
        }
        if( isset( $_GET["lang"] ) ) {
            $_SESSION["lang"] = $_GET["lang"];
            setcookie( "lang", $_SESSION["lang"] );
        }

        return $_SESSION["lang"];
    }

    function translate( $str, $override=false, $reverse=false, $html=true ) {

        require_once( $_SERVER["DOCUMENT_ROOT"] . "/class/dbconn.php" );
//         global $lang;
//         $language = $override ? $override : $lang;
        $language = $override ? $override : $_SESSION["lang"];

        if( is_int( $str ) ) {
            $conn = dbconn::open();
            $query = "SELECT $language FROM sysTranslation WHERE ID=$str";
            $result = $conn->query($query);
            dbconn::close( $conn );
            $row = $result->fetch();
        } else {
            $str = addslashes( $str );
            if( isset( $language ) && $language != "de" ) {
                $conn = dbconn::open();
                $str = utf8_decode($str);
                $query = "SELECT $language FROM sysTranslation WHERE de='$str'";
                $result = $conn->query($query);
                dbconn::close( $conn );
                $row = $result->fetch();
            } else if( $reverse ) {
                $conn = dbconn::open();
                $str = utf8_decode($str);
                $query = "SELECT de FROM sysTranslation WHERE fr='$str' OR it='$str' OR en='$str'";
                $result = $conn->query($query);
                dbconn::close( $conn );
                $row = $result->fetch();
            } else {
                return ( $html ) ? htmlentities( $str, ENT_COMPAT | ENT_HTML401, "UTF-8" ) : $str;
            }
        }
     
 
        $translation = ( $row[0] != "" ) ? utf8_encode($row[0]) : $str;
        return ( $html ) ? htmlentities( $translation, ENT_COMPAT | ENT_HTML401, "UTF-8" ) : $translation;
    }
?>