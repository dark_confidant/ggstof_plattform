<?php
    require_once( $_SERVER["DOCUMENT_ROOT"] . "/class/dbconn.php" );

    function getContent( $contentID, $lang=false ) {
//         global $lang;
        $lang = $lang ? $lang : $_SESSION["lang"];

        $conn = dbconn::open();

        $query = "SELECT $lang AS text,timestamp FROM tblContent WHERE contentID=$contentID";
        $retval = $conn->query($query)
            ->fetch(PDO::FETCH_ASSOC);

        dbconn::close( $conn );

        return $retval;
    }

    function setContent( $contentID, $content ) {
//         global $lang;
        $lang = $_SESSION["lang"];

        $conn = dbconn::open();
        $content = $content->quote();

        $query = "UPDATE tblContent SET $lang='$content' WHERE contentID=$contentID";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function getContentExt( $contentID=NULL, $lang=false ) {
        global $access;//, $lang;

        $lang = $lang ? $lang : $_SESSION["lang"];

        $conn = dbconn::open();

        $query = isset( $contentID ) ? "SELECT * FROM tblContent2 WHERE contentID=$contentID" : "SELECT * FROM tblContent2 WHERE lang='$lang' AND access<=$access ORDER BY title DESC, creationTime ASC";
        $result = $conn->query($query);
        $retval = array();
        while( $retval[] = $result->fetch(PDO::FETCH_ASSOC) ) { };
        array_pop( $retval );

        dbconn::close( $conn );

        return $retval;
    }

    function setContentExt( $content, $contentID=NULL ) {
//         global $lang;
        $lang = $_SESSION["lang"];

        $conn = dbconn::open();
        for( $i=0; $i<count( $content ); $i++ ) {
            $content[$i] = mysql_real_escape_string( $content[$i] );
        }

        $query = isset( $contentID ) ? "UPDATE tblContent2 SET title='$content[0]',text='$content[1]',access='$content[2]' WHERE contentID=$contentID" : "INSERT INTO tblContent2 (title,text,lang,access,creationTime) VALUES ('$content[0]','$content[1]','$lang','$content[2]','" . date( "Y-m-d h-i-s" ) . "')";
        $result = $conn->query($query);


        dbconn::close( $conn );
    }

    function deleteContentExt( $contentID ) {
        $conn = dbconn::open();

        $query = "DELETE FROM tblContent2 WHERE contentID=$contentID";
        $result = $conn->query($query);


        dbconn::close( $conn );
    }

    function getEmailContent( $emailID, $lang=false ) {
        $lang = $lang ? $lang : $_SESSION["lang"];

        $conn = dbconn::open();

        $query = "SELECT *,subject_$lang AS subject,body_$lang AS body,absender
                  FROM tblEmail
                  LEFT JOIN linkEmailAbsender ON tblEmail.absenderID=linkEmailAbsender.absenderID
                  WHERE emailID=$emailID";
        $result = $conn->query($query);

        $retval = $result->fetch(PDO::FETCH_ASSOC);

        dbconn::close( $conn );

        return $retval;
    }

    function setEmailContent( $titel, $absenderID, $subject, $body, $lang=false, $emailID=false ) {
        $lang = $lang ? $lang : $_SESSION["lang"];

        $conn = dbconn::open();
        $subject = mysql_real_escape_string( $subject );
        $body = mysql_real_escape_string( $body );

        if( $emailID ) {
            $query = "UPDATE tblEmail SET titel='$titel',absenderID='$absenderID',subject_$lang='$subject',body_$lang='$body' WHERE emailID=$emailID";
        } else {
            $query = "INSERT INTO tblEmail (titel,absenderID,subject_$lang,body_$lang) VALUES ('$titel','$absenderID','$subject','$body')";
        }
        $result = $conn->query($query);


        dbconn::close( $conn );
    }

    function deleteEmailContent( $emailID ) {
        $conn = dbconn::open();

        $query = "DELETE FROM tblEmail WHERE emailID=$emailID";
        $result = $conn->query($query);


        dbconn::close( $conn );
    }
?>