<?php
    require_once( "{$_SERVER["DOCUMENT_ROOT"]}/class/dbconn.php" );

    function logEmail( $adressat, $success, $emailID, $emailTitle ) {
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip,success,param0,param1) VALUES (12,'$adressat','{$_SERVER["REMOTE_ADDR"]}'," . (int) $success . ",'$emailID','" . mysql_escape_string( $emailTitle ) . "')";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logInvitation( $userID, $adressat ) {
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip,success,param0) VALUES (10,'$userID','{$_SERVER["REMOTE_ADDR"]}',1,'$adressat')";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logLogin( $userID, $success=0 ) {
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip,success) VALUES (1,'$userID','{$_SERVER["REMOTE_ADDR"]}',$success)";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logLogout( $userID ) {
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip,success) VALUES (4,'$userID','{$_SERVER["REMOTE_ADDR"]}',1)";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logPassword( $userID, $success=0 ) {
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip,success) VALUES (2,'$userID','{$_SERVER["REMOTE_ADDR"]}',$success)";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logProfileChange( $userID ) {
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip,success) VALUES (11,'$userID','{$_SERVER["REMOTE_ADDR"]}',1)";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logReminder( $userID, $typID, $email ) {
        if( !in_array( $typID, array( 6, 7, 8, 9 ) ) ) {
            die( "Falsche typID" );
        }
        $str = addslashes( $str );
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip,param0) VALUES ($typID,'$userID','{$_SERVER["REMOTE_ADDR"]}','$email')";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logSearch( $userID, $str="NULL", $success=0 ) {
        $str = addslashes( $str );
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip,success,param0) VALUES (5,'$userID','{$_SERVER["REMOTE_ADDR"]}',$success,'$str')";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logUnknownEmail( $userID ) {
        $conn = dbconn::open();
        $query = "INSERT INTO sysLog (typID,userID,ip) VALUES (3,'$userID','{$_SERVER["REMOTE_ADDR"]}')";
        $conn->query($query);

        dbconn::close( $conn );
    }

    function logUserChange( $userOld, $userNew ) {
        $conn = dbconn::open();
        $query = "UPDATE sysLog SET userID='$userNew' WHERE userID='$userOld'";
        $conn->query($query);

        dbconn::close( $conn );
    }
?>