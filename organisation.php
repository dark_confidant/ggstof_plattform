<?php
    require_once('head.php');
    ggstof_head();

    date_default_timezone_set( "Europe/Zurich" );


    $action = array_key_exists('action', $_REQUEST) ? $_REQUEST["action"]: '';
    $context = array_key_exists('context', $_REQUEST) ? $_REQUEST["context"]: '';

    if( $action == "DELETE" && $_SESSION["rights"]["editContent"] ) {
        deleteContentExt( $_GET["item"] );
    }
?>




<?php
include("include/head.inc.php");
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Organisation" ); ?></title>
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/accordeon.js"></script>
<script type="text/javascript" src="script/viewTabContent.js"></script>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</head>




<!-- start body -->
<body id="organisation">




<!-- start #navigationLeft -->
<?php
include("include/navigationLeft.inc.php");
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
$pagePublic = 1;
include("include/navigationTop.inc.php");
?>
<!-- end #navigationTop -->



<!-- start #content -->
<div id="content" class="partner">




<!-- start tabs, credits to facebook.com -->
<div class="home_main_item">
<div class="newsfeed_header">
<div id="newsfeed_tabs_wrapper">
<div id="newsfeed_tabs" class="Tabset_tabset">
<div class="HomeTabs">
<!-- spans to avoid href -->
<span id="One" class="HomeTabs_tab HomeTabs_first<?php if( empty( $context ) || $context == "mitglieder" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Mitglieder" ) ?></span>
<span id="Two" class="HomeTabs_tab<?php if( $context == "gv" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Vereinsversammlung" ) ?></span>
<span id="Three" class="HomeTabs_tab<?php if( $context == "vorstand" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Vorstand" ) ?></span>
<span id="Four" class="HomeTabs_tab<?php if( $context == "revision" ) { echo " Tabset_selected"; } ?>"><?php echo translate( "Revisionsstelle" ) ?></span>
</div>
</div>
</div>
</div>
</div>
<!-- end tabs -->


<!-- start #tab1 -->
<div class="tabContent" id="tabOne"<?php if( empty( $context ) || $context == "mitglieder" ) { echo " style=\"display:block\""; } ?>>

<div class="tabContentHolder">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=6&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=mitglieder"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
<?php } ?>

<?php
    $content = getContent( 6 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

</div>

</div>
<!-- end #tab1 -->




<!-- start #tab2 -->
<div class="tabContent" id="tabTwo"<?php if( $context == "gv" ) { echo " style=\"display:block\""; } ?>>

<div class="tabContentHolder">

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<a href="fckeditor.php?extended=true&amp;action=ADD&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=gv"><img src="image/template/add.png" alt="<?php echo translate( "Neuen Eintrag hinzufügen" ); ?>" title="<?php echo translate( "Neuen Eintrag hinzufügen" ); ?>" /></a>
<?php } ?>


<?php
    $access = ( $_SESSION["rights"]["roleID"] ) ? $_SESSION["rights"]["roleID"] : 0;
    $content = getContentExt();
    for( $i=0; $i<count( $content ); $i++ ) {
        if( $content[$i]["title"] != $content[$i-1]["title"] ) {
?>
<div class="accordeon<?php if( $i == 0 ) { echo " accordeonProfile show"; } ?>">
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( str_replace( "0_", "", $content[$i]["title"] ) ); ?></span></a>
<div class="groupChildren">
<div class="child">
<ul class="unorderedListAccordeon">
<?php
        }
        echo "<li>" . str_replace( "<p>", "", str_replace( "</p>", "", stripslashes( $content[$i]["text"] ) ) );
        if( $_SESSION["rights"]["editContent"] ) {
?>
<a class="liEditButtons" href="fckeditor.php?extended=true&amp;action=EDIT&amp;item=<?php echo $content[$i]["contentID"]; ?>&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=gv"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ); ?>" title="<?php echo translate( "Bearbeiten" ); ?>" /></a>
<a href="<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>?action=DELETE&amp;item=<?php echo $content[$i]["contentID"]; ?>&amp;context=gv"><img src="image/template/delete.png" alt="<?php echo translate( "Löschen" ); ?>" title="<?php echo translate( "Löschen" ); ?>" /></a>
<?php
        }
        echo "</li>";
        if( $content[$i]["title"] != $content[$i+1]["title"] ) {
?>
</ul>
</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
</div>
<!-- end .accordeon -->
<?php
        }
        $timestamp[] = strtotime( $content[$i]["timestamp"] );
    }
?>


</div>
<!-- end .tabContentHolder -->

</div>
<!-- end #tab2 -->




<!-- start #tab3 -->
<div class="tabContent" id="tabThree"<?php if( $context == "vorstand" ) { echo " style=\"display:block\""; } ?>>

<?php
    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID,funktionID,(funktionID=2) AS praesident,(funktionID=5) AS vrbofgsts
              FROM tblVerein
              LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID
              WHERE organisationRegionID=2 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
              GROUP BY tblVerein.personID
              ORDER BY praesident DESC,vrbofgsts ASC,name";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $vorstand[$n] = new person( $row[0] );
        $vorstand[$n]->getMilitaryData();
        $vorstand[$n]->getSocietyData();
        $vorstand[$n]->funktionID = $row[1];
        $n++;
    }
//     $email = array( "praesident@ggstof.ch", "vize1@ggstof.ch", "vize2@ggstof.ch", "kassier@ggstof.ch", "aktuar@ggstof.ch" );

    for( $i=0; $i<$n; $i++ ) {
        $j = 0;
        while( $vorstand[$i]->verein[$j]->funktionID != $vorstand[$i]->funktionID && $j<count( $vorstand[$i]->verein ) ) {
            $j++;
        }
        echo "<div class=\"vorstandSeparator\">";

        echo "<img class=\"vorstandImage\" src=\"include/profilePhoto.php?member={$vorstand[$i]->id}\" alt=\"\" />";
        if( $vorstand[$i]->funktionID == 2 || $vorstand[$i]->funktionID == 5 ) {
            echo "<div class=\"vorstandTopText bold\">" . translate( $vorstand[$i]->verein[$j]->vereinFunktion ) . "</div>";
        }
        $k = 0;
        while( $vorstand[$i]->militaer[$k]->datumBeginn > date( "Y-m-d" ) ) {
            $k++;
        }
        
        echo "<div class=\"vorstandTopText\">" . translate( $vorstand[$i]->militaer[$k]->dienstgrad, $vorstand[$i]->person->sprache ) . " " . translate( $vorstand[$i]->militaer[$k]->zusatzDg, $vorstand[$i]->person->sprache ) . "</div>";
        echo "<div class=\"vorstandTopText\">"; if( isset( $_SESSION["rights"]["roleID"] ) ) { echo "<a href=\"member_profile.php?member=" .  $vorstand[$i]->id . "\">"; } echo $vorstand[$i]->person->vorname . " ". $vorstand[$i]->person->vorname2 . " ". $vorstand[$i]->person->name; if( isset( $_SESSION["rights"]["roleID"] ) ) { echo "</a>"; } echo "</div>";
        echo "<ul class=\"unorderedList\">";
        echo "<li>" . translate( "Im Amt seit" ) . " " . substr( $vorstand[$i]->verein[$j]->datumBeginn, 0, 4 ) . "</li>";
        if( $vorstand[$i]->funktionID != 5 ) {
            echo "<li>" . translate( "gewählt bis zur Vereinsversammlung" ) . " " . substr( $vorstand[$i]->verein[$j]->datumEnde, 0, 4 ) . "</li>";
        }
        if( !empty( $vorstand[$i]->verein[$j]->bemerkungen) )  {
            echo "<li>" . translate( "Funktion" ) . ": " . nl2br( $vorstand[$i]->verein[$j]->bemerkungen)  . "</li>";
        }
        list( $coded, $key ) = encrypt_email( $vorstand[$i]->person->email );
        $name = $vorstand[$i]->person->vorname . ' ' . $vorstand[$i]->person->vorname2 . ' ' . $vorstand[$i]->person->name;
        echo "<li><a href=\"#\" onclick=\"this.href=decrypt_email( '$coded','$key' )\">" . translate( "E-Mail an" ) . " {$name}</a></li>";
        echo "</ul>";
        echo "<div class=\"clear\"></div>";

        echo "</div>";
    }
?>


<div class="accordeon accordeonProfile">

<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Der Vorstand informiert" ) ?></span></a>

<div class="groupChildren">

<div class="child">

<?php //if( $_SESSION["rights"]["editContent"] ) { ?>
<!--a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=7&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=vorstand"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a-->
<?php //} ?>

<div class="unorderedListAccordeon">
<?php
    $blog = blogLastEntries( 5, 9 );
    echo "<ul class=\"memberBlogList\">";
    for( $i=0; $i<count( $blog ); $i++ ) {
        $post_title = $blog[$i]["post_title"];
        echo "<li><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">{$post_title}</a></li>";
    }
    echo "</ul>";

/*    $content = getContent( 7 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );*/
?>
</div>

</div>
<!-- end .child -->

</div>
<!-- end .groupChildren -->

</div>
<!-- end .parent -->

</div>
<!-- end .accordeon -->



<div class="accordeon">

<?php if( $_SESSION["rights"]["roleID"] >= 4 ) { ?>
<div class="parent"><a onclick="noScrollingToTop(); return false;" class="menuParent" href="#"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate( "Interne Dokumente Vorstand" ) ?></span></a>
<div class="groupChildren">
<div class="child">

<a class="editButtonTop" href="fckeditor.php?action=EDIT&amp;item=8&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>&amp;context=vorstand"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>

<div class="unorderedListAccordeon">
<?php
    $content = getContent( 8 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>
</div>

</div>
<!-- end .child -->
</div>
<!-- end .groupChildren -->
</div>
<!-- end .parent -->
<?php } ?>

</div>
<!-- end .accordeon -->




</div>
<!-- end #tab3 -->




<!-- start #tab4 -->
<div class="tabContent" id="tabFour"<?php if( $context == "revision" ) { echo " style=\"display:block\""; } ?>>

<?php
    $conn = dbconn::open();
    $query = "SELECT tblVerein.personID,funktionID FROM tblVerein LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID WHERE funktionID=8 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE() ORDER BY name";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $vorstand[$n] = new person( $row[0] );
        $vorstand[$n]->getMilitaryData();
        $vorstand[$n]->getSocietyData();
        $vorstand[$n]->funktionID = $row[1];
        $n++;
    }

    for( $i=0; $i<$n; $i++ ) {
        $j = 0;
        while( $vorstand[$i]->verein[$j]->funktionID != $vorstand[$i]->funktionID && $j<count( $vorstand[$i]->verein ) ) {
            $j++;
        }
        echo "<div class=\"vorstandSeparator\">";

        echo "<img class=\"vorstandImage\" src=\"include/profilePhoto.php?member=" . $vorstand[$i]->id . "&amp;deceased=" . ( $vorstand[$i]->person->todesdatum != "0000-00-00" ) . "\" alt=\"\" />";
        echo "<div class=\"vorstandTopText bold\">" . translate( $vorstand[$i]->verein[$j]->vereinFunktion ) . "</div>";
        $k = 0;
        while( $vorstand[$i]->militaer[$k]->datumBeginn > date( "Y-m-d" ) ) {
            $k++;
        }
        echo "<div class=\"vorstandTopText\">" . translate( $vorstand[$i]->militaer[$k]->dienstgrad, $vorstand[$i]->person->sprache ) . " " . translate( $vorstand[$i]->militaer[$k]->zusatzDg, $vorstand[$i]->person->sprache ) . "</div>";
        echo "<div class=\"vorstandTopText\">"; if( isset( $_SESSION["rights"]["roleID"] ) ) { echo "<a href=\"member_profile.php?member=" .  $vorstand[$i]->id . "\">"; } echo $vorstand[$i]->person->vorname . " ". $vorstand[$i]->person->vorname2 . " ". $vorstand[$i]->person->name; if( isset( $_SESSION["rights"]["roleID"] ) ) { echo "</a>"; } echo "</div>";
        echo "<ul class=\"unorderedList\">";
        echo "<li>" . translate( "Im Amt seit" ) . " " . substr( $vorstand[$i]->verein[$j]->datumBeginn, 0, 4 ) . "</li>";
        echo "<li>" . translate( "gewählt bis zur GV" ) . " " . substr( $vorstand[$i]->verein[$j]->datumEnde, 0, 4 ) . "</li>";
//         echo "<li><a href=\"mailto:revisionsstelle@ggstof.ch\">revisionsstelle@ggstof.ch</a></li>";
        list( $coded, $key ) = encrypt_email( "revisionsstelle@ggstof.ch" );
        echo "<li><a href=\"#\" onclick=\"this.href=decrypt_email( '" . $coded . "','" . $key . "' )\">" . translate( "E-Mail an" ) . " " . translate( "Revisionsstelle" ) . "</a></li>";
        echo "</ul>";
        echo "<div class=\"clear\"></div>";

        echo "</div>";
    }
?>

</div>
<!-- end #tab4 -->




</div>
<!-- end #content -->



<?php
    $timestamp = date( "d.m.Y", max( $timestamp ) );
    include( "include/footer.inc.php" );
?>

</body>
</html>