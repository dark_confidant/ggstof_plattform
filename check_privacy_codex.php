<?php
    require_once('head.php');
    ggstof_head();

    $pers = new person( $_SESSION["rights"]["personID"] );
?>



<?php
include("include/head.inc.php");
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ); ?> (GGstOf)</title>
</head>




<!-- start body -->
<body>




<!-- start #navigationLeft -->
<?php
include("include/navigationLeft.inc.php");
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
include("include/navigationTop.inc.php");
?>
<!-- end #navigationTop -->




<!-- start #content -->
<div id="content">

<form class="formUpdateProfil" id="formMemberProfil" action="member_profile.php" method="post" enctype="multipart/form-data">
    <fieldset id="updateProfileAboveTabs" class="fieldsetEditForm">
        <table>
            <tr><td class="legend"><?php echo translate( "Um den Mitgliederbereich nutzen zu können, müssen Sie der Datenschutz-Vereinbarung und dem Codex zustimmen." ); ?></td></tr>
        </table>
        <table>
            <tr><td><label><?php echo translate( "Ich akzeptiere" ); ?> <a href="privacy.php" onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=600,left=50,top=50,status'); return false"><?php echo translate( "die Datenschutzbestimmungen" ); ?></a></label><input type="checkbox" name="inputPrivat[datenschutz]" value="1" <?php if( $pers->person->datenschutz == 1 ) { echo "checked=\"checked\" "; } ?>/></td></tr>
            <tr><td><label><?php echo translate( "Ich akzeptiere" ); ?> <a href="codex.php" onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=600,left=50,top=50,status'); return false"><?php echo translate( "den Codex" ); ?></a></label><input type="checkbox" name="inputPrivat[codex]" value="1" <?php if( $pers->person->codex == 1 ) { echo "checked=\"checked\" "; } ?>/></td></tr>
            <tr><td><label><?php echo translate( "Bitte senden Sie mir Informationen primär auf meine" ); ?></label><select name="inputPrivat[channelID]"><?php echo makeOptions( "linkPersonChannel", ( empty( $pers->person->channelID ) ? 98 : $pers->person->channelID ) ); ?></select></td></tr>
            <tr><td><label><?php echo translate( "Senden Sie mir bitte die Rechnung" ); ?></label><select name="inputPrivat[paymentChannelID]"><?php echo makeOptions( "linkZahlungenChannel", ( empty( $pers->person->paymentChannelID ) ? 6 : $pers->person->paymentChannelID ) ); ?></select></td></tr>
        </table>
        <table>
            <tr><td>
                <input type="hidden" name="member" value="<?php echo $pers->id; ?>" />
                <input type="hidden" name="action" value="UPDATE" />
                <input type="submit" class="formsSubmitButton" value="<?php echo translate( "Aktualisieren" ); ?>" />
                <input type="button" value="<?php echo translate( "Logout" ); ?>" onclick="location='logout.php'" />
            </td></tr>
        </table>
        <table>
            <tr><td class="legend"><?php echo translate( "Haben Sie Fragen und Bemerkungen zu Codex/Datenschutz? Kontaktieren Sie bitte den" ) . " " . "<a href=\"mailto:netzwerk@ggstof.ch\">" . translate( "Projektleiter Netzwerk" ) . "</a>."; ?></td></tr>
        </table>
    </fieldset>
</form>


</div>
<!-- end #content -->




<?php
    include( "include/footer.inc.php" );
?>

</body>
<!-- end body -->

</html>