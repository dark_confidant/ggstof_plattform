<?php
    require_once('head.php');
    ggstof_head();

    $email = $_POST["email"];

    if( empty( $hash ) && empty( $email ) && empty( $_POST["newPassword"] ) ) {
        $feedback = translate( "Geben Sie hier Ihre E-Mail-Adresse ein. Sie werden anschliessend einen Link auf eine Seite erhalten, auf der Sie Ihr Passwort neu setzen k�nnen." );
    }

    if( $email ) {
        $criteria = array( "tblPersonen.email"=>$email, "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
        $adressat = searchMember( $criteria );

        if( count( $adressat ) == 1 ) {
//             $hash = reset_password( $email );
//             $retval = sendmail( 8, $adressat[0]["tblPersonen.personID"], array( "hash"=>$hash ) );
            $mail = new email( 8, $adressat[0]["tblPersonen.personID"] );
            $retval = $mail->send();
            $feedback = ( $retval[0] == 1 ) ? translate( "Ein Link wurde Ihnen per Mail zugestellt. Bitte aktivieren Sie ihn innerhalb der n�chsten 15 Minuten." ) : translate( "Ein Fehler ist aufgetreten." );
        } else {
//             mail( "netzwerk@ggstof.ch", "password.php: Unbekannte E-Mail-Adresse", "Mit folgender, nicht verzeichneter E-Mail-Adresse wurde versucht, das Passwort neu zu setzen: <$email>", "From: plattform@ggstof.ch\r\nContent-type: text/plain; charset=iso-8859-1\r\nX-Mailer: PHP/" . phpversion() );
            logUnknownEmail( $email );
            $feedback = translate( "Diese E-Mail-Adresse ist bei uns nicht verzeichnet oder Sie sind bisher nicht als Mitglied registriert. Wenn Sie nicht mehr wissen, mit welcher E-Mail-Adresse Sie registriert sind, wenden Sie sich bitte an" ) . " <a href=\"mailto:plattform@ggstof.ch\">plattform@ggstof.ch</a>.";
        }
    }

    $get = array_keys( $_GET );
    $hash = preg_match( "/^[0-9a-f]{40}$/", $get[0] ) ? $get[0] : NULL;
    if( $hash ) {
        $conn = dbconn::open();

        $query = "SELECT * FROM sysAuth WHERE hash='$hash'";
        $result = $conn->query($query);


        $result = $result->fetch(PDO::FETCH_ASSOC);

        if( $result ) {
            if( $result["expiry"] >= time() ) {
                if( $result["clicks"] == 0 ) {
                    $userID = $result["email"];
                    $feedback = translate( "Geben Sie hier innerhalb von 12 Minuten Ihr neues Passwort ein." );

                    $query = "UPDATE sysAuth SET clicks=clicks+1 WHERE hash='$hash'";
                    $conn->query($query);

                } else {
                    $feedback = translate( "Dieser Link wurde bereits einmal verwendet." ). " " . translate( "Bitte wiederholen Sie den Vorgang." );
                    logPassword( $result["email"] );
                }
            } else {
                $feedback = translate( "Dieser Link ist nicht mehr g�ltig." ) . " " . translate( "Bitte wiederholen Sie den Vorgang." );
                logPassword( $result["email"] );
            }
        } else {
            $feedback = translate( "Kein Eintrag gefunden." );
        }

        $query = "DELETE FROM sysAuth WHERE expiry<" . (time()-60*60*24*30);
        $conn->query($query);


        dbconn::close( $conn );
    }

    if( $_POST["userID"] && validate_password( $_POST["newPassword"], $_POST["newPassword2"] ) ) {
        $success = userRights( $_POST["userID"], "password", sha1( $_POST["newPassword"] ) );
        logPassword( $_POST["userID"], 1 );
        $feedback = $success ? translate( "Sie haben erfolgreich ein neues Passwort definiert." ) : translate( "Ein Fehler ist aufgetreten." );
    }
?>




<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf)"; ?></title>
<link rel="stylesheet" type="text/css" href="style/lightbox.css" media="screen" />
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/xmlHttp.js"></script>
<script type="text/javascript" src="script/password.js"></script>
<?php
    if( $userID ) {
?>
<script type="text/javascript">
<!--

function timeout() {
    document.forms["newPassword"].elements["submitPassword"].disabled = true;
    window.alert( "<?php echo translate( "Die Zeit ist abgelaufen." ) . " " . translate( "Bitte wiederholen Sie den Vorgang." ); ?>" );
    window.location = "password.php";
}

window.setTimeout( "timeout()", 720000 );

//-->
</script>
<?php
    }
?>
</head>



<body>



<!-- start #navigationLeft -->
<?php
    include( "include/navigationLeft.inc.php" );
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
    include( "include/navigationTop.inc.php" );
?>
<!-- end #navigationTop -->


<div id="content">

<p><?php echo $feedback; ?></p>

<form id="newPassword" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <table>
<?php
    if( empty( $hash ) && empty( $email ) && empty( $_POST["newPassword"] ) ) {
?>
            <tr><td>
                <label><?php echo translate( "E-Mail-Adresse" ); ?></label>
                <input name="email" type="text" />
                <input type="submit" value="<?php echo translate( "OK" ); ?>" />
            </td></tr>
<?php
    } //if( empty( $hash ) && empty( $email ) && empty( $_POST["newPassword"] )
    if( $userID ) {
?>
            <tr><td><label><?php echo translate( "Passwort" ); ?>:</label><input name="newPassword" type="password" onkeyup="checkPassword( document.forms['newPassword'], document.images )" /></td></tr>
            <tr><td><label><?php echo translate( "Passwort wiederholen" ); ?>:</label><input name="newPassword2" type="password" onkeyup="checkPassword( document.forms['newPassword'], document.images )" /></td></tr>
            <tr><td>
                <img id="pwok0" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 8 Zeichen" ); ?><br />
                <img id="pwok1" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 1 Kleinbuchstabe" ); ?> (a-z)<br />
                <img id="pwok2" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 1 Grossbuchstabe" ); ?> (A-Z)<br />
                <img id="pwok3" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 1 Ziffer" ); ?> (0-9)<br />
                <img id="pwok4" src="image/template/delete.png" alt="" /> <?php echo translate( "Mindestens 1 Sonderzeichen" ); ?> (. , : ; + - _ * ! @ $ % &amp;)<br />
                <img id="pwok5" src="image/template/delete.png" alt="" /> <?php echo translate( "Passw�rter stimmen �berein" ); ?>
            </td></tr>
            <tr><td>
                <input name="submitPassword" type="submit" value="<?php echo translate( "OK" ); ?>" disabled="disabled" />
                <input name="userID" type="hidden" value="<?php echo $userID; ?>" />
            </td></tr>
<?php
    } //if( $userID )
?>
        </table>
    </fieldset>
</form>

</div><!-- end #content -->


<?php
    include( "include/footer.inc.php" );
?>

</body>

</html>