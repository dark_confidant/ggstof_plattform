<?php

// load classes and functions
function __autoload($cls) { require_once("class/$cls.php"); }
foreach(glob("class/site/*.php") as $cls) { require_once($cls); }
foreach(glob("function/*.php") as $fnc) { require_once($fnc); }

// additional functions
function contentRead($contentID) {
	$conn = dbconn::open( "ggstofch" );
	$query = "SELECT * FROM tblContent WHERE contentID = '$contentID' ORDER BY timestamp DESC LIMIT 1";
	$result = $conn->query($query);

	while( $row = $result->fetch(PDO::FETCH_NUM) ) {
		$entry[] = $row;
	}
	dbconn::close( $conn );
	return $entry;
}
function iOflogLogin( $userID, $success=0 ) {
	$conn = dbconn::open();
	$query = "INSERT INTO sysLog (typID,userID,ip,success,param1) VALUES (1,'$userID','{$_SERVER["REMOTE_ADDR"]}',$success,'iOf')";
	$conn->query($query);

	dbconn::close( $conn );
}
function iOflogSearch($typ, $userID, $str="NULL", $success=0 ) {
	$str = addslashes( $str );
	$conn = dbconn::open();
	$query = "INSERT INTO sysLog (typID,userID,ip,success,param0,param1) VALUES ($typ,'$userID','{$_SERVER["REMOTE_ADDR"]}',$success,'$str','iOf')";
	$conn->query($query);

	dbconn::close( $conn );
}

// expire header
header("Expires: Wed, 12 Jul 2000 12:00:00 GMT"); 
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

// set vars
$output = "";
$member_id = 0;
$user_id = 0;

// filter input
$action = strtolower(htmlentities($_GET["action"], ENT_QUOTES));
$email = htmlentities($_GET["email"], ENT_QUOTES);
$pass = htmlentities($_GET["pass"], ENT_QUOTES);
$token = htmlentities($_GET["token"], ENT_QUOTES);
$profile = intval($_GET["profile"]);
$search = htmlspecialchars(utf8_decode(html_entity_decode($_GET["search"], ENT_QUOTES)), ENT_QUOTES);


// blog
if (substr_count($action, "blog")) {
    $blog = blogLastEntries(10);
    $output .= "<blog>\r";
    for($i=0;$i<count($blog);$i++) {
    	$output .= "<entry>\r";    
        $output .= "<id>".str_replace("http://blog.ggstof.ch/?p=", "", $blog[$i]["guid"])."</id>\r";
        $output .= "<date>".str_replace(" ", "", str_replace(":", "", str_replace("-", "", $blog[$i]["post_date"])))."</date>\r"; 
        $output .= "<title>".$blog[$i]["post_title"]."</title>\r";
        $blog[$i]["post_content"] = str_replace("<img", "<img_hidden", $blog[$i]["post_content"]);
        $output .= "<text>".htmlspecialchars($blog[$i]["post_content"], ENT_QUOTES)."</text>\r";
        $output .= "<comments>{$blog[$i]["comment_count"]}</comments>\r"; 
    	$output .= "</entry>\r"; 
    }
	$output .= "</blog>\r";
}


// login
if ($action == "login") {
	$output .= "<login>\r";
	if ($email != "" && $pass != "") {
		$conn = dbconn::open( "ggstofch" );
		$query = "SELECT personID,email AS userID,password,datenschutz,codex,roleID,role,organisationRegionID AS regionID,editProfile,viewNonpublicProfile,addProfile,closeProfile,confirmProfile,createList,exportData,editContent,uploadDocs,deleteDocs,viewPayment,editPayment
				  FROM tblPersonen
				  LEFT JOIN linkSysRoles ON tblPersonen.roleID=linkSysRoles.linkRoleID
				  LEFT JOIN (SELECT personID AS pID,organisationRegionID FROM tblVerein WHERE organisationRegionID BETWEEN 3 AND 10 AND datumEnde>=CURDATE()) AS qryVerein ON tblPersonen.personID=qryVerein.pID
				  WHERE email='$email'";
		$result = $conn->query($query);
		$retval = $result->fetch(PDO::FETCH_ASSOC);
		if (sha1($pass) === $retval["password"]) {
			$output .= "<status>1</status>\r";
	    	$output .= "<token>".base64_encode($retval["personID"]."-".$retval["password"])."</token>\r";
	    	iOflogLogin($retval["userID"], 1);
	    } else {
    		$output .= "<status>0</status>\r";
    		iOflogLogin($retval["userID"], 0);
	    }
	} else {
    	$output .= "<status>0</status>\r";
	}
	$output .= "</login>\r";
}


// check token
if ($token != "") {
	$output .= "<token>\r";
	list($temp_member_id, $temp_member_pass) = explode("-", base64_decode($token), 2);
	$temp_member_id = intval($temp_member_id);
	$temp_member_pass = htmlentities($temp_member_pass, ENT_QUOTES);
	if ($temp_member_id > 0 && $temp_member_pass != "") {
		$conn = dbconn::open( "ggstofch" );
		$query = "SELECT personID,email AS userID,password,datenschutz,codex,roleID,role,organisationRegionID AS regionID,editProfile,viewNonpublicProfile,addProfile,closeProfile,confirmProfile,createList,exportData,editContent,uploadDocs,deleteDocs,viewPayment,editPayment
				  FROM tblPersonen
				  LEFT JOIN linkSysRoles ON tblPersonen.roleID=linkSysRoles.linkRoleID
				  LEFT JOIN (SELECT personID AS pID,organisationRegionID FROM tblVerein WHERE organisationRegionID BETWEEN 3 AND 10 AND datumEnde>=CURDATE()) AS qryVerein ON tblPersonen.personID=qryVerein.pID
				  WHERE personID='$temp_member_id'";		
		$result = $conn->query($query);
		$retval = $result->fetch(PDO::FETCH_ASSOC);
		if ($temp_member_pass === $retval["password"]) {
			$member_id = $retval["personID"];
			$user_id = $retval["userID"];
    		$output .= "<status>1</status>\r";
		} else {
    		$output .= "<status>0</status>\r";
    		iOflogLogin($retval["userID"], 0);
		}
	} else {
    	$output .= "<status>0</status>\r";
	}
	$output .= "</token>\r";
}


// search
if (substr_count($action, "search") && $member_id > 0) {
	$output .= "<search>\r";
	if ($search != "") {
		$conn = dbconn::open( "ggstofch" );
		$query = "SELECT personID,email AS userID,password,datenschutz,codex,roleID,role,organisationRegionID AS regionID,editProfile,viewNonpublicProfile,addProfile,closeProfile,confirmProfile,createList,exportData,editContent,uploadDocs,deleteDocs,viewPayment,editPayment
				  FROM tblPersonen
				  LEFT JOIN linkSysRoles ON tblPersonen.roleID=linkSysRoles.linkRoleID
				  LEFT JOIN (SELECT personID AS pID,organisationRegionID FROM tblVerein WHERE organisationRegionID BETWEEN 3 AND 10 AND datumEnde>=CURDATE()) AS qryVerein ON tblPersonen.personID=qryVerein.pID
				  WHERE name='$search' AND tblPersonen.veroeffentlichungID > 0";		
		$result = $conn->query($query);
		iOflogSearch(5, $user_id, "name=$search", mysql_num_rows($result), 1);

		dbconn::close( $conn );
		$n = 0;
		while( $row = $result->fetch(PDO::FETCH_NUM) ) {
			$member = new person($row[0]);
			$member->getMilitaryData();
			$member->getSocietyData();
			$member->funktionID = $row[1];
			$k = 0;
			while($member->militaer[$k]->datumBeginn > date( "Y-m-d" ) ) { $k++; }
			$output .= "<entry>\r";
			$output .= "<id>{$member->id}</id>\r";			
			$output .= "<vorname>".$member->person->vorname."</vorname>\r";
			$output .= "<vorname2>".$member->person->vorname2."</vorname2>\r";
			$output .= "<name>".$member->person->name."</name>\r";
			$output .= "<dienstgrad>".$member->militaer[$k]->dienstgrad."</dienstgrad>\r";
			$output .= "<photo>http://plattform.ggstof.ch/include/profilePhoto.php?member={$member->id}</photo>\r";			
			$output .= "</entry>\r";			
		}
	} else {
    	$output .= "<status>0</status>\r";
	}
	$output .= "</search>\r";
}


// profile
if (substr_count($action, "profile") && $member_id > 0) {
	$output .= "<profile>\r";
	if ($profile == 0) { $profile = $member_id; }
	if ($profile > 0) {
		$conn = dbconn::open( "ggstofch" );
		$query = "SELECT personID,email AS userID,password,datenschutz,codex,roleID,role,organisationRegionID AS regionID,editProfile,viewNonpublicProfile,addProfile,closeProfile,confirmProfile,createList,exportData,editContent,uploadDocs,deleteDocs,viewPayment,editPayment
				  FROM tblPersonen
				  LEFT JOIN linkSysRoles ON tblPersonen.roleID=linkSysRoles.linkRoleID
				  LEFT JOIN (SELECT personID AS pID,organisationRegionID FROM tblVerein WHERE organisationRegionID BETWEEN 3 AND 10 AND datumEnde>=CURDATE()) AS qryVerein ON tblPersonen.personID=qryVerein.pID
				  WHERE personID='$profile' AND tblPersonen.veroeffentlichungID > 0";		
		$result = $conn->query($query);
		iOflogSearch(20, $user_id, "personID=$profile", mysql_num_rows($result), 1);
		$retval = $result->fetch(PDO::FETCH_ASSOC);
		if ($profile == $retval["personID"]) {
			$member = new person($retval["personID"]);
        	$member->getMilitaryData();
        	$member->getSocietyData();
        	$member->funktionID = $row[1];
			$k = 0;
			while($member->militaer[$k]->datumBeginn > date( "Y-m-d" ) ) { $k++; }
    		$output .= "<status>1</status>\r";
			$output .= "<id>".$retval["personID"]."</id>\r";
			$output .= "<anredeID>".$member->person->anredeID."</anredeID>\r";
			$output .= "<vorname>".$member->person->vorname."</vorname>\r";
			$output .= "<vorname2>".$member->person->vorname2."</vorname2>\r";
			$output .= "<name>".$member->person->name."</name>\r";
			$output .= "<geburtsdatum>".$member->person->geburtsdatum."</geburtsdatum>\r";
			$output .= "<spracheID>".$member->person->spracheID."</spracheID>\r";
			$output .= "<adresse>".$member->person->adresse."</adresse>\r";
			$output .= "<postfach>".$member->person->postfach."</postfach>\r";
			$output .= "<plz>".$member->person->plz."</plz>\r";
			$output .= "<ort>".$member->person->ort."</ort>\r";
			$output .= "<land>".$member->person->land."</land>\r";
			$output .= "<email>".$member->person->email."</email>\r";
			$output .= "<tf>".$member->person->tf."</tf>\r";
			$output .= "<mobil>".$member->person->mobil."</mobil>\r";
			$output .= "<url>".$member->person->url."</url>\r";
			$output .= "<xing>".$member->person->xing."</xing>\r";
			$output .= "<linkedin>".$member->person->linkedin."</linkedin>\r";
			$output .= "<facebook>".$member->person->facebook."</facebook>\r";
			$output .= "<dienstgrad>".$member->militaer[$k]->dienstgrad."</dienstgrad>\r";
			$output .= "<photo>http://plattform.ggstof.ch/include/profilePhoto.php?member={$member->id}</photo>\r";
		} else {
    		$output .= "<status>0</status>\r";
		}
	} else {
    	$output .= "<status>0</status>\r";
	}
	$output .= "</profile>\r";
}


// fokus
if (substr_count($action, "fokus")) {
    $fokus = contentRead(4);
    $output .= "<fokus>\r";
    $entries = explode("<a ", str_replace("\r", "", str_replace("\n", "", str_replace("<p>", "", str_replace("</p>", "", str_replace("<strong>", "", str_replace("</strong>", "", str_replace("<br />", "", $fokus[0][de]))))))));
    foreach ($entries as $entry) {
    	$title = substr($entry, strpos($entry, ">")+1);
    	$title = substr($title, 0, strpos($title, "</a>"));
    	$link = substr($entry, strpos($entry, "href=\"")+6);
    	$link = substr($link, 0, strpos($link, "\""));
    	if ($title != "" && $link != "") {
    		$output .= "<entry>\r";    
        	$output .= "<title>".html_entity_decode($title, ENT_QUOTES)."</title>\r";
        	$link = str_replace("../", "", $link);
        	if (substr($link, 0, 7) != "http://") { $link = "http://plattform.ggstof.ch/".$link; }
        	$output .= "<link>$link</link>\r"; 
    		$output .= "</entry>\r"; 
    	}
    }
    $output .= "</fokus>\r";
}


// statuten
if (substr_count($action, "statuten")) {
    $statuten = contentRead(5);
    $output .= "<statuten>\r";
	$output .= html_entity_decode($statuten[0][de], ENT_QUOTES);
    $output .= "</statuten>\r";
}


// vorstand
if (substr_count($action, "vorstand")) {
    $output .= "<vorstand>\r";
	$conn = dbconn::open( "ggstofch" );
    $query = "SELECT tblVerein.personID,funktionID,(funktionID=2) AS praesident,(funktionID=5) AS vrbofgsts
              FROM tblVerein
              LEFT JOIN tblPersonen ON tblVerein.personID=tblPersonen.personID
              WHERE organisationRegionID=2 AND datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
              GROUP BY tblVerein.personID
              ORDER BY praesident DESC,vrbofgsts ASC,name";
    $result = $conn->query($query);

    dbconn::close( $conn );
    $n = 0;
    while( $row = $result->fetch(PDO::FETCH_NUM) ) {
        $vorstand[$n] = new person( $row[0] );
        $vorstand[$n]->getMilitaryData();
        $vorstand[$n]->getSocietyData();
        $vorstand[$n]->funktionID = $row[1];
        $n++;
    }
    for( $i=0; $i<$n; $i++ ) {
        $j = 0;
        while( $vorstand[$i]->verein[$j]->funktionID != $vorstand[$i]->funktionID && $j<count( $vorstand[$i]->verein ) ) {
            $j++;
        }
        $output .= "<entry>\r";
		$output .= "<id>{$vorstand[$i]->id}</id>\r";
		$output .= "<photo>http://plattform.ggstof.ch/include/profilePhoto.php?member={$vorstand[$i]->id}</photo>\r";
		if( $vorstand[$i]->funktionID == 2 || $vorstand[$i]->funktionID == 5 ) {
			$output .= "<role>{$vorstand[$i]->verein[$j]->vereinFunktion}</role>\r";
		}
        $k = 0;
        while( $vorstand[$i]->militaer[$k]->datumBeginn > date( "Y-m-d" ) ) {
            $k++;
        }
		$output .= "<rank>".translate( $vorstand[$i]->militaer[$k]->dienstgrad, $vorstand[$i]->person->sprache ." ".translate( $vorstand[$i]->militaer[$k]->zusatzDg, $vorstand[$i]->person->sprache )."</rank>\r");
		if ($vorstand[$i]->person->vorname2 != "") {
			$output .= "<name>".$vorstand[$i]->person->vorname." ".$vorstand[$i]->person->vorname2." ".$vorstand[$i]->person->name."</name>\r";
		} else {
			$output .= "<name>".$vorstand[$i]->person->vorname." ".$vorstand[$i]->person->name."</name>\r";
		}
		$output .= "<start>".substr($vorstand[$i]->verein[$j]->datumBeginn, 0, 4)."</start>\r";
        if ($vorstand[$i]->funktionID != 5) {
        	$output .= "<until>".substr($vorstand[$i]->verein[$j]->datumEnde, 0, 4)."</until>\r";
        }
        if (!empty($vorstand[$i]->verein[$j]->bemerkungen)) {
            $output .= "<role>{$vorstand[$i]->verein[$j]->bemerkungen}</role>\r";
        }
		$output .= "<email>".$vorstand[$i]->person->email."</email>\r";
        $output .= "</entry>\r";
    }
    $output .= "</vorstand>\r";
}


// kontakt
if (substr_count($action, "kontakt")) {
    $output .= "<kontakt>\r";
    $output .= "<B>Gesellschaft der Generalstabsoffiziere (GGstOf)</B><BR>\r";
    $output .= "Vorstand<BR>\r";
    $output .= "6000 Luzern<BR>\r";
    $output .= "<BR>\r";
    $output .= "<U>Verantwortlich:</U><BR>\r";
    $output .= "Chef Kommunikation<BR>\r";
    $output .= "Oberstlt i Gst Markus M. Müller<BR>\r";
    $output .= "<A HREF=\"mailto:info@ggstof.ch\">info@ggstof.ch</A><BR>\r";
    $output .= "</kontakt>\r";
}


// print output
header("Content-Type: text/xml");
print "<";
print "?xml version=\"1.0\" encoding=\"utf-8\"?";
print ">\r\r";
print "<iof>\r";
print "\r";
print $output;
print "</iof>\r";

?>