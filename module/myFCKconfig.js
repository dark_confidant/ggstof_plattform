// define a reduced toolbar set
FCKConfig.ToolbarSets["GGstOf"] = [
    ['Source'],
    ['Cut','Copy','Paste','PasteText','PasteWord'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    ['Link','Unlink','Anchor'],
    ['Image','Table','Rule','Smiley','SpecialChar','PageBreak'],
    '/',
    ['FontFormat'],
    ['Bold','Italic','StrikeThrough','-','Subscript','Superscript'],
    ['OrderedList','UnorderedList','-','Outdent','Indent','Blockquote'],
    ['About']
];

FCKConfig.EnterMode = 'br';
