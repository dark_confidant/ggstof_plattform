<?php
    require_once('head.php');
    ggstof_head(true);

    function fncUdiff( $a, $b ) {
        if( $a["tblPersonen.personID"] > $b["tblPersonen.personID"] ) {
            return 1;
        } else if( $a["tblPersonen.personID"] < $b["tblPersonen.personID"] ) {
            return -1;
        } else {
            return 0;
        }
    }

    date_default_timezone_set( "Europe/Zurich" );


    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }

    if( $_SESSION["rights"]["roleID"] < 3 ) {
        die( translate( "Zugriff verweigert." ) );
    }

    $context = $_REQUEST["context"];
    $action  = $_REQUEST["action"];
    $period  = $_REQUEST["q"];
    $channel = $_REQUEST["c"];
    $data    = $_REQUEST;
// print_r( $data );
// exit;

    switch( $context ) {
        case "rechnungen":
            switch( $action ) {
                case "INSERT":
// exit;
                    ob_clean();
                    ob_flush();
                    if( $data["betrag"] ) {
                        setMembershipfee( date( "Y" ), $data["betrag"] );
                    }

// echo "1";
                    $invoiceMember    = $data["invoiceMember"];
                    $personID         = array_values( array_intersect_key( $data["personID"], $invoiceMember ) );
                    $jahr             = array_fill( 0, count( $invoiceMember ), date( "Y" ) );
                    $datumRechnung    = array_fill( 0, count( $invoiceMember ), $data["datumRechnung"] );
                    $paymentChannelID = array_values( array_intersect_key( $data["paymentChannelID"], $invoiceMember ) );

                    $retval = invoice( $personID, $jahr, $datumRechnung, $paymentChannelID );

                    $pdfBulk = ( $period < 3 ) ? new pdfLetterInvoice( "P", "mm", "A4" ) : new pdfLetterInvoiceSubperiod( "P", "mm", "A4" );
                    $pdfBulk->Init();
                    $members = array_keys( $data["invoiceMember"] );
                    $output = false;
                    for( $i=0; $i<count( $members ); $i++ ) {
                        if( in_array( $data["paymentChannelID"][$members[$i]], array( 1, 3 ) ) ) {
// echo "2";
                            $email = new email( $period < 3 ? 21 : 22, $data["personID"][$members[$i]], $data["paymentChannelID"][$members[$i]] );
                            $email->addCC( "kassier@ggstof.ch" );
                            $email->send();
// echo "3";
                        }
                        if( in_array( $data["paymentChannelID"][$members[$i]], array( 2, 4 ) ) ) {
// echo "4";
                            $pdf = ( $period < 3 ) ? new pdfLetterInvoice( "P", "mm", "A4" ) : new pdfLetterInvoiceSubperiod( "P", "mm", "A4" );
                            $pdf->Init();
                            $pdf->Compose( $data["personID"][$members[$i]], 2 );
                            $emailPDF = new email( 40, $data["personID"][$members[$i]], $data["paymentChannelID"][$members[$i]] );
                            $emailPDF->addAttachment( $pdf );
                            $emailPDF->addCC( "kassier@ggstof.ch" );
                            $emailPDF->send();
// echo "5";
                        }
                        if( in_array( $data["paymentChannelID"][$members[$i]], array( 5, 6 ) ) ) {
// echo "6";
                            $pdfBulk->Compose( $data["personID"][$members[$i]], $data["paymentChannelID"][$members[$i]] );
                            $download = true;
// echo "7";
                        }
                    }
                    if( $download ) {
// echo "8";
                        $pdfBulk->Download( "Jahresbeitrag " . date( "Y" ) . ".pdf" );
                    }
// exit;
                    if( !$retval ) {
                        header( "Location: admin.php?context=kassier" );
                    } else {
                        die( $retval );
                    }

                    break;

                case "EXPORT":
                    switch( $period ) {
                        case 1:
                            $h1 = translate( "Rechnungen an Mitgliederbestand per" ) . " 31.12." . ( date( "Y" ) - 1 ) . " " . translate( "ausstellen" );

                            // Aktuelle Liste GGstOf Mitglieder ohne Neumitglieder...
                            $criteria = array( "qryVerein.datumBeginn"=>array( "0000-00-00", ( date( "Y" )-1 ) . "-12-31" ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
                            $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            // ... und ohne neu-brevetierte Gst Of
                            $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) );
                            $exclude  = searchMember( $criteria, false, false, "tblPersonen.personID" );
                            $list     = array_values( array_udiff( $list, $exclude, "fncUdiff" ) );
                            break;

                        case 2:
                            $h1 = translate( "Rechnungen an Neumitglieder aus Q1 ausstellen" );

                            // Beitritte im Q1 ohne neu-brevetierte Gst Of
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-03-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                            $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-03-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>"NULL" );
                            $include  = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $list     = array_merge( $list, $include );
                            break;

                        case 3:
                            $h1 = translate( "Rechnungen an Neumitglieder aus Q2 ausstellen" );

                            // Beitritte im Q2 ohne neu-brevetierte Gst Of
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-04-01", date( "Y" ) . "-06-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                            $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-04-01", date( "Y" ) . "-06-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>"NULL" );
                            $include  = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $list     = array_merge( $list, $include );
                            break;

                        case 4:
                            $h1 = translate( "Rechnungen an Neumitglieder aus Q3 ausstellen" );

                            // Beitritte im Q3 ohne neu-brevetierte Gst Of
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-07-01", date( "Y" ) . "-09-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                            $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-07-01", date( "Y" ) . "-09-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>"NULL" );
                            $include  = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $list     = array_merge( $list, $include );
                            break;
                    }
                    // ... und ohne Eintr�ge mit Vorsch�ssen aus diesem Jahr und ohne Eintr�ge von bereits eingegangenen Einzahlungen
                    $criteria = array( "qryZahlungen.typID"=>1, "qryZahlungen.jahr"=>date( "Y" ), "qryZahlungen.datumEinzahlung"=>array( "0000-01-01", "9999-12-31" ) );
                    $exclude  = searchMember( $criteria, false, false, "tblPersonen.personID" );
                    $list     = array_values( array_udiff( $list, $exclude, "fncUdiff" ) );
                    $criteria = array( "qryZahlungen.typID"=>3, "qryZahlungen.jahr"=>date( "Y" ), "qryZahlungen.datumEinzahlung"=>array( "0000-01-01", "9999-12-31" ) );
                    $exclude  = searchMember( $criteria, false, false, "tblPersonen.personID" );
                    $list     = array_values( array_udiff( $list, $exclude, "fncUdiff" ) );
                    // ... und ohne Eintr�ge von bereits gestellten Rechnungen
                    $criteria = array( "qryZahlungen.jahr"=>date( "Y" ), "qryZahlungen.datumRechnung"=>array( "0000-01-01", "9999-12-31" ) );
                    $exclude  = searchMember( $criteria, false, false, "tblPersonen.personID" );
                    $list     = array_values( array_udiff( $list, $exclude, "fncUdiff" ) );
                    unset( $exclude );

                    switch( $_GET["format"] ) {
//                         case "pdf":
//                             break;

                        case "xls":
                            for( $i=0; $i<count( $list ); $i++ ) {
                                $content[0][$i] = strip_spaces( translate( $list[$i]["qryDienstgrad.dienstgrad"], $list[$i]["linkPersonSprache.sprache"], false, false ) . " " . translate( $list[$i]["qryDienstgrad.zusatzDg"], $list[$i]["linkPersonSprache.sprache"], false, false  ));
                                $content[0][$i] = utf8_decode($content[0][$i]);
                                $content[1][$i] = strip_spaces( $list[$i]["tblPersonen.vorname"] . " " . $list[$i]["tblPersonen.vorname2"] );
                                $content[2][$i] = $list[$i]["tblPersonen.name"];
                                $content[3][$i] = $list[$i]["tblPersonen.adresse"];
                                $content[4][$i] = $list[$i]["tblPersonen.postfach"];
                                $content[5][$i] = $list[$i]["tblPersonen.plz"];
                                $content[6][$i] = $list[$i]["tblPersonen.ort"];
                                $content[7][$i] = $list[$i]["tblPersonen.land"];
                                $content[8][$i] = $list[$i]["tblPersonen.email"];
                                $content[9][$i] = $list[$i]["linkPersonSprache.sprache"];
                                $content[10][$i] = $list[$i]["linkZahlungenChannel.paymentChannel"];
                                // "Z1-Z7"
                                $content[11][$i] = translate( $list[$i]["linkPersonAnrede.anrede"], $list[$i]["linkPersonSprache.sprache"], false, false ) . ( $list[$i]["qryDienstgrad.zusatzDgID"] == 4 ? "" : " " . $content[0][$i] );
                                $content[12][$i] = $content[2][$i] . " " . $content[1][$i] . ( $list[$i]["qryDienstgrad.zusatzDgID"] == 4 ? ", " . $content[0][$i] : "");
                                $tbl = $list[$i]["tblPersonen.paymentChannelID"] == 5 ? "qryBeruf" : "tblPersonen";
                                $content[13][$i] = $list[$i]["$tbl.postfach"];
                                $content[14][$i] = $list[$i]["$tbl.adresse"];
                                $content[15][$i] = $list[$i]["$tbl.plz"] . " " . $list[$i]["$tbl.ort"];
                                $content[16][$i] = str_replace( array( "Schweiz", "Suisse", "Svizzera", "Switzerland" ), "", $list[$i]["$tbl.land"] );
                                $content[17][$i] = translate( $list[$i]["linkPersonAnrede.anredeText"], $list[$i]["linkPersonSprache.sprache"], false, false ) . " " . translate( $list[$i]["qryDienstgrad.dienstgradText"], $list[$i]["linkPersonSprache.sprache"], false, false );
                                //Bugfix
                                $content[17][$i] = str_replace('Division?r', translate( 'Divisionär', $list[$i]["linkPersonSprache.sprache"], false, false ), $content[17][$i]);
                            }
                            $headerrow = array( translate( "Dienstgrad" ), translate( "Vorname" ), translate( "Name" ), translate( "Adresse" ), translate( "Postfach" ), translate( "PLZ" ), translate( "Ort" ), translate( "Land" ), translate( "E-Mail" ), translate( "Sprache" ), translate( "Versandweg" ), "Z1", "Z2", "Z3", "Z4", "Z5", "Z6", "Z7" );
                            export2xls( "Export", $content, $headerrow );
                            break;
                    }

                    break;

                default:
                    switch( $period ) {
                        case 1:
                            $h1 = translate( "Rechnungen an Mitgliederbestand per" ) . " 31.12." . ( date( "Y" ) - 1 ) . " " . translate( "ausstellen" );

                            // Aktuelle Liste GGstOf Mitglieder ohne Neumitglieder...
                            if( $channel == "p" ) {
                                // postalisch
                                $criteria = array( "qryBeitritt.beitrittsdatum"=>array( "0000-00-00", ( date( "Y" )-1 ) . "-12-31" ), "qryAustritt.austrittsdatum"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "tblPersonen.paymentChannelID"=>array( 5, 6 ) );
                                $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                                $h1 .= " (" . translate( "postalisch" ) . ")";
                            } elseif( $channel == "e" ) {
                                // elektronisch
                                $criteria = array( "qryBeitritt.beitrittsdatum"=>array( "0000-00-00", ( date( "Y" )-1 ) . "-12-31" ), "qryAustritt.austrittsdatum"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "tblPersonen.paymentChannelID"=>array( 1, 4 ) );
                                $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                                $h1 .= " (" . translate( "elektronisch" ) . ")";
                            } else {
                                // alle
                                $criteria = array( "qryBeitritt.beitrittsdatum"=>array( "0000-00-00", ( date( "Y" )-1 ) . "-12-31" ), "qryAustritt.austrittsdatum"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
                                $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            }
                            // ... und ohne neu-brevetierte Gst Of
                            $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) );
                            $exclude  = searchMember( $criteria, false, false, "tblPersonen.personID" );
                            $list     = array_values( array_udiff( $list, $exclude, "fncUdiff" ) );
                            break;

                        case 2:
                            $h1 = translate( "Rechnungen an Neumitglieder aus Q1 ausstellen" );

                            // Beitritte im Q1 ohne neu-brevetierte Gst Of
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-03-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                            $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-03-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>"NULL" );
                            $include  = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $list     = array_merge( $list, $include );
                            break;

                        case 3:
                            $h1 = translate( "Rechnungen an Neumitglieder aus Q2 ausstellen" );

                            // Beitritte im Q2 ohne neu-brevetierte Gst Of
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-04-01", date( "Y" ) . "-06-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                            $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-04-01", date( "Y" ) . "-06-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>"NULL" );
                            $include  = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $list     = array_merge( $list, $include );
                            break;

                        case 4:
                            $h1 = translate( "Rechnungen an Neumitglieder aus Q3 ausstellen" );

                            // Beitritte im Q3 ohne neu-brevetierte Gst Of
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-07-01", date( "Y" ) . "-09-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( "0000-00-00", date( "Y" )-1 . "-12-31" ) );
                            $list     = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $criteria = array( "qryBeitritt.beitrittsdatum"=>array( date( "Y" ) . "-07-01", date( "Y" ) . "-09-30" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>"NULL" );
                            $include  = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,qryBeruf.*,qryZahlungen.*,linkPersonSprache.sprache,linkPersonAnrede.anrede,linkPersonAnrede.anredeText,linkZahlungenChannel.paymentChannel" );
                            $list     = array_merge( $list, $include );
                            break;
                    }
                    // ... und ohne Eintr�ge mit Vorsch�ssen aus diesem Jahr und ohne Eintr�ge von bereits eingegangenen Einzahlungen
                    $criteria = array( "qryZahlungen.typID"=>1, "qryZahlungen.jahr"=>date( "Y" ), "qryZahlungen.datumEinzahlung"=>array( "0000-01-01", "9999-12-31" ) );
                    $exclude  = searchMember( $criteria, false, false, "tblPersonen.personID" );
                    $list     = array_values( array_udiff( $list, $exclude, "fncUdiff" ) );
                    $criteria = array( "qryZahlungen.typID"=>3, "qryZahlungen.jahr"=>date( "Y" ), "qryZahlungen.datumEinzahlung"=>array( "0000-01-01", "9999-12-31" ) );
                    $exclude  = searchMember( $criteria, false, false, "tblPersonen.personID" );
                    $list     = array_values( array_udiff( $list, $exclude, "fncUdiff" ) );
                    // ... und ohne Eintr�ge von bereits gestellten Rechnungen
                    $criteria = array( "qryZahlungen.jahr"=>date( "Y" ), "qryZahlungen.datumRechnung"=>array( "0000-01-01", "9999-12-31" ) );
                    $exclude  = searchMember( $criteria, false, false, "tblPersonen.personID" );
                    $list     = array_values( array_udiff( $list, $exclude, "fncUdiff" ) );
                    unset( $exclude );

                    $datumRechnung = date( "Y-m-d" );
                    for( $i=0; $i<count( $list ); $i++ ) {
                        $paymentChannelID[$i] = $list[$i]["tblPersonen.paymentChannelID"];
                    }

                    $invoiceMember = count( $list ) ? array_fill( 0, count( $list ), "1" ) : array();

                    $betrag = getMembershipfee( date( "Y" ) );
//                     $betrag = count( $list ) ? array_fill( 0, count( $list ), $row[0] ) : array();

                    break;
            }

            break;

        case "mitgliederbeitraege":
            $h1 = translate( "Erfasste Einzahlungen" );

            // Erfasste Einzahlungen
            $conn = dbconn::open();

            $query = "SELECT tblZahlungen.*,tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache,
                      SUM(tblZahlungen.betrag*(tblZahlungen.jahr=YEAR(CURDATE())-0)) AS sum0,
                      SUM(tblZahlungen.betrag*(tblZahlungen.jahr=YEAR(CURDATE())-1)) AS sum1,
                      SUM(tblZahlungen.betrag*(tblZahlungen.jahr=YEAR(CURDATE())-2)) AS sum2,
                      SUM(tblZahlungen.betrag*(tblZahlungen.jahr=YEAR(CURDATE())-3)) AS sum3,
                      SUM(tblZahlungen.betrag*(tblZahlungen.jahr=YEAR(CURDATE())-4)) AS sum4,
                      SUM(tblZahlungen.betrag) AS sumTotal
                      FROM tblZahlungen
                      LEFT JOIN tblPersonen ON tblZahlungen.personID=tblPersonen.personID
                      LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID
                      LEFT JOIN (
                          SELECT * FROM (
                              SELECT personID,dienstgradID,dienstgrad,zusatzDg.zusatz AS zusatzDg
                              FROM tblMilitaer
                              LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                              LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                              WHERE datumBeginn<=CURDATE()
                              ORDER BY datumBeginn DESC
                          ) AS qryDienstgrad2
                          GROUP BY personID
                      ) AS qryDienstgrad ON tblZahlungen.personID=qryDienstgrad.personID
                      WHERE tblZahlungen.typID=1 AND tblZahlungen.datumEinzahlung>'0000-00-00' AND tblZahlungen.jahr BETWEEN YEAR(CURDATE())-4 AND YEAR(CURDATE())
                      GROUP BY tblZahlungen.personID
                      ORDER BY tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
            $result = $conn->query($query);


            $list = array();
            while( $row = mysql_fetch_alias_assoc( $result ) ) {
                $list[] = $row;
            }

            $query = "SELECT
                      SUM(tblZahlungen.jahr=YEAR(CURDATE())-0) AS sum0,
                      SUM(tblZahlungen.jahr=YEAR(CURDATE())-1) AS sum1,
                      SUM(tblZahlungen.jahr=YEAR(CURDATE())-2) AS sum2,
                      SUM(tblZahlungen.jahr=YEAR(CURDATE())-3) AS sum3,
                      SUM(tblZahlungen.jahr=YEAR(CURDATE())-4) AS sum4,
                      COUNT(tblZahlungen.betrag) AS sumTotal
                      FROM tblZahlungen
                      WHERE tblZahlungen.typID=1 AND tblZahlungen.datumEinzahlung>'0000-00-00' AND tblZahlungen.jahr BETWEEN YEAR(CURDATE())-4 AND YEAR(CURDATE())";
            $result = $conn->query($query);


            while( $row = mysql_fetch_alias_assoc( $result ) ) {
                $list[] = $row;
            }

            dbconn::close( $conn );

            break;

        case "mitgliederbeitraege2":
            $h1 = translate( "Zahlungen nach Konto-Eingang" );

            // Erfasste Einzahlungen
            $conn = dbconn::open();

            $query = "SELECT tblZahlungen.*,tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache,
                      SUM(tblZahlungen.betrag*(YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-0)*(tblZahlungen.jahr=YEAR(CURDATE()))*(tblZahlungen.typID=1)) AS beitrag0,
                      SUM(tblZahlungen.betrag*(YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-0)*(tblZahlungen.jahr<YEAR(CURDATE()))*(tblZahlungen.typID=1)) AS beitrag0a,
                      SUM(tblZahlungen.betrag*(YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-0)*(tblZahlungen.typID=3)) AS vorauszahlung0,
                      SUM(tblZahlungen.betrag*(YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-0)*(tblZahlungen.typID=2)) AS spende0,
                      SUM(tblZahlungen.betrag*(YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-1)*(tblZahlungen.typID=1)) AS beitrag1,
                      SUM(tblZahlungen.betrag*(YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-1)*(tblZahlungen.typID=3)) AS vorauszahlung1,
                      SUM(tblZahlungen.betrag*(YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-1)*(tblZahlungen.typID=2)) AS spende1,
                      SUM(tblZahlungen.betrag) AS total
                      FROM tblZahlungen
                      LEFT JOIN tblPersonen ON tblZahlungen.personID=tblPersonen.personID
                      LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID
                      LEFT JOIN (
                          SELECT * FROM (
                              SELECT personID,dienstgradID,dienstgrad,zusatzDg.zusatz AS zusatzDg
                              FROM tblMilitaer
                              LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                              LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                              WHERE datumBeginn<=CURDATE()
                              ORDER BY datumBeginn DESC
                          ) AS qryDienstgrad2
                          GROUP BY personID
                      ) AS qryDienstgrad ON tblZahlungen.personID=qryDienstgrad.personID
                      WHERE YEAR(tblZahlungen.datumEinzahlung) BETWEEN YEAR(CURDATE())-1 AND YEAR(CURDATE())
                      GROUP BY tblZahlungen.personID
                      ORDER BY tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
            $result = $conn->query($query);


            $list = array();
            while( $row = mysql_fetch_alias_assoc( $result ) ) {
                $list[] = $row;
            }

            $query = "SELECT
                      SUM((YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-0)*(tblZahlungen.jahr=YEAR(CURDATE()))*(tblZahlungen.typID=1)) AS beitrag0,
                      SUM((YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-0)*(tblZahlungen.jahr<YEAR(CURDATE()))*(tblZahlungen.typID=1)) AS beitrag0a,
                      SUM((YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-0)*(tblZahlungen.typID=3)) AS vorauszahlung0,
                      SUM((YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-0)*(tblZahlungen.typID=2)) AS spende0,
                      SUM((YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-1)*(tblZahlungen.typID=1)) AS beitrag1,
                      SUM((YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-1)*(tblZahlungen.typID=3)) AS vorauszahlung1,
                      SUM((YEAR(tblZahlungen.datumEinzahlung)=YEAR(CURDATE())-1)*(tblZahlungen.typID=2)) AS spende1,
                      COUNT(tblZahlungen.betrag) AS total
                      FROM tblZahlungen
                      WHERE YEAR(tblZahlungen.datumEinzahlung) BETWEEN YEAR(CURDATE())-1 AND YEAR(CURDATE())";
            $result = $conn->query($query);


            while( $row = mysql_fetch_alias_assoc( $result ) ) {
                $list[] = $row;
            }

            dbconn::close( $conn );

            break;

        case "einzahlung":
            $h1 = translate( "Einzahlungen erfassen" ) . "/" . translate( "Mahnungen ausstellen" );

//             if( $action == "UPDATE" ) {
// //                 $retval = payment( array_values( array_intersect_key( $data["zahlungID"], $data["check"] ) ), array_values( array_intersect_key( $data["datumEinzahlung"], $data["check"] ) ) );
//                 $retval = payment( $data["zahlungID"], $data["datumEinzahlung"] );
//                 if( $retval ) {
//                     die( $retval );
//                 }
//             }
            if( $action == "REMIND" ) {
                $pdfBulk = new pdfLetterPaymentReminder( "P", "mm", "A4" );
                $pdfBulk->Init();
                $output = false;
                for( $i=0; $i<count( $data["zahlungID"] ); $i++ ) {
                    if( !$data["check"][$i] ) continue;

                    if( $data["channel"] != "pdf" && in_array( $data["paymentChannelID"][$i], array( 1, 3 ) ) ) {
                        $email = new email( 20, $data["personID"][$i], $data["paymentChannelID"][$i] );
                        $email->addCC( "kassier@ggstof.ch" );
                        $email->send();

                        $conn = dbconn::open();
                        $query = "UPDATE tblZahlungen
                                  SET anzahlMahnungen=anzahlMahnungen+1,datumMahnung=CURDATE(),paymentChannelID={$data["paymentChannelID"][$i]}
                                  WHERE zahlungID={$data["zahlungID"][$i]}";
                        $conn->query($query);

                        dbconn::close( $conn );
                    }
                    if( $data["channel"] != "pdf" && in_array( $data["paymentChannelID"][$i], array( 2, 4 ) ) ) {
                        $pdf = new pdfLetterPaymentReminder( "P", "mm", "A4" );
                        $pdf->Init();
                        $pdf->Compose( $data["personID"][$i], 2 );
                        $emailPDF = new email( 41, $data["personID"][$i], $data["paymentChannelID"][$i] );
                        $emailPDF->addAttachment( $pdf );
                        $emailPDF->addCC( "kassier@ggstof.ch" );
                        $emailPDF->send();

                        $conn = dbconn::open();
                        $query = "UPDATE tblZahlungen
                                  SET anzahlMahnungen=anzahlMahnungen+1,datumMahnung=CURDATE(),paymentChannelID={$data["paymentChannelID"][$i]}
                                  WHERE zahlungID={$data["zahlungID"][$i]}";
                        $conn->query($query);

                        dbconn::close( $conn );
                    }
                    if( $data["channel"] != "email" && in_array( $data["paymentChannelID"][$i], array( 5, 6 ) ) ) {
                        $pdfBulk->Compose( $data["personID"][$i], $data["paymentChannelID"][$i] );
                        $download = true;

                        $conn = dbconn::open();
                        $query = "UPDATE tblZahlungen
                                  SET anzahlMahnungen=anzahlMahnungen+1,datumMahnung=CURDATE(),paymentChannelID={$data["paymentChannelID"][$i]}
                                  WHERE zahlungID={$data["zahlungID"][$i]}";
                        $conn->query($query);

                        dbconn::close( $conn );
                    }
                }
                if( $download ) {
//                     $pdfBulk->Download( "Mahnungen " . date( "Y-m-d" ) . ".pdf" );
                    $pdfBulk->Download( "Mahnung-{$data["zahlungID"][0]}--" . date( "Y-m-d" ) . ".pdf" );
                }

//                 header( "Location: admin.php?context=kassier" );
            }

            // Ausstehende Beitr�ge aktuelles Jahr
//             $criteria = array( "tblZahlungen.typID"=>1, "tblZahlungen.jahr"=>date( "Y" ), "tblZahlungen.datumEinzahlung"=>"0000-00-00" );
            $criteria = array( "tblZahlungen.typID"=>1, "tblZahlungen.datumEinzahlung"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
            $list = searchPayment( $criteria, "tblZahlungen.datumRechnung,tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort" );

            break;

        case "ausstehend":
            $h1 = translate( "Ausstehende Mitgliederbeiträge" );

            if( $action == "REMIND" ) {
                $members = array_keys( $data["remindMember"] );
                $pdfBulk = new pdfLetterPaymentLastCall( "P", "mm", "A4" );
                $pdfBulk->Init();
                $output = false;
                for( $i=0; $i<count( $members ); $i++ ) {
                    $pdfBulk->Compose( $data["personID"][$members[$i]] );
                    $download = true;

                    $conn = dbconn::open();
                    $query = "UPDATE tblZahlungen
                              SET anzahlMahnungen=anzahlMahnungen+1,datumMahnung=CURDATE()
                              WHERE personID={$data["personID"][$members[$i]]} AND datumEinzahlung='0000-00-00'";
                    $conn->query($query);

                    dbconn::close( $conn );
                }
                if( $download ) {
                    $pdfBulk->Download( "Aufforderungen " . date( "Y-m-d" ) . ".pdf" );
                }

            }

            // Ausstehende Beitr�ge
            $criteria = array( "tblZahlungen.typID"=>1, "tblZahlungen.datumEinzahlung"=>"0000-00-00", "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ), "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
            $list = searchPayment( $criteria, "tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort,tblZahlungen.datumMahnung DESC" );

            break;

        case "vorauszahlungen":
            $h1 = translate( "Vorauszahlungen" );

            // Vorsch�sse
            $criteria = array( "tblZahlungen.typID"=>3 );
            $list = searchPayment( $criteria );

            break;

        case "spenden":
            $h1 = translate( "Spenden" );

            // Spenden
            $conn = dbconn::open();
            $query = "SELECT tblZahlungen.*,tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache,
                      SUM(tblZahlungen.betrag*(1-ABS(SIGN(tblZahlungen.jahr-YEAR(CURDATE())+0)))) AS sum0,
                      SUM(tblZahlungen.betrag*(1-ABS(SIGN(tblZahlungen.jahr-YEAR(CURDATE())+1)))) AS sum1,
                      SUM(tblZahlungen.betrag*(1-ABS(SIGN(tblZahlungen.jahr-YEAR(CURDATE())+2)))) AS sum2,
                      SUM(tblZahlungen.betrag*(1-ABS(SIGN(tblZahlungen.jahr-YEAR(CURDATE())+3)))) AS sum3,
                      SUM(tblZahlungen.betrag*(1-ABS(SIGN(tblZahlungen.jahr-YEAR(CURDATE())+4)))) AS sum4,
                      SUM(tblZahlungen.betrag) AS sumTotal
                      FROM tblZahlungen
                      LEFT JOIN tblPersonen ON tblZahlungen.personID=tblPersonen.personID
                      LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID
                      LEFT JOIN (
                          SELECT personID,dienstgradID,dienstgrad,zusatzDg.zusatz AS zusatzDg
                          FROM tblMilitaer
                          LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                          LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                          WHERE datumBeginn<=CURDATE() AND datumEnde>=CURDATE()
                          GROUP BY personID
                          ORDER BY datumBeginn DESC
                      ) AS qryDienstgrad ON tblZahlungen.personID=qryDienstgrad.personID
                      WHERE tblZahlungen.typID=2 AND tblZahlungen.jahr<=YEAR(CURDATE())
                      GROUP BY tblZahlungen.personID
                      ORDER BY tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2,tblPersonen.ort";
            $result = $conn->query($query);

            dbconn::close( $conn );

            $list = array();
            while( $row = mysql_fetch_alias_assoc( $result ) ) {
                $list[] = $row;
            }

            break;
    }

    $xlsButton = new imgButton;
    $xlsButton->href = "{$_SERVER["SCRIPT_NAME"]}?context=rechnungen&amp;q=$period&amp;action=EXPORT&amp;format=xls";
    $xlsButton->class = "";
    $xlsButton->src = "image/template/excel-16x16.png";
    $xlsButton->imgclass = "icon";
    $xlsButton->alt = "Excel-Liste exportieren";
    $xlsButton->title = "Excel-Liste exportieren";
    $xlsButton->caption = "Export";
?>




<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Rechnungen stellen" ); ?></title>
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/accordeon.js"></script>
<script type="text/javascript" src="script/xmlHttp.js"></script>
<script type="text/javascript" src="script/sorttable.js"></script>
<script type="text/javascript" src="script/tools.js"></script>
<script type="text/javascript">
<!--

imgDummy = new Image();
imgDummy.src = "image/dummy.gif";
imgWheel = new Image();
imgWheel.src = "image/template/spinwheel.gif";
imgOk = new Image();
imgOk.src = "image/template/ok.png";

var button;
var img;

// -->
</script>
</head>


<body>


<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>

<div id="content">

<?php
    switch( $context ) {
        case "rechnungen":
?>
<h1><?php echo $h1; ?></h1>

<?php $limit = ((count($list) > 300) ? 300 : count($list)); ?>
<form id="formInvoice" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
<fieldset>
<input type="hidden" name="context" value="rechnungen" />
<input type="hidden" name="action" value="INSERT" />
<input type="hidden" name="q" value="<?php echo $period; ?>" />

<p>
<?php echo translate( "Datum" ) . " " . translate( "der" ) . " " . $limit . "/" . count( $list ) . " " . translate( "Rechnungstellungen" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")"; ?>: <input type="text" name="datumRechnung" value="<?php echo $datumRechnung; ?>" />
<?php echo translate( "Mitgliederbeitrag" ); ?>: <?php if( $betrag ) { echo $betrag; } else { ?><input type="text" name="betrag" value="" /><?php } ?> CHF
</p>    
<table class="tableGrid tableMargin">
<thead>
<tr>
<th class="firstColumn"><?php echo translate( "Alle" ); ?> <input type="checkbox" checked="checked" onclick="this.checked=true; for( i=0; i<<?php echo count( $list ); ?>; i++ ) { document.forms['formInvoice'].elements['invoiceMember['+i+']'].checked=(true && !document.forms['formInvoice'].elements['invoiceMember['+i+']'].disabled); }" />/<input type="checkbox" onclick="this.checked=false; for( i=0; i<<?php echo count( $list ); ?>; i++ ) { document.forms['formInvoice'].elements['invoiceMember['+i+']'].checked=false; } return false;" /></th>
<th><?php echo translate( "Name" ); ?></th>
<th><?php echo translate( "Versandweg" ); ?></th>
<th><?php echo translate( "Adresse" ) . "/" . translate( "E-Mail" ); ?></th>
</tr>
</thead>
<tbody>
<?php
    $select = new select( NULL, "linkZahlungenChannel" );

    for( $i=0; $i<$limit; $i++ ) {
        echo "<tr>";
        echo "<td class=\"firstColumn\"><input name=\"invoiceMember[$i]\" value=\"1\" type=\"checkbox\" checked=\"checked\" /></td>";
        echo "<td><a href=\"member_profile.php?member={$list[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate( $list[$i]["qryDienstgrad.dienstgrad"], $list[$i]["linkPersonSprache.sprache"] ) . " " . translate( $list[$i]["qryDienstgrad.zusatzDg"], $list[$i]["linkPersonSprache.sprache"] ) . "<br /><strong>" . $list[$i]["tblPersonen.name"] . "</strong> " . $list[$i]["tblPersonen.vorname"] . " " . $list[$i]["tblPersonen.vorname2"]."</a><input type=\"hidden\" name=\"personID[$i]\" value=\"{$list[$i]["tblPersonen.personID"]}\" /></td>";
        echo "<td>";
        $select->output( array( "name"=>"paymentChannelID[$i]", "selected"=>$paymentChannelID[$i], "onchange"=>"document.getElementById('versandweg[$i][1]').style.display='none'; document.getElementById('versandweg[$i][2]').style.display='none'; document.getElementById('versandweg[$i][3]').style.display='none'; document.getElementById('versandweg[$i][4]').style.display='none'; document.getElementById('versandweg[$i][5]').style.display='none'; document.getElementById('versandweg[$i][6]').style.display='none'; document.getElementById('versandweg[$i]['+this.value+']').style.display='';" ) );
        echo "</td>
                  <td>
                  <div id=\"versandweg[$i][1]\"" . ( $paymentChannelID[$i] != 1 ? " style=\"display:none\"" : "" ) . "><a href=\"mailto:{$list[$i]["qryBeruf.email"]}\">{$list[$i]["qryBeruf.email"]}</a></div>
                  <div id=\"versandweg[$i][2]\"" . ( $paymentChannelID[$i] != 2 ? " style=\"display:none\"" : "" ) . "><a href=\"mailto:{$list[$i]["qryBeruf.email"]}\">{$list[$i]["qryBeruf.email"]}</a></div>
                  <div id=\"versandweg[$i][3]\"" . ( $paymentChannelID[$i] != 3 ? " style=\"display:none\"" : "" ) . "><a href=\"mailto:{$list[$i]["tblPersonen.email"]}\">{$list[$i]["tblPersonen.email"]}</a></div>
                  <div id=\"versandweg[$i][4]\"" . ( $paymentChannelID[$i] != 4 ? " style=\"display:none\"" : "" ) . "><a href=\"mailto:{$list[$i]["tblPersonen.email"]}\">{$list[$i]["tblPersonen.email"]}</a></div>
                  <div id=\"versandweg[$i][5]\"" . ( $paymentChannelID[$i] != 5 ? " style=\"display:none\"" : "" ) . ">" . $list[$i]["qryBeruf.adresse"] . " " . ( !empty( $list[$i]["qryBeruf.postfach"] ) ? ", " . $list[$i]["qryBeruf.postfach"] : "" ) . "<br />" . $list[$i]["qryBeruf.plz"] . " " . $list[$i]["qryBeruf.ort"]  . ( !in_array( $list[$i]["qryBeruf.land"], array( "Schweiz", "Suisse", "Svizzera", "Switzerland", "" ) ) ? ", " . $list[$i]["qryBeruf.land"] : "" ) . "</div>
                  <div id=\"versandweg[$i][6]\"" . ( $paymentChannelID[$i] != 6 ? " style=\"display:none\"" : "" ) . ">" . $list[$i]["tblPersonen.adresse"] . " " . ( !empty( $list[$i]["tblPersonen.postfach"]) ? ", " . $list[$i]["tblPersonen.postfach"] : "" ) . "<br />" . $list[$i]["tblPersonen.plz"] . " " . $list[$i]["tblPersonen.ort"] . ( !in_array( $list[$i]["tblPersonen.land"], array( "Schweiz", "Suisse", "Svizzera", "Switzerland", "" ) ) ? ", " . $list[$i]["tblPersonen.land"] : "" ) . "</div>
                  </td>
              </tr>";
    }
?>
</tbody>
</table>
<input type="button" class="formsSubmitButton" value="<?php echo translate( "Rechnungen erstellen und versenden" ); ?>" onclick="<?php if( !$betrag ) { echo "if( document.forms['formInvoice'].elements['betrag'].value == '' ) { window.alert( '" . translate( "Bitte Mitgliederbeitrag eingeben" ) . ".' ); return false; } "; } ?>document.forms['formInvoice'].submit(); this.disabled=true;" />
<p><?php echo translate( "Der Rechnungsversand kann einige Zeit in Anspruch nehmen." ); ?></p>
</fieldset>
</form>

<p><br /><?php $xlsButton->output(); ?></p>

<?php
        break;

        case "mitgliederbeitraege":
?>
<h1><?php echo $h1; ?></h1>

<table class="tableGrid tableMargin">
<thead>
<tr>
<th class="firstColumn"><?php echo translate( "Name" ); ?></th>
<th><?php echo date( "Y" ); ?></th>
<th><?php echo date( "Y" )-1; ?></th>
<th><?php echo date( "Y" )-2; ?></th>
<th><?php echo date( "Y" )-3; ?></th>
<th><?php echo date( "Y" )-4; ?></th>
<th><?php echo translate( "Total" ); ?></th>
</tr>
</thead>
<?php
    echo "<tbody>";
    for( $i=0; $i<count( $list )-1; $i++ ) {
        echo "<tr>";
        echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$list[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate( $list[$i]["qryDienstgrad.dienstgrad"], $list[$i]["linkPersonSprache.sprache"] ) . " " . translate( $list[$i]["qryDienstgrad.zusatzDg"], $list[$i]["linkPersonSprache.sprache"] ) . "<br /><strong>".$list[$i]["tblPersonen.name"]."</strong> ".$list[$i]["tblPersonen.vorname"]." ".$list[$i]["tblPersonen.vorname2"]."<br />".$list[$i]["tblPersonen.ort"]."</a>";
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum0"] ? $list[$i][".sum0"] : "&nbsp;" ) . "</td>"; $sums0 += $list[$i][".sum0"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum1"] ? $list[$i][".sum1"] : "&nbsp;" ) . "</td>"; $sums1 += $list[$i][".sum1"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum2"] ? $list[$i][".sum2"] : "&nbsp;" ) . "</td>"; $sums2 += $list[$i][".sum2"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum3"] ? $list[$i][".sum3"] : "&nbsp;" ) . "</td>"; $sums3 += $list[$i][".sum3"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum4"] ? $list[$i][".sum4"] : "&nbsp;" ) . "</td>"; $sums4 += $list[$i][".sum4"];
        echo "<td class=\"bold alignRight\">{$list[$i][".sumTotal"]}</td>"; $sumsTotal += $list[$i][".sumTotal"];
        echo "</tr>";
    }
    echo "</tbody>";
    echo "<tfoot>";
    echo "<tr>";
    echo "<td class=\"bold firstColumn\">" . translate( "Total" ) . "</td>";
    echo "<td class=\"bold alignRight\">$sums0</td>";
    echo "<td class=\"bold alignRight\">$sums1</td>";
    echo "<td class=\"bold alignRight\">$sums2</td>";
    echo "<td class=\"bold alignRight\">$sums3</td>";
    echo "<td class=\"bold alignRight\">$sums4</td>";
    echo "<td class=\"bold alignRight\">CHF $sumsTotal</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td class=\"firstColumn\">" . translate( "Anzahl Einzahlungen" ) . "</td>";
    echo "<td class=\"alignRight\">{$list[$i][".sum0"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".sum1"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".sum2"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".sum3"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".sum4"]}</td>";
    echo "<td class=\"bold alignRight\">{$list[$i][".sumTotal"]}</td>";
    echo "</tr>";
    echo "</tfoot>";
?>
</table>

<?php
        break;

        case "mitgliederbeitraege2":
?>
<h1><?php echo $h1; ?></h1>

<table class="tableGrid tableMargin">
<thead>
<tr>
<th class="firstColumn"><?php echo translate( "Name" ); ?></th>
<th><?php echo translate( "Beiträge" ) . " " . date( "Y" ); ?></th>
<th><?php echo translate( "Beiträge" ) . " " . translate( "aus früheren Jahren" ); ?></th>
<th><?php echo translate( "Vorauszahlungen" ) . " " . date( "Y" ); ?></th>
<th><?php echo translate( "Spenden" ) . " " . date( "Y" ); ?></th>
<th><?php echo translate( "Beiträge" ) . " " . ( date( "Y" )-1 ); ?></th>
<th><?php echo translate( "Vorauszahlungen" ) . " " . ( date( "Y" )-1 ); ?></th>
<th><?php echo translate( "Spenden" ) . " " . ( date( "Y" )-1 ); ?></th>
<th><?php echo translate( "Total" ); ?></th>
</tr>
</thead>
<?php
    echo "<tbody>";
    for( $i=0; $i<count( $list )-1; $i++ ) {
        echo "<tr>";
        echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$list[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate( $list[$i]["qryDienstgrad.dienstgrad"], $list[$i]["linkPersonSprache.sprache"] ) . " " . translate( $list[$i]["qryDienstgrad.zusatzDg"], $list[$i]["linkPersonSprache.sprache"] ) . "<br /><strong>".$list[$i]["tblPersonen.name"]."</strong> ".$list[$i]["tblPersonen.vorname"]." ".$list[$i]["tblPersonen.vorname2"]."<br />".$list[$i]["tblPersonen.ort"]."</a>";
        echo "<td class=\"alignRight\">" . ( $list[$i][".beitrag0"] ? $list[$i][".beitrag0"] : "&nbsp;" ) . "</td>"; $beitraege0 += $list[$i][".beitrag0"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".beitrag0a"] ? $list[$i][".beitrag0a"] : "&nbsp;" ) . "</td>"; $beitraege0a += $list[$i][".beitrag0a"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".vorauszahlung0"] ? $list[$i][".vorauszahlung0"] : "&nbsp;" ) . "</td>"; $vorauszahlungen0 += $list[$i][".vorauszahlung0"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".spende0"] ? $list[$i][".spende0"] : "&nbsp;" ) . "</td>"; $spenden0 += $list[$i][".spende0"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".beitrag1"] ? $list[$i][".beitrag1"] : "&nbsp;" ) . "</td>"; $beitraege1 += $list[$i][".beitrag1"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".vorauszahlung1"] ? $list[$i][".vorauszahlung1"] : "&nbsp;" ) . "</td>"; $vorauszahlungen1 += $list[$i][".vorauszahlung1"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".spende1"] ? $list[$i][".spende1"] : "&nbsp;" ) . "</td>"; $spenden1 += $list[$i][".spende1"];
        echo "<td class=\"bold alignRight\">{$list[$i][".total"]}</td>"; $totals += $list[$i][".total"];
        echo "</tr>";
    }
    echo "</tbody>";
    echo "<tfoot>";
    echo "<tr>";
    echo "<td class=\"bold firstColumn\">" . translate( "Total" ) . "</td>";
    echo "<td class=\"bold alignRight\">$beitraege0</td>";
    echo "<td class=\"bold alignRight\">$beitraege0a</td>";
    echo "<td class=\"bold alignRight\">$vorauszahlungen0</td>";
    echo "<td class=\"bold alignRight\">$spenden0</td>";
    echo "<td class=\"bold alignRight\">$beitraege1</td>";
    echo "<td class=\"bold alignRight\">$vorauszahlungen1</td>";
    echo "<td class=\"bold alignRight\">$spenden1</td>";
    echo "<td class=\"bold alignRight\">CHF $totals</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td class=\"firstColumn\">" . translate( "Anzahl Einzahlungen" ) . "</td>";
    echo "<td class=\"alignRight\">{$list[$i][".beitrag0"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".beitrag0a"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".vorauszahlung0"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".spende0"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".beitrag1"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".vorauszahlung1"]}</td>";
    echo "<td class=\"alignRight\">{$list[$i][".spende1"]}</td>";
    echo "<td class=\"bold alignRight\">{$list[$i][".total"]}</td>";
    echo "</tr>";
    echo "</tfoot>";
?>
</table>

<?php
        break;

        case "einzahlung":
?>
<div class="accordeon accordeonProfile">

<h1><?php echo $h1; ?></h1>

<p>
<?php
    echo translate( "Ausstehende Beiträge" ) . " (" . count( $list ) . ")";

    $sublist = array();
    $j = 0;
    for( $i=0; $i<count( $list ); $i++ ) {
        $sublist[$j][] = $list[$i];
        $j += (int) ( substr( $list[$i]["tblZahlungen.datumRechnung"], 0, 4 ) == date( "Y" ) && ceil( substr( $list[$i]["tblZahlungen.datumRechnung"], 5, 2 )/3 ) != ceil( substr( $list[$i+1]["tblZahlungen.datumRechnung"], 5, 2 )/3 ) || substr( $list[$i]["tblZahlungen.datumRechnung"], 0, 4 ) != substr( $list[$i+1]["tblZahlungen.datumRechnung"], 0, 4 ) );
    }
    unset( $list );

    $select = new select( NULL, "linkZahlungenChannel" );
?>
</p>

<form id="formPayment" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
<fieldset>
<input type="hidden" name="context" value="einzahlung" />
<input type="hidden" name="action" value="UPDATE" />
<input type="hidden" name="channel" value="" />

<?php
    $k = 0;
    for( $i=0; $i<count( $sublist ); $i++ ) {
?>

<div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop(); return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo substr( $sublist[$i][0]["tblZahlungen.datumRechnung"], 0, 4 ) . ( substr( $sublist[$i][0]["tblZahlungen.datumRechnung"], 0, 4 ) == date( "Y" ) ? "Q" . ceil( substr( $sublist[$i][0]["tblZahlungen.datumRechnung"], 5, 2 )/3 ) : "" ) . " (" . count( $sublist[$i] ) . ")"; ?>:</span></a>
<div class="groupChildren">
<div class="child">

<!--p>
<?php
    echo translate( "Ausgewählte" ) . ": ";
?>
<input type="button" class="formsSubmitButton" value="<?php echo translate( "E-Mail Mahnungen versenden" ); ?>" onclick="var form=document.forms['formPayment']; if( window.confirm( '<?php echo translate( "Mahnungen auslösen?" ); ?>' ) ) { form.elements['action'].value='REMIND'; form.elements['channel'].value = 'email'; form.submit(); }" />
<input type="button" class="formsSubmitButton" value="<?php echo translate( "Postalische Mahnungen (PDF) erstellen" ); ?>" onclick="var form=document.forms['formPayment']; form.elements['action'].value='REMIND'; form.elements['channel'].value='pdf'; form.submit();" />
</p-->

<table class="tableGrid tableMargin sortable">
<thead>
<tr>
<!--th class="firstColumn nowrap sorttable_nosort"><?php echo translate( "Alle" ); ?> <input type="checkbox" checked="checked" onclick="this.checked=true; for( i=<?php echo $k; ?>; i<<?php echo ( $k + count( $sublist[$i] ) ); ?>; i++ ) { document.forms['formPayment'].elements['check['+i+']'].checked=true; }" />/<input type="checkbox" onclick="this.checked=false; for( i=<?php echo $k; ?>; i<<?php echo ( $k + count( $sublist[$i] ) ); ?>; i++ ) { document.forms['formPayment'].elements['check['+i+']'].checked=false; }" /></th-->
<th class="firstColumn sorttable_sorted"><?php echo translate( "Grad" ) . " " . translate( "Name" ) . " " . translate( "Vorname" ) . ", " . translate( "Wohnort" ); ?><span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
<th><?php echo translate( "Datum der Rechnungsstellung" ) . "/<br />" . translate( "Mahnung" ); ?></th>
<th class="sorttable_nosort"><?php echo translate( "Datum des Zahlungseingangs" ) . "<br />(" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")"; ?></th>
<th><?php echo translate( "Versandweg" ); ?></th>
</tr>
</thead>
<tbody>
<?php
        for( $j=0; $j<count( $sublist[$i] ); $j++ ) {
            echo "<tr>";
//             echo "<td class=\"firstColumn\"><input name=\"check[$k]\" type=\"checkbox\" value=\"1\" /></td>";
            echo "<td class=\"firstColumn\" sorttable_customkey=\"{$sublist[$i][$j]["tblPersonen.name"]}{$sublist[$i][$j]["tblPersonen.vorname"]}{$sublist[$i][$j]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$sublist[$i][$j]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate( $sublist[$i][$j]["qryDienstgrad.dienstgrad"], $sublist[$i][$j]["linkPersonSprache.sprache"] ) . " " . translate( $sublist[$i][$j]["qryDienstgrad.zusatzDg"], $sublist[$i][$j]["linkPersonSprache.sprache"] ) . "<br /><strong>".$sublist[$i][$j]["tblPersonen.name"]."</strong> ".$sublist[$i][$j]["tblPersonen.vorname"]." ".$sublist[$i][$j]["tblPersonen.vorname2"]."<br />" . $sublist[$i][$j]["tblPersonen.ort"] . ( !in_array( $sublist[$i][$j]["tblPersonen.land"], array( "Schweiz", "Suisse", "Svizzera", "Switzerland", "" ) ) ? ", " . $sublist[$i][$j]["tblPersonen.land"] : "" ) . "</a></td>";
            echo "<td>" . ( substr( $sublist[$i][$j]["tblZahlungen.datumRechnung"], 0, 4 )<date( "Y" ) ? "<strong>" : "" ) . "{$sublist[$i][$j]["tblZahlungen.datumRechnung"]}" . ( $sublist[$i][$j]["tblZahlungen.anzahlMahnungen"] ? "<br />{$sublist[$i][$j]["tblZahlungen.datumMahnung"]} ({$sublist[$i][$j]["tblZahlungen.anzahlMahnungen"]})" : "" ) . ( substr( $sublist[$i][$j]["tblZahlungen.datumRechnung"], 0, 4 )<date( "Y" ) ? "</strong>" : "" ) . "</td>";
            echo "<td><input type=\"text\" name=\"datumEinzahlung[$k]\" value=\"\" maxlength=\"10\" />
                      <input type=\"button\" name=\"buttonPaymentDate[$k]\" value=\"" . translate( "Erfassen" ) . "\" onclick=\"str = document.forms['formPayment'].elements['datumEinzahlung[$k]'].value; if( str.search(/[0-9]{4}-[0-9]{2}-[0-9]{2}/) != -1 ) { button=this; img=document.getElementById( 'spinwheel1$k' ); sendXmlHttpRequest( 'setPaymentDate.php', 'zahlungID={$sublist[$i][$j]["tblZahlungen.zahlungID"]}&amp;datumEinzahlung='+document.forms['formPayment'].elements['datumEinzahlung[$k]'].value, 'startSpin( button, img );', 'stopSpin( button, img, imgOk );' ); } else { window.alert( '" . translate( "Falsches Datumsformat" ) . "' ); }\" /><img id=\"spinwheel1$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />
                  </td>";
            echo "<td sorttable_customkey=\"{$sublist[$i][$j]["linkZahlungenChannel.paymentChannel"]}\">";
            $select->output( array( "name"=>"paymentChannelID[$k]", "selected"=>$sublist[$i][$j]["tblZahlungen.paymentChannelID"] ) );
            echo " <input type=\"button\" value=\"" . translate( "Mahnung ausstellen" ) . "\" onclick=\"if( document.forms['formPayment'].elements['paymentChannelID[$k]'].value == 5 || document.forms['formPayment'].elements['paymentChannelID[$k]'].value == 6 ) { window.location='{$_SERVER["SCRIPT_NAME"]}?action=REMIND&amp;context=einzahlung&amp;zahlungID[0]={$sublist[$i][$j]["tblZahlungen.zahlungID"]}&amp;personID[0]={$sublist[$i][$j]["tblPersonen.personID"]}&amp;check[0]=1&amp;paymentChannelID[0]='+document.forms['formPayment'].elements['paymentChannelID[$k]'].value; } else { button=this; img=document.getElementById( 'spinwheel2$k' ); sendXmlHttpRequest( 'paymentReminder.php', 'zahlungID={$sublist[$i][$j]["tblZahlungen.zahlungID"]}&amp;paymentChannelID='+document.forms['formPayment'].elements['paymentChannelID[$k]'].value, 'startSpin( button, img );', 'stopSpin( button, img, imgOk );' ); }\" /><img id=\"spinwheel2$k\" src=\"image/" . ( $sublist[$i][$j]["tblPersonen.todesdatum"] != "0000-00-00" ? "template/alert.gif" : "dummy.gif" ) . "\" width=\"16\" alt=\"\" />";
            echo "</td>";
            echo "</tr>";
            $k++;
        }
?>
</tbody>
</table>

</div><!-- end .child -->
</div><!-- end .groupChildren -->
</div><!-- end .parent -->

<?php
    }
?>

<!--p class="marginTop">
<input type="button" class="formsSubmitButton" value="<?php echo translate( "Datum der Zahlungseingänge erfassen" ); ?>" onclick="var form=document.forms['formPayment']; var check=true; var send=true; for( i=0; i<<?php echo count( $sublist[$i] ); ?>; i++ ) { check &= !( form.elements['datumEinzahlung['+i+']'].value != '' && form.elements['datumEinzahlung['+i+']'].value < '<?php echo date( "Y" )-5; ?>-01-01' ) } if( !check ) { send = window.confirm( '<?php echo translate( "Mindestens ein Einzahlungsdatum liegt vor" ) . " " . ( date( "Y" )-5 ) . " - " . translate( "Weiterfahren?" ); ?>' ) } if( send ) { form.elements['action'].value = 'UPDATE'; form.submit(); }" />
</p-->

</fieldset>
</form>

</div><!-- .accordeon -->

<?php
        break;

        case "ausstehend":
?>
<h1><?php echo $h1; ?></h1>

<form id="formAusstehend" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
<fieldset>
<input type="hidden" name="context" value="ausstehend" />
<input type="hidden" name="action" value="REMIND" />

<p><?php echo translate( "Ausstehende Mitgliederbeiträge" ) . " (" . count( $list ) . ")"; ?></p>

<table class="tableGrid tableMargin sortable">
<thead>
<tr>
<th class="firstColumn sorttable_sorted"><?php echo translate( "Grad" ) . " " . translate( "Name" ) . " " . translate( "Vorname" ) . ", " . translate( "Wohnort" ); ?><span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
<th><?php echo translate( "Mahnungen" ); ?></th>
<th><?php echo translate( "Summe ausstehender Beiträge" ); ?></th>
<th><?php echo translate( "Postalische Aufforderung" ); ?></th>
</tr>
</thead>
<tbody>
<?php
    for( $i=0; $i<count( $list ); $i++ ) {
        if( $list[$i]["tblPersonen.personID"] != $list[$i-1]["tblPersonen.personID"] ) {
            echo "<tr>";
            echo "<td class=\"firstColumn\" sorttable_customkey=\"{$list[$i]["tblPersonen.name"]}{$list[$i]["tblPersonen.vorname"]}{$list[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$list[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate( $list[$i]["qryDienstgrad.dienstgrad"], $list[$i]["linkPersonSprache.sprache"] ) . " " . translate( $list[$i]["qryDienstgrad.zusatzDg"], $list[$i]["linkPersonSprache.sprache"] ) . "<br /><strong>".$list[$i]["tblPersonen.name"]."</strong> ".$list[$i]["tblPersonen.vorname"]." ".$list[$i]["tblPersonen.vorname2"]."<br />" . $list[$i]["tblPersonen.ort"] . ( !in_array( $list[$i]["tblPersonen.land"], array( "Schweiz", "Suisse", "Svizzera", "Switzerland", "" ) ) ? ", " . $list[$i]["tblPersonen.land"] : "" ) . "</a></td>";
            echo "<td>";
        }
        if( $list[$i]["tblZahlungen.datumMahnung"] != "0000-00-00" ) {
            echo "{$list[$i]["tblZahlungen.datumMahnung"]}<br />";
        }
        if( $list[$i]["tblPersonen.personID"] != $list[$i+1]["tblPersonen.personID"] ) {
            echo "</td>";

            $conn = dbconn::open();
            $query = "SELECT SUM(betrag) AS summe,COUNT(DISTINCT jahr) AS jahre
                      FROM tblZahlungen
                      WHERE personID={$list[$i]["tblPersonen.personID"]} AND typID=1 AND datumEinzahlung='0000-00-00'";
            $result = $conn->query($query);

            $row = $result->fetch(PDO::FETCH_ASSOC);
            dbconn::close( $conn );

            echo "<td>CHF {$row["summe"]}" . ( ( $list[$i]["tblPersonen.todesdatum"] != "0000-00-00" ) ? " <img src=\"image/template/alert.gif\" alt=\"\" />" : "" ) . "</td>";
            echo "<td><input name=\"remindMember[$i]\" type=\"checkbox\" " . ( $row["jahre"] > 1 ? "checked=\"checked\" " : "" ) . "/><input name=\"personID[$i]\" type=\"hidden\" value=\"{$list[$i]["tblPersonen.personID"]}\" /></td>";
            echo "</tr>";
        }
    }
?>
</tbody>
</table>
<input type="submit" class="formsSubmitButton" value="<?php echo translate( "Postalische Aufforderung (PDF) erstellen" ); ?>" />

</fieldset>
</form>

<?php
        break;

        case "vorauszahlungen":
?>
<h1><?php echo $h1; ?></h1>

<form id="formAdvance">
<fieldset>

<table class="tableGrid tableMargin">
<thead>
<tr>
<th class="firstColumn"><?php echo translate( "Jahr" ); ?></th>
<th><?php echo translate( "Name" ); ?></th>
<th><?php echo translate( "In Beitrag ändern" ); ?></th>
</tr>
</thead>
<tbody>
<?php
    for( $i=0; $i<count( $list ); $i++ ) {
        echo "<tr>";
        echo "<td class=\"firstColumn\">{$list[$i]["tblZahlungen.jahr"]}</td>";
        echo "<td><a href=\"member_profile.php?member={$list[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate( $list[$i]["qryDienstgrad.dienstgrad"], $list[$i]["linkPersonSprache.sprache"] ) . " " . translate( $list[$i]["qryDienstgrad.zusatzDg"], $list[$i]["linkPersonSprache.sprache"] ) . "<br /><strong>".$list[$i]["tblPersonen.name"]."</strong> ".$list[$i]["tblPersonen.vorname"]." ".$list[$i]["tblPersonen.vorname2"]."</a>";
        echo "<td><input type=\"button\" value=\"" . translate( "In Beitrag ändern" ) . "\"  onclick=\"obj=document.getElementById( 'waitImg$i' ); sendXmlHttpRequest( 'setPaymentType.php', 'id={$list[$i]["tblZahlungen.zahlungID"]}&amp;typ=1', 'obj.src=\'image/template/ajax_wait.gif\';', 'obj.src=\'image/dummy.gif\';' ); return false;\" /><img id=\"waitImg$i\" src=\"image/dummy.gif\" alt=\"\" width=\"16\" /></td>";
        echo "</tr>";
    }
?>
</tbody>
</table>

</fieldset>
</form>

<?php
        break;

        case "spenden":
?>
<h1><?php echo $h1; ?></h1>

<table class="tableGrid tableMargin sortable">
<thead>
<tr>
<th class="firstColumn sorttable_sorted"><?php echo translate( "Name" ); ?><span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
<th><?php echo date( "Y" ); ?></th>
<th><?php echo date( "Y" )-1; ?></th>
<th><?php echo date( "Y" )-2; ?></th>
<th><?php echo date( "Y" )-3; ?></th>
<th><?php echo date( "Y" )-4; ?></th>
<th><?php echo translate( "Letzte 5 Jahre" ); ?></th>
<th><?php echo translate( "Total" ); ?></th>
</tr>
</thead>
<?php
    echo "<tbody>";
    for( $i=0; $i<count( $list ); $i++ ) {
        echo "<tr>";
        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$list[$i]["tblPersonen.name"]}{$list[$i]["tblPersonen.vorname"]}{$list[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$list[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate( $list[$i]["qryDienstgrad.dienstgrad"], $list[$i]["linkPersonSprache.sprache"] ) . " " . translate( $list[$i]["qryDienstgrad.zusatzDg"], $list[$i]["linkPersonSprache.sprache"] ) . "<br /><strong>".$list[$i]["tblPersonen.name"]."</strong> ".$list[$i]["tblPersonen.vorname"]." ".$list[$i]["tblPersonen.vorname2"]."</a>";
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum0"] ? $list[$i][".sum0"] : "&nbsp;" ) . "</td>"; $sums0 += $list[$i][".sum0"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum1"] ? $list[$i][".sum1"] : "&nbsp;" ) . "</td>"; $sums1 += $list[$i][".sum1"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum2"] ? $list[$i][".sum2"] : "&nbsp;" ) . "</td>"; $sums2 += $list[$i][".sum2"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum3"] ? $list[$i][".sum3"] : "&nbsp;" ) . "</td>"; $sums3 += $list[$i][".sum3"];
        echo "<td class=\"alignRight\">" . ( $list[$i][".sum4"] ? $list[$i][".sum4"] : "&nbsp;" ) . "</td>"; $sums4 += $list[$i][".sum4"];
        $sumLast5 = $list[$i][".sum0"] + $list[$i][".sum1"] + $list[$i][".sum2"] + $list[$i][".sum3"] + $list[$i][".sum4"];
        echo "<td class=\"bold alignRight\">" . ( $sumLast5 ? $sumLast5 : "&nbsp;" ) . "</td>"; $sumsLast5 += $sumLast5;
        echo "<td class=\"bold alignRight\">{$list[$i][".sumTotal"]}</td>"; $sumsTotal += $list[$i][".sumTotal"];
        echo "</tr>";
    }
    echo "</tbody>";
    echo "<tfoot>";
    echo "<tr>";
    echo "<td class=\"bold firstColumn\">" . translate( "Total" ) . "</td>";
    echo "<td class=\"bold alignRight\">$sums0</td>";
    echo "<td class=\"bold alignRight\">$sums1</td>";
    echo "<td class=\"bold alignRight\">$sums2</td>";
    echo "<td class=\"bold alignRight\">$sums3</td>";
    echo "<td class=\"bold alignRight\">$sums4</td>";
    echo "<td class=\"bold alignRight\">CHF $sumsLast5</td>";
    echo "<td class=\"bold alignRight\">CHF $sumsTotal</td>";
    echo "</tr>";
    echo "</tfoot>";
?>
</table>

<?php
        break;
    }
?>

</div>

<?php
    include( "include/footer.inc.php" );
?>


</body>
</html>