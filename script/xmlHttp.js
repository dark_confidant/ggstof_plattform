var xmlHttp = new Object();

function sendXmlHttpRequest( callee, params, preaction, postaction ) {
    // Mozilla, Opera, Safari sowie Internet Explorer 7
    if ( typeof XMLHttpRequest != "undefined" ) {
        xmlHttp[callee+params] = new XMLHttpRequest();
    }

    if( !xmlHttp[callee+params] ) {
        // Internet Explorer 6 und �lter
        try {
            xmlHttp[callee+params] = new ActiveXObject( "Msxml2.XMLHTTP" );
        } catch(e) {
            try {
                xmlHttp[callee+params] = new ActiveXObject( "Microsoft.XMLHTTP" );
            } catch(e) {
                xmlHttp[callee+params] = null;
            }
        }
    }

    if( xmlHttp[callee+params] ) {
        eval( preaction );

        xmlHttp[callee+params].open( "POST", "function/xmlhttp/"+callee, true );
        //Send the proper header information along with the request
        xmlHttp[callee+params].setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
        xmlHttp[callee+params].setRequestHeader( "Content-length", params.length );
        xmlHttp[callee+params].setRequestHeader( "Connection", "close" );

        xmlHttp[callee+params].onreadystatechange = function() {  // Call a function when the state changes.
            if ( xmlHttp[callee+params].readyState == 4 && xmlHttp[callee+params].status == 200 ) {
                if ( xmlHttp[callee+params].responseText != "" ) {
                    window.alert( xmlHttp[callee+params].responseText );
                }
                eval( postaction );
                return true;
            }
        }

        xmlHttp[callee+params].send( params );
    }
}