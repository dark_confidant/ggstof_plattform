// source: Michael Imstepf
// date: 21/02/2009
// purpose: hide warning stating that JS is disabled
// dependencies: screen.css

$(document).ready(function(){

$("#warningNoJs").css("display", "none");

});



