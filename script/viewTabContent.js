// source: Michael Imstepf
// date: 11/10/2008
// purpose: change content of member_profile.php by clicking on tabs without having to reload the page
// dependencies: member_profile.php, screen.css

$(document).ready(function(){

$(".HomeTabs_tab").click(function () {

// read what tab is clicked
var tab = $(this).attr("id");

// hide visible text
$(".tabContent").hide();

// change selected tabs
$(".HomeTabs_tab").removeClass("Tabset_selected");
$(this).addClass("Tabset_selected");

// set div with content for pressed tab and fade div in
var tabDiv = "#tab" + tab;
$(tabDiv).fadeIn("slow");

});
});



