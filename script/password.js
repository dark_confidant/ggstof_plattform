function checkPassword( formHandle, imgHandle ) {
    criteria = new Array();
    ok = true;
    pw1 = formHandle.elements["newPassword"].value;
    pw2 = formHandle.elements["newPassword2"].value;

    criteria[0] = ( pw1.length >= 8 );
    criteria[1] = ( pw1.search( /[a-z]+/ ) > -1 );
    criteria[2] = ( pw1.search( /[A-Z]+/ ) > -1 );
    criteria[3] = ( pw1.search( /[0-9]+/ ) > -1 );
    criteria[4] = ( pw1.search( /[.,:;+\-_*!@$%&]+/ ) > -1 );
    criteria[5] = ( pw1 && pw1 == pw2 );

    for( i=0; i<6; i++ ) {
        ok &= criteria[i];
        if( imgHandle ) {
            imgHandle["pwok"+i].src = ( criteria[i] ) ? "../image/template/ok.png" : "../image/template/delete.png";
        }
    }

    formHandle.elements["submitPassword"].disabled = ( formHandle.elements["oldPassword"] ) ? !( formHandle.elements["oldPassword"].value!="" && ok ) : !ok;

    return ok;
}