/*Copyright 2007 by Marco van Hylckama Vlieg

modified by Michael Imstepf 2008

web: http://www.i-marco.nl/weblog/
email: marco@i-marco.nl*/

function noScrollingToTop() { 
// empty function
};




function initMenu() {

$('.groupChildren').hide();

if($('div.accordeon').hasClass('showFirst')) {
$('.showFirst').show();
}

// displaying all search results
if($('div.accordeon').hasClass('showAll')) {
$('.groupChildren').show();
$('.expandCollapse').attr({ src: 'image/template/expanded.gif', alt: '' });
}


$('.accordeon .menuParent').click(
function() {
	
// apply only to one image if there are several accordeon menus
$(this).addClass('selected');


// hide until toggle is finnished since IE can't toggle (relatively) positioned dl properly (overlapping of content)
$('.selected').next().children().css('visibility', 'hidden');
$(this).next().slideToggle(
function() {
$('.child:hidden').css('visibility', 'visible');
}
);


// change image
if($('.selected > .expandCollapse').attr('src')=='image/template/collapsed.gif')
{$('.selected > .expandCollapse').attr({ src: 'image/template/expanded.gif', alt: '' });}
else
{$('.selected > .expandCollapse').attr({ src: 'image/template/collapsed.gif', alt: '' });}

// apply only to one image if there are several accordeon menus
$(this).removeClass('selected');

}
);



// reset when opening new tab
$('.HomeTabs_tab').click( function() {
$('.expandCollapse').attr({ src: 'image/template/collapsed.gif', alt: '' });
$('.groupChildren').hide();
} );




// show content of accordeon in member_profile.php when loading page
//17 Jan 09 $('.show div a').next().show();
$('.show .groupChildren').show();
$('.show .expandCollapse').attr({ src: 'image/template/expanded.gif', alt: '' });

// show content of accordeons in member_profile.php and open first accordeon in each tab
$('.HomeTabs_tab').click(
function() {

$('.show div a').next().show();
$('.show .expandCollapse').attr({ src: 'image/template/expanded.gif', alt: '' });

}
);




}



$(document).ready(function() {initMenu();});


