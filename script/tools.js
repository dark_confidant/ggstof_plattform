function decrypt_email( coded, key ) {
// Email obfuscator script 2.1 by Tim Williams, University of Arizona
// Random encryption key feature by Andrew Moulden, Site Engineering Ltd
// This code is freeware provided these four comment lines remain intact
// A wizard to generate this code is at http://www.jottings.com/obfuscator/

    shift = coded.length;
    href = "";
    for( i=0; i<coded.length; i++ ) {
        if( key.indexOf(coded.charAt(i)) == -1 ) {
            ltr = coded.charAt(i);
            href += (ltr);
        } else {
            ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length;
            href += (key.charAt(ltr));
        }
    }
    return "mailto:" + href;
}

function startSpin( button, img ) {
    if( button ) {
        button.disabled = true;
    }
    if( img ) {
        img.src = "image/template/spinwheel.gif";
    }
}

function stopSpin( button, img, imgNew ) {
    if( img ) {
        img.src = imgNew ? imgNew.src : "image/dummy.gif";
    }
    if( button ) {
        button.disabled = false;
    }
}