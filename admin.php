<?php
    require_once('head.php');
    ggstof_head(true);


date_default_timezone_set("Europe/Zurich");


if (!$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"]) {
    header("Location: check_privacy_codex.php");
}

if ($_SESSION["rights"]["roleID"] < 3) {
    die(translate("Zugriff verweigert."));
}

//     $context = isset( $_GET["context"] ) ? $_GET["context"] : "pflege";

$context = array_key_exists('context', $_REQUEST) ? $_REQUEST["context"]: '';

$sort = array_key_exists('sort', $_REQUEST) ? $_REQUEST["sort"]: '';
$dir = array_key_exists('dir', $_REQUEST) ? $_REQUEST['dir']: "ASC";
$order = ($sort) ? "$sort $dir,name,vorname,vorname2,tblPersonen.ort" : "name $dir,vorname,vorname2,tblPersonen.ort";

if (array_key_exists('action', $_GET) ? $_GET["action"]: '' == "EXPORT") {
    $result = memberList($_GET["list"], array_key_exists('param', $_GET) ? $_GET["param"]: '');

    switch (array_key_exists('list', $_GET) ? $_GET["list"]: '') {
        case 2:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[1][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[2][$i] = $result[$i]["tblPersonen.name"];
                $content[3][$i] = $result[$i]["tblPersonen.adresse"];
                $content[4][$i] = $result[$i]["tblPersonen.postfach"];
                $content[5][$i] = $result[$i]["tblPersonen.plz"];
                $content[6][$i] = $result[$i]["tblPersonen.ort"];
                $content[7][$i] = $result[$i]["tblPersonen.email"];
                $content[8][$i] = substr($result[$i]["tblPersonen.geburtsdatum"], 0, 4);
                // "Z1-Z7"
                $content[9][$i] = translate($result[$i]["linkPersonAnrede.anrede"], $result[$i]["linkPersonSprache.sprache"], false, false) . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? "" : " " . $content[0][$i]);
                $content[10][$i] = $content[2][$i] . " " . $content[1][$i] . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? ", " . $content[0][$i] : "");
                $tbl = $result[$i]["tblPersonen.channelID"] == 3 ? "qryBeruf" : "tblPersonen";
                $content[11][$i] = $result[$i]["$tbl.postfach"];
                $content[12][$i] = $result[$i]["$tbl.adresse"];
                $content[13][$i] = $result[$i]["$tbl.plz"] . " " . $result[$i]["$tbl.ort"];
                $content[14][$i] = str_replace(array("Schweiz", "Suisse", "Svizzera", "Switzerland"), "", $result[$i]["$tbl.land"]);
                $content[15][$i] = translate($result[$i]["linkPersonAnrede.anredeText"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.dienstgradText"], $result[$i]["linkPersonSprache.sprache"], false, false);

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Adresse"), translate("Postfach"), translate("PLZ"), translate("Ort"), translate("E-Mail"), translate("Jahrgang"), "Z1", "Z2", "Z3", "Z4", "Z5", "Z6", "Z7");
            break;

        case 3:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[1][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[2][$i] = $result[$i]["tblPersonen.name"];
                $content[3][$i] = $result[$i]["tblPersonen.adresse"];
                $content[4][$i] = $result[$i]["tblPersonen.postfach"];
                $content[5][$i] = $result[$i]["tblPersonen.plz"];
                $content[6][$i] = $result[$i]["tblPersonen.ort"];
                $content[7][$i] = $result[$i]["tblPersonen.land"];
                $content[8][$i] = $result[$i]["tblPersonen.email"];
                $content[9][$i] = $result[$i]["linkPersonSprache.sprache"];
                // "Z1-Z7"
                $content[10][$i] = translate($result[$i]["linkPersonAnrede.anrede"], $result[$i]["linkPersonSprache.sprache"], false, false) . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? "" : " " . $content[0][$i]);
                $content[11][$i] = $content[2][$i] . " " . $content[1][$i] . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? ", " . $content[0][$i] : "");
                $tbl = $result[$i]["tblPersonen.channelID"] == 3 ? "qryBeruf" : "tblPersonen";
                $content[12][$i] = $result[$i]["$tbl.postfach"];
                $content[13][$i] = $result[$i]["$tbl.adresse"];
                $content[14][$i] = $result[$i]["$tbl.plz"] . " " . $result[$i]["$tbl.ort"];
                $content[15][$i] = str_replace(array("Schweiz", "Suisse", "Svizzera", "Switzerland"), "", $result[$i]["$tbl.land"]);
                $content[16][$i] = translate($result[$i]["linkPersonAnrede.anredeText"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.dienstgradText"], $result[$i]["linkPersonSprache.sprache"], false, false);

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Adresse"), translate("Postfach"), translate("PLZ"), translate("Ort"), translate("Land"), translate("E-Mail"), translate("Sprache"), "Z1", "Z2", "Z3", "Z4", "Z5", "Z6", "Z7");
            break;

        case 4:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = $result[$i]["qryBeitritt.beitrittsdatum"];
                $content[1][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[2][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[3][$i] = $result[$i]["tblPersonen.name"];
                $content[4][$i] = $result[$i]["tblPersonen.adresse"];
                $content[5][$i] = $result[$i]["tblPersonen.postfach"];
                $content[6][$i] = $result[$i]["tblPersonen.plz"];
                $content[7][$i] = $result[$i]["tblPersonen.ort"];
                $content[8][$i] = $result[$i]["tblPersonen.email"];

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Beitrittsdatum"), translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Adresse"), translate("Postfach"), translate("PLZ"), translate("Ort"), translate("E-Mail"));
            break;

        case 9:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[1][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[2][$i] = $result[$i]["tblPersonen.name"];
                $content[3][$i] = $result[$i]["tblPersonen.ort"];
                $content[4][$i] = $result[$i]["tblPersonen.email"];
                $content[5][$i] = $result[$i]["tblPersonen.tf"];
                $content[6][$i] = $result[$i]["tblPersonen.mobil"];
                $content[7][$i] = substr($result[$i]["tblPersonen.geburtsdatum"], 0, 4);
                $content[8][$i] = $result[$i]["qryMilitaer.funktion"];
                $content[9][$i] = $result[$i]["qryMilitaer.einteilung"];
                $content[10][$i] = $result[$i]["qryBeruf.funktion"];
                $content[11][$i] = $result[$i]["qryBeruf.organisation"];
                $content[12][$i] = $result[$i]["linkPersonSprache.sprache"];
                // "Z1-Z7"
                $content[13][$i] = translate($result[$i]["linkPersonAnrede.anrede"], $result[$i]["linkPersonSprache.sprache"], false, false) . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? "" : " " . $content[0][$i]);
                $content[14][$i] = $content[1][$i] . " " . $content[2][$i] . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? ", " . $content[0][$i] : "");
                $tbl = $result[$i]["tblPersonen.channelID"] == 3 ? "qryBeruf" : "tblPersonen";
                $content[15][$i] = $result[$i]["$tbl.postfach"];
                $content[16][$i] = $result[$i]["$tbl.adresse"];
                $content[17][$i] = $result[$i]["$tbl.plz"] . " " . $result[$i]["$tbl.ort"];
                $content[18][$i] = str_replace(array("Schweiz", "Suisse", "Svizzera", "Switzerland"), "", $result[$i]["$tbl.land"]);
                $content[19][$i] = translate($result[$i]["linkPersonAnrede.anredeText"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.dienstgradText"], $result[$i]["linkPersonSprache.sprache"], false, false);

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Ort"), translate("E-Mail"), translate("Telefon"), translate("Mobile"), translate("Jahrgang"), translate("Aktuelle Mil-Funktion"), translate("Aktuelle Mil-Einteilung"), translate("Aktuelle Berufs-Funktion"), translate("Aktuelle Berufs-Organisation"), translate("Sprache"), "Z1", "Z2", "Z3", "Z4", "Z5", "Z6", "Z7");
            break;

        case 11:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = $result[$i]["qryBeitritt.beitrittsdatum"];
                $content[1][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[2][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[3][$i] = $result[$i]["tblPersonen.name"];
                $content[4][$i] = $result[$i]["tblPersonen.adresse"];
                $content[5][$i] = $result[$i]["tblPersonen.postfach"];
                $content[6][$i] = $result[$i]["tblPersonen.plz"];
                $content[7][$i] = $result[$i]["tblPersonen.ort"];
                $content[8][$i] = $result[$i]["tblPersonen.email"];

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Beitrittsdatum"), translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Adresse"), translate("Postfach"), translate("PLZ"), translate("Ort"), translate("E-Mail"));
            break;

        case 12:
            for ($i = 0; $i < count($result); $i++) {
                $person = new person($result[$i]["tblPersonen.personID"]);
                $modifier = new person($person->modifier);
                $content[0][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[1][$i] = $result[$i]["tblPersonen.name"];
                $content[2][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[3][$i] = $result[$i]["tblPersonen.postfach"];
                $content[4][$i] = $result[$i]["tblPersonen.adresse"];
                $content[5][$i] = $result[$i]["tblPersonen.plz"];
                $content[6][$i] = $result[$i]["tblPersonen.ort"];
                $content[7][$i] = $result[$i]["tblPersonen.email"];
                $content[8][$i] = $result[$i]["tblPersonen.tf"];
                $content[9][$i] = $result[$i]["tblPersonen.mobil"];
                $content[10][$i] = $result[$i]["qryBeruf.organisation"];
                $content[11][$i] = $result[$i]["qryBeruf.adresse"];
                $content[12][$i] = $result[$i]["qryBeruf.plz"];
                $content[13][$i] = $result[$i]["qryBeruf.ort"];
                $content[14][$i] = $result[$i]["linkPersonChannel.channel"];
                $content[15][$i] = $result[$i]["linkPersonSprache.sprache"];
                $content[16][$i] = $result[$i]["tblPersonen.veroeffentlichungID"];
                $content[17][$i] = ($person->modDateAdmin > $person->modDateOwner) ? $person->modDateAdmin : $person->modDateOwner;
                $content[18][$i] = ($person->modDateAdmin > $person->modDateOwner) ? $modifier->person->vorname . " " . $modifier->person->vorname2 . " " . $modifier->person->name : $person->person->vorname . " " . $person->person->vorname2 . " " . $person->person->name;
                // "Z1-Z7"
                $content[19][$i] = translate($result[$i]["linkPersonAnrede.anrede"], $result[$i]["linkPersonSprache.sprache"], false, false) . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? "" : " " . $content[0][$i]);
                $content[20][$i] = $content[2][$i] . " " . $content[1][$i] . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? ", " . $content[0][$i] : "");
                $tbl = $result[$i]["tblPersonen.channelID"] == 3 ? "qryBeruf" : "tblPersonen";
                $content[21][$i] = $result[$i]["$tbl.postfach"];
                $content[22][$i] = $result[$i]["$tbl.adresse"];
                $content[23][$i] = $result[$i]["$tbl.plz"] . " " . $result[$i]["$tbl.ort"];
                $content[24][$i] = str_replace(array("Schweiz", "Suisse", "Svizzera", "Switzerland"), "", $result[$i]["$tbl.land"]);
                $content[25][$i] = translate($result[$i]["linkPersonAnrede.anredeText"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.dienstgradText"], $result[$i]["linkPersonSprache.sprache"], false, false);

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Dienstgrad", false, false, false), translate("Name", false, false, false), translate("Vorname", false, false, false), translate("Postfach", false, false, false), translate("Wohnadresse", false, false, false), translate("PLZ", false, false, false), translate("Ort", false, false, false), translate("E-Mail", false, false, false), translate("Tf", false, false, false), translate("Mobile", false, false, false), translate("Organisation", false, false, false), translate("Geschäftsadresse", false, false, false), translate("PLZ", false, false, false), translate("Ort", false, false, false), translate("Kommunikationskanal", false, false, false), translate("Sprache", false, false, false), translate("Veröffentlicht", false, false, false), translate("zuletzt bearbeitet", false, false, false), translate("bearbeitet von", false, false, false), "Z1", "Z2", "Z3", "Z4", "Z5", "Z6", "Z7");
            break;

        case 14:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = $result[$i]["qryBeitritt.beitrittsdatum"];
                $content[1][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[2][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[3][$i] = $result[$i]["tblPersonen.name"];
                $content[4][$i] = $result[$i]["tblPersonen.adresse"];
                $content[5][$i] = $result[$i]["tblPersonen.postfach"];
                $content[6][$i] = $result[$i]["tblPersonen.plz"];
                $content[7][$i] = $result[$i]["tblPersonen.ort"];
                $content[8][$i] = $result[$i]["tblPersonen.land"];
                $content[9][$i] = $result[$i]["tblPersonen.email"];
                $content[10][$i] = $result[$i]["linkPersonSprache.sprache"];

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Beitrittsdatum"), translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Adresse"), translate("Postfach"), translate("PLZ"), translate("Ort"), translate("Land"), translate("E-Mail"), translate("Sprache"));
            break;

        case 15:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = $result[$i]["qryBeitritt.beitrittsdatum"];
                $content[1][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[2][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[3][$i] = $result[$i]["tblPersonen.name"];
                $content[4][$i] = $result[$i]["tblPersonen.adresse"];
                $content[5][$i] = $result[$i]["tblPersonen.postfach"];
                $content[6][$i] = $result[$i]["tblPersonen.plz"];
                $content[7][$i] = $result[$i]["tblPersonen.ort"];
                $content[8][$i] = $result[$i]["tblPersonen.land"];
                $content[9][$i] = $result[$i]["tblPersonen.email"];
                $content[10][$i] = $result[$i]["linkPersonSprache.sprache"];

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Beitrittsdatum"), translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Adresse"), translate("Postfach"), translate("PLZ"), translate("Ort"), translate("Land"), translate("E-Mail"), translate("Sprache"));
            break;

        case 16:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = $result[$i]["qryBeitritt.beitrittsdatum"];
                $content[1][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[2][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[3][$i] = $result[$i]["tblPersonen.name"];
                $content[4][$i] = $result[$i]["tblPersonen.adresse"];
                $content[5][$i] = $result[$i]["tblPersonen.postfach"];
                $content[6][$i] = $result[$i]["tblPersonen.plz"];
                $content[7][$i] = $result[$i]["tblPersonen.ort"];
                $content[8][$i] = $result[$i]["tblPersonen.land"];
                $content[9][$i] = $result[$i]["tblPersonen.email"];
                $content[10][$i] = $result[$i]["linkPersonSprache.sprache"];

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Beitrittsdatum"), translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Adresse"), translate("Postfach"), translate("PLZ"), translate("Ort"), translate("Land"), translate("E-Mail"), translate("Sprache"));
            break;

        case 17:
            for ($i = 0; $i < count($result); $i++) {
                $content[0][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[1][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[2][$i] = $result[$i]["tblPersonen.name"];
                $content[3][$i] = $result[$i]["tblPersonen.adresse"];
                $content[4][$i] = $result[$i]["tblPersonen.postfach"];
                $content[5][$i] = $result[$i]["tblPersonen.plz"];
                $content[6][$i] = $result[$i]["tblPersonen.ort"];
                $content[7][$i] = $result[$i]["tblPersonen.email"];
                $content[8][$i] = $result[$i]["tblZahlungen.datumRechnung"];
                $content[9][$i] = $result[$i]["tblZahlungen.betrag"];

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Dienstgrad"), translate("Vorname"), translate("Name"), translate("Adresse"), translate("Postfach"), translate("PLZ"), translate("Ort"), translate("E-Mail"), translate("Datum der Rechnungsstellung"), translate("Betrag") . " CHF");
            break;

        case 31:
            for ($i = 0; $i < count($result); $i++) {
                $person = new person($result[$i]["tblPersonen.personID"]);
                $modifier = new person($person->modifier);
                $content[0][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[1][$i] = $result[$i]["tblPersonen.name"];
                $content[2][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[3][$i] = $result[$i]["tblPersonen.postfach"];
                $content[4][$i] = $result[$i]["tblPersonen.adresse"];
                $content[5][$i] = $result[$i]["tblPersonen.plz"];
                $content[6][$i] = $result[$i]["tblPersonen.ort"];
                $content[7][$i] = $result[$i]["tblPersonen.email"];
                $content[8][$i] = $result[$i]["tblPersonen.tf"];
                $content[9][$i] = $result[$i]["tblPersonen.mobil"];
                $content[10][$i] = $result[$i]["qryBeruf.organisation"];
                $content[11][$i] = $result[$i]["qryBeruf.adresse"];
                $content[12][$i] = $result[$i]["qryBeruf.plz"];
                $content[13][$i] = $result[$i]["qryBeruf.ort"];
                $content[14][$i] = $result[$i]["linkPersonChannel.channel"];
                $content[15][$i] = $result[$i]["linkPersonSprache.sprache"];
                $content[16][$i] = $result[$i]["tblPersonen.veroeffentlichungID"];
                $content[17][$i] = ($person->modDateAdmin > $person->modDateOwner) ? $person->modDateAdmin : $person->modDateOwner;
                $content[18][$i] = ($person->modDateAdmin > $person->modDateOwner) ? $modifier->person->vorname . " " . $modifier->person->vorname2 . " " . $modifier->person->name : $person->person->vorname . " " . $person->person->vorname2 . " " . $person->person->name;
                // "Z1-Z7"
                $content[19][$i] = translate($result[$i]["linkPersonAnrede.anrede"], $result[$i]["linkPersonSprache.sprache"], false, false) . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? "" : " " . $content[0][$i]);
                $content[20][$i] = $content[2][$i] . " " . $content[1][$i] . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? ", " . $content[0][$i] : "");
                $tbl = $result[$i]["tblPersonen.channelID"] == 3 ? "qryBeruf" : "tblPersonen";
                $content[21][$i] = $result[$i]["$tbl.postfach"];
                $content[22][$i] = $result[$i]["$tbl.adresse"];
                $content[23][$i] = $result[$i]["$tbl.plz"] . " " . $result[$i]["$tbl.ort"];
                $content[24][$i] = str_replace(array("Schweiz", "Suisse", "Svizzera", "Switzerland"), "", $result[$i]["$tbl.land"]);
                $content[25][$i] = translate($result[$i]["linkPersonAnrede.anredeText"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.dienstgradText"], $result[$i]["linkPersonSprache.sprache"], false, false);

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Dienstgrad", false, false, false), translate("Name", false, false, false), translate("Vorname", false, false, false), translate("Postfach", false, false, false), translate("Wohnadresse", false, false, false), translate("PLZ", false, false, false), translate("Ort", false, false, false), translate("E-Mail", false, false, false), translate("Tf", false, false, false), translate("Mobile", false, false, false), translate("Organisation", false, false, false), translate("Geschäftsadresse", false, false, false), translate("PLZ", false, false, false), translate("Ort", false, false, false), translate("Kommunikationskanal", false, false, false), translate("Sprache", false, false, false), translate("Veröffentlicht", false, false, false), translate("zuletzt bearbeitet", false, false, false), translate("bearbeitet von", false, false, false), "Z1", "Z2", "Z3", "Z4", "Z5", "Z6", "Z7");
            break;

        case 32:
            for ($i = 0; $i < count($result); $i++) {
                $person = new person($result[$i]["tblPersonen.personID"]);
                $modifier = new person($person->modifier);
                $content[0][$i] = strip_spaces(translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"], false, false));
                $content[1][$i] = $result[$i]["tblPersonen.name"];
                $content[2][$i] = strip_spaces($result[$i]["tblPersonen.vorname"] . " " . $result[$i]["tblPersonen.vorname2"]);
                $content[3][$i] = $result[$i]["tblPersonen.postfach"];
                $content[4][$i] = $result[$i]["tblPersonen.adresse"];
                $content[5][$i] = $result[$i]["tblPersonen.plz"];
                $content[6][$i] = $result[$i]["tblPersonen.ort"];
                $content[7][$i] = $result[$i]["tblPersonen.email"];
                $content[8][$i] = $result[$i]["tblPersonen.tf"];
                $content[9][$i] = $result[$i]["tblPersonen.mobil"];
                $content[10][$i] = $result[$i]["qryBeruf.organisation"];
                $content[11][$i] = $result[$i]["qryBeruf.adresse"];
                $content[12][$i] = $result[$i]["qryBeruf.plz"];
                $content[13][$i] = $result[$i]["qryBeruf.ort"];
                $content[14][$i] = $result[$i]["linkPersonChannel.channel"];
                $content[15][$i] = $result[$i]["linkPersonSprache.sprache"];
                $content[16][$i] = $result[$i]["tblPersonen.veroeffentlichungID"];
                $content[17][$i] = ($person->modDateAdmin > $person->modDateOwner) ? $person->modDateAdmin : $person->modDateOwner;
                $content[18][$i] = ($person->modDateAdmin > $person->modDateOwner) ? $modifier->person->vorname . " " . $modifier->person->vorname2 . " " . $modifier->person->name : $person->person->vorname . " " . $person->person->vorname2 . " " . $person->person->name;
                // "Z1-Z7"
                $content[19][$i] = translate($result[$i]["linkPersonAnrede.anrede"], $result[$i]["linkPersonSprache.sprache"], false, false) . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? "" : " " . $content[0][$i]);
                $content[20][$i] = $content[2][$i] . " " . $content[1][$i] . ($result[$i]["qryDienstgrad.zusatzDgID"] == 4 ? ", " . $content[0][$i] : "");
                $tbl = $result[$i]["tblPersonen.channelID"] == 3 ? "qryBeruf" : "tblPersonen";
                $content[21][$i] = $result[$i]["$tbl.postfach"];
                $content[22][$i] = $result[$i]["$tbl.adresse"];
                $content[23][$i] = $result[$i]["$tbl.plz"] . " " . $result[$i]["$tbl.ort"];
                $content[24][$i] = str_replace(array("Schweiz", "Suisse", "Svizzera", "Switzerland"), "", $result[$i]["$tbl.land"]);
                $content[25][$i] = translate($result[$i]["linkPersonAnrede.anredeText"], $result[$i]["linkPersonSprache.sprache"], false, false) . " " . translate($result[$i]["qryDienstgrad.dienstgradText"], $result[$i]["linkPersonSprache.sprache"], false, false);

                $result[$i] = NULL;
            }
            $headerrow = array(translate("Dienstgrad", false, false, false), translate("Name", false, false, false), translate("Vorname", false, false, false), translate("Postfach", false, false, false), translate("Wohnadresse", false, false, false), translate("PLZ", false, false, false), translate("Ort", false, false, false), translate("E-Mail", false, false, false), translate("Tf", false, false, false), translate("Mobile", false, false, false), translate("Organisation", false, false, false), translate("Geschäftsadresse", false, false, false), translate("PLZ", false, false, false), translate("Ort", false, false, false), translate("Kommunikationskanal", false, false, false), translate("Sprache", false, false, false), translate("Veröffentlicht", false, false, false), translate("zuletzt bearbeitet", false, false, false), translate("bearbeitet von", false, false, false), "Z1", "Z2", "Z3", "Z4", "Z5", "Z6", "Z7");
            break;

        case 33:
            for ($i = 0; $i < count($result); $i++) {
                $content[$i] = $result[$i]["tblPersonen.personID"];
            }
            break;
    }

    switch ($_GET["format"]) {
        case "pdf":
            export2pdf("Export", $content);
            break;

        case "xls":
            export2xls("Export", $content, $headerrow);
            break;
    }
}

// Instantiate site objects
$addButton = new imgAddButton;
$addButton->class = "";
$deleteButton = new imgDeleteButton;
$deleteButton->class = "";
$editButton = new imgEditButton;
$editButton->class = "";
$mailButton = new imgButton;
$mailButton->src = "image/template/mail.gif";
$mailButton->class = "";
$xlsButton = new imgButton;
$xlsButton->src = "image/template/excel-16x16.png";
$xlsButton->class = "";
$xlsButton->imgclass = "icon";
$xlsButton->alt = "Excel-Liste exportieren";
$xlsButton->title = "Excel-Liste exportieren";
$pdfButton = new imgButton;
$pdfButton->src = "image/template/pdf.png";
$pdfButton->class = "";
$pdfButton->imgclass = "icon";
$pdfButton->alt = "PDF exportieren";
$pdfButton->title = "PDF exportieren";
?>


<?php
include("include/head.inc.php");
?>
<title><?php echo translate("Gesellschaft der Generalstabsoffiziere") . " (GGstOf) - " . translate("Administration"); ?></title>
<script type="text/javascript" src="script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="script/accordeon.js"></script>
<script type="text/javascript" src="script/xmlHttp.js"></script>
<script type="text/javascript" src="script/sorttable.js"></script>
<script type="text/javascript" src="script/tools.js"></script>
<script type="text/javascript">
    <!--

    imgDummy = new Image();
    imgDummy.src = "image/dummy.gif";
    imgWheel = new Image();
    imgWheel.src = "image/template/spinwheel.gif";
    imgOk = new Image();
    imgOk.src = "image/template/ok.png";

    var img;
    var button;
    var todo;
    var done;

    function checkIfDone() {
        if (done == todo) {
            img.src = imgOk.src;
            button.disabled = false;
//         window.alert( done+" von "+todo+" E-Mails erfolgreich versandt." );
        } else {
            window.setTimeout("checkIfDone()", 1000);
        }
    }

    // -->
</script>
</head>


<body id="admin">


<?php
include("include/navigationLeft.inc.php");
include("include/navigationTop.inc.php");
?>


<div id="content">


    <div class="accordeon accordeonProfile">


        <!-- #################################################################### -->


        <?php if ($context == "kommunikation") { ?>
            <ul>
                <li><a href="admin.php?context=blog" onclick="window.open(this.href);
                return false"><?php echo translate("Blog"); ?></a></li>
                <li><a href="admin.php?context=texte" onclick="window.open(this.href);
                return false"><?php echo translate("Texte"); ?></a></li>
                <li><a href="admin.php?context=versand" onclick="window.open(this.href);
                return false"><?php echo translate("Versand"); ?></a></li>
                <li><a href="admin.php?context=autoversand" onclick="window.open(this.href);
                return false"><?php echo translate("Automatischer Versand"); ?></a></li>
                <li><a href="admin.php?context=nichtmitglieder" onclick="window.open(this.href);
                return false"><?php echo translate("Aktuelle Liste NICHT-Mitglieder"); ?></a></li>
                <li><a href="admin.php?context=loginfail" onclick="window.open(this.href);
                return false"><?php echo translate("Login-Fails"); ?></a></li>
                <li class="last"><a href="admin.php?context=zustelloptionen" onclick="window.open(this.href);
                return false"><?php echo translate("Zustelloptionen"); ?></a></li>
            </ul>
        <?php } ?>

        <?php if ($context == "blog") { ?>

            <table class="tableGrid tableMargin">
                <thead>
                <tr>
                    <th class="firstColumn"><?php echo translate("Mail"); ?></th>
                    <th><?php echo translate("Blog-Eintrag"); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $blog = blogLastEntries(5);
                echo "<ul id=\"memberBlogList\">";
                for ($i = 0; $i < count($blog); $i++) {
                    $post_content = utf8_encode(strip_tags($blog[$i]["post_content"]));

                    echo "<tr>";
                    echo "<td class=\"firstColumn\">";
                    $mailButton->output(array("onclick" => "noScrollingToTop(); if( confirm( '" . translate("Wollen Sie wirklich einen Hinweis an alle Mitglieder verschicken?") . "' ) ) { obj=document.getElementById( 'mailIcon$i' ); sendXmlHttpRequest( 'emailer.php', 'id=2&amp;param[blogID]={$blog[$i]["ID"]}&amp;recipientsID=2', 'obj.src=\'image/template/spinwheel.gif\';', 'obj.src=\'image/template/mail.gif\';' ); } return false;", "id" => "mailIcon$i", "alt" => "Mail an alle", "title" => "Mail an alle"));
                    echo "<br />";
                    echo "<br />";
                    $mailButton->output(array("onclick" => "noScrollingToTop(); if( confirm( '" . translate("Wollen Sie wirklich einen Hinweis an alle Mitglieder A-M verschicken?") . "' ) ) { obj=document.getElementById( 'mailIcon2_$i' ); sendXmlHttpRequest( 'emailer.php', 'id=2&amp;param[blogID]={$blog[$i]["ID"]}&amp;recipientsID=23', 'obj.src=\'image/template/spinwheel.gif\';', 'obj.src=\'image/template/mail.gif\';' ); } return false;", "id" => "mailIcon2_$i", "alt" => "Mail an A-M", "title" => "Mail an A-M"));
                    echo "<br />";
                    $mailButton->output(array("onclick" => "noScrollingToTop(); if( confirm( '" . translate("Wollen Sie wirklich einen Hinweis an alle Mitglieder N-Z verschicken?") . "' ) ) { obj=document.getElementById( 'mailIcon3_$i' ); sendXmlHttpRequest( 'emailer.php', 'id=2&amp;param[blogID]={$blog[$i]["ID"]}&amp;recipientsID=24', 'obj.src=\'image/template/spinwheel.gif\';', 'obj.src=\'image/template/mail.gif\';' ); } return false;", "id" => "mailIcon3_$i", "alt" => "Mail an N-Z", "title" => "Mail an N-Z"));
                    echo "</td>";
                    echo "<td><strong>" . utf8_encode($blog[$i]["post_title"]) . "</strong> &ndash; {$blog[$i]["post_date"]}<br />" . (strlen($post_content) > 500 ? substr($post_content, 0, 500) . "..." : $post_content) . "<br /><a href=\"{$blog[$i]["guid"]}\" onclick=\"window.open( this.href ); return false;\">{$blog[$i]["guid"]}</a></td>";
                    echo "</tr>";
                }
                echo "</ul>";
                ?>
                </tbody>
            </table>

            <?php
            $result = memberList(28);
            foreach ($result as $element) {
                if (empty($element["tblPersonen.email"]) && $element["tblPersonen.blogChannelID"] == 2 || empty($element["qryBeruf.email"]) && $element["tblPersonen.blogChannelID"] == 3) {
                    $result2[] = $element;
                } else {
                    $result1[] = $element;
                }
            }
            unset($result, $element);
            ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Abonnenten"); ?> (<?php echo count($result1); ?>
                        ):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result1) > 0) { ?>

                            <table class="tableGrid tableMargin">
                                <thead>
                                <tr>
                                    <th class="firstColumn"><?php echo translate("Name"); ?></th>
                                    <th><?php echo translate("E-Mail"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result1); $i++) {
                                    $blogChannel = array(2 => $result1[$i]["tblPersonen.email"], 3 => $result1[$i]["qryBeruf.email"]);
                                    $email = $blogChannel[$result1[$i]["tblPersonen.blogChannelID"]];

                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " " . $result1[$i][".fullName"] . "</a></td>";
                                    echo "<td>$email</td>";
                                    echo "</tr>";
                                }

                                unset($result1);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Abonnenten") . " " . translate("ohne") . " " . translate("E-Mail-Adresse"); ?>
                        (<?php echo count($result2); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result2) > 0) { ?>

                            <table class="tableGrid tableMargin">
                                <thead>
                                <tr>
                                    <th class="firstColumn"><?php echo translate("Name"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result2); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$result2[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result2[$i]["qryDienstgrad.dienstgrad"], $result2[$i]["linkPersonSprache.sprache"]) . " " . translate($result2[$i]["qryDienstgrad.zusatzDg"], $result2[$i]["linkPersonSprache.sprache"]) . " " . $result2[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result2);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php
            $result = memberList(29);
            foreach ($result as $element) {
                if (empty($element["tblPersonen.email"]) && empty($element["qryBeruf.email"])) {
                    $result2[] = $element;
                } else {
                    $result1[] = $element;
                }
            }
            unset($result, $element);
            ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Nicht-Abonnenten"); ?>
                        (<?php echo count($result1); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result1) > 0) { ?>

                            <table class="tableGrid tableMargin">
                                <thead>
                                <tr>
                                    <th class="firstColumn"><?php echo translate("Name"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result1); $i++) {
                                    echo "<tr>";
                                }

                                unset($result1);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Nicht-Abonnenten") . " " . translate("ohne") . " " . translate("E-Mail-Adresse"); ?>
                        (<?php echo count($result2); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result2) > 0) { ?>

                            <table class="tableGrid tableMargin">
                                <thead>
                                <tr>
                                    <th class="firstColumn"><?php echo translate("Name"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result2); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$result2[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result2[$i]["qryDienstgrad.dienstgrad"], $result2[$i]["linkPersonSprache.sprache"]) . " " . translate($result2[$i]["qryDienstgrad.zusatzDg"], $result2[$i]["linkPersonSprache.sprache"]) . " " . $result2[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result2);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


        <?php } ?>

        <?php if ($context == "texte") { ?>

            <table class="tableGrid tableMargin">
                <thead>
                <tr>
                    <th class="firstColumn"><?php echo translate("Template"); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $conn = dbconn::open();
                $query = "SELECT emailID FROM tblEmail ORDER BY titel";
                $result = $conn->query($query);

                dbconn::close($conn);

                while ($row = $result->fetch(PDO::FETCH_NUM)) {
                    $content = getEmailContent($row[0]);
                    ?>
                    <tr>
                        <td class="firstColumn">
                            <?php
                            var_dump($content["titel"]);
                            if (!in_array($content["emailID"], array(2, 8, 20, 21, 22, 26, 28, 29))) {
                                $deleteButton->output(array("href" => "emaileditor.php?action=DELETE&amp;item={$content["emailID"]}&amp;caller=" . basename($_SERVER["SCRIPT_NAME"]) . "&amp;context=texte"));
                            }
                            echo " <strong>" . $content["titel"] . "</strong> ";
                            $mailButton->output(array("onclick" => "noScrollingToTop(); obj=document.getElementById( 'mailIcon{$content["emailID"]}' ); sendXmlHttpRequest( 'emailer.php', 'id={$content["emailID"]}&amp;recipientsID=0', 'obj.src=\'image/template/ajax_wait.gif\';', 'obj.src=\'image/template/mail.gif\';' ); return false;", "id" => "mailIcon{$content["emailID"]}", "alt" => "Test-Mail", "title" => "Test-Mail"));
                            echo "<br /><br /><strong>" . translate("Von") . ":</strong> " . stripslashes($content["absender"]) . "<br /><br />";

                            foreach (array("de", "fr", "it") as $language) {
                                ?>
                                <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                            class="parent fontSize">
                                            <?php
                                            if(!empty($content["subject_$language"])) {
                                                echo utf8_decode(htmlentities(stripslashes($content["subject_$language"])));
                                            } else {
                                                echo "wurde noch nicht erstellt";
                                            }
                                            ?>

                                        </span></a>

                                    <div class="groupChildren">
                                        <div class="child">
                                            <?php
                                            $editButton->output(array("href" => "emaileditor.php?action=EDIT&amp;item={$content["emailID"]}&amp;language=$language&amp;caller=" . basename($_SERVER["SCRIPT_NAME"]) . "&amp;context=texte"));
                                            echo "<br /><br />" . utf8_decode(nl2br(htmlentities(stripslashes($content["body_$language"]))));
                                            ?>
                                        </div>
                                        <!-- end .child -->
                                    </div>
                                    <!-- end .groupChildren -->
                                </div><!-- end .parent -->
                                <?php
                            } // foreach( array( "de", "fr", "it" ) as $lang )
                            ?>
                        </td>
                    </tr>
                    <?php
                } // while( $content = $result->fetch(PDO::FETCH_NUM) )
                ?>
                <tr>
                    <td class="firstColumn" colspan="2">
                        <?php
                        $addButton->output(array("href" => "emaileditor.php?action=ADD&amp;caller=" . basename($_SERVER["SCRIPT_NAME"]) . "&amp;context=texte"));
                        ?>
                    </td>
                </tr>
                </tbody>
            </table>

            <p>
                <strong><?php echo translate("Variablen"); ?>:</strong><br/>
                <?php
                $vars = new variableSet();
                $vars->output();
                ?>
            </p>

        <?php } ?>

        <?php if ($context == "versand") { ?>

            <form id="versand" class="formUpdateProfil formEmailTemplate" action="">
                <fieldset>
                    <table>
                        <tr>
                            <td>
                                <label><?php echo translate("Template"); ?></label>
                                <?php

                                $select = new select("template");
                                $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
                                $select->emptyrow = true;
                                $select->selected = 0;
                                $select->onchange = "document.forms['versand'].elements['sendButton'].disabled=document.forms['versand'].elements['showRecipients'].disabled=!(document.forms['versand'].elements['template'].value!='0' && document.forms['versand'].elements['recipientsID'].value!='0')";
                                $select->output();
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><?php echo translate("Adressaten"); ?></label>
                                <?php
                                $select = new select("recipientsID", "sysRecipients");
                                $select->emptyrow = true;
                                $select->selected = 0;
                                $select->onchange = "document.forms['versand'].elements['sendButton'].disabled=document.forms['versand'].elements['showRecipients'].disabled=!(document.forms['versand'].elements['template'].value!='0' && document.forms['versand'].elements['recipientsID'].value!='0')";
                                $select->output();
                                ?>
                                <input type="button" name="showRecipients"
                                       value="<?php echo translate("Empfänger zeigen"); ?>"
                                       onclick="window.open('<?php echo "{$_SERVER["SCRIPT_NAME"]}?context=recipients&amp;recipientsID=" ?>' + document.forms['versand'].elements['recipientsID'].value);"
                                       disabled="disabled"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label></label>
                                <input type="button" name="sendButton" class="formsSubmitButton"
                                       value="<?php echo translate("Senden"); ?>" disabled="disabled"
                                       onclick="if (confirm('<?php echo translate("Wollen Sie wirklich eine E-Mail an die Empfängergruppe"); ?> \'' + document.forms['versand'].elements['recipientsID'].options[document.forms['versand'].elements['recipientsID'].selectedIndex].text + '\' <?php echo translate("schicken"); ?>?')) {
                                           obj = this;
                                           pic = document.getElementById('spinwheel');
                                           sendXmlHttpRequest('emailer.php', 'id=' + document.forms['versand'].elements['template'].value + '&amp;recipientsID=' + document.forms['versand'].elements['recipientsID'].value, 'obj.disabled=true; pic.src=\'image/template/ajax_wait.gif\';', 'pic.src=\'image/dummy.gif\'; obj.disabled=false;');
                                           }
                                           return false;"/>
                                <img id="spinwheel" src="image/dummy.gif" alt=""/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>

        <?php } ?>

        <?php if ($context == "autoversand") { ?>

            <h1><?php echo translate("Automatischer Versand"); ?></h1>
            <form id="autoversand" action="">
                <fieldset>
                    <table class="tableGrid tableMargin sortable">
                        <thead>
                        <tr>
                            <th class="firstColumn sorttable_sorted"><?php echo translate("Titel"); ?><span
                                    id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                            <th><?php echo translate("Monat"); ?></th>
                            <th><?php echo translate("Tag"); ?></th>
                            <th><?php echo translate("Wochentag"); ?></th>
                            <th><?php echo translate("Bedingung"); ?></th>
                            <th><?php echo translate("Inhalt"); ?></th>
                            <th><?php echo translate("Empfängergruppe"); ?></th>
                            <th><?php echo translate("letzte Ausführung") . "/<br />" . translate("nächste Ausführung"); ?></th>
                            <th><?php echo translate("Aktiv"); ?></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $selectEmail = new select("emailID");
                        $selectEmail->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
                        $selectRecipients = new select("recipientsID", "sysRecipients");
                        $months = array("*", "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
                        $weekdays = array("*", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag");

                        $conn = dbconn::open();
                        $query = "SELECT *
              FROM sysCronVersand
              ORDER BY name";
                        $result = $conn->query($query);

                        dbconn::close($conn);

                        while ($cronjob = $result->fetch(PDO::FETCH_ASSOC)) {
                            echo "<tr>";
                            echo "<td class=\"firstColumn\"><input name=\"name[{$cronjob["cronID"]}]\" type=\"text\" value=\"{$cronjob["name"]}\" /></td>";
                            echo "<td><select name=\"month[{$cronjob["cronID"]}]\">";
                            for ($m = 0; $m < count($months); $m++) {
                                echo "<option value=\"$m\"" . ($cronjob["month"] == $m ? " selected=\"selected\"" : "") . ">" . translate($months[$m]) . "</option>";
                            }
                            echo "</select></td>";
                            echo "<td><select name=\"day[{$cronjob["cronID"]}]\">";
                            for ($d = 0; $d < 32; $d++) {
                                echo "<option value=\"$d\"" . ($cronjob["day"] == $d ? " selected=\"selected\"" : "") . ">" . ($d ? $d : "*") . "</option>";
                            }
                            echo "</select></td>";
                            echo "<td><select name=\"weekday[{$cronjob["cronID"]}]\">";
                            for ($w = 0; $w < count($weekdays); $w++) {
                                echo "<option value=\"$w\"" . ($cronjob["weekday"] == $w ? " selected=\"selected\"" : "") . ">" . translate($weekdays[$w]) . "</option>";
                            }
                            echo "</select></td>";
                            echo "<td><input name=\"conditionID[{$cronjob["cronID"]}]\" type=\"text\" value=\"{$cronjob["conditionID"]}\" /></td>";
                            echo "<td>";
                            $selectEmail->output(array("name" => "emailID[{$cronjob["cronID"]}]", "selected" => $cronjob["emailID"]));
                            echo "</td>";
                            echo "<td>";
                            $selectRecipients->output(array("name" => "recipientsID[{$cronjob["cronID"]}]", "selected" => $cronjob["recipientsID"]));
                            echo "</td>";
                            $nextExecution = time();
                            while (!((!$cronjob["month"] || $cronjob["month"] == date("m", $nextExecution)) && (!$cronjob["day"] || $cronjob["day"] == date("d", $nextExecution)) && (!$cronjob["weekday"] || $cronjob["weekday"] == date("N", $nextExecution)))) {
                                $nextExecution += 60 * 60 * 24;
                            }
                            echo "<td>" . ($cronjob["lastExecution"] ? $cronjob["lastExecution"] : "&ndash;") . "<br />{$weekdays[date("N", $nextExecution)]}, " . date("Y-m-d", $nextExecution) . "</td>";
                            echo "<td><input name=\"active[{$cronjob["cronID"]}]\" type=\"checkbox\" " . ($cronjob["active"] ? "checked=\"checked\" " : "") . "/></td>";
                            echo "<td><input type=\"button\" value=\"" . translate("Speichern") . "\" onclick=\"sendXmlHttpRequest( 'handleCronVersand.php', 'action=UPDATE&amp;cronID={$cronjob["cronID"]}&amp;name='+document.forms['autoversand'].elements['name[{$cronjob["cronID"]}]'].value+'&amp;month='+document.forms['autoversand'].elements['month[{$cronjob["cronID"]}]'].value+'&amp;day='+document.forms['autoversand'].elements['day[{$cronjob["cronID"]}]'].value+'&amp;weekday='+document.forms['autoversand'].elements['weekday[{$cronjob["cronID"]}]'].value+'&amp;conditionID='+document.forms['autoversand'].elements['conditionID[{$cronjob["cronID"]}]'].value+'&amp;emailID='+document.forms['autoversand'].elements['emailID[{$cronjob["cronID"]}]'].value+'&amp;recipientsID='+document.forms['autoversand'].elements['recipientsID[{$cronjob["cronID"]}]'].value+'&amp;active='+Number(document.forms['autoversand'].elements['active[{$cronjob["cronID"]}]'].checked), '', 'location.reload();' );\" /> <input type=\"button\" value=\"" . translate("Löschen") . "\" onclick=\"if( window.confirm( '" . translate("Löschen") . "?' ) ) { sendXmlHttpRequest( 'handleCronVersand.php', 'action=DELETE&amp;cronID={$cronjob["cronID"]}', '', 'location.reload();' ); }\" /></td>";
                            echo "</tr>";
                        }
                        echo "<tr>";
                        echo "<td class=\"firstColumn\"><input name=\"name[0]\" type=\"text\" value=\"\" /></td>";
                        echo "<td><select name=\"month[0]\">";
                        for ($m = 0; $m < count($months); $m++) {
                            echo "<option value=\"$m\">" . translate($months[$m]) . "</option>";
                        }
                        echo "</select></td>";
                        echo "<td><select name=\"day[0]\">";
                        for ($d = 0; $d < 32; $d++) {
                            echo "<option value=\"$d\">" . ($d ? $d : "*") . "</option>";
                        }
                        echo "</select></td>";
                        echo "<td><select name=\"weekday[0]\">";
                        for ($w = 0; $w < count($weekdays); $w++) {
                            echo "<option value=\"$w\">" . translate($weekdays[$w]) . "</option>";
                        }
                        echo "</select></td>";
                        echo "<td><input name=\"conditionID[0]\" type=\"text\" value=\"\" /></td>";
                        echo "<td>";
                        $selectEmail->output(array("name" => "emailID[0]", "selected" => 0));
                        echo "</td>";
                        echo "<td>";
                        $selectRecipients->output(array("name" => "recipientsID[0]", "selected" => 0));
                        echo "</td>";
                        echo "<td>&nbsp;</td>";
                        echo "<td><input name=\"active[0]\" type=\"checkbox\" /></td>";
                        echo "<td><input type=\"button\" value=\"" . translate("Speichern") . "\" onclick=\"sendXmlHttpRequest( 'handleCronVersand.php', 'action=INSERT&amp;name='+document.forms['autoversand'].elements['name[0]'].value+'&amp;month='+document.forms['autoversand'].elements['month[0]'].value+'&amp;day='+document.forms['autoversand'].elements['day[0]'].value+'&amp;weekday='+document.forms['autoversand'].elements['weekday[0]'].value+'&amp;conditionID='+document.forms['autoversand'].elements['conditionID[0]'].value+'&amp;emailID='+document.forms['autoversand'].elements['emailID[0]'].value+'&amp;recipientsID='+document.forms['autoversand'].elements['recipientsID[0]'].value+'&amp;active='+Number(document.forms['autoversand'].elements['active[0]'].checked), '', 'location.reload();' );\" /> <input type=\"button\" value=\"" . translate("Abbrechen") . "\" onclick=\"location.reload();\" /></td>";
                        echo "</tr>";
                        ?>
                        </tbody>
                    </table>
                </fieldset>
            </form>

        <?php } ?>

        <?php if ($context == "recipients") { ?>

            <?php
            $conn = dbconn::open();
            $query = "SELECT listID,name,param
              FROM sysRecipients
              WHERE recipientsID={$_GET["recipientsID"]}";
            $result = $conn->query($query);

            dbconn::close($conn);

            $row = $result->fetch(PDO::FETCH_NUM);
            if(!isset($row["listID"])) {
                $row["listID"] = '';
            }
            if(!isset($row["param"])) {
                $row["param"] = '';
            }
            $result = memberList($row["listID"], $row["param"]);
            if (count($result) > 0) {
                ?>
                <h1><?php echo $row["name"] . " (" . count($result) . ")"; ?></h1>
                <table class="tableGrid tableMargin sortable">
                    <thead>
                    <tr>
                        <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    for ($i = 0; $i < count($result); $i++) {
                        echo "<tr>";
                        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                        echo "</tr>";
                    }

                    unset($result);
                    ?>
                    </tbody>
                </table>

            <?php } ?>

        <?php } ?>

        <?php if ($context == "nichtmitglieder") { ?>

            <?php
            $conn = dbconn::open();
            $query = "SELECT DISTINCT typID,typ
              FROM tblBeruf
              LEFT JOIN linkBerufTyp ON tblBeruf.typID=linkBerufTyp.linkTypID
              ORDER BY typID";
            $result = $conn->query($query);

            dbconn::close($conn);

            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
            $select->selected = 46;
            ?>

            <form name="nichtmitglieder">
                <fieldset>

                    <h1><?php echo translate("Aktuelle Liste NICHT-Mitglieder"); ?></h1>

                    <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=9&amp;format=xls")); ?></p>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(9, $row[0]);
//         if( count( $result1 ) > 0 ) {
                        ?>

                        <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                    class="parent fontSize"><?php echo translate($row[1]); ?>
                                    (<?php echo count($result1); ?>)</span></a>

                            <div class="groupChildren">
                                <div class="child">

                                    <table class="tableGrid tableMargin sortable">
                                        <thead>
                                        <tr>
                                            <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                            <th><?php echo translate("E-Mail"); ?></th>
                                            <th><?php echo translate("Eingeladen am"); ?></th>
                                            <th class="sorttable_nosort"><?php echo translate("Einladung"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($i = 0; $i < count($result1); $i++) {
                                            echo "<tr>";
                                            echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"]) . "<br />" . $result1[$i][".fullName"] . "</a></td>";
                                            echo "<td>{$result1[$i]["tblPersonen.email"]}</td>";
                                            echo "<td>" . substr($result1[$i]["tblPersonen.remindDate"], 0, 10) . "</td>";
                                            echo "<td class=\"nowrap\">";
                                            $select->output(array("name" => "template$k"));
                                            echo "<input type=\"button\" value=\"" . translate("Senden") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['nichtmitglieder'].elements['template$k'].value+'&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;nofeedback=true', 'startSpin( button, img );', 'stopSpin( button, img, imgOk );' ); return false;\" " . ($result1[$i]["tblPersonen.email"] ? "" : "disabled=\"disabled\"") . " />";
                                            echo "<img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />";
                                            echo "</td>";
                                            echo "</tr>";
                                            $k++;
                                        }

                                        unset($result1);
                                        ?>
                                        </tbody>
                                    </table>

                                </div>
                                <!-- end .child -->
                            </div>
                            <!-- end .groupChildren -->
                        </div><!-- end .parent -->
                        <?php
//         } // if( count( $result1 ) > 0 )
                    } // while( $row = $result->fetch(PDO::FETCH_NUM) )
                    unset($result);
                    ?>

                </fieldset>
            </form>

        <?php } ?>

        <?php if ($context == "zustelloptionen") { ?>

            <?php $result = memberList(23); ?>

            <form id="formChannel">
                <fieldset>

                    <h1><?php echo translate("Zustelloptionen"); ?> (<?php echo count($result); ?>):</h1>

                    <?php if (count($result) > 0) { ?>
                        <table class="tableGrid tableMargin sortable">
                            <thead>
                            <tr>
                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                        id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                <th><?php echo translate("Kanal"); ?></th>
                                <th><?php echo translate("Post"); ?></th>
                                <th><?php echo translate("E-Mail"); ?></th>
                                <th><?php echo translate("Rechnung"); ?></th>
                                <th><?php echo translate("Blog"); ?></th>
                                <th class="sorttable_nosort">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $selectChannel = new select("channelID", "linkPersonChannel");
                            $selectPostChannel = new select("postChannelID", "linkPersonChannelPost");
                            $selectMailChannel = new select("mailChannelID", "linkPersonChannelMail");
                            $selectPaymentChannel = new select("paymentChannelID", "linkZahlungenChannel");
                            $selectBlogChannel = new select("blogChannelID", "linkBlogChannel");

                            for ($i = 0; $i < count($result); $i++) {
                                echo "<tr>";
                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . "<br />" . $result[$i][".fullName"] . "</a></td>";
                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result[$i]["linkPersonChannel.channel"]}\">";
                                $selectChannel->output(array("name" => "channelID_$i", "selected" => $result[$i]["tblPersonen.channelID"]));
                                echo "</td>";
                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result[$i]["linkPersonChannelPost.postChannel"]}\">";
                                $selectPostChannel->output(array("name" => "postChannelID_$i", "selected" => $result[$i]["tblPersonen.postChannelID"]));
                                echo "</td>";
                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result[$i]["linkPersonChannelMail.mailChannel"]}\">";
                                $selectMailChannel->output(array("name" => "mailChannelID_$i", "selected" => $result[$i]["tblPersonen.mailChannelID"]));
                                echo "</td>";
                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result[$i]["linkZahlungenChannel.paymentChannel"]}\">";
                                $selectPaymentChannel->output(array("name" => "paymentChannelID_$i", "selected" => $result[$i]["tblPersonen.paymentChannelID"]));
                                echo "</td>";
                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result[$i]["linkBlogChannel.blogChannel"]}\">";
                                $selectBlogChannel->output(array("name" => "blogChannelID_$i", "selected" => $result[$i]["tblPersonen.blogChannelID"]));
                                echo "</td>";
                                echo "<td class=\"nowrap\"><input type=\"button\" name=\"button_$i\" value=\"" . translate("OK") . "\" onclick=\"obj=this; pic=document.getElementById( 'ajaxwait_$i' ); sendXmlHttpRequest( 'setChannel.php', 'member={$result[$i]["tblPersonen.personID"]}&amp;channelID='+document.forms['formChannel'].elements['channelID_$i'].value+'&amp;postChannelID='+document.forms['formChannel'].elements['postChannelID_$i'].value+'&amp;mailChannelID='+document.forms['formChannel'].elements['mailChannelID_$i'].value+'&amp;paymentChannelID='+document.forms['formChannel'].elements['paymentChannelID_$i'].value+'&amp;blogChannelID='+document.forms['formChannel'].elements['blogChannelID_$i'].value, 'obj.disabled=true; pic.src=\'image/template/ajax_wait.gif\';', 'pic.src=\'image/dummy.gif\'; obj.disabled=false;' ); return false;\" /> <img id=\"ajaxwait_$i\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                echo "</tr>";
                            }

                            unset($result);
                            ?>
                            </tbody>
                        </table>
                    <?php } ?>

                </fieldset>
            </form>

        <?php } ?>


        <!-- #################################################################### -->


        <?php if ($context == "kassier") { ?>
            <ul>
                <li>
                    <?php echo translate("Rechnungen an Mitgliederbestand per"); ?>
                    31.12.<?php echo(date("Y") - 1); ?> <?php echo translate("ausstellen"); ?><br/>
                    &ndash; <a href="payment.php?context=rechnungen&amp;q=1&amp;c=p" onclick="window.open(this.href);
                    return false"><?php echo translate("postalisch"); ?></a><br/>
                    &ndash; <a href="payment.php?context=rechnungen&amp;q=1&amp;c=e" onclick="window.open(this.href);
                    return false"><?php echo translate("elektronisch"); ?></a>
                </li>
                <li><a href="payment.php?context=mitgliederbeitraege" onclick="window.open(this.href);
                return false"><?php echo translate("Erfasste Einzahlungen"); ?></a></li>
                <li><a href="payment.php?context=einzahlung" onclick="window.open(this.href);
                return false"><?php echo translate("Einzahlungen erfassen"); ?>
                        /<?php echo translate("Mahnungen ausstellen"); ?></a></li>
                <li class="last"><a href="payment.php?context=ausstehend" onclick="window.open(this.href);
                return false"><?php echo translate("Ausstehende Mitgliederbeiträge"); ?></a></li>
                <li><a href="payment.php?context=rechnungen&amp;q=2" onclick="window.open(this.href);
                return false"><?php echo translate("Rechnungen an Neumitglieder aus Q1 ausstellen"); ?></a></li>
                <li><a href="payment.php?context=rechnungen&amp;q=3" onclick="window.open(this.href);
                return false"><?php echo translate("Rechnungen an Neumitglieder aus Q2 ausstellen"); ?></a></li>
                <li class="last"><a href="payment.php?context=rechnungen&amp;q=4" onclick="window.open(this.href);
                return false"><?php echo translate("Rechnungen an Neumitglieder aus Q3 ausstellen"); ?></a></li>
                <li><a href="payment.php?context=vorauszahlungen" onclick="window.open(this.href);
                return false"><?php echo translate("Vorauszahlungen"); ?></a></li>
                <li><a href="payment.php?context=spenden" onclick="window.open(this.href);
                return false"><?php echo translate("Spenden"); ?></a></li>
                <li><a href="payment.php?context=mitgliederbeitraege2" onclick="window.open(this.href);
                return false"><?php echo translate("Zahlungen nach Konto-Eingang"); ?></a></li>
                <li class="last">Statistik</li>
            </ul>
        <?php } ?>


        <!-- #################################################################### -->


        <?php if ($context == "service") { ?>

            <?php
            $conn = dbconn::open();
            $query = "SELECT param0 FROM sysStor ORDER BY storID ASC";
            $result = $conn->query($query);

            dbconn::close($conn);

            $row = array();
            while ($row[] = $result->fetch(PDO::FETCH_NUM)) {

            }
            ?>

            <ul>
                <li><a href="admin.php?context=nieeingeloggt" onclick="window.open(this.href);
                return false;"><?php echo translate("Neue Mitglieder") . ", " . translate("nie eingeloggt") . " ({$row[0][0]})"; ?></a>
                </li>
                <li><a href="admin.php?context=niebearbeitet" onclick="window.open(this.href);
                return false;"><?php echo translate("Neue Mitglieder") . ", " . translate("nie bearbeitet") . " ({$row[1][0]})"; ?></a>
                </li>
                <li><a href="admin.php?context=unvollstaendig" onclick="window.open(this.href);
                return false;"><?php echo translate("Neue Mitglieder") . ", " . translate("unvollständig") . " ({$row[2][0]})"; ?></a>
                </li>
                <li><a href="admin.php?context=veroeffentlicht" onclick="window.open(this.href);
                return false;"><?php echo translate("Veröffentlichte Profile") . " ({$row[3][0]})"; ?></a></li>
                <li><a href="admin.php?context=veroeffentlichung" onclick="window.open(this.href);
                return false;"><?php echo translate("Veröffentlichung"); ?></a></li>
                <li><a href="admin.php?context=keineaktualisierung" onclick="window.open(this.href);
                return false;"><?php echo translate("Keine Aktualisierung") . " ({$row[8][0]})"; ?></a></li>
                <li class="last"><a href="admin.php?context=offlineaktualisiert" onclick="window.open(this.href);
                return false;"><?php echo translate("Offline aktualisiert") . " ({$row[4][0]})"; ?></a></li>
                <li><a href="admin.php?context=einteilung" onclick="window.open(this.href);
                return false;"><?php echo translate("Aktuelle Einteilung"); ?></a></li>
                <li><a href="admin.php?context=vorgesvb" onclick="window.open(this.href);
                return false;"><?php echo translate("Vorgesetzte Verbände"); ?></a></li>
                <li><a href="admin.php?context=vorgesvbstab" onclick="window.open(this.href);
                return false;"><?php echo translate("Vorgesetzte Verbände Stab"); ?></a></li>
                <li><a href="admin.php?context=vorgesvballe" onclick="window.open(this.href);
                return false;"><?php echo translate("Vorgesetzte Verbände ALLE"); ?></a></li>
                <li><a href="admin.php?context=typ" onclick="window.open(this.href);
                return false;"><?php echo translate("Typ"); ?></a></li>
                <li><a href="admin.php?context=brevet" onclick="window.open(this.href);
                return false;"><?php echo translate("Brevet"); ?></a></li>
                <li><a href="admin.php?context=branchen" onclick="window.open(this.href);
                return false;"><?php echo translate("Branchen"); ?></a></li>
                <li><a href="admin.php?context=branchenalle" onclick="window.open(this.href);
                return false;"><?php echo translate("Branchen ALLE"); ?></a></li>
                <li><a href="admin.php?context=abschluessetitel" onclick="window.open(this.href);
                return false;"><?php echo translate("Abschlüsse") . "/" . translate("Titel"); ?></a></li>
                <li><a href="admin.php?context=abschluesseinstitution" onclick="window.open(this.href);
                return false;"><?php echo translate("Abschlüsse") . "/" . translate("Institution"); ?></a></li>
                <li><a href="admin.php?context=abschluessetyp" onclick="window.open(this.href);
                return false;"><?php echo translate("Abschlüsse") . "/" . translate("Typ"); ?></a></li>
                <li><a href="admin.php?context=regionen" onclick="window.open(this.href);
                return false;"><?php echo translate("Regionen"); ?></a></li>
                <li class="last"><a href="admin.php?context=almostlost" onclick="window.open(this.href);
                return false;"><?php echo translate("Almost lost") . " ({$row[5][0]})"; ?></a></li>
            </ul>
            <p><a href="member_profile.php?action=EDIT&amp;element=0">Neues Profil erstellen</a></p>
        <?php } ?>

        <?php
        $icons = array("exclamation.png", "flag_green.png", "page_user_dark.gif", "find.png");
        ?>

        <?php if ($context == "nieeingeloggt") { ?>

            <?php
            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
            $select->selected = 32;

            $result = memberList(38, 0);

            $result1 = array();
            $j = 0;
            for ($i = 0; $i < count($result); $i++) {
                $result1[$j][] = $result[$i];
                $j += (int)(substr($result[$i]["qryBeitritt.beitrittsdatum"], 0, 4) != substr($result[$i + 1]["qryBeitritt.beitrittsdatum"], 0, 4));
            }
            ?>

            <form name="nieeingeloggt">
                <fieldset>

                    <h1><?php echo translate("Neue Mitglieder") . ", " . translate("nie eingeloggt"); ?>
                        (<?php echo count($result); ?>)</h1>

                    <?php
                    unset($result);
                    $k = 0;
                    for ($i = 0; $i < count($result1); $i++) {
                        ?>

                        <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                    class="parent fontSize"><?php echo translate("Beitrittsjahr") . " " . substr($result1[$i][0]["qryBeitritt.beitrittsdatum"], 0, 4) . " (" . count($result1[$i]); ?>
                                    ):</span></a>

                            <div class="groupChildren">
                                <div class="child">

                                    <p>
                                        <?php
                                        echo translate("Ausgewählte") . ": ";
                                        $select->output(array("name" => "template"));
                                        ?>
                                        <input type="button" value="<?php echo translate("Senden"); ?>"
                                               onclick="button = this;
                                                   img = document.getElementById('spinwheel_<?php echo substr($result1[$i][0]["qryBeitritt.beitrittsdatum"], 0, 4); ?>');
                                                   todo = 0;
                                                   done = 0;
                                                   for (i =<?php echo $k; ?>; i <<?php echo($k + count($result1[$i])); ?>; i++) {
                                                   if (document.forms['nieeingeloggt'].elements['check[' + i + ']'].checked) {
                                                   startSpin(button, img);
                                                   sendXmlHttpRequest('emailer.php', 'id=' + document.forms['nieeingeloggt'].elements['template[' + i + ']'].value + '&amp;personID=' + document.forms['nieeingeloggt'].elements['check[' + i + ']'].value + '&amp;nofeedback=true', 'todo++;', 'done++;');
                                                   }
                                                   }
                                                   checkIfDone();"/>
                                        <img
                                            id="spinwheel_<?php echo substr($result1[$i][0]["qryBeitritt.beitrittsdatum"], 0, 4); ?>"
                                            src="image/dummy.gif" width="16" alt=""/>
                                    </p>

                                    <table class="tableGrid tableMargin sortable">
                                        <thead>
                                        <tr>
                                            <th class="firstColumn nowrap sorttable_nosort"><?php echo translate("Alle"); ?>
                                                <input type="checkbox" checked="checked" onclick="this.checked = true;
                                                    for (i =<?php echo $k; ?>; i <<?php echo($k + count($result1[$i])); ?>; i++) {
                                                    document.forms['nieeingeloggt'].elements['check[' + i + ']'].checked = (true && !document.forms['nieeingeloggt'].elements['check[' + i + ']'].disabled);
                                                    }"/>/<input type="checkbox" onclick="this.checked = false;
                                                    for (i =<?php echo $k; ?>; i <<?php echo($k + count($result1[$i])); ?>; i++) {
                                                    document.forms['nieeingeloggt'].elements['check[' + i + ']'].checked = false;
                                                    }"/></th>
                                            <th><?php echo translate("Name"); ?></th>
                                            <th><?php echo translate("Anzeige"); ?></th>
                                            <th class="sorttable_sorted_reverse"><?php echo translate("Beitrittsdatum"); ?>
                                                <span id="sorttable_sortrevind">&nbsp;&#x25BE;</span></th>
                                            <th><?php echo translate("Reminder"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($j = 0; $j < count($result1[$i]); $j++) {
                                            echo "<tr>";
                                            echo "<td class=\"firstColumn\"><input name=\"check[$k]\" type=\"checkbox\" value=\"{$result1[$i][$j]["tblPersonen.personID"]}\" " . (($result1[$i][$j]["tblPersonen.email"] || $result1[$i][$j]["tblPersonen.channelID"] == 100 && $result1[$i][$j]["qryBeruf.email"]) ? "" : "disabled=\"disabled\"") . " /></td>";
                                            echo "<td sorttable_customkey=\"{$result1[$i][$j]["tblPersonen.name"]}{$result1[$i][$j]["tblPersonen.vorname"]}{$result1[$i][$j]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i][$j]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i][$j]["qryDienstgrad.dienstgrad"], $result1[$i][$j]["linkPersonSprache.sprache"]) . " " . translate($result1[$i][$j]["qryDienstgrad.zusatzDg"], $result1[$i][$j]["linkPersonSprache.sprache"]) . "<br />" . $result1[$i][$j][".fullName"] . "</a></td>";
                                            echo "<td><img src=\"image/template/{$icons[$result1[$i][$j]["tblPersonen.veroeffentlichungID"]]}\" alt=\"\" title=\"{$result1[$i][$j]["linkPersonVeroeffentlichung.veroeffentlichung"]}\" /></td>";
                                            echo "<td>{$result1[$i][$j]["qryBeitritt.beitrittsdatum"]}</td>";
                                            echo "<td class=\"nowrap\" sorttable_customkey=\"{$result1[$i][$j]["tblPersonen.remindDate"]}\">" . substr($result1[$i][$j]["tblPersonen.remindDate"], 0, 10) . "<br />";
                                            $select->output(array("name" => "template[$k]", "selected" => 32));
                                            echo "<input type=\"button\" value=\"" . translate("Senden") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['nieeingeloggt'].elements['template[$k]'].value+'&amp;personID={$result1[$i][$j]["tblPersonen.personID"]}&amp;nofeedback=true', 'startSpin( button, img );', 'stopSpin( button, img, imgOk );' ); return false;\" " . (($result1[$i][$j]["tblPersonen.email"] || $result1[$i][$j]["tblPersonen.channelID"] == 100 && $result1[$i][$j]["qryBeruf.email"]) ? "" : "disabled=\"disabled\"") . " />";
                                            echo "<img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />";
                                            echo "</td>";
                                            echo "</tr>";
                                            $k++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>

                                </div>
                                <!-- end .child -->
                            </div>
                            <!-- end .groupChildren -->
                        </div><!-- end .parent -->
                        <?php
                    } // for( $i=0; $i<count( $result1 ); $i++ )
                    unset($result1);
                    ?>
                </fieldset>
            </form>

        <?php } ?>

        <?php if ($context == "niebearbeitet") { ?>

            <?php
            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
            $select->selected = 1;

            list($result, $login) = memberList(38, 1);
            ?>

            <form name="niebearbeitet">
                <fieldset>

                    <h1><?php echo translate("Neue Mitglieder") . ", " . translate("nie bearbeitet"); ?>
                        (<?php echo count($result); ?>)</h1>

                    <?php if (count($result) > 0) { ?>
                        <table class="tableGrid tableMargin sortable">
                            <thead>
                            <tr>
                                <th class="firstColumn"><?php echo translate("Name"); ?></th>
                                <th><?php echo translate("Anzeige"); ?></th>
                                <th class="sorttable_sorted_reverse"><?php echo translate("Beitrittsdatum"); ?><span
                                        id="sorttable_sortrevind">&nbsp;&#x25BE;</span></th>
                                <th><?php echo translate("letzter Login"); ?></th>
                                <th><?php echo translate("Reminder"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            arsort($login);
                            $ix = array_keys($login);

                            for ($i = 0; $i < count($ix); $i++) {
                                echo "<tr>";
                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$ix[$i]]["tblPersonen.name"]}{$result[$ix[$i]]["tblPersonen.vorname"]}{$result[$ix[$i]]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$ix[$i]]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$ix[$i]]["qryDienstgrad.dienstgrad"], $result[$ix[$i]]["linkPersonSprache.sprache"]) . " " . translate($result[$ix[$i]]["qryDienstgrad.zusatzDg"], $result[$ix[$i]]["linkPersonSprache.sprache"]) . "<br />" . $result[$ix[$i]][".fullName"] . "</a></td>";
                                echo "<td><img src=\"image/template/{$icons[$result[$ix[$i]]["tblPersonen.veroeffentlichungID"]]}\" alt=\"\" title=\"{$result[$ix[$i]]["linkPersonVeroeffentlichung.veroeffentlichung"]}\" /></td>";
                                echo "<td>{$result[$ix[$i]]["qryBeitritt.beitrittsdatum"]}</td>";
                                echo "<td>{$login[$ix[$i]]}</td>";
                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result[$ix[$i]]["tblPersonen.remindDate"]}\">" . substr($result[$ix[$i]]["tblPersonen.remindDate"], 0, 10) . "<br />";
                                $select->output(array("name" => "template$i"));
                                echo "<input type=\"button\" value=\"" . translate("Senden") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$i' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['niebearbeitet'].elements['template$i'].value+'&amp;personID={$result[$ix[$i]]["tblPersonen.personID"]}&amp;nofeedback=true', 'startSpin( button, img );', 'stopSpin( button, img, imgOk );' ); return false;\" " . (($result[$ix[$i]]["tblPersonen.channelID"] == 1 && $result[$ix[$i]]["tblPersonen.email"] || $result[$ix[$i]]["tblPersonen.channelID"] == 100 && $result[$ix[$i]]["qryBeruf.email"]) ? "" : "disabled=\"disabled\"") . " />";
                                echo "<img id=\"spinwheel$i\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />";
                                echo "</td>";
                                echo "</tr>";
                            }

                            unset($ix, $login, $result);
                            ?>
                            </tbody>
                        </table>
                    <?php } ?>

                </fieldset>
            </form>

        <?php } ?>

        <?php if ($context == "unvollstaendig") { ?>

            <?php
            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");

            list($result, $login) = memberList(38, 2);
            ?>

            <form name="unvollstaendig">
                <fieldset>

                    <h1><?php echo translate("Neue Mitglieder") . ", " . translate("unvollständig"); ?>
                        (<?php echo count($result); ?>)</h1>

                    <p>
                        <?php echo translate("Ausgewählte"); ?>:
                        <?php $select->output(array("name" => "template", "selected" => 24)); ?>
                        <input type="button" value="<?php echo translate("Senden"); ?>" onclick="button = this;
                            img = document.getElementById('spinwheel');
                            todo = 0;
                            done = 0;
                            for (i = 0; i <<?php echo count($result); ?>; i++) {
                            if (document.forms['unvollstaendig'].elements['remindMember[' + i + ']'].checked) {
                            startSpin(button, img);
                            sendXmlHttpRequest('emailer.php', 'id=' + document.forms['unvollstaendig'].elements['template'].value + '&amp;personID=' + document.forms['unvollstaendig'].elements['remindMember[' + i + ']'].value + '&amp;nofeedback=true', 'todo++;', 'done++;');
                            }
                            }
                            checkIfDone();
                            return false;"/>
                        <img id="spinwheel" src="image/dummy.gif" width="16" alt=""/>
                    </p>

                    <?php if (count($result) > 0) { ?>
                        <table class="tableGrid tableMargin sortable">
                            <thead>
                            <tr>
                                <th class="firstColumn nowrap sorttable_nosort"><?php echo translate("Alle"); ?> <input
                                        type="checkbox" checked="checked" onclick="this.checked = true;
                                        for (i = 0; i <<?php echo count($result); ?>; i++) {
                                        document.forms['unvollstaendig'].elements['remindMember[' + i + ']'].checked = (true && !document.forms['unvollstaendig'].elements['remindMember[' + i + ']'].disabled);
                                        }"/>/<input type="checkbox" onclick="this.checked = false;
                                        for (i = 0; i <<?php echo count($result); ?>; i++) {
                                        document.forms['unvollstaendig'].elements['remindMember[' + i + ']'].checked = false;
                                        }
                                        return false;"/></th>
                                <th><?php echo translate("Name"); ?></th>
                                <th><?php echo translate("Anzeige"); ?></th>
                                <th><?php echo translate("Beitrittsdatum"); ?></th>
                                <th><?php echo translate("zuletzt bearbeitet"); ?></th>
                                <th class="sorttable_sorted_reverse"><?php echo translate("letzter Login"); ?><span
                                        id="sorttable_sortrevind">&nbsp;&#x25BE;</span></th>
                                <th><?php echo translate("Reminder"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            arsort($login);
                            $ix = array_keys($login);

                            for ($i = 0; $i < count($ix); $i++) {
                                echo "<tr>";
                                echo "<td class=\"firstColumn\"><input name=\"remindMember[$i]\" value=\"{$result[$ix[$i]]["tblPersonen.personID"]}\" type=\"checkbox\" " . (($result[$ix[$i]]["tblPersonen.channelID"] == 1 && $result[$ix[$i]]["tblPersonen.email"] || $result[$ix[$i]]["tblPersonen.channelID"] == 100 && $result[$ix[$i]]["qryBeruf.email"]) ? "" : "disabled=\"disabled\"") . " /></td>";
                                echo "<td sorttable_customkey=\"{$result[$ix[$i]]["tblPersonen.name"]}{$result[$ix[$i]]["tblPersonen.vorname"]}{$result[$ix[$i]]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$ix[$i]]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$ix[$i]]["qryDienstgrad.dienstgrad"], $result[$ix[$i]]["linkPersonSprache.sprache"]) . " " . translate($result[$ix[$i]]["qryDienstgrad.zusatzDg"], $result[$ix[$i]]["linkPersonSprache.sprache"]) . "<br />" . $result[$ix[$i]][".fullName"] . "</a></td>";
                                echo "<td><img src=\"image/template/{$icons[$result[$ix[$i]]["tblPersonen.veroeffentlichungID"]]}\" alt=\"\" title=\"{$result[$ix[$i]]["linkPersonVeroeffentlichung.veroeffentlichung"]}\" /></td>";
                                echo "<td>{$result[$ix[$i]]["qryBeitritt.beitrittsdatum"]}</td>";
                                echo "<td>{$result[$ix[$i]]["modDateOwner"]}</td>";
                                echo "<td>{$login[$ix[$i]]}</td>";
                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result[$ix[$i]]["tblPersonen.remindDate"]}\">" . substr($result[$ix[$i]]["tblPersonen.remindDate"], 0, 10) . "<br />";
                                $select->output(array("name" => "template$i", "selected" => 24));
                                echo "<input type=\"button\" value=\"" . translate("Senden") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$i' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['unvollstaendig'].elements['template$i'].value+'&amp;personID={$result[$ix[$i]]["tblPersonen.personID"]}&amp;nofeedback=true', 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" " . (($result[$ix[$i]]["tblPersonen.channelID"] == 1 && $result[$ix[$i]]["tblPersonen.email"] || $result[$ix[$i]]["tblPersonen.channelID"] == 100 && $result[$ix[$i]]["qryBeruf.email"]) ? "" : "disabled=\"disabled\"") . " />";
                                echo "<img id=\"spinwheel$i\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />";
                                echo "</td>";
                                echo "</tr>";
                            }

                            unset($ix, $login, $result);
                            ?>
                            </tbody>
                        </table>
                    <?php } ?>

                </fieldset>
            </form>

        <?php } ?>

        <?php if ($context == "veroeffentlicht") { ?>

            <?php
            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");

            list($result, $modDateOwner) = memberList(1);
            ?>

            <form name="formReminder">
                <fieldset>

                    <h1><?php echo translate("Veröffentlichte Profile"); ?> (<?php echo count($result); ?>)</h1>

                    <p>
                        <?php echo translate("Ausgewählte"); ?>:
                        <?php $select->output(array("name" => "template", "selected" => 10)); ?>
                        <input type="button" name="sendButton" value="<?php echo translate("Senden"); ?>"
                               onclick="obj = this;
                                   pic = document.getElementById('ajaxwait');
                                   todo = 0;
                                   done = 0;
                                   for (i = 0; i <<?php echo count($result); ?>; i++) {
                                   if (document.forms['formReminder'].elements['remindMember[' + i + ']'].checked) {
                                   obj.disabled = true;
                                   pic.src = 'image/template/ajax_wait.gif';
                                   sendXmlHttpRequest('emailer.php', 'id=' + document.forms['formReminder'].elements['template'].value + '&amp;personID=' + document.forms['formReminder'].elements['remindMember[' + i + ']'].value + '&amp;nofeedback=true', 'todo++;', 'done++;');
                                   }
                                   }
                                   checkIfDone();
                                   return false;"/>
                        <img id="ajaxwait" src="image/dummy.gif" width="16" alt=""/>
                    </p>

                    <?php if (count($result) > 0) { ?>
                        <table class="tableGrid tableMargin sortable">
                            <thead>
                            <tr>
                                <th class="firstColumn nowrap sorttable_nosort"><?php echo translate("Alle"); ?> <input
                                        type="checkbox" checked="checked" onclick="this.checked = true;
                                        for (i = 0; i <<?php echo count($result); ?>; i++) {
                                        document.forms['formReminder'].elements['remindMember[' + i + ']'].checked = (true && !document.forms['formReminder'].elements['remindMember[' + i + ']'].disabled);
                                        }"/>/<input type="checkbox" onclick="this.checked = false;
                                        for (i = 0; i <<?php echo count($result); ?>; i++) {
                                        document.forms['formReminder'].elements['remindMember[' + i + ']'].checked = false;
                                        }
                                        return false;"/></th>
                                <th><?php echo translate("Name"); ?></th>
                                <th><?php echo translate("Anzeige"); ?></th>
                                <th class="sorttable_sorted_reverse"><?php echo translate("zuletzt bearbeitet"); ?><span
                                        id="sorttable_sortrevind">&nbsp;&#x25BE;</span></th>
                                <th><?php echo translate("letzter Login"); ?></th>
                                <th><?php echo translate("Reminder"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            arsort($modDateOwner);
                            $ix = array_keys($modDateOwner);

                            for ($i = 0; $i < count($ix); $i++) {
                                echo "<tr>";
                                echo "<td class=\"firstColumn\"><input name=\"remindMember[$i]\" value=\"{$result[$ix[$i]]["tblPersonen.personID"]}\" type=\"checkbox\" " . ($result[$ix[$i]]["tblPersonen.email"] || $result[$ix[$i]]["qryBeruf.email"] ? (strtotime($modDateOwner[$ix[$i]]) < strtotime("-1 year") ? "checked=\"checked\" " : "") : "disabled=\"disabled\" ") . "/></td>";
                                echo "<td sorttable_customkey=\"{$result[$ix[$i]]["tblPersonen.name"]}{$result[$ix[$i]]["tblPersonen.vorname"]}{$result[$ix[$i]]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$ix[$i]]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$ix[$i]]["qryDienstgrad.dienstgrad"], $result[$ix[$i]]["linkPersonSprache.sprache"]) . " " . translate($result[$ix[$i]]["qryDienstgrad.zusatzDg"], $result[$ix[$i]]["linkPersonSprache.sprache"]) . "<br />" . $result[$ix[$i]][".fullName"] . "</a></td>";
                                echo "<td><img src=\"image/template/{$icons[$result[$ix[$i]]["tblPersonen.veroeffentlichungID"]]}\" alt=\"\" title=\"{$result[$ix[$i]]["linkPersonVeroeffentlichung.veroeffentlichung"]}\" /></td>";
                                echo "<td>{$modDateOwner[$ix[$i]]}</td>";
                                echo "<td>{$result[$ix[$i]]["login"]}</td>";
                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result[$ix[$i]]["tblPersonen.remindDate"]}\">" . substr($result[$ix[$i]]["tblPersonen.remindDate"], 0, 10) . "<br />";
                                $select->output(array("name" => "template_$i", "selected" => 10));
                                echo "<input type=\"button\" name=\"sendButton_$i\" value=\"" . translate("Senden") . "\" onclick=\"obj=this; pic=document.getElementById( 'ajaxwait_$i' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['formReminder'].elements['template_$i'].value+'&amp;personID={$result[$ix[$i]]["tblPersonen.personID"]}', 'obj.disabled=true; pic.src=\'image/template/ajax_wait.gif\';', 'pic.src=\'image/dummy.gif\'; obj.disabled=false;' ); return false;\" " . ($result[$ix[$i]]["tblPersonen.email"] || $result[$ix[$i]]["qryBeruf.email"] ? "" : "disabled=\"disabled\"") . " />";
                                echo "<img id=\"ajaxwait_$i\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />";
                                echo "</td>";
                                echo "</tr>";
                            }

                            unset($ix, $modDateOwner, $result);
                            ?>
                            </tbody>
                        </table>
                    <?php } ?>

                </fieldset>
            </form>

        <?php } ?>

        <?php if ($context == "veroeffentlichung") { ?>

            <form name="formVeroeffentlichung">
                <fieldset>

                    <?php
                    $conn = dbconn::open();
                    $query = "SELECT *
              FROM linkPersonVeroeffentlichung
              ORDER BY (veroeffentlichungID=0), veroeffentlichungID";
                    $result0 =$conn->query($query);

                    dbconn::close($conn);

                    while ($row = $result0->fetch(PDO::FETCH_NUM)) {
                        $result = memberList(61, $row[0]);
                        ?>

                        <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                    class="parent fontSize"><?php echo translate($row[1]); ?>
                                    (<?php echo count($result); ?>):</span></a>

                            <div class="groupChildren">
                                <div class="child">

                                    <?php if (count($result) > 0) { ?>
                                        <table class="tableGrid tableMargin">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn nowrap sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("letzter Login") . " " . translate("von Mitglied"); ?></th>
                                                <th><?php echo translate("zuletzt bearbeitet") . " " . translate("von Mitglied"); ?></th>
                                                <th><?php echo translate("zuletzt bearbeitet") . " " . translate("von Admin"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                                $member = new person($result[$i]["tblPersonen.personID"]);
                                                echo "<td>{$result[$i]["login"]}</td>";
                                                echo "<td>" . substr($member->modDateOwner, 0, 10) . "</td>";
                                                echo "<td>" . substr($member->modDateAdmin, 0, 10) . "</td>";
                                                echo "</tr>";
                                            }

                                            unset($result);
                                            ?>
                                            </tbody>
                                        </table>

                                        <?php
                                    } // if( count( $result ) > 0 )
                                    ?>

                                </div>
                                <!-- end .child -->
                            </div>
                            <!-- end .groupChildren -->
                        </div><!-- end .parent -->

                        <?php
                    } // while( $row = mysql_fetch_row( $result0 ) )
                    ?>

                </fieldset>
            </form>

        <?php } ?>

        <?php if ($context == "keineaktualisierung") { ?>

            <?php
            $result = memberList(45);
            ?>

            <h1><?php echo translate("Keine Aktualisierung"); ?> (<?php echo count($result); ?>)</h1>

            <?php if (count($result) > 0) { ?>
                <table class="tableGrid tableMargin sortable">
                    <thead>
                    <tr>
                        <th class="firstColumn fixWidthSearchResults sorttable_sorted"><?php echo translate("Name"); ?>
                            <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    for ($i = 0; $i < count($result); $i++) {
                        echo "<tr>";
                        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                        echo "</tr>";
                    }

                    unset($result);
                    ?>
                    </tbody>
                </table>
            <?php } ?>

        <?php } ?>

        <?php if ($context == "offlineaktualisiert") { ?>

            <?php
            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");

            list($result, $modDateAdmin) = memberList(62);
            ?>

            <form name="formReminder">
                <fieldset>

                    <h1><?php echo translate("Offline aktualisiert"); ?> (<?php echo count($result); ?>)</h1>

                    <!--p>
                                <?php echo translate("Ausgewählte"); ?>:
                        <input type="button" name="sendButton" value="<?php echo translate("Veröffentlichen"); ?>" onclick="for( i=0; i<<?php echo count($result); ?>; i++ ) { if( document.forms['formReminder'].elements['remindMember['+i+']'].checked ) { sendXmlHttpRequest( 'publishMember.php', 'id='+document.forms['formReminder'].elements['remindMember['+i+']'].value, 'document.getElementById( \'ajaxwait_'+i+'\' ).src=\'image/template/ajax_wait.gif\';', 'document.getElementById( \'ajaxwait_'+i+'\' ).src=\'image/dummy.gif\';' ); } } return false;" />
                        </p-->

                    <?php if (count($result) > 0) { ?>
                        <table class="tableGrid tableMargin sortable">
                            <thead>
                            <tr>
                                <th class="firstColumn"><?php echo translate("Name"); ?></th>
                                <!--th class="sorttable_nosort"><?php echo translate("Veröffentlichen"); ?> <input type="checkbox" checked="checked" onclick="this.checked=true; for( i=0; i<<?php echo count($result); ?>; i++ ) { document.forms['formReminder'].elements['remindMember['+i+']'].checked=true; }" />/<input type="checkbox" onclick="this.checked=false; for( i=0; i<<?php echo count($result); ?>; i++ ) { document.forms['formReminder'].elements['remindMember['+i+']'].checked=false; } return false;" /></th-->
                                <th class="sorttable_sorted_reverse"><?php echo translate("zuletzt bearbeitet von Admin"); ?>
                                    <span id="sorttable_sortrevind">&nbsp;&#x25BE;</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            arsort($modDateAdmin);
                            $ix = array_keys($modDateAdmin);

                            for ($i = 0; $i < count($ix); $i++) {
                                echo "<tr>";
                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$ix[$i]]["tblPersonen.name"]}{$result[$ix[$i]]["tblPersonen.vorname"]}{$result[$ix[$i]]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$ix[$i]]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$ix[$i]]["qryDienstgrad.dienstgrad"], $result[$ix[$i]]["linkPersonSprache.sprache"]) . " " . translate($result[$ix[$i]]["qryDienstgrad.zusatzDg"], $result[$ix[$i]]["linkPersonSprache.sprache"]) . "<br />" . $result[$ix[$i]][".fullName"] . "</a></td>";
//         echo "<td><input name=\"remindMember[$i]\" value=\"{$result[$ix[$i]]["tblPersonen.personID"]}\" type=\"checkbox\" /><img id=\"ajaxwait_$i\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                echo "<td>{$modDateAdmin[$ix[$i]]}</td>";
                                echo "</tr>";
                            }

                            unset($ix, $modDateAdmin, $result);
                            ?>
                            </tbody>
                        </table>
                    <?php } ?>

                </fieldset>
            </form>

        <?php } ?>

        <?php
        if ($context == "einteilung") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT IFNULL(einteilung,'')
              FROM tblMilitaer
              WHERE datumEnde>=CURDATE()
              ORDER BY einteilung";
            $result = $conn->query($query);

            dbconn::close($conn);

            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
            $select->selected = 52;
            ?>

            <form name="einteilung">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(46, $row[0]);
                        if (count($result1) > 0) {
                            ?>
                            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                        class="parent fontSize"><?php echo $row[0]; ?> (<?php echo count($result1); ?>
                                        ):</span></a>

                                <div class="groupChildren">
                                    <div class="child">

                                        <p>
                                            <?php
                                            $select->output(array("name" => "select$k"));
                                            ?>
                                            <input type="button" value="<?php echo translate("Senden"); ?>"
                                                   onclick="button = this;
                                                       img = document.getElementById('spin<?php echo $k; ?>');
                                                       sendXmlHttpRequest('emailer.php', 'id=' + document.forms['einteilung'].elements['select<?php echo $k; ?>'].value + '&amp;list=L20&amp;param=<?php echo $row[0]; ?>', 'startSpin( button, img );', 'stopSpin( button, img );');"/>
                                            <img id="spin<?php echo $k; ?>" src="image/dummy.gif" width="16" alt=""/>
                                            <input type="button" value="<?php echo translate("Test-Mail"); ?>"
                                                   onclick="button = this;
                                                       sendXmlHttpRequest('emailer.php', 'id=' + document.forms['einteilung'].elements['select<?php echo $k; ?>'].value + '&amp;recipientsID=0&amp;param=<?php echo urlencode($row[0]); ?>', 'startSpin( button );', 'stopSpin( button );');"/>
                                        </p>

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Funktion"); ?></th>
                                                <th><?php echo translate("Mitgliederstatus"); ?></th>
                                                <th><?php echo translate("E-Mail"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Neue Einteilung"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " " . $result1[$i][".fullName"] . "</a></td>";
                                                echo "<td>{$result1[$i]["qryMilitaer.funktion"]} {$result1[$i]["qryMilitaer.zusatzFkt"]}" . (preg_match("/^[kc]dt/i", $result1[$i]["qryMilitaer.funktion"]) ? " {$result1[$i]["qryMilitaer.einteilung"]}" : "") . "</td>";
                                                echo "<td>" . ($result1[$i]["qryVerein.funktionID"] <= 13 ? translate("Mitglied") : $result1[$i]["qryVerein.vereinFunktion"]) . "</td>";
                                                echo "<td>{$result1[$i]["tblPersonen.email"]}</td>";
                                                echo "<td class=\"nowrap\"><input name=\"einteilung$k\" type=\"text\" value=\"{$result1[$i]["qryMilitaer.einteilung"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'setMilEinteilung.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;militaerID={$result1[$i]["qryMilitaer.militaerID"]}&amp;einteilung='+document.forms['einteilung'].elements['einteilung$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end .child -->
                                </div>
                                <!-- end .groupChildren -->
                            </div><!-- end .parent -->

                            <?php
                        } // if( count( $result1 ) > 0 )
                    } // while( $row = $result->fetch(PDO::FETCH_NUM) )
                    ?>

                </fieldset>
            </form>

        <?php } // if( $context == "einteilung" )  ?>

        <?php
        if ($context == "vorgesvb") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT IFNULL(vorgesvb,'')
              FROM tblMilitaer
              WHERE datumEnde>=CURDATE() AND funktion LIKE '_dt' AND zusatzFktID BETWEEN 1 AND 3
              ORDER BY vorgesvb";
            $result = $conn->query($query);

            dbconn::close($conn);
            ?>

            <form name="vorgesvb">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(51, $row[0]);
                        if (count($result1) > 0) {
                            ?>
                            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                        class="parent fontSize"><?php echo $row[0]; ?> (<?php echo count($result1); ?>
                                        ):</span></a>

                                <div class="groupChildren">
                                    <div class="child">

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Aktuelle Einteilung"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Name"); ?></th>
                                                <th><?php echo translate("Funktion"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Neuer Vorges Vb"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\">{$result1[$i]["qryMilitaer.einteilung"]}</td>";
                                                echo "<td sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " " . $result1[$i][".fullName"] . "</a></td>";
                                                echo "<td>{$result1[$i]["qryMilitaer.funktion"]} {$result1[$i]["qryMilitaer.zusatzFkt"]}</td>";
                                                echo "<td class=\"nowrap\"><input name=\"vorgesvb$k\" type=\"text\" value=\"{$result1[$i]["qryMilitaer.vorgesVb"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'setMilVorgesVb.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;militaerID={$result1[$i]["qryMilitaer.militaerID"]}&amp;vorgesVb='+document.forms['vorgesvb'].elements['vorgesvb$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end .child -->
                                </div>
                                <!-- end .groupChildren -->
                            </div><!-- end .parent -->

                            <?php
                        } // if( count( $result1 ) > 0 )
                    } // while( $row = $result->fetch(PDO::FETCH_NUM) )
                    ?>

                </fieldset>
            </form>

        <?php } // if( $context == "vorgesvb" )  ?>

        <?php
        if ($context == "vorgesvbstab") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT IFNULL(vorgesvb,'')
              FROM tblMilitaer
              WHERE datumEnde>=CURDATE() AND funktion NOT LIKE '_dt' AND zusatzFktID BETWEEN 1 AND 3
              ORDER BY vorgesvb";
            $result = $conn->query($query);

            dbconn::close($conn);
            ?>

            <form name="vorgesvb">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(68, $row[0]);
                        if (count($result1) > 0) {
                            ?>
                            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                        class="parent fontSize"><?php echo $row[0]; ?> (<?php echo count($result1); ?>
                                        ):</span></a>

                                <div class="groupChildren">
                                    <div class="child">

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Aktuelle Einteilung"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Von") . "/" . translate("Bis"); ?></th>
                                                <th><?php echo translate("Name"); ?></th>
                                                <th><?php echo translate("Funktion"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Neuer Vorges Vb"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\">{$result1[$i]["qryMilitaer.einteilung"]}</td>";
                                                echo "<td>" . substr($result1[$i]["qryMilitaer.datumBeginn"], 0, 4) . "-" . str_replace("9999", translate("heute"), substr($result1[$i]["qryMilitaer.datumEnde"], 0, 4)) . "</td>";
                                                echo "<td sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " " . $result1[$i][".fullName"] . "</a></td>";
                                                echo "<td>{$result1[$i]["qryMilitaer.funktion"]} {$result1[$i]["qryMilitaer.zusatzFkt"]}</td>";
                                                echo "<td class=\"nowrap\"><input name=\"vorgesvb$k\" type=\"text\" value=\"{$result1[$i]["qryMilitaer.vorgesVb"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'setMilVorgesVb.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;militaerID={$result1[$i]["qryMilitaer.militaerID"]}&amp;vorgesVb='+document.forms['vorgesvb'].elements['vorgesvb$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end .child -->
                                </div>
                                <!-- end .groupChildren -->
                            </div><!-- end .parent -->

                            <?php
                        } // if( count( $result1 ) > 0 )
                    } // while( $row = $result->fetch(PDO::FETCH_NUM) )
                    ?>

                </fieldset>
            </form>

        <?php } // if( $context == "vorgesvbstab" ) ?>

        <?php
        if ($context == "vorgesvballe") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT IFNULL(vorgesVb,'')
              FROM tblMilitaer
              WHERE (funktion LIKE '_dt' OR funktion LIKE 'Chef') AND zusatzFktID BETWEEN 1 AND 3
              ORDER BY vorgesVb";
            $result = $conn->query($query);

            dbconn::close($conn);
            ?>

            <form name="vorgesvballe">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(60, $row[0]);
                        if (count($result1) > 0) {
                            ?>
                            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                        class="parent fontSize"><?php echo $row[0]; ?> (<?php echo count($result1); ?>
                                        ):</span></a>

                                <div class="groupChildren">
                                    <div class="child">

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Einteilung"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Von") . "/" . translate("Bis"); ?></th>
                                                <th><?php echo translate("Name"); ?></th>
                                                <th><?php echo translate("Funktion"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Neuer Vorges Vb"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\">{$result1[$i]["qryMilitaer.einteilung"]}</td>";
                                                echo "<td>" . substr($result1[$i]["qryMilitaer.datumBeginn"], 0, 4) . "-" . str_replace("9999", translate("heute"), substr($result1[$i]["qryMilitaer.datumEnde"], 0, 4)) . "</td>";
                                                echo "<td sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryMilitaer.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryMilitaer.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " " . $result1[$i][".fullName"] . "</a></td>";
                                                echo "<td>{$result1[$i]["qryMilitaer.funktion"]} {$result1[$i]["qryMilitaer.zusatzFkt"]}</td>";
                                                echo "<td class=\"nowrap\"><input name=\"vorgesvb$k\" type=\"text\" value=\"{$result1[$i]["qryMilitaer.vorgesVb"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'setMilVorgesVb.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;militaerID={$result1[$i]["qryMilitaer.militaerID"]}&amp;vorgesVb='+document.forms['vorgesvballe'].elements['vorgesvb$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end .child -->
                                </div>
                                <!-- end .groupChildren -->
                            </div><!-- end .parent -->

                            <?php
                        } // if( count( $result1 ) > 0 )
                    } // while( $row = $result->fetch(PDO::FETCH_NUM) )
                    ?>

                </fieldset>
            </form>

        <?php } // if( $context == "vorgesvballe" )  ?>

        <?php if ($context == "typ") { ?>

            <?php $result = memberList(19); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Undefined"); ?> (<?php echo count($result); ?>
                        ):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail privat"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["tblPersonen.email"]}\">{$result[$i]["tblPersonen.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20, 6); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Pensionäre"); ?> (<?php echo count($result); ?>
                        ):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail privat"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["tblPersonen.email"]}\">{$result[$i]["tblPersonen.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20, 3); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Berufsoffiziere"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail geschäftlich"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20, 2); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Verwaltung"); ?> (<?php echo count($result); ?>
                        ):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail geschäftlich"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20, 1); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Privatwirtschaft"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail geschäftlich"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20, 4); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Verband") . "/" . translate("Organisation"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail geschäftlich"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <h1><br/>Nur Mitglieder</h1>

            <?php $result = memberList(19.1); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Undefined"); ?> (<?php echo count($result); ?>
                        ):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail privat"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["tblPersonen.email"]}\">{$result[$i]["tblPersonen.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20.1, 6); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Pensionäre"); ?> (<?php echo count($result); ?>
                        ):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail privat"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["tblPersonen.email"]}\">{$result[$i]["tblPersonen.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20.1, 3); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Berufsoffiziere"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail geschäftlich"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20.1, 2); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Verwaltung"); ?> (<?php echo count($result); ?>
                        ):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail geschäftlich"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20.1, 1); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Privatwirtschaft"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail geschäftlich"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(20.1, 4); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Verband") . "/" . translate("Organisation"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("Funktion") . ", " . translate("Organisation"); ?></th>
                                    <th class="fixWidthSearchResults"><?php echo translate("E-Mail geschäftlich"); ?></th>
                                    <th class="fixWidthSearchResults sorttable_nosort"><?php echo translate("Berufstyp setzen"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . (empty($result[$i]["qryBeruf.funktion"]) ? "N/A" : $result[$i]["qryBeruf.funktion"]) . ", " . (empty($result[$i]["qryBeruf.organisation"]) ? "N/A" : $result[$i]["qryBeruf.organisation"]) . "</td>";
                                    echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                                    echo "<td><a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php','action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=1' ); this.className='disabled' } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=4' ); this.className='disabled' } return false;\">" . translate("Verb/Org") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;member={$result[$i]["tblPersonen.personID"]}&amp;typ=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

        <?php } ?>

        <?php
        if ($context == "brevet") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT brevetierungsdatum
              FROM (
                  SELECT personID,LEFT(MIN(datumBeginn),4) AS brevetierungsdatum
                  FROM tblMilitaer
                  WHERE dienstgradID BETWEEN 1 AND 2
                  GROUP BY personID
              ) AS qry
              WHERE brevetierungsdatum>'0000-00-00'
              ORDER BY brevetierungsdatum DESC";
            $result = $conn->query($query);

            dbconn::close($conn);

            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
            $select->selected = 49;
            ?>

            <form name="brevet">
                <fieldset>

                    <?php
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(24, $row[0]);
                        $result2 = memberList(25, $row[0]);
                        ?>
                        <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                    class="parent fontSize"><?php echo translate("Brevetierungsjahr") . " " . $row[0]; ?>
                                    (<?php echo count($result1); ?>):</span></a>

                            <div class="groupChildren">
                                <div class="child">

                                    <?php if (count($result1) > 0) { ?>

                                        <p>
                                            <?php
                                            $select->output(array("name" => "jahr{$row[0]}"));
                                            ?>
                                            <input type="button" value="<?php echo translate("Senden"); ?>"
                                                   onclick="button = this;
                                                       img = document.getElementById('spinwheel<?php echo $row[0]; ?>');
                                                       sendXmlHttpRequest('emailer.php', 'id=' + document.forms['brevet'].elements['jahr<?php echo $row[0]; ?>'].value + '&amp;list=L18&amp;param=<?php echo $row[0]; ?>', 'startSpin( button, img );', 'stopSpin( button, img );');"/>
                                            <img id="spinwheel<?php echo $row[0]; ?>" src="image/dummy.gif" width="16"
                                                 alt=""/>
                                            <input type="button" value="<?php echo translate("Test-Mail"); ?>"
                                                   onclick="button = this;
                                                       sendXmlHttpRequest('emailer.php', 'id=' + document.forms['brevet'].elements['jahr<?php echo $row[0]; ?>'].value + '&amp;recipientsID=0&amp;param=<?php echo $row[0]; ?>', 'startSpin( button );', 'stopSpin( button );');"/>
                                        </p>

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Jahrgang"); ?></th>
                                                <th><?php echo translate("Berufstyp") . " " . translate("Brevetierungsjahr"); ?></th>
                                                <th><?php echo translate("Berufstyp") . " " . translate("aktuell"); ?></th>
                                                <th><?php echo translate("E-Mail"); ?></th>
                                                <th><?php echo translate("Mitgliederstatus"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $j = 0;
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . ($result1[$i]["qryDienstgrad.zusatzDgID"] != 4 && $result1[$i]["tblPersonen.todesdatum"] == "0000-00-00" && ($result1[$i]["tblPersonen.geburtsdatum"] != "0000-00-00" && substr($result1[$i]["tblPersonen.geburtsdatum"], 0, 4) < date("Y") - 60 || substr($result1[$i]["qryBrevet.brevetierungsdatum"], 0, 4) < date("Y") - 30) ? " </a><a href=\"#\" onclick=\"noScrollingToTop(); sendXmlHttpRequest( 'setZusatzDg.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;militaerID={$result1[$i]["qryMilitaer.militaerID"]}&amp;zusatzDgID=4' ); return false;\"><img src=\"image/template/alert.gif\" alt=\"\" /></a><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" : "") . " {$result1[$i][".fullName"]}</a>" . ($result1[$i]["tblPersonen.todesdatum"] != "0000-00-00" ? " (&dagger;)" : "") . "</td>";
                                                echo "<td>" . str_replace("0000", "", substr($result1[$i]["tblPersonen.geburtsdatum"], 0, 4)) . "</td>";
                                                echo "<td>";
                                                if ($result1[$i]["tblPersonen.personID"] == $result2[$j]["tblPersonen.personID"]) {
                                                    if ($result2[$j]["qryBeruf.typID"] != 0) {
                                                        echo translate($result2[$j]["qryBeruf.typ"]);
                                                    } else {
                                                        echo "<a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=3&amp;year={$row[0]}' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=2&amp;year={$row[0]}' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=1&amp;year={$row[0]}' ); this.className='disabled'; } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=6&amp;year={$row[0]}' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a>";
                                                    }
                                                    $j++;
                                                } else {
                                                    echo "<a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=3&amp;year={$row[0]}' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=2&amp;year={$row[0]}' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=1&amp;year={$row[0]}' ); this.className='disabled'; } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=6&amp;year={$row[0]}' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a>";
                                                }
                                                echo "</td>";
                                                echo "<td>";
                                                if ($result1[$i]["qryBeruf.typID"] != 0) {
                                                    echo translate($result1[$i]["qryBeruf.typ"]);
                                                } else {
                                                    echo "<a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=3' ); this.className='disabled' } return false;\">" . translate("BO") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=2' ); this.className='disabled' } return false;\">" . translate("Verw") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=1' ); this.className='disabled'; } return false;\">" . translate("Priv") . "</a> | <a href=\"#\" onclick=\"noScrollingToTop(); if( this.className != 'disabled' ) { sendXmlHttpRequest( 'setJobType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;typID=6' ); this.className='disabled' } return false;\">" . translate("Pens") . "</a>";
//             echo "<td class=\"nowrap\"><input name=\"einteilung$k\" type=\"text\" value=\"{$result1[$i]["qryMilitaer.einteilung"]}\" /> <input type=\"button\" value=\"" . translate( "OK" ) . "\" onclick=\"img=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'setMilEinteilung.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;militaerID={$result1[$i]["qryMilitaer.militaerID"]}&amp;einteilung='+document.forms['einteilung'].elements['einteilung$k'].value, 'img.src=imgWheel.src;', 'window.setTimeout( \'img.src=imgDummy.src\', 500 );' );\" /> <img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                }
                                                echo "</td>";
                                                echo "<td>" . translate($result1[$i]["tblPersonen.email"] ? "Ja" : "Nein") . "</td>";
                                                echo "<td>{$result1[$i]["qryVerein.vereinFunktion"]}</td>";
                                                echo "</tr>";
                                            }

                                            unset($result1, $result2);
                                            ?>
                                            </tbody>
                                        </table>
                                    <?php } // if( count( $result1 ) > 0 )  ?>

                                </div>
                                <!-- end .child -->
                            </div>
                            <!-- end .groupChildren -->
                        </div><!-- end .parent -->

                    <?php } // while( $row = $result->fetch(PDO::FETCH_NUM) ) ?>

                    <?php $result = memberList(26); ?>
                    <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                class="parent fontSize"><?php echo translate("Fehler"); ?>
                                (<?php echo count($result); ?>):</span></a>

                        <div class="groupChildren">
                            <div class="child">

                                <?php if (count($result) > 0) { ?>

                                    <table class="tableGrid tableMargin sortable">
                                        <thead>
                                        <tr>
                                            <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($i = 0; $i < count($result); $i++) {
                                            echo "<tr>";
                                            echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                            echo "</tr>";
                                        }

                                        unset($result);
                                        ?>
                                        </tbody>
                                    </table>
                                <?php } ?>

                            </div>
                            <!-- end .child -->
                        </div>
                        <!-- end .groupChildren -->
                    </div>
                    <!-- end .parent -->

                </fieldset>
            </form>

        <?php } // if( $context == "brevet" )  ?>

        <?php
        if ($context == "branchen") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT branche
              FROM tblBeruf
              WHERE datumEnde>=CURDATE() AND typID BETWEEN 0 AND 5
              ORDER BY branche";
            $result = $conn->query($query);

            dbconn::close($conn);

            $select = new select(NULL, "linkBerufPosition");
            $select->emptyrow = true;
            ?>

            <form id="branchen">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(27, $row[0]);
                        ?>
                        <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                    class="parent fontSize"><?php echo $row[0]; ?> (<?php echo count($result1); ?>
                                    ):</span></a>

                            <div class="groupChildren">

                                <div class="child">
                                    <?php if (count($result1) > 0) { ?>

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Funktion"); ?></th>
                                                <th><?php echo translate("Organisation"); ?></th>
                                                <th><?php echo translate("Position"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Branche"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " {$result1[$i][".fullName"]}</a></td>";
                                                echo "<td>{$result1[$i]["qryBeruf.funktion"]}</td>";
                                                echo "<td>{$result1[$i]["qryBeruf.organisation"]}</td>";
                                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result1[$i]["qryBeruf.position"]}\">";
                                                $select->output(array("name" => "position$k", "selected" => $result1[$i]["qryBeruf.positionID"]));
                                                echo " <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_position$k' ); sendXmlHttpRequest( 'setJobPosition.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;berufID={$result1[$i]["qryBeruf.berufID"]}&amp;positionID='+document.forms['branchen'].elements['position$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_position$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "<td class=\"nowrap\"><input name=\"branche$k\" type=\"text\" value=\"{$result1[$i]["qryBeruf.branche"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_branche$k' ); sendXmlHttpRequest( 'setJobSector.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;berufID={$result1[$i]["qryBeruf.berufID"]}&amp;branche='+document.forms['branchen'].elements['branche$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_branche$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>
                                    <?php } // if( count( $result1 ) > 0 ) ?>

                                </div>
                                <!-- end .child -->
                            </div>
                            <!-- end .groupChildren -->
                        </div><!-- end .parent -->

                    <?php } // while( $row = $result->fetch(PDO::FETCH_NUM) ) ?>

                </fieldset>
            </form>

        <?php } // if( $context == "branchen" ) ?>

        <?php
        if ($context == "branchenalle") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT branche
              FROM tblBeruf
              WHERE typID BETWEEN 0 AND 5
              ORDER BY branche";
            $result = $conn->query($query);

            dbconn::close($conn);

            $select = new select(NULL, "linkBerufPosition");
            $select->emptyrow = true;
            ?>

            <form id="branchenalle">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(52, $row[0]);
                        ?>
                        <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                    class="parent fontSize"><?php echo $row[0]; ?> (<?php echo count($result1); ?>
                                    ):</span></a>

                            <div class="groupChildren">

                                <div class="child">
                                    <?php if (count($result1) > 0) { ?>

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Funktion"); ?></th>
                                                <th><?php echo translate("Organisation"); ?></th>
                                                <th><?php echo translate("Position"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Branche"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " {$result1[$i][".fullName"]}</a></td>";
                                                echo "<td>{$result1[$i]["qryBeruf.funktion"]}</td>";
                                                echo "<td>{$result1[$i]["qryBeruf.organisation"]}</td>";
                                                echo "<td class=\"nowrap\" sorttable_customkey=\"{$result1[$i]["qryBeruf.position"]}\">";
                                                $select->output(array("name" => "position$k", "selected" => $result1[$i]["qryBeruf.positionID"]));
                                                echo " <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_position$k' ); sendXmlHttpRequest( 'setJobPosition.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;berufID={$result1[$i]["qryBeruf.berufID"]}&amp;positionID='+document.forms['branchenalle'].elements['position$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_position$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "<td class=\"nowrap\"><input name=\"branche$k\" type=\"text\" value=\"{$result1[$i]["qryBeruf.branche"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_branche$k' ); sendXmlHttpRequest( 'setJobSector.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;berufID={$result1[$i]["qryBeruf.berufID"]}&amp;branche='+document.forms['branchenalle'].elements['branche$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_branche$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>
                                    <?php } // if( count( $result1 ) > 0 ) ?>

                                </div>
                                <!-- end .child -->
                            </div>
                            <!-- end .groupChildren -->
                        </div><!-- end .parent -->

                    <?php } // while( $row = $result->fetch(PDO::FETCH_NUM) )  ?>

                </fieldset>
            </form>

        <?php } // if( $context == "branchenalle" ) ?>

        <?php
        if ($context == "abschluessetitel") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT IFNULL(titel,'')
              FROM tblBerufAusbildung
              ORDER BY titel";
            $result = $conn->query($query);

            dbconn::close($conn);

            $select = new select("select", "linkBerufAusbildungTyp");
            $select->emptyrow = true;
            ?>

            <form name="ausbildung">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(47, $row[0]);
                        if (count($result1) > 0) {
                            ?>
                            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                        class="parent fontSize"><?php echo $row[0]; ?> (<?php echo count($result1); ?>
                                        ):</span></a>

                                <div class="groupChildren">
                                    <div class="child">

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Institution"); ?></th>
                                                <th><?php echo translate("Jahr"); ?></th>
                                                <th><?php echo translate("Mitgliederstatus"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Titel"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Typ"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " {$result1[$i][".fullName"]}</a></td>";
                                                echo "<td class=\"nowrap\"><input name=\"institution$k\" type=\"text\" value=\"{$result1[$i]["qryBerufAusbildung.institution"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"img=document.getElementById( 'spinwheel_institution$k' ); sendXmlHttpRequest( 'setEducationInstitution.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;institution='+document.forms['ausbildung'].elements['institution$k'].value, 'startSpin( this, img );', 'window.setTimeout( \'stopSpin( this, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_institution$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "<td>{$result1[$i]["qryBerufAusbildung.abschlussjahr"]}</td>";
                                                echo "<td>{$result1[$i]["qryVerein.vereinFunktion"]}</td>";
                                                echo "<td class=\"nowrap\"><input name=\"titel$k\" type=\"text\" value=\"{$result1[$i]["qryBerufAusbildung.titel"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_titel$k' ); sendXmlHttpRequest( 'setEducationTitle.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;titel='+document.forms['ausbildung'].elements['titel$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_titel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "<td class=\"nowrap\">";
                                                $select->output(array("name" => "typ$k", "selected" => $result1[$i]["qryBerufAusbildung.typID"]));
                                                echo " <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_typ$k' ); sendXmlHttpRequest( 'setEducationType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;typID='+document.forms['ausbildung'].elements['typ$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_typ$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end .child -->
                                </div>
                                <!-- end .groupChildren -->
                            </div><!-- end .parent -->

                            <?php
                        } // if( count( $result1 ) > 0 )
                    } // while( $row = $result->fetch(PDO::FETCH_NUM) )
                    ?>

                </fieldset>
            </form>

        <?php } // if( $context == "abschluessetitel" ) ?>

        <?php
        if ($context == "abschluesseinstitution") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT IFNULL(institution,'')
              FROM tblBerufAusbildung
              ORDER BY institution";
            $result = $conn->query($query);

            dbconn::close($conn);

            $select = new select("select", "linkBerufAusbildungTyp");
            $select->emptyrow = true;
            ?>

            <form name="ausbildung">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(48, $row[0]);
                        if (count($result1) > 0) {
                            ?>
                            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                        class="parent fontSize"><?php echo $row[0]; ?> (<?php echo count($result1); ?>
                                        ):</span></a>

                                <div class="groupChildren">
                                    <div class="child">

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Institution"); ?></th>
                                                <th><?php echo translate("Jahr"); ?></th>
                                                <th><?php echo translate("Mitgliederstatus"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Titel"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Typ"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " {$result1[$i][".fullName"]}</a></td>";
                                                echo "<td class=\"nowrap\"><input name=\"institution$k\" type=\"text\" value=\"{$result1[$i]["qryBerufAusbildung.institution"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"img=document.getElementById( 'spinwheel_institution$k' ); sendXmlHttpRequest( 'setEducationInstitution.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;institution='+document.forms['ausbildung'].elements['institution$k'].value, 'startSpin( this, img );', 'window.setTimeout( \'stopSpin( this, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_institution$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "<td>{$result1[$i]["qryBerufAusbildung.abschlussjahr"]}</td>";
                                                echo "<td>{$result1[$i]["qryVerein.vereinFunktion"]}</td>";
                                                echo "<td class=\"nowrap\"><input name=\"titel$k\" type=\"text\" value=\"{$result1[$i]["qryBerufAusbildung.titel"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_titel$k' ); sendXmlHttpRequest( 'setEducationTitle.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;titel='+document.forms['ausbildung'].elements['titel$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_titel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "<td class=\"nowrap\">";
                                                $select->output(array("name" => "typ$k", "selected" => $result1[$i]["qryBerufAusbildung.typID"]));
                                                echo " <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_typ$k' ); sendXmlHttpRequest( 'setEducationType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;typID='+document.forms['ausbildung'].elements['typ$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_typ$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end .child -->
                                </div>
                                <!-- end .groupChildren -->
                            </div><!-- end .parent -->

                            <?php
                        } // if( count( $result1 ) > 0 )
                    } // while( $row = $result->fetch(PDO::FETCH_NUM) )
                    ?>

                </fieldset>
            </form>

        <?php } // if( $context == "abschluesseinstitution" ) ?>

        <?php
        if ($context == "abschluessetyp") {
            $conn = dbconn::open();
            $query = "SELECT DISTINCT typID,typ
              FROM tblBerufAusbildung
              LEFT JOIN linkBerufAusbildungTyp ON tblBerufAusbildung.typID=linkBerufAusbildungTyp.linkTypID
              ORDER BY typ";
            $result10 =$conn->query($query);

            dbconn::close($conn);

            $select = new select("select", "linkBerufAusbildungTyp");
            $select->emptyrow = true;
            ?>

            <form name="ausbildung">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row10 = $result10->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(49, $row10[0]);
                        if (count($result1) > 0) {
                            ?>
                            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                        class="parent fontSize"><?php echo translate($row10[1]); ?>
                                        (<?php echo count($result1); ?>):</span></a>

                                <div class="groupChildren">
                                    <div class="child">

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("Institution"); ?></th>
                                                <th><?php echo translate("Jahr"); ?></th>
                                                <th><?php echo translate("Mitgliederstatus"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Titel"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Typ"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " {$result1[$i][".fullName"]}</a></td>";
                                                echo "<td class=\"nowrap\"><input name=\"institution$k\" type=\"text\" value=\"{$result1[$i]["qryBerufAusbildung.institution"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"img=document.getElementById( 'spinwheel_institution$k' ); sendXmlHttpRequest( 'setEducationInstitution.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;institution='+document.forms['ausbildung'].elements['institution$k'].value, 'startSpin( this, img );', 'window.setTimeout( \'stopSpin( this, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_institution$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "<td>{$result1[$i]["qryBerufAusbildung.abschlussjahr"]}</td>";
                                                echo "<td>{$result1[$i]["qryVerein.vereinFunktion"]}</td>";
                                                echo "<td class=\"nowrap\"><input name=\"titel$k\" type=\"text\" value=\"{$result1[$i]["qryBerufAusbildung.titel"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_titel$k' ); sendXmlHttpRequest( 'setEducationTitle.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;titel='+document.forms['ausbildung'].elements['titel$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_titel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "<td class=\"nowrap\">";
                                                $select->output(array("name" => "typ$k", "selected" => $result1[$i]["qryBerufAusbildung.typID"]));
                                                echo " <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_typ$k' ); sendXmlHttpRequest( 'setEducationType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;typID='+document.forms['ausbildung'].elements['typ$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_typ$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end .child -->
                                </div>
                                <!-- end .groupChildren -->
                            </div><!-- end .parent -->

                            <?php
                        } // if( count( $result1 ) > 0 )
                    } // while( $row10 = mysql_fetch_row( $result10 ) )
                    ?>

                    <?php
                    $conn = dbconn::open();

                    $query = "SELECT DISTINCT typID,typ
              FROM tblBerufAusbildung
              LEFT JOIN linkBerufAusbildungTyp ON tblBerufAusbildung.typID=linkBerufAusbildungTyp.linkTypID
              WHERE typID>0
              ORDER BY typ";
                    $result20 =$conn->query($query);


                    $query = "SELECT *
              FROM linkBerufTyp";
                    $result30 =$conn->query($query);


                    dbconn::close($conn);

                    while ($row30 = $result30->fetch(PDO::FETCH_NUM) ) {
                        echo "<h1><br />{$row30[1]}</h1>";
                        while ($row20 = $result20->fetch(PDO::FETCH_NUM)) {
                            $result1 = memberList(50, array($row20[0], $row30[0]));
                            if (count($result1) > 0) {
                                ?>
                                <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                        return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                            class="parent fontSize"><?php echo translate($row20[1]); ?>
                                            (<?php echo count($result1); ?>):</span></a>

                                    <div class="groupChildren">
                                        <div class="child">

                                            <table class="tableGrid tableMargin sortable">
                                                <thead>
                                                <tr>
                                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                    <th><?php echo translate("Institution"); ?></th>
                                                    <th><?php echo translate("Jahr"); ?></th>
                                                    <th><?php echo translate("Mitgliederstatus"); ?></th>
                                                    <th class="sorttable_nosort"><?php echo translate("Titel"); ?></th>
                                                    <th class="sorttable_nosort"><?php echo translate("Typ"); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                for ($i = 0; $i < count($result1); $i++) {
                                                    echo "<tr>";
                                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " {$result1[$i][".fullName"]}</a></td>";
                                                    echo "<td class=\"nowrap\"><input name=\"institution$k\" type=\"text\" value=\"{$result1[$i]["qryBerufAusbildung.institution"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"img=document.getElementById( 'spinwheel_institution$k' ); sendXmlHttpRequest( 'setEducationInstitution.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;institution='+document.forms['ausbildung'].elements['institution$k'].value, 'startSpin( this, img );', 'window.setTimeout( \'stopSpin( this, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_institution$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                    echo "<td>{$result1[$i]["qryBerufAusbildung.abschlussjahr"]}</td>";
                                                    echo "<td>{$result1[$i]["qryVerein.vereinFunktion"]}</td>";
                                                    echo "<td class=\"nowrap\"><input name=\"titel$k\" type=\"text\" value=\"{$result1[$i]["qryBerufAusbildung.titel"]}\" /> <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_titel$k' ); sendXmlHttpRequest( 'setEducationTitle.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;titel='+document.forms['ausbildung'].elements['titel$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_titel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                    echo "<td class=\"nowrap\">";
                                                    $select->output(array("name" => "typ$k", "selected" => $result1[$i]["qryBerufAusbildung.typID"]));
                                                    echo " <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel_typ$k' ); sendXmlHttpRequest( 'setEducationType.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;ausbildungID={$result1[$i]["qryBerufAusbildung.ausbildungID"]}&amp;typID='+document.forms['ausbildung'].elements['typ$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' );\" /> <img id=\"spinwheel_typ$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                    echo "</tr>";
                                                    $k++;
                                                }
                                                ?>
                                                </tbody>
                                            </table>

                                        </div>
                                        <!-- end .child -->
                                    </div>
                                    <!-- end .groupChildren -->
                                </div><!-- end .parent -->

                                <?php
                            } // if( count( $result1 ) > 0 )
                        } // while( $row20 = mysql_fetch_row( $result20 ) )
                        // mysql_data_seek($result20, 0);
                    } // while( $row30 = mysql_fetch_row( $result30 ) )
                    ?>

                </fieldset>
            </form>

        <?php } // if( $context == "abschluessetyp" )  ?>

        <?php
        if ($context == "regionen") {
            $conn = dbconn::open();
            $query = "SELECT linkOrganisationID,organisation
              FROM linkVereinOrganisation
              WHERE linkOrganisationID BETWEEN 3 AND 10
              ORDER BY organisation";
            $result = $conn->query($query);

            dbconn::close($conn);

            $select = new select(NULL, "linkVereinOrganisation");
            $select->emptyrow = true;
            ?>

            <form name="regionen">
                <fieldset>

                    <?php
                    $k = 0;
                    while ($row = $result->fetch(PDO::FETCH_NUM)) {
                        $result1 = memberList(30, $row[0]);
                        ?>
                        <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                    class="parent fontSize"><?php echo $row[1]; ?> (<?php echo count($result1); ?>
                                    ):</span></a>

                            <div class="groupChildren">
                                <div class="child">

                                    <?php if (count($result1) > 0) { ?>

                                        <p><a href="map.php?regionID=<?php echo urlencode($row[0]); ?>" onclick="window.open(this.href);
                    return false;"><?php echo translate("Darstellung auf einer Karte"); ?></a></p>

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th><?php echo translate("PLZ"); ?>/<?php echo translate("Ort"); ?></th>
                                                <th class="sorttable_nosort"><?php echo translate("Region"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result1); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " {$result1[$i][".fullName"]}</a></td>";
                                                echo "<td><a href=\"http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q=" . str_replace(" ", "+", $result1[$i]["tblPersonen.ort"]) . ",+" . str_replace(" ", "+", $result1[$i]["tblPersonen.land"]) . "&amp;iwloc=addr\" onclick=\"window.open(this.href); return false;\" title=\"" . translate("Adresse auf GoogleMaps anzeigen") . "\">{$result1[$i]["tblPersonen.plz"]} {$result1[$i]["tblPersonen.ort"]}</a></td>";
                                                echo "<td class=\"nowrap\">";
                                                $select->output(array("name" => "region$k", "selected" => $result1[$i]["qryVerein.organisationRegionID"]));
                                                echo " <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"button=this; img=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'setRegion.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;vereinID={$result1[$i]["qryVerein.vereinID"]}&amp;organisationRegionID='+document.forms['regionen'].elements['region$k'].value, 'startSpin( button, img );', 'window.setTimeout( \'stopSpin( button, img, imgOk )\', 500 );' ); return false;\" /><img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                                echo "</tr>";
                                                $k++;
                                            }

                                            unset($result1);
                                            ?>
                                            </tbody>
                                        </table>
                                    <?php } // if( count( $result1 ) > 0 ) ?>

                                </div>
                                <!-- end .child -->
                            </div>
                            <!-- end .groupChildren -->
                        </div><!-- end .parent -->

                    <?php } // while( $row = $result->fetch(PDO::FETCH_NUM) ) ?>

                    <?php
                    $conn = dbconn::open();
                    $query = "SELECT tblPersonen.*,qryDienstgrad.*,qryVerein.*,linkPersonSprache.*
              FROM tblPersonen
              LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID
              LEFT JOIN (
                  SELECT personID,dienstgradID,dienstgrad,dienstgradText,zusatzDgID,zusatzDg.zusatz AS zusatzDg
                  FROM tblMilitaer
                  LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
                  LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
                  WHERE datumBeginn<=CURDATE()
                  ORDER BY datumBeginn DESC
              ) AS qryDienstgrad ON tblPersonen.personID=qryDienstgrad.personID
              LEFT JOIN (
                  SELECT *
                  FROM tblVerein
                  WHERE datumBeginn<=CURDATE() AND organisationRegionID BETWEEN 3 AND 10
                  ORDER BY datumEnde DESC
              ) AS qryVerein ON tblPersonen.personID=qryVerein.personID
              LEFT JOIN (
                  SELECT *
                  FROM tblVerein
                  WHERE datumBeginn<=CURDATE() AND datumEnde>=CURDATE() AND organisationRegionID=1 AND funktionID=1
              ) AS qryVerein2 ON tblPersonen.personID=qryVerein2.personID
              WHERE qryVerein.vereinID IS NULL AND qryVerein2.vereinID IS NOT NULL
              GROUP BY tblPersonen.personID
              ORDER BY tblPersonen.plz,tblPersonen.name,tblPersonen.vorname,tblPersonen.vorname2";
                    $result = $conn->query($query);

                    dbconn::close($conn);

                    $result1 = array();
                    while ($result1[] = mysql_fetch_alias_assoc($result)) {

                    }
                    array_pop($result1);
                    ?>

                    <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                class="parent fontSize"><?php echo translate("Undefined"); ?>
                                (<?php echo count($result1); ?>):</span></a>

                        <div class="groupChildren">
                            <div class="child">

                                <?php if (count($result1) > 0) { ?>

                                    <p>
                                        <?php
                                        echo translate("Für alle ausgewählten") . " ";
                                        $select->output(array("name" => "region"));
                                        echo " <input type=\"button\" value=\"" . translate("OK") . "\" onclick=\"f=document.forms['regionen']; for( i=$k; i<" . ($k + count($result1)) . "; i++ ) { if( f.elements['check'+i].checked ) { f.elements['region'+i].value=f.elements['region'].value; f.elements['okButton'+i].click(); f.elements['check'+i].checked=false; } } return false;\" />";
                                        ?>
                                    </p>
                                    <table class="tableGrid tableMargin sortable">
                                        <thead>
                                        <tr>
                                            <th class="firstColumn sorttable_nosort">&nbsp;</th>
                                            <th><?php echo translate("Name"); ?></th>
                                            <th class="sorttable_sorted"><?php echo translate("PLZ"); ?>
                                                /<?php echo translate("Ort"); ?><span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span>
                                            </th>
                                            <th class="sorttable_nosort"><?php echo translate("Region"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($i = 0; $i < count($result1); $i++) {
                                            echo "<tr>";
                                            echo "<td class=\"firstColumn\"><input name=\"check$k\" value=\"1\" type=\"checkbox\" /></td>";
                                            echo "<td sorttable_customkey=\"{$result1[$i]["tblPersonen.name"]}{$result1[$i]["tblPersonen.vorname"]}{$result1[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result1[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result1[$i]["qryDienstgrad.dienstgrad"], $result1[$i]["linkPersonSprache.sprache"]) . " " . translate($result1[$i]["qryDienstgrad.zusatzDg"], $result1[$i]["linkPersonSprache.sprache"]) . " {$result1[$i][".fullName"]}</a></td>";
                                            echo "<td><a href=\"http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q=" . str_replace(" ", "+", $result1[$i]["tblPersonen.ort"]) . ",+" . str_replace(" ", "+", $result1[$i]["tblPersonen.land"]) . "&amp;iwloc=addr\" onclick=\"window.open(this.href); return false;\" title=\"" . translate("Adresse auf GoogleMaps anzeigen") . "\">{$result1[$i]["tblPersonen.plz"]} {$result1[$i]["tblPersonen.ort"]}</a></td>";
                                            echo "<td class=\"nowrap\">";
                                            $select->output(array("name" => "region$k", "selected" => $result1[$i]["qryVerein.organisationRegionID"]));
                                            echo " <input type=\"button\" name=\"okButton$k\" value=\"" . translate("OK") . "\" onclick=\"button$k=this; img$k=document.getElementById( 'spinwheel$k' ); sendXmlHttpRequest( 'setRegion.php', 'action=UPDATE&amp;personID={$result1[$i]["tblPersonen.personID"]}&amp;vereinID=NULL&amp;organisationRegionID='+document.forms['regionen'].elements['region$k'].value, 'startSpin( button$k, img$k );', 'window.setTimeout( \'stopSpin( button$k, img$k, imgOk )\', 500 );' ); return false;\" /><img id=\"spinwheel$k\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" /></td>";
                                            echo "</tr>";
                                            $k++;
                                        }

                                        unset($result1);
                                        ?>
                                        </tbody>
                                    </table>
                                <?php } // if( count( $result1 ) > 0 ) ?>

                            </div>
                            <!-- end .child -->
                        </div>
                        <!-- end .groupChildren -->
                    </div>
                    <!-- end .parent -->

                </fieldset>
            </form>

        <?php } // if( $context == "regionen" ) ?>

        <?php if ($context == "almostlost") { ?>

            <?php $result = memberList(42); ?>
            <h1><?php echo translate("Almost lost"); ?> (<?php echo count($result); ?>)</h1>

            <?php if (count($result) > 0) { ?>

                <table class="tableGrid tableMargin sortable">
                    <thead>
                    <tr>
                        <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                        <th><?php echo translate("Geburtsdatum"); ?></th>
                        <th><?php echo translate("Private Adresse"); ?></th>
                        <th><?php echo translate("Geschäftsadresse"); ?></th>
                        <th><?php echo translate("Private E-Mail"); ?></th>
                        <th><?php echo translate("Geschäfts E-Mail"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    for ($i = 0; $i < count($result); $i++) {
                        echo "<tr>";
                        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . "<br />" . $result[$i][".fullName"] . "</a></td>";
                        echo "<td>{$result[$i]["tblPersonen.geburtsdatum"]}</td>";
                        echo "<td>{$result[$i]["tblPersonen.adresse"]}" . ($result[$i]["tblPersonen.postfach"] ? "<br />{$result[$i]["tblPersonen.postfach"]}" : "") . "<br />{$result[$i]["tblPersonen.plz"]} {$result[$i]["tblPersonen.ort"]}</td>";
                        echo "<td>{$result[$i]["qryBeruf.adresse"]}" . ($result[$i]["qryBeruf.postfach"] ? "<br />{$result[$i]["qryBeruf.postfach"]}" : "") . "<br />{$result[$i]["qryBeruf.plz"]} {$result[$i]["qryBeruf.ort"]}</td>";
                        echo "<td><a href=\"mailto:{$result[$i]["tblPersonen.email"]}\">{$result[$i]["tblPersonen.email"]}</a></td>";
                        echo "<td><a href=\"mailto:{$result[$i]["qryBeruf.email"]}\">{$result[$i]["qryBeruf.email"]}</a></td>";
                        echo "</tr>";
                    }

                    unset($result);
                    ?>
                    </tbody>
                </table>
            <?php } ?>

        <?php } ?>


        <!-- #################################################################### -->


        <?php if ($context == "statistik") { ?>
            <ul>
                <li><a href="admin.php?context=statistikaktuell" onclick="window.open(this.href);
                return false"><?php echo translate("Aktuell"); ?></a></li>
                <li><a href="admin.php?context=statistikvorjahr" onclick="window.open(this.href);
                return false"><?php echo translate("Vorjahr"); ?></a></li>
                <li><a href="admin.php?context=statistikallgemein" onclick="window.open(this.href);
                return false"><?php echo translate("Allgemein"); ?></a></li>
                <li class="last"><a href="admin.php?context=population" onclick="window.open(this.href);
                return false"><?php echo translate("Population"); ?></a></li>
                <li><a href="admin.php?context=alter" onclick="window.open(this.href);
                return false"><?php echo translate("Alter"); ?></a></li>
                <li><a href="admin.php?context=sprache" onclick="window.open(this.href);
                return false"><?php echo translate("Sprache"); ?></a></li>
                <li class="last"><a href="admin.php?context=geburtstage" onclick="window.open(this.href);
                return false"><?php echo translate("Geburtstage"); ?></a></li>
            </ul>
        <?php } ?>

        <?php if ($context == "statistikaktuell") { ?>

            <h1>Aktuell</h1>

            <?php $result = memberList(2); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Aktuelle Liste GGstOf Mitglieder"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=2&amp;format=xls")); ?></p>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th><?php echo translate("Typ"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>{$result[$i]["qryBeruf.typ"]}</td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(4); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Liste der Beitritte in diesem Jahr"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=4&amp;format=xls")); ?></p>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th><?php echo translate("Beitrittsdatum"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . str_replace("0000-00-00", "", $result[$i]["qryBeitritt.beitrittsdatum"]) . "</td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php
            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
            $select->selected = 14;

            $result = memberList(11);
            ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Neu brevetierte Gst Of"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=11&amp;format=xls")); ?></p>

                            <form id="formBrevet">
                                <fieldset>
                                    <table class="tableGrid tableMargin sortable">
                                        <thead>
                                        <tr>
                                            <th class="firstColumn"><?php echo translate("Name"); ?></th>
                                            <th class="sorttable_sorted_reverse"><?php echo translate("Reminder") . "-/" . translate("Beitrittsdatum"); ?>
                                                <span id="sorttable_sortrevind">&nbsp;&#x25BE;</span></th>
                                            <th><?php echo translate("Funktion"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($i = 0; $i < count($result); $i++) {
                                            echo "<tr>";
                                            echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                            echo "<td>";
                                            if ($result[$i]["qryVerein.funktionID"] < 99) {
                                                echo str_replace("0000-00-00", "", $result[$i]["qryBeitritt.beitrittsdatum"]);
                                            } else if ($result[$i]["qryVerein.funktionID"] == 100) {
                                                echo substr($result[$i]["qryVerein.timestamp"], 0, 10);
                                            } else {
                                                echo substr($result[$i]["tblPersonen.remindDate"], 0, 10) . " ";
                                                $select->output(array("name" => "template_$i"));
                                                echo "<input type=\"button\" name=\"sendButton\" value=\"" . translate("Senden") . "\" onclick=\"obj=this; pic=document.getElementById( 'ajaxwait_$i' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['formBrevet'].elements['template_$i'].value+'&amp;personID={$result[$i]["tblPersonen.personID"]}', 'obj.disabled=true; pic.src=\'image/template/ajax_wait.gif\';', 'pic.src=\'image/dummy.gif\'; obj.disabled=false;' ); return false;\" />";
                                                echo "<img id=\"ajaxwait_$i\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />";
                                            }
                                            echo "</td>";
                                            echo "<td>{$result[$i]["qryVerein.vereinFunktion"]}</td>";
                                            echo "</tr>";
                                        }

                                        unset($result, $select);
                                        ?>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </form>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(6); ?>

            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Liste der Austritte und Verstorbenen in diesem Jahr"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th><?php echo translate("Mitglied bis"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "" . (($result[$i]["tblPersonen.todesdatum"] != "0000-00-00") ? " (&dagger; " . substr($result[$i]["tblPersonen.todesdatum"], 0, 4) . ")" : "") . "</a></td>";
                                    echo "<td>" . str_replace("9999-12-31", "", $result[$i]["qryAustritt.austrittsdatum"]) . "</td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

        <?php } ?>

        <?php if ($context == "statistikvorjahr") { ?>

            <h1>Vorjahr</h1>

            <?php $result = memberList(5); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Liste der Beitritte"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th><?php echo translate("Beitrittsdatum"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>{$result[$i]["qryBeitritt.beitrittsdatum"]}</td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(7); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Liste der Austritte"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th><?php echo translate("Mitglied bis"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>" . str_replace("9999-12-31", "", $result[$i]["qryAustritt.austrittsdatum"]) . "</td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(40); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Liste der Verstorbenen"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th><?php echo translate("Mitglied bis"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "" . (($result[$i]["tblPersonen.todesdatum"] != "0000-00-00") ? " (&dagger; " . substr($result[$i]["tblPersonen.todesdatum"], 0, 4) . ")" : "") . "</a></td>";
                                    echo "<td>" . str_replace("9999-12-31", "", $result[$i]["qryAustritt.austrittsdatum"]) . "</td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php
            $select = new select();
            $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
            $select->selected = 14;

            $result = memberList(41);
            ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Neu brevetierte Gst Of"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=11&amp;format=xls")); ?></p>

                            <form id="formBrevet">
                                <fieldset>
                                    <table class="tableGrid tableMargin sortable">
                                        <thead>
                                        <tr>
                                            <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                                <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                            <th class="sorttable_nosort"><?php echo translate("Beitrittsdatum"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($i = 0; $i < count($result); $i++) {
                                            echo "<tr>";
                                            echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                            echo "<td>";
                                            if ($result[$i]["qryBeitritt.beitrittsdatum"]) {
                                                echo str_replace("0000-00-00", "", $result[$i]["qryBeitritt.beitrittsdatum"]);
                                            } else {
                                                echo substr($result[$i]["tblPersonen.remindDate"], 0, 10) . " ";
                                                $select->output(array("name" => "template_$i"));
                                                echo "<input type=\"button\" name=\"sendButton\" value=\"" . translate("Senden") . "\" onclick=\"obj=this; pic=document.getElementById( 'ajaxwait_$i' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['formBrevet'].elements['template_$i'].value+'&amp;personID={$result[$i]["tblPersonen.personID"]}', 'obj.disabled=true; pic.src=\'image/template/ajax_wait.gif\';', 'pic.src=\'image/dummy.gif\'; obj.disabled=false;' ); return false;\" />";
                                                echo "<img id=\"ajaxwait_$i\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />";
                                            }
                                            echo "</td>";
                                            echo "</tr>";
                                        }

                                        unset($result, $select);
                                        ?>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </form>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

        <?php } ?>

        <?php if ($context == "statistikallgemein") { ?>

            <h1>Allgemein</h1>

            <?php $result = memberList(8); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Liste der Verstorbenen"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . " (&dagger; " . substr($result[$i]["tblPersonen.todesdatum"], 0, 4) . ")</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(10); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Kein Interesse an GGstOf"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    <th><?php echo translate("Typ"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "<td>{$result[$i]["qryBeruf.typ"]}</td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(13); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Kein Interesse an Armee"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

        <?php } ?>

        <?php if ($context == "population") { ?>

            <?php
            $conn = dbconn::open();
            $query = "SELECT *
              FROM linkPersonVeroeffentlichung
              ORDER BY (veroeffentlichungID=0), veroeffentlichungID";
            $result0 =$conn->query($query);

            dbconn::close($conn);

            while ($row = $result0->fetch(PDO::FETCH_NUM) ) {
                $result = memberList(61, $row[0]);
                ?>

                <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                            class="parent fontSize"><?php echo translate($row[1]); ?> (<?php echo count($result); ?>
                            ):</span></a>

                    <div class="groupChildren">
                        <div class="child">

                            <?php if (count($result) > 0) { ?>
                                <table class="tableGrid tableMargin">
                                    <thead>
                                    <tr>
                                        <th class="firstColumn nowrap sorttable_sorted"><?php echo translate("Name"); ?>
                                            <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    for ($i = 0; $i < count($result); $i++) {
                                        echo "<tr>";
                                        echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                        echo "</tr>";
                                    }

                                    unset($result);
                                    ?>
                                    </tbody>
                                </table>

                                <?php
                            } // if( count( $result ) > 0 )
                            ?>

                        </div>
                        <!-- end .child -->
                    </div>
                    <!-- end .groupChildren -->
                </div><!-- end .parent -->

                <?php
            } // while( $row = mysql_fetch_row( $result0 ) )
            ?>


            <?php $result = memberList(9, "*"); ?>

            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("NICHT-Mitglieder"); ?>
                        (<?php echo count($result); ?>)</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <table class="tableGrid tableMargin">
                            <thead>
                            <tr>
                                <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                        id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for ($i = 0; $i < count($result); $i++) {
                                echo "<tr>";
                                echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                echo "</tr>";
                            }

                            unset($result);
                            ?>
                            </tbody>
                        </table>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(63); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Ohne Interesse"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(8); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Liste der Verstorbenen"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

        <?php } ?>

        <?php if ($context == "alter") { ?>

            <?php
            for ($a = 31; $a < 65; $a += 5) {
                $result = memberList(34, $a);
                ?>
                <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                            class="parent fontSize"><?php echo translate("Alter Mitglieder") . " " . ($a == 31 ? 30 : $a) . "-" . ($a + 4); ?>
                            (<?php echo count($result); ?>):</span></a>

                    <div class="groupChildren">
                        <div class="child">

                            <?php if (count($result) > 0) { ?>

                                <table class="tableGrid tableMargin sortable">
                                    <thead>
                                    <tr>
                                        <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                            <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    for ($i = 0; $i < count($result); $i++) {
                                        echo "<tr>";
                                        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . ($result[$i]["qryDienstgrad.zusatzDgID"] != 4 && ($result[$i]["tblPersonen.geburtsdatum"] != "0000-00-00" && substr($result[$i]["tblPersonen.geburtsdatum"], 0, 4) < date("Y") - 60 || substr($result[$i]["qryBrevet.brevetierungsdatum"], 0, 4) < date("Y") - 30) ? " </a><a href=\"#\" onclick=\"noScrollingToTop(); sendXmlHttpRequest( 'setZusatzDg.php', 'action=UPDATE&amp;personID={$result[$i]["tblPersonen.personID"]}&amp;militaerID={$result[$i]["qryMilitaer.militaerID"]}&amp;zusatzDgID=4' ); return false;\"><img src=\"image/template/alert.gif\" alt=\"\" /></a><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" : "") . " " . $result[$i][".fullName"] . "</a></td>";
                                        echo "</tr>";
                                    }

                                    unset($result);
                                    ?>
                                    </tbody>
                                </table>
                            <?php } ?>

                        </div>
                        <!-- end .child -->
                    </div>
                    <!-- end .groupChildren -->
                </div><!-- end .parent -->

                <?php
            } // for( $a=31; $a<65; $a+=5 )
            ?>

            <?php $result = memberList(35); ?>
            <div class="parent last"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Alter Mitglieder"); ?>
                        >65 (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . ($result[$i]["qryDienstgrad.zusatzDgID"] != 4 && ($result[$i]["tblPersonen.geburtsdatum"] != "0000-00-00" && substr($result[$i]["tblPersonen.geburtsdatum"], 0, 4) < date("Y") - 60 || substr($result[$i]["qryBrevet.brevetierungsdatum"], 0, 4) < date("Y") - 30) ? " </a><a href=\"#\" onclick=\"noScrollingToTop(); sendXmlHttpRequest( 'setZusatzDg.php', 'action=UPDATE&amp;personID={$result[$i]["tblPersonen.personID"]}&amp;militaerID={$result[$i]["qryMilitaer.militaerID"]}&amp;zusatzDgID=4' ); return false;\"><img src=\"image/template/alert.gif\" alt=\"\" /></a><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" : "") . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php
            for ($a = 31; $a < 65; $a += 5) {
                $result = memberList(36, $a);
                ?>
                <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                            class="parent fontSize"><?php echo translate("Alter Nicht-Mitglieder") . " " . ($a == 31 ? 30 : $a) . "-" . ($a + 4); ?>
                            (<?php echo count($result); ?>):</span></a>

                    <div class="groupChildren">
                        <div class="child">

                            <?php if (count($result) > 0) { ?>

                                <table class="tableGrid tableMargin sortable">
                                    <thead>
                                    <tr>
                                        <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                            <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    for ($i = 0; $i < count($result); $i++) {
                                        echo "<tr>";
                                        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . ($result[$i]["qryDienstgrad.zusatzDgID"] != 4 && ($result[$i]["tblPersonen.geburtsdatum"] != "0000-00-00" && substr($result[$i]["tblPersonen.geburtsdatum"], 0, 4) < date("Y") - 60 || substr($result[$i]["qryBrevet.brevetierungsdatum"], 0, 4) < date("Y") - 30) ? " </a><a href=\"#\" onclick=\"noScrollingToTop(); sendXmlHttpRequest( 'setZusatzDg.php', 'action=UPDATE&amp;personID={$result[$i]["tblPersonen.personID"]}&amp;militaerID={$result[$i]["qryMilitaer.militaerID"]}&amp;zusatzDgID=4' ); return false;\"><img src=\"image/template/alert.gif\" alt=\"\" /></a><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" : "") . " " . $result[$i][".fullName"] . "</a></td>";
                                        echo "</tr>";
                                    }

                                    unset($result);
                                    ?>
                                    </tbody>
                                </table>
                            <?php } ?>

                        </div>
                        <!-- end .child -->
                    </div>
                    <!-- end .groupChildren -->
                </div><!-- end .parent -->

                <?php
            } // for( $a=31; $a<65; $a+=5 )
            ?>

            <?php $result = memberList(37); ?>
            <div class="parent last"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Alter Nicht-Mitglieder"); ?>
                        >65 (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="fixWidthSearchResults firstColumn sorttable_sorted"><?php echo translate("Name"); ?>
                                        <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . ($result[$i]["qryDienstgrad.zusatzDgID"] != 4 && ($result[$i]["tblPersonen.geburtsdatum"] != "0000-00-00" && substr($result[$i]["tblPersonen.geburtsdatum"], 0, 4) < date("Y") - 60 || substr($result[$i]["qryBrevet.brevetierungsdatum"], 0, 4) < date("Y") - 30) ? " </a><a href=\"#\" onclick=\"noScrollingToTop(); sendXmlHttpRequest( 'setZusatzDg.php', 'action=UPDATE&amp;personID={$result[$i]["tblPersonen.personID"]}&amp;militaerID={$result[$i]["qryMilitaer.militaerID"]}&amp;zusatzDgID=4' ); return false;\"><img src=\"image/template/alert.gif\" alt=\"\" /></a><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" : "") . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

        <?php } ?>

        <?php if ($context == "sprache") { ?>

            <?php $result = memberList(31, 1); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Deutsch") . " (" . translate("Mitglieder") . ")"; ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=31&amp;param=1&amp;format=xls")); ?></p>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(32, 1); ?>
            <div class="parent last"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Deutsch") . " (" . translate("Nicht-Mitglieder, kein Interesse... ohne verstorbene") . ")"; ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=32&amp;param=1&amp;format=xls")); ?></p>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(31, 2); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Französisch") . " (" . translate("Mitglieder") . ")"; ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=31&amp;param=2&amp;format=xls")); ?></p>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(32, 2); ?>
            <div class="parent last"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Französisch") . " (" . translate("Nicht-Mitglieder, kein Interesse... ohne verstorbene") . ")"; ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=32&amp;param=2&amp;format=xls")); ?></p>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(31, 3); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Italienisch") . " (" . translate("Mitglieder") . ")"; ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=31&amp;param=3&amp;format=xls")); ?></p>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(32, 3); ?>
            <div class="parent last"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Italienisch") . " (" . translate("Nicht-Mitglieder, kein Interesse... ohne verstorbene") . ")"; ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=32&amp;param=3&amp;format=xls")); ?></p>

                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php if (false) { ?>
                <?php $result = memberList(31, 4); ?>
                <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                            class="parent fontSize"><?php echo translate("Englisch") . " (" . translate("Mitglieder") . ")"; ?>
                            (<?php echo count($result); ?>):</span></a>

                    <div class="groupChildren">

                        <div class="child">
                            <?php if (count($result) > 0) { ?>

                                <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=31&amp;param=4&amp;format=xls")); ?></p>

                                <table class="tableGrid tableMargin sortable">
                                    <thead>
                                    <tr>
                                        <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                                id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    for ($i = 0; $i < count($result); $i++) {
                                        echo "<tr>";
                                        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                        echo "</tr>";
                                    }

                                    unset($result);
                                    ?>
                                    </tbody>
                                </table>
                            <?php } ?>

                        </div>
                        <!-- end .child -->
                    </div>
                    <!-- end .groupChildren -->
                </div><!-- end .parent -->

                <?php $result = memberList(32, 4); ?>
                <div class="parent last"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                            class="parent fontSize"><?php echo translate("Englisch") . " (" . translate("Nicht-Mitglieder, kein Interesse... ohne verstorbene") . ")"; ?>
                            (<?php echo count($result); ?>):</span></a>

                    <div class="groupChildren">

                        <div class="child">
                            <?php if (count($result) > 0) { ?>

                                <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=32&amp;param=4&amp;format=xls")); ?></p>

                                <table class="tableGrid tableMargin sortable">
                                    <thead>
                                    <tr>
                                        <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                                id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    for ($i = 0; $i < count($result); $i++) {
                                        echo "<tr>";
                                        echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                        echo "</tr>";
                                    }

                                    unset($result);
                                    ?>
                                    </tbody>
                                </table>
                            <?php } ?>

                        </div>
                        <!-- end .child -->
                    </div>
                    <!-- end .groupChildren -->
                </div><!-- end .parent -->
            <?php } // if( false )  ?>

        <?php } ?>

        <?php if ($context == "geburtstage") { ?>

            <form id="formBirthday">
                <fieldset>

                    <?php
                    $select = new select();
                    $select->query("SELECT emailID,titel FROM tblEmail ORDER BY titel");
                    $select->selected = 38;

                    $monate = array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");

                    for ($m = 0; $m < 12; $m++) {
                        $result = memberList(39, $m + 1);
                        ?>

                        <div class="parent last"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                                    class="parent fontSize"><?php echo translate($monate[$m]); ?>
                                    (<?php echo count($result); ?>):</span></a>

                            <div class="groupChildren">

                                <div class="child">
                                    <?php if (count($result) > 0) { ?>

                                        <table class="tableGrid tableMargin sortable">
                                            <thead>
                                            <tr>
                                                <th class="firstColumn sorttable_sorted"><?php echo translate("Tag"); ?>
                                                    <span id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                                <th class="fixWidthSearchResults"><?php echo translate("Name"); ?></th>
                                                <th><?php echo translate("Jahrgang"); ?></th>
                                                <th class="sorttable_nosort"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for ($i = 0; $i < count($result); $i++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\">" . substr($result[$i]["tblPersonen.geburtsdatum"], -2) . "</td>";
                                                echo "<td sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                                echo "<td>" . substr($result[$i]["tblPersonen.geburtsdatum"], 0, 4) . "</td>";
                                                echo "<td class=\"nowrap\">";
                                                $select->output(array("name" => "template_{$m}_{$i}"));
                                                echo "<input name=\"sendButton_$i\" type=\"button\" value=\"" . translate("Senden") . "\" onclick=\"obj=this; pic=document.getElementById( 'ajaxwait_{$m}_{$i}' ); sendXmlHttpRequest( 'emailer.php', 'id='+document.forms['formBirthday'].elements['template_{$m}_{$i}'].value+'&amp;personID={$result[$i]["tblPersonen.personID"]}', 'obj.disabled=true; pic.src=\'image/template/ajax_wait.gif\';', 'pic.src=\'image/dummy.gif\'; obj.disabled=false;' ); return false;\" />";
                                                echo "<img id=\"ajaxwait_{$m}_{$i}\" src=\"image/dummy.gif\" width=\"16\" alt=\"\" />";
                                                echo "</td>";
                                                echo "</tr>";
                                            }

                                            unset($result);
                                            ?>
                                            </tbody>
                                        </table>
                                    <?php } ?>

                                </div>
                                <!-- end .child -->
                            </div>
                            <!-- end .groupChildren -->
                        </div><!-- end .parent -->

                    <?php } // for()  ?>

                </fieldset>
            </form>

        <?php } ?>


        <!-- #################################################################### -->


        <?php if ($context == "admin") { ?>

            <?php
            $conn = dbconn::open();
            $query = "SELECT param0 FROM sysStor ORDER BY storID";
            $result = $conn->query($query);

            dbconn::close($conn);

            $row = array();
            while ($row[] = $result->fetch(PDO::FETCH_NUM)) {

            }
            ?>

            <ul>
                <li><a href="admin.php?context=log" onclick="window.open(this.href);
                return false"><?php echo translate("Log"); ?></a></li>
                <!--li><a href="admin.php?context=import" onclick="window.open( this.href ); return false"><?php echo translate("Import neue Gst Of"); ?></a></li-->
                <li><a href="admin.php?context=auswertung" onclick="window.open(this.href);
                return false"><?php echo translate("Auswertung"); ?></a></li>
                <li><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=12&amp;format=xls", "caption" => translate("Export Gst S") . " ({$row[6][0]})")); ?></li>
                <li class="last"><?php $pdfButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=33&amp;format=pdf", "caption" => translate("Nicht publizierte Mitglieder") . " ({$row[7][0]})")); ?></li>
            </ul>
        <?php } ?>

        <?php if ($context == "log") { ?>

                <?php
                $conn = dbconn::open();
//     $query = "SELECT sysLog.*,linkSysLogTyp.typ,qryPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache
//               FROM sysLog
//               LEFT JOIN linkSysLogTyp ON sysLog.typID=linkSysLogTyp.typID
//               LEFT JOIN (
//                   SELECT personID,vorname,vorname2,name,email,veroeffentlichungID,spracheID FROM tblPersonen
//                   WHERE email<>''
//               ) AS qryPersonen ON sysLog.userID=qryPersonen.email
//               LEFT JOIN linkPersonSprache ON qryPersonen.spracheID=linkPersonSprache.linkSpracheID
//               LEFT JOIN (
//                   SELECT personID,dienstgrad,zusatzDg.zusatz AS zusatzDg
//                   FROM tblMilitaer
//                   LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
//                   LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
//                   WHERE datumBeginn<=CURDATE()
//                   ORDER BY datumBeginn DESC
//               ) AS qryDienstgrad ON qryPersonen.personID=qryDienstgrad.personID
//               WHERE sysLog.timestamp>=DATE_SUB(CURDATE(),INTERVAL 7 DAY)
//               GROUP BY sysLog.loginID
//               ORDER BY sysLog.timestamp DESC";
                $query = "SELECT sysLog.*,linkSysLogTyp.typ,qryPersonen.*
              FROM sysLog
              LEFT JOIN linkSysLogTyp ON sysLog.typID=linkSysLogTyp.typID
              LEFT JOIN (
                  SELECT personID,email FROM tblPersonen
                  WHERE email<>''
              ) AS qryPersonen ON sysLog.userID=qryPersonen.email
              WHERE sysLog.timestamp>=DATE_SUB(CURDATE(),INTERVAL 7 DAY)
              GROUP BY sysLog.loginID
              ORDER BY sysLog.timestamp DESC";
                $result = $conn->query($query);

                dbconn::close($conn);

                $i = 0;
                $result1 = array(array());
                while ($row = mysql_fetch_alias_assoc($result) ) {
                    $prev = end($result1[$i]);
                    if (substr($prev["sysLog.timestamp"], 0, 10) != substr($row["sysLog.timestamp"], 0, 10)) {
                        $i++;
                    }
                    $result1[$i][] = $row;
                }

                unset($result);

                $success = array("delete.png", "ok.png");

                $lastSuccessfulLogin = array();
                for ($i = 0; $i < count($result1); $i++) {
                    if (count($result1[$i]) > 0) {
//             $successAll = true;
//             for( $j=0; $j<count( $result1[$i] ); $j++ ) {
//                 $successAll &= $result1[$i][$j]["sysLog.success"];
//             }
                        ?>

            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo substr($result1[$i][0]["sysLog.timestamp"], 0, 10); ?> (<?php echo count($result1[$i]); ?>): <!--img src="image/template/<?php echo $success[$successAll]; ?>" alt="" /--></span></a>
                            <div class="groupChildren">

                                <div class="child">

                                    <table class="tableGrid tableMargin">
                                        <thead>
                                            <tr>
                                                <th class="firstColumn"><?php echo translate("Zeit"); ?></th>
                                                <th class="fixWidthSearchResults"><?php echo translate("Name"); ?></th>
                                                <th><?php echo translate("IP-Adresse"); ?></th>
                                                <th><?php echo translate("Ereignis"); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($j = 0; $j < count($result1[$i]); $j++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\">" . substr($result1[$i][$j]["sysLog.timestamp"], -8) . "</td>";
                                                echo "<td>";
                                                if ($result1[$i][$j]["qryPersonen.personID"]) {
                                                    $pers = new person($result1[$i][$j]["qryPersonen.personID"]);
                                                    $pers->getMilitaryData();
                                                    echo "<a href=\"member_profile.php?member={$pers->id}\" onclick=\"window.open( this.href ); return false;\">" . translate($pers->current->militaer->dienstgrad, $pers->person->sprache) . " " . translate($pers->current->militaer->zusatzDg, $pers->person->sprache) . " {$pers->person->fullName}</a>";
                                                    if ($pers->person->veroeffentlichungID != 1) {
                                                        echo " <img src=\"image/template/page_user_dark.gif\" alt=\"Profil nicht vollständig angezeigt\" title=\"Profil nicht vollständig angezeigt\" />";
                                                    }
                                                } else {
                                                    echo $result1[$i][$j]["sysLog.userID"];
                                                }
                                                echo "</td>";
                                                echo "<td><a href=\"http://www.db.ripe.net/whois?form_type=simple&full_query_string=&searchtext={$result1[$i][$j]["sysLog.ip"]}&do_search=Search\" onclick=\"window.open( this.href ); return false;\">{$result1[$i][$j]["sysLog.ip"]}</a></td>";
                                                echo "<td>";
                                                if ($result1[$i][$j]["sysLog.param1"] == "iOf") {
                                                    echo "<img src=\"image/template/telephone.png\" alt=\"\" /> ";
                                                }
                                                if ($result1[$i][$j]["sysLog.typID"] < 4) {
                                                    echo "<img src=\"image/template/{$success[$result1[$i][$j]["sysLog.success"]]}\" alt=\"\" /> ";
                                                } else if ($result1[$i][$j]["sysLog.typID"] == 11) {
                                                    echo "<img src=\"image/template/action_refresh.gif\" alt=\"\" /> ";
                                                } else if ($result1[$i][$j]["sysLog.typID"] == 12) {
                                                    echo "<img src=\"image/template/mail2.png\" alt=\"\" /> ";
                                                }
                                                echo translate($result1[$i][$j]["linkSysLogTyp.typ"]);
                                                if ($result1[$i][$j]["sysLog.typID"] < 4 && $pers->id) {
                                                    if ($result1[$i][$j]["sysLog.success"]) {
                                                        $lastSuccessfulLogin["{$pers->id}"] = $result1[$i][$j]["sysLog.timestamp"];
                                                    } else {
                                                        if (!array_key_exists($pers->id, $lastSuccessfulLogin)) {
                                                            echo " <input type=\"button\" value=\"Mail Login FAIL\" onclick=\"obj=this; sendXmlHttpRequest( 'emailer.php', 'id=25&amp;personID={$pers->id}', 'obj.disabled=true;', 'obj.disabled=false;' ); return false;\" />";
                                                        }
                                                    }
                                                }
                                                if ($result1[$i][$j]["sysLog.typID"] == 5) {
                                                    echo " nach <a href=\"search.php?field=" . urlencode(strtok($result1[$i][$j]["sysLog.param0"], "=")) . "&amp;expr=" . urlencode(strtok("=")) . "\" onclick=\"window.open( this.href ); return false;\">{$result1[$i][$j]["sysLog.param0"]}</a>";
                                                } else if ($result1[$i][$j]["sysLog.typID"] == 10) {
                                                    $result2 = searchMember(array("tblPersonen.email" => $result1[$i][$j]["sysLog.param0"]));
                                                    echo " " . translate("an") . " " . translate($result2[0]["qryDienstgrad.dienstgrad"], $result2[0]["linkPersonSprache.sprache"]) . " " . translate($result2[0]["qryDienstgrad.zusatzDg"], $result2[0]["linkPersonSprache.sprache"]) . " {$result2[0][".fullName"]}";
                                                } else if ($result1[$i][$j]["sysLog.typID"] == 12) {
                                                    echo " &laquo;{$result1[$i][$j]["sysLog.param1"]}&raquo;";
                                                } else if ($result1[$i][$j]["sysLog.typID"] == 20) {
                                                    echo " <a href=\"member_profile.php?member=" . substr($result1[$i][$j]["sysLog.param0"], 9) . "\" onclick=\"window.open( this.href ); return false;\">{$result1[$i][$j]["sysLog.param0"]}</a>";
                                                }
                                                echo "</td>";
                                                echo "</tr>";
                                                unset($pers);
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div><!-- end .child -->
                            </div><!-- end .groupChildren -->
                        </div><!-- end .parent -->

                        </fieldset>
                        </form>

                        <?php
                    }
                }

                unset($result1);
                ?>

            <?php } ?>
        <?php if ($context == "loginfail") { ?>

                <?php
                $conn = dbconn::open();
                $query = "SELECT sysLog.*,linkSysLogTyp.typ,qryPersonen.*
              FROM sysLog
              LEFT JOIN linkSysLogTyp ON sysLog.typID=linkSysLogTyp.typID
              LEFT JOIN (
                  SELECT personID,email FROM tblPersonen
                  WHERE email<>''
              ) AS qryPersonen ON sysLog.userID=qryPersonen.email
              WHERE (linkSysLogTyp.typID = 1 AND sysLog.success = 0) AND userID<>''
              GROUP BY sysLog.loginID
              ORDER BY sysLog.timestamp DESC";
                $result = $conn->query($query);

                dbconn::close($conn);

                $i = 0;
                $result1 = array(array());
                while ($row = mysql_fetch_alias_assoc($result) ) {
                    $prev = end($result1[$i]);
                    if (substr($prev["sysLog.timestamp"], 0, 10) != substr($row["sysLog.timestamp"], 0, 10)) {
                        $i++;
                    }
                    $result1[$i][] = $row;
                }

                unset($result);

                $success = array("delete.png", "ok.png");

                $lastSuccessfulLogin = array();
                for ($i = 0; $i < count($result1); $i++) {
                    if (count($result1[$i]) > 0) {
//             $successAll = true;
//             for( $j=0; $j<count( $result1[$i] ); $j++ ) {
//                 $successAll &= $result1[$i][$j]["sysLog.success"];
//             }
                        ?>

            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                    return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo substr($result1[$i][0]["sysLog.timestamp"], 0, 10); ?> (<?php echo count($result1[$i]); ?>): <!--img src="image/template/<?php echo $success[$successAll]; ?>" alt="" /--></span></a>
                            <div class="groupChildren">
                                <div class="child">
                                    <table class="tableGrid tableMargin">
                                        <thead>
                                            <tr>
                                                <th class="firstColumn"><?php echo translate("Zeit"); ?></th>
                                                <th class="fixWidthSearchResults"><?php echo translate("Name"); ?></th>
                                                <th><?php echo translate("IP-Adresse"); ?></th>
                                                <th><?php echo translate("Ereignis"); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($j = 0; $j < count($result1[$i]); $j++) {
                                                echo "<tr>";
                                                echo "<td class=\"firstColumn\">" . substr($result1[$i][$j]["sysLog.timestamp"], -8) . "</td>";
                                                echo "<td>";
                                                if ($result1[$i][$j]["qryPersonen.personID"]) {
                                                    $pers = new person($result1[$i][$j]["qryPersonen.personID"]);
                                                    $pers->getMilitaryData();
                                                    echo "<a href=\"member_profile.php?member={$pers->id}\" onclick=\"window.open( this.href ); return false;\">" . translate($pers->current->militaer->dienstgrad, $pers->person->sprache) . " " . translate($pers->current->militaer->zusatzDg, $pers->person->sprache) . " {$pers->person->fullName}</a>";
                                                    if ($pers->person->veroeffentlichungID != 1) {
                                                        echo " <img src=\"image/template/page_user_dark.gif\" alt=\"Profil nicht vollständig angezeigt\" title=\"Profil nicht vollständig angezeigt\" />";
                                                    }
                                                } else {
                                                    echo $result1[$i][$j]["sysLog.userID"];
                                                }
                                                echo "</td>";
                                                echo "<td><a href=\"http://www.db.ripe.net/whois?form_type=simple&full_query_string=&searchtext={$result1[$i][$j]["sysLog.ip"]}&do_search=Search\" onclick=\"window.open( this.href ); return false;\">{$result1[$i][$j]["sysLog.ip"]}</a></td>";
                                                echo "<td>";
                                                if ($result1[$i][$j]["sysLog.param1"] == "iOf") {
                                                    echo "<img src=\"image/template/telephone.png\" alt=\"\" /> ";
                                                }
                                                if ($result1[$i][$j]["sysLog.typID"] < 4) {
                                                    echo "<img src=\"image/template/{$success[$result1[$i][$j]["sysLog.success"]]}\" alt=\"\" /> ";
                                                } else if ($result1[$i][$j]["sysLog.typID"] == 11) {
                                                    echo "<img src=\"image/template/action_refresh.gif\" alt=\"\" /> ";
                                                } else if ($result1[$i][$j]["sysLog.typID"] == 12) {
                                                    echo "<img src=\"image/template/mail2.png\" alt=\"\" /> ";
                                                }
                                                echo translate($result1[$i][$j]["linkSysLogTyp.typ"]);
                                                if ($result1[$i][$j]["sysLog.typID"] < 4 && $pers->id) {
                                                    if ($result1[$i][$j]["sysLog.success"]) {
                                                        $lastSuccessfulLogin["{$pers->id}"] = $result1[$i][$j]["sysLog.timestamp"];
                                                    } else {
                                                        if (!array_key_exists($pers->id, $lastSuccessfulLogin)) {
                                                            echo " <input type=\"button\" value=\"Mail Login FAIL\" onclick=\"obj=this; sendXmlHttpRequest( 'emailer.php', 'id=25&amp;personID={$pers->id}', 'obj.disabled=true;', 'obj.disabled=false;' ); return false;\" />";
                                                        }
                                                    }
                                                }
                                                echo " <input type=\"button\" value=\"Delete\" onclick=\"obj=this; sendXmlHttpRequest( 'delete.php', 'typID={$result1[$i][$j]["sysLog.typID"]}&loginID={$result1[$i][$j]["sysLog.loginID"]}&amp;personID={$pers->id}', 'obj.disabled=true;', 'obj.disabled=false;' ); return location.reload();\" />";
                                                echo "</td>";
                                                echo "</tr>";
                                                unset($pers);
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- end .child -->
                            </div><!-- end .groupChildren -->
                        </div><!-- end .parent -->

                        </fieldset>
                        </form>

            <?php
        }
    }

    unset($result1);
    ?>

<?php } ?>
        <?php if ($context == "import_neu") { ?>
            <link href="style/dropzone.min.css" type="text/css" rel="stylesheet"/>
            <script src="script/dropzone.js"></script>

            <form action="function/upload.php" class="dropzone" style="width: 300px; margin-bottom: 20px;"></form>

            <form method="POST" action="excel_import.php" class="excel">
                <select name="select" id="files">
                    <option value="-1">Datei auswählen!</option>

                    <?php
                    $path = "uploads";
                    $files = scandir($path);
                    foreach ($files as $key => $file) {
                        if ($file != '.' && $file != '..') {
                            echo '<option value="' . urlencode($file) . '">' . $file . '</option>';
                        }
                    }
                    ?>
                </select>
                <input type="submit"/>
            </form>

        <?php } ?>
        <?php if ($context == "import") { ?>

                <p>Anleitung</p>

                <p>
                <form name="import" id="import" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <input type="hidden" name="context" value="import" />

    <?php
    if (empty($_POST["brevetierungsdatum"])) {
        ?>

                            <label for="brevetierungsdatum"><?php echo translate("Brevetierungsdatum"); ?></label> <input type="text" id="brevetierungsdatum" name="brevetierungsdatum" maxlength="10" /><br />
                            <input type="submit" value="<?php echo translate("Weiter"); ?> >" />

        <?php
    } elseif (!empty($_POST["brevetierungsdatum"]) && !isset($_FILES["csvfile"])) {
        ?>

                            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
                return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt="" /><span class="parent fontSize"><?php echo translate("Standard-Variablen"); ?></span></a>
                                <div class="groupChildren">
                                    <div class="child">

                                        <h1><?php echo translate("Persönliche Informationen"); ?></h1>
                                        <table class="tableGrid tableMargin">
                                            <thead>
                                                <tr>
                                                    <th class="firstColumn"><?php echo translate("Anrede"); ?></th>
                                                    <th><?php echo translate("Aktualisierung"); ?></th>
                                                    <th><?php echo translate("Channel"); ?></th>
                                                    <th><?php echo translate("Post-Channel"); ?></th>
                                                    <th><?php echo translate("Mail-Channel"); ?></th>
                                                    <th><?php echo translate("Blog-Channel"); ?></th>
                                                    <th><?php echo translate("Rolle"); ?></th>
                                                    <th><?php echo translate("Mod Date"); ?></th>
                                                    <th><?php echo translate("Modifier"); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="firstColumn"><?php $select = new select("inputPrivat[anredeID]", "linkPersonAnrede", 1);
        $select->output(); ?></td>
                                                    <td><?php $select = new select("inputPrivat[aktualisierungID]", "linkPersonAktualisierung", 3);
        $select->output(); ?></td>
                                                    <td><?php $select = new select("inputPrivat[channelID]", "linkPersonChannel", 98);
        $select->output(); ?></td>
                                                    <td><?php $select = new select("inputPrivat[postChannelID]", "linkPersonChannelPost", 1);
        $select->output(); ?></td>
                                                    <td><?php $select = new select("inputPrivat[mailChannelID]", "linkPersonChannelMail", 1);
                    $select->output(); ?></td>
                                                    <td><?php $select = new select("inputPrivat[blogChannelID]", "linkBlogChannel", 1);
                    $select->output(); ?></td>
                                                    <td><?php $select = new select("inputPrivat[roleID]", "linkSysRoles", 1);
                    $select->output(); ?></td>
                                                    <td><input type="text" name="inputPrivat[modDateAdmin]" value="<?php echo $_POST["brevetierungsdatum"]; ?> 17:00:00" /></td>
                                                    <td><?php echo $_SESSION["userName"]; ?><input type="hidden" name="inputPrivat[modifierID]" value="<?php echo $_SESSION["userName"]; ?>" /></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <h1><?php echo translate("Militär"); ?></h1>
                                        <table class="tableGrid tableMargin">
                                            <thead>
                                                <tr>
                                                    <th class="firstColumn"><?php echo translate("Anfangsdatum"); ?></th>
                                                    <!--th><?php echo translate("Funktion"); ?></th-->
                                                    <th><?php echo translate("Dienstgrad"); ?></th>
                                                    <th><?php echo translate("Mod Date"); ?></th>
                                                    <th><?php echo translate("Modifier"); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="firstColumn"><input type="text" name="inputMilitaer[modDateAdmin]" value="<?php echo $_POST["brevetierungsdatum"]; ?>" /></td>
                                                    <!--td><input type="text" name="inputMilitaer[funktion]" value="Gst Of" /></td-->
                                                    <td><?php $select = new select("inputMilitaer[dienstgradID]", "linkMilitaerDienstgrad", 2);
                    $select->output(); ?></td>
                                                    <td><input type="text" name="inputMilitaer[modDateAdmin]" value="<?php echo $_POST["brevetierungsdatum"]; ?> 17:00:00" /></td>
                                                    <td><?php echo $_SESSION["userName"]; ?><input type="hidden" name="inputMilitaer[modifierID]" value="<?php echo $_SESSION["userName"]; ?>" /></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div><!-- end .child -->
                                </div><!-- end .groupChildren -->
                            </div><!-- end .parent -->

                            <label for="csvfile"><?php echo translate("CSV-Datei auswählen"); ?></label> <input id="csvfile" name="csvfile" type="file" /><br />
                            <input type="submit" value="<?php echo translate("Weiter"); ?> >" />
                            <input type="hidden" name="brevetierungsdatum" value="<?php echo $_POST["brevetierungsdatum"]; ?>" />

                                        <?php
                                    } else {
                                        ?>

                                        <?php
                                        $fp = fopen($_FILES["csvfile"]["tmp_name"], "r");
                                        $content = array();
                                        while (( $content[] = fgetcsv($fp, 0, ",") ) !== false) {
                                            
                                        }
                                        fclose($fp);
// print_r($content);
                                        foreach ($content as $line) {
                                            if (count($line)) {
                                                print_r($line);
                                                echo "<br />";
                                            }
                                        }
                                    }
                                    ?>

                    </fieldset>
                </form>
                </p>

<?php } ?>

        <?php if ($context == "auswertung") { ?>

            <?php $result = memberList(57); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Erfasste Gst Of"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(54); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("davon lebend"); ?> (<?php echo count($result); ?>
                        ):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(58); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("davon mit Bild"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

            <?php $result = memberList(59); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("davon veröffentlicht"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">
                    <div class="child">

                        <?php if (count($result) > 0) { ?>
                            <table class="tableGrid tableMargin sortable">
                                <thead>
                                <tr>
                                    <th class="firstColumn sorttable_sorted"><?php echo translate("Name"); ?><span
                                            id="sorttable_sortfwdind">&nbsp;&#x25B4;</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                for ($i = 0; $i < count($result); $i++) {
                                    echo "<tr>";
                                    echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\"><a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open( this.href ); return false;\">" . translate($result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"]) . " " . translate($result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"]) . " " . $result[$i][".fullName"] . "</a></td>";
                                    echo "</tr>";
                                }

                                unset($result);
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

        <?php } ?>

        <?php if ($context == "export") { ?>

            <?php $result = memberList(12); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Export Gst S"); ?> (<?php echo count($result); ?>
                        ):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $xlsButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=12&amp;format=xls")); ?></p>

                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->


            <?php $result = memberList(33); ?>
            <div class="parent"><a class="menuParent" href="#" onclick="noScrollingToTop();
            return false;"><img class="expandCollapse" src="image/template/collapsed.gif" alt=""/><span
                        class="parent fontSize"><?php echo translate("Nicht publizierte Mitglieder"); ?>
                        (<?php echo count($result); ?>):</span></a>

                <div class="groupChildren">

                    <div class="child">
                        <?php if (count($result) > 0) { ?>

                            <p><?php $pdfButton->output(array("href" => "{$_SERVER["SCRIPT_NAME"]}?action=EXPORT&amp;list=33&amp;format=pdf")); ?></p>

                        <?php } ?>

                    </div>
                    <!-- end .child -->
                </div>
                <!-- end .groupChildren -->
            </div><!-- end .parent -->

        <?php } ?>


        <!-- #################################################################### -->


    </div>
    <!-- .accordeon -->


</div>
<!-- #content -->


<?php
include("include/footer.inc.php");
?>


</body>

</html>