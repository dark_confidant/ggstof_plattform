<?php

    require_once('head.php');
    ggstof_head();

    require_once( "function/log.php" );

    setcookie( "lang", $_SESSION["lang"] );

    if( $_SESSION["userID"] ) {
        logLogout( $_SESSION["userID"] );
    }

    $_SESSION = array();
    if( isset( $_COOKIE[session_name()] ) ) {
        setcookie( session_name(), '', time()-42000, '/' );
    }
    session_unset();
    session_destroy();
    $_POST = array();
    header( "Location: index.php" );
//     $uri = ( isset( $_REQUEST["request"] ) ) ? "?request=" . $_REQUEST["request"] : "";
//     header( "Location: login.php$uri" );
?>