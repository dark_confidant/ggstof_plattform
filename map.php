<?php
    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }

    if( $_SESSION["rights"]["roleID"] < 3 ) {
        die( translate( "Zugriff verweigert." ) );
    }

    // Regionen
    if( isset( $_REQUEST["regionID"] ) ) {
        $criteria = array( "qryVerein.organisationRegionID"=>$_REQUEST["regionID"], "qryVerein.datumBeginn"=>array( "0000-00-00", date( "Y-m-d" ) ), "qryVerein.datumEnde"=>array( date( "Y-m-d" ), "9999-12-31" ) );
        $result = searchMember( $criteria );
    }
?>




<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf)"; ?></title>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
<!--

    var people   = new Array();
    var adresses = new Array();
<?php
    for( $i=0; $i<count( $result ); $i++ ) {
        $people = str_replace( "  ", " ", translate( $result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"] ) . " " . translate( $result[$i]["qryDienstgrad.zusatzDg"] ) . " {$result[$i][".fullName"]}" );
        echo "    people[$i]   = \"$people\";\n";
        echo "    adresses[$i] = \"{$result[$i]["tblPersonen.adresse"]}, {$result[$i]["tblPersonen.ort"]}, {$result[$i]["tblPersonen.land"]}\";\n";
    }
    echo "    var n = $i;\n";
?>

    var map;
    var latlng;
    var geocoder = new google.maps.Geocoder();
    var marker = new Array();
    var lat = new Array( 90, -90 );
    var lng = new Array( 180, -180 );
    var i = 0;

    function initialize() {
        latlng = new google.maps.LatLng( 46.818188, 8.227512 );
        var mapOptions = {
            zoom: 7,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        map = new google.maps.Map( document.getElementById( "map_canvas" ), mapOptions );

        if( n ) {
            document.getElementById( "waitimg" ).src = "image/template/ajax_wait.gif";
            geopos();
        }
    }

    function geopos() {
        geocoder.geocode( { address: adresses[i], language: "<?php echo $_SESSION["lang"]; ?>", region: "ch" }, function( results, status ) {
            if( status == google.maps.GeocoderStatus.OK ) {
                latlng = results[0].geometry.location;
                lat[0] = Math.min( lat[0], latlng.lat() );
                lat[1] = Math.max( lat[1], latlng.lat() );
                lng[0] = Math.min( lng[0], latlng.lng() );
                lng[1] = Math.max( lng[1], latlng.lng() );

                marker[0] = new google.maps.Marker( {
                    position: latlng,
                    map: map,
                    title: people[i]
                } );

                if( i < n-1 ) {
                    i++;
                    window.setTimeout( "geopos()", 1000 );
                } else {
                    latlng = new google.maps.LatLng( (lat[0]+lat[1])/2, (lng[0]+lng[1])/2 );
                    map.setCenter( latlng );
                    map.setZoom( 9 );
                    document.getElementById( "waitimg" ).src = "image/dummy.gif";
                }
//             } else {
//                 window.alert( "Geocode was not successful for the following reason: " + status );
            }
        });
    }

//-->
</script>
</head>


<body onload="initialize()">



<!-- start #navigationLeft -->
<?php
    include( "include/navigationLeft.inc.php" );
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
    include( "include/navigationTop.inc.php" );
?>
<!-- end #navigationTop -->



<div id="content">

<?php if( isset( $_REQUEST["regionID"] ) ) { ?>
<p><?php echo translate( "Mitglieder" ) . " " . translate( "in der" ) . " " . translate( $result[0]["qryVerein.organisation"] ); ?> <img id="waitimg" src="image/dummy.gif" alt="" /></p>
<?php } ?>

<div id="map_canvas" style="width:640px; height:480px"></div>

</div><!-- end content -->

<?php
    include( "include/footer.inc.php" );
?>


</body>
</html>