<?php
    require_once('head.php');
    ggstof_head();
?>



<?php
include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ); ?> (GGstOf)</title>
</head>




<!-- start body -->
<body>




<!-- start #navigationLeft -->
<?php
$hideMenu = true;
include( "include/navigationLeft.inc.php" );
?>
<!-- end #navigationLeft -->




<!-- start #navigationTop -->
<?php
$hideSearch = true;
include( "include/navigationTop.inc.php" );
?>
<!-- end #navigationTop -->




<!-- start #content -->
<div id="content">

<div class="contentHolder">

<?php
    $content = getContent( 10 );
    echo stripslashes( $content["text"] );
    $timestamp[] = strtotime( $content["timestamp"] );
?>

<?php if( $_SESSION["rights"]["editContent"] ) { ?>
<div class="editButtonHolder">
<a class="startEditButtonTop" href="fckeditor.php?action=EDIT&amp;item=10&amp;caller=<?php echo basename( $_SERVER["SCRIPT_NAME"] ); ?>"><img src="image/template/edit.gif" alt="<?php echo translate( "Bearbeiten" ) ?>" title="<?php echo translate( "Bearbeiten" ) ?>" /></a>
</div>
<?php } ?>

</div>
<!-- end .contentHolder -->

</div>
<!-- end #content -->




<?php
    $timestamp = date( "d.m.Y", max( $timestamp ) );
    include( "include/footer.inc.php" );
?>

</body>
<!-- end body -->

</html>