<?php
    require_once('head.php');
    ggstof_head(true);

    if( !$_SESSION["rights"]["datenschutz"] || !$_SESSION["rights"]["codex"] ) {
        header( "Location: check_privacy_codex.php" );
    }


    switch( $context = $_REQUEST["context"] ) {
        case "neumitglieder":
            $title = translate( "Die letzten 20 Beitritte" );
/*            $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1 );
            $result = searchMember( $criteria, "qryBeitritt.beitrittsdatum DESC", false, "tblPersonen.*,qryMilitaer.*,qryBeitritt.beitrittsdatum,linkPersonSprache.sprache" );*/
            $result = memberList( 22 );
            array_splice( $result, 20 );
            usort( $result, "fncMemberListUsort" );
            break;

        case "neubrevetiert":
            $title = translate( "Neu brevetierte Gst Of" );
/*            $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>99, "qryBeitritt.beitrittsdatum"=>"NULL", "qryBrevet.brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) );
            $include = searchMember( $criteria, false, false, "tblPersonen.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,linkPersonSprache.sprache" ); // Nicht-Mitglieder
            $criteria = array( "qryVerein.organisationRegionID"=>1, "qryVerein.funktionID"=>1, "qryBrevet.brevetierungsdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) );
            $result = array_merge( $include, searchMember( $criteria, false, false, "tblPersonen.*,qryMilitaer.*,qryBeruf.*,qryBerufAusbildung.*,linkPersonSprache.sprache" ) ); // = case 3*/
            $result = memberList( 11 );
            usort( $result, "fncMemberListUsort" );
            break;

        case "verstorben":
            $title = translate( "Verstorbene Gst Of" ) . " " . date( "Y" );
/*            $criteria = array( "tblPersonen.todesdatum"=>array( date( "Y" ) . "-01-01", date( "Y" ) . "-12-31" ) );
            $result = searchMember( $criteria, false, false, "tblPersonen.*,qryMilitaer.*,linkPersonSprache.sprache" );*/
            $result = memberList( 21 );
            break;
    }

/*    $mailButton = new imgButton;
    $mailButton->src = "image/template/mail2.png";*/
?>

<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf) - " . translate( "Mitgliederbewegungen" ); ?></title>
<script type="text/javascript" src="script/sorttable.js"></script>
</head>

<body id="member">

<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>


<div id="content">

<div class="startItem">

<p><strong><?php echo $title; ?></strong></p>

<?php
    if( $context == "neubrevetiert" ) {
        echo "<table class=\"tableGrid tableMargin sortable\">";
        echo "<tr>";
        echo "<th class=\"firstColumn sorttable_sorted\">" . translate( "Name" ) . "<span id=\"sorttable_sortfwdind\">&nbsp;&#x25B4;</span></th>";
        echo "<th>" . translate( "Jahrgang" ) . "</th>";
        echo "<th>" . translate( "Wohnort" ) . "</th>";
        echo "<th>" . translate( "Abschluss" ) . "</th>";
        echo "<th>" . translate( "Beruf" ) . "</th>";
        echo "<th>" . translate( "Typ" ) . "</th>";
        echo "<th>" . translate( "Einteilung" ) . "</th>";
        echo "</tr>";
        for( $i=0; $i<count( $result ); $i++ ) {
            if( $result[$i]["tblPersonen.veroeffentlichungID"] || $_SESSION["rights"]["viewNonpublicProfile"] ) {
                echo "<tr>";
                echo "<td class=\"firstColumn\" sorttable_customkey=\"{$result[$i]["tblPersonen.name"]}{$result[$i]["tblPersonen.vorname"]}{$result[$i]["tblPersonen.vorname2"]}\">";
                if( $result[$i]["tblPersonen.veroeffentlichungID"] < 3 || $_SESSION["rights"]["viewNonpublicProfile"] || $result[$i]["tblPersonen.personID"] == $_SESSION["personID"] ) {
                    echo "<a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open(this.href); return false;\">";
                }
                echo translate( $result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"] ) . " " . translate( $result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"] ) . "<br />" . $result[$i][".fullName"];
                if( $result[$i]["tblPersonen.veroeffentlichungID"] < 3 || $_SESSION["rights"]["viewNonpublicProfile"] || $result[$i]["tblPersonen.personID"] == $_SESSION["personID"] ) {
                    echo "</a>";
                }
                echo "</td>";
                echo "<td>" . substr( $result[$i]["tblPersonen.geburtsdatum"], 0, 4 ) . " <a href=\"search.php?field=tblPersonen.geburtsdatum&amp;expr=" . urlencode( substr( $result[$i]["tblPersonen.geburtsdatum"], 0, 4 ) ) . "\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . " " . translate( "mit Jahrgang" ) . " " . substr( $result[$i]["tblPersonen.geburtsdatum"], 0, 4 ) . "\" /></a></td>";
                echo "<td>" . $result[$i]["tblPersonen.ort"] . "</td>";
                echo "<td>" . $result[$i]["qryBerufAusbildung.titel"] . "<br />" . $result[$i]["qryBerufAusbildung.institution"] . "</td>";
                echo "<td>" . $result[$i]["qryBeruf.funktion"] . "<br />" . $result[$i]["qryBeruf.organisation"] . "</td>";
                echo "<td>" . $result[$i]["qryBeruf.typ"] . "</td>";
                echo "<td class=\"nowrap\">" . $result[$i]["qryMilitaer.einteilung"] . " <a href=\"search.php?field=qryMilitaer.einteilung&amp;expr=" . urlencode( $result[$i]["qryMilitaer.einteilung"] ) . "\"><img src=\"image/template/search2.png\" alt=\"\" title=\"" . translate( "Suche weitere Mitglieder" ) . ", " . translate( "die eingeteilt sind bei" ) . " " . $result[$i]["qryMilitaer.einteilung"] . "\" /></a></td>";
                echo "</tr>";
            }
        }
        echo "</table>";
    } else {
        echo "<ul class=\"memberBlogList\">";
        for( $i=0; $i<count( $result ); $i++ ) {
            if( $result[$i]["tblPersonen.veroeffentlichungID"] || $_SESSION["rights"]["viewNonpublicProfile"] ) {
                echo "<li>";
                if( $result[$i]["tblPersonen.veroeffentlichungID"] < 3 || $_SESSION["rights"]["viewNonpublicProfile"] || $result[$i]["tblPersonen.personID"] == $_SESSION["personID"] ) {
                    echo "<a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\" onclick=\"window.open(this.href); return false;\">";
                }
                echo translate( $result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"] );
                if( $result[$i]["tblPersonen.todesdatum"] == "0000-00-00" ) {
                    echo " " . translate( $result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"] );
                }
                echo $result[$i][".fullName"];
                if( $result[$i]["tblPersonen.veroeffentlichungID"] < 3 || $_SESSION["rights"]["viewNonpublicProfile"] || $result[$i]["tblPersonen.personID"] == $_SESSION["personID"] ) {
                    echo "</a>";
                }
                echo "</li>";
            }
        }
        echo "</ul>";
    }
?>

</div><!-- startItem -->

<?php
//     $conn = dbconn::open();
//     $query = "SELECT * FROM (
//                   SELECT personID FROM (
//                       SELECT personID,MIN(datumBeginn) AS beitrittsdatum
//                       FROM tblVerein
//                       WHERE funktionID BETWEEN 1 AND 13 AND organisationRegionID=1
//                       GROUP BY personID
//                       ORDER BY beitrittsdatum DESC
//                       LIMIT 0,20
//                   ) AS qryBeitritt
//                   UNION (
//                     SELECT tblVerein.personID
//                     FROM tblVerein
//                     LEFT JOIN (
//                         SELECT personID,MIN(datumBeginn) AS brevetierungsdatum
//                         FROM tblMilitaer
//                         WHERE dienstgradID BETWEEN 1 AND 2
//                         GROUP BY personID
//                     ) AS qryBrevet ON tblVerein.personID=qryBrevet.personID
//                     WHERE organisationRegionID=1 AND (funktionID=1 OR funktionID=99) AND brevetierungsdatum BETWEEN '" . date( "Y" ) . "-01-01' AND '" . date( "Y" ) . "-12-31'
//                   )
//                   UNION (
//                     SELECT personID
//                     FROM tblPersonen
//                     WHERE todesdatum BETWEEN '" . date( "Y" ) . "-01-01' AND '" . date( "Y" ) . "-12-31'
//                   )
//               ) AS qry
//               LEFT JOIN tblPersonen ON qry.personID=tblPersonen.personID
//               LEFT JOIN linkPersonSprache ON tblPersonen.spracheID=linkPersonSprache.linkSpracheID
//               LEFT JOIN (
//                   SELECT personID,datumBeginn,datumEnde,dienstgradID,dienstgrad,zusatzDgID,zusatzDg.zusatz AS zusatzDg
//                   FROM tblMilitaer
//                   LEFT JOIN linkMilitaerDienstgrad ON tblMilitaer.dienstgradID=linkMilitaerDienstgrad.linkDienstgradID
//                   LEFT JOIN linkMilitaerZusatz AS zusatzDg ON tblMilitaer.zusatzDgID=zusatzDg.linkZusatzID
//                   WHERE datumBeginn<=CURDATE()
//                   ORDER BY datumEnde DESC
//               ) AS qryMilitaer ON qry.personID=qryMilitaer.personID
//               GROUP BY qry.personID
//               ORDER BY tblPersonen.name";
//     $result = $conn->query($query);
//
//     dbconn::close( $conn );
// 
//     $retval = array();
//     while( $row = mysql_fetch_alias_assoc( $result ) ) {
//         $retval[] = $row;
//     }
//     $result = $retval;
?>

</div><!-- content -->

<?php
    include( "include/footer.inc.php" );
?>

</body>

</html>