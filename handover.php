<?php
    require_once('head.php');
    ggstof_head(true);

    if( $_SESSION["rights"]["roleID"] < 5 ) {
        die( translate( "Zugriff verweigert." ) );
    }

    $dir0 = isset( $_REQUEST["direction"] ) ? $_REQUEST["direction"] : "0";
    $dir1 = abs( $dir0-1 );
    ${"member$dir0"} = "vorgaenger";
    ${"member$dir1"} = "nachfolger";

    $action = $_REQUEST["action"];
    $context = $_REQUEST["context"];
    $militaerID = $_REQUEST["militaerID"];
    $berufID = $_REQUEST["berufID"];
    $expr = $_REQUEST["expr"];

    if( $action && isset( $_REQUEST ) ) {
        if( $action == "SEARCH" ) {
            $$member0 = new person( $_REQUEST["member0"] );
            if( $context == "militaer" ) {
                $$member0->getMilitaryData( $militaerID );
            } elseif( $context == "beruf" ) {
                $$member0->getJobData( $berufID );
            }

            $criteria = array( "tblPersonen.name"=>$expr );
            $result = searchMember( $criteria, false, false, "tblPersonen.*,qryDienstgrad.*,linkPersonSprache.sprache" );
        }

        if( $action == "PREPARE" ) {
            $$member0 = new person( $_REQUEST["member0"] );
            $$member0->getMilitaryData();
            $$member0->getJobData();
            $$member1 = new person( $_REQUEST["member1"] );
            $$member1->getMilitaryData();
            $$member1->getJobData();

            if( $_REQUEST["direction"] == 1 ) { // Nachfolger > Vorg�nger
                if( $context == "militaer" ) {
                    $m = 0;
                    while( $nachfolger->militaer[$m]->militaerID != $militaerID ) {
                        $m++;
                        if( $m > 100 ) {
                            die( translate( "Fehler: militaerID nicht gefunden." ) );
                        }
                    }
                    if( $nachfolger->militaer[$m+1] ) {
                        foreach( $nachfolger->militaer[$m+1] as $key=>$value ) {
                            $n[0][$key] = $value;
                        }
                    }
                    foreach( $nachfolger->militaer[$m] as $key=>$value ) {
                        $n[1][$key] = $value;
                    }
//                     $n[1]["datumBeginn"] = $_REQUEST["datum"];

                    $v[0]["dienstgradID"] = $v[1]["dienstgradID"] = $n[1]["dienstgradID"];
                    $v[0]["zusatzDgID"]   = $v[1]["zusatzDgID"]   = $n[1]["zusatzDgID"];
//                     $v[0]["dienstgradID"] = $vorgaenger->militaer[0]->dienstgradID;
//                     $v[0]["zusatzDgID"]   = $vorgaenger->militaer[0]->zusatzDgID;
//                     $v[0]["datumBeginn"]  = "0000-00-00";
                    $v[0]["datumEnde"]    = date( "Y-m-d", strtotime( $n[1]["datumBeginn"] )-60*60*24 );
                    $v[0]["einteilung"]   = $n[1]["einteilung"];
                    $v[0]["vorgesVb"]     = $n[1]["vorgesVb"];
                    $v[0]["funktion"]     = $n[1]["funktion"];
                    $v[0]["zusatzFktID"]  = $n[1]["zusatzFktID"];
                    $v[0]["url"]          = $n[1]["url"];
                } elseif( $context == "beruf" ) {
                    $b = 0;
                    while( $nachfolger->beruf[$b]->berufID != $berufID ) {
                        $b++;
                        if( $b > 100 ) {
                            die( translate( "Fehler: berufID nicht gefunden." ) );
                        }
                    }
                    if( $nachfolger->beruf[$b+1] ) {
                        foreach( $nachfolger->beruf[$b+1] as $key=>$value ) {
                            $n[0][$key] = $value;
                        }
                    }
                    foreach( $nachfolger->beruf[$b] as $key=>$value ) {
                        $n[1][$key] = $value;
                    }
//                     $n[1]["datumBeginn"] = $_REQUEST["datum"];

//                     $v[0]["datumBeginn"]  = "0000-00-00";
                    $v[0]["datumEnde"]    = date( "Y-m-d", strtotime( $n[1]["datumBeginn"] )-60*60*24 );
                    $v[0]["branche"]      = $n[1]["branche"];
                    $v[0]["organisation"] = $n[1]["organisation"];
                    $v[0]["abteilung"]    = $n[1]["abteilung"];
                    $v[0]["funktion"]     = $n[1]["funktion"];
                    $v[0]["url"]          = $n[1]["url"];
                    $v[0]["positionID"]   = $n[1]["positionID"];
                    $v[0]["typID"]        = $n[1]["typID"];
                    $v[0]["tags"]         = $n[1]["tags"];
                    $v[0]["adresse"]      = $n[1]["adresse"];
                    $v[0]["postfach"]     = $n[1]["postfach"];
                    $v[0]["plz"]          = $n[1]["plz"];
                    $v[0]["ort"]          = $n[1]["ort"];
                    $v[0]["land"]         = $n[1]["land"];
                    $v[0]["tf"]           = $n[1]["tf"];
                }
            } else { // Vorg�nger > Nachfolger
                if( $context == "militaer" ) {
                    $m = 0;
                    while( $vorgaenger->militaer[$m]->militaerID != $militaerID ) {
                        $m++;
                        if( $m > 100 ) {
                            die( translate( "Fehler: militaerID nicht gefunden." ) );
                        }
                    }
                    foreach( $vorgaenger->militaer[$m] as $key=>$value ) {
                        $v[0][$key] = $value;
                    }
                    if( $vorgaenger->militaer[$m-1] ) {
                        foreach( $vorgaenger->militaer[$m-1] as $key=>$value ) {
                            $v[1][$key] = $value;
                        }
                    } else {
                        $v[1]["dienstgradID"] = $vorgaenger->militaer[0]->dienstgradID;
                        $v[1]["zusatzDgID"]   = $vorgaenger->militaer[0]->zusatzDgID;
                    }
//                     $v[0]["datumEnde"]   = date( "Y-m-d", strtotime( $_REQUEST["datum"] )-60*60*24 );
//                     $v[1]["datumBeginn"] = date( "Y-m-d", strtotime( $v[0]["datumEnde"] )+60*60*24 );

                    $m = 0;
                    while( $nachfolger->militaer[$m]->datumBeginn >= $v[0]["datumEnde"] && $m < 100 ) {
                        $m++;
                    }
                    if( $nachfolger->militaer[$m] ) {
                        foreach( $nachfolger->militaer[$m] as $key=>$value ) {
                            $n[0][$key] = $value;
                        }
                    }
                    $n[0]["datumEnde"]    = $v[0]["datumEnde"];
//                     $n[1]["dienstgradID"] = $v[1]["dienstgradID"] = $v[0]["dienstgradID"];
//                     $n[1]["zusatzDgID"]   = $v[1]["zusatzDgID"]   = $v[0]["zusatzDgID"];
                    $n[1]["dienstgradID"] = $v[0]["dienstgradID"];
                    $n[1]["zusatzDgID"]   = $v[0]["zusatzDgID"];
                    $n[1]["datumBeginn"]  = date( "Y-m-d", strtotime( $v[0]["datumEnde"] )+60*60*24 );
//                     $n[1]["datumEnde"]    = "9999-12-31";
                    $n[1]["einteilung"]   = $v[0]["einteilung"];
                    $n[1]["vorgesVb"]     = $v[0]["vorgesVb"];
                    $n[1]["funktion"]     = $v[0]["funktion"];
                    $n[1]["zusatzFktID"]  = $v[0]["zusatzFktID"];
                    $n[1]["url"]          = $v[0]["url"];
                } elseif( $context == "beruf" ) {
                    $b = 0;
                    while( $vorgaenger->beruf[$b]->berufID != $berufID ) {
                        $b++;
                        if( $b > 100 ) {
                            die( translate( "Fehler: berufID nicht gefunden." ) );
                        }
                    }
                    foreach( $vorgaenger->beruf[$b] as $key=>$value ) {
                        $v[0][$key] = $value;
                    }
                    if( $vorgaenger->beruf[$b-1] ) {
                        foreach( $vorgaenger->beruf[$b-1] as $key=>$value ) {
                            $v[1][$key] = $value;
                        }
                    }
//                     $v[0]["datumEnde"]   = date( "Y-m-d", strtotime( $_REQUEST["datum"] )-60*60*24 );
//                     $v[1]["datumBeginn"] = date( "Y-m-d", strtotime( $v[0]["datumEnde"] )+60*60*24 );

                    $b = 0;
                    while( $nachfolger->beruf[$b]->datumBeginn >= $v[0]["datumEnde"] && $b < 100 ) {
                        $b++;
                    }
                    if( $nachfolger->beruf[$b] ) {
                        foreach( $nachfolger->beruf[$b] as $key=>$value ) {
                            $n[0][$key] = $value;
                        }
                    }
                    $n[0]["datumEnde"]    = $v[0]["datumEnde"];
                    $n[1]["datumBeginn"]  = date( "Y-m-d", strtotime( $v[0]["datumEnde"] )+60*60*24 );
//                     $n[1]["datumEnde"]    = "9999-12-31";
                    $n[1]["branche"]      = $v[0]["branche"];
                    $n[1]["organisation"] = $v[0]["organisation"];
                    $n[1]["abteilung"]    = $v[0]["abteilung"];
                    $n[1]["funktion"]     = $v[0]["funktion"];
                    $n[1]["url"]          = $v[0]["url"];
                    $n[1]["positionID"]   = $v[0]["positionID"];
                    $n[1]["typID"]        = $v[0]["typID"];
                    $n[1]["tags"]         = $v[0]["tags"];
                    $n[1]["adresse"]      = $v[0]["adresse"];
                    $n[1]["postfach"]     = $v[0]["postfach"];
                    $n[1]["plz"]          = $v[0]["plz"];
                    $n[1]["ort"]          = $v[0]["ort"];
                    $n[1]["land"]         = $v[0]["land"];
                    $n[1]["tf"]           = $v[0]["tf"];
                    $n[1]["email"]        = $n[0]["email"];
                }
            }
        }

        if( $action == "UPDATE" ) {
            $$member0 = new person( $_REQUEST["member0"] );
            $$member0->getMilitaryData();
            $$member0->getJobData();
            $$member1 = new person( $_REQUEST["member1"] );
            $$member1->getMilitaryData();
            $$member1->getJobData();

            $data = $_REQUEST;

            if( $context == "militaer" ) {
                for( $i=0; $i<4; $i++ ) {
                    $id[$i] = $data["inputMilitaer"][$i]["militaerID"] ? $data["inputMilitaer"][$i]["militaerID"] : NULL;
                    $data["inputMilitaer"][$i] = array_diff_key( $data["inputMilitaer"][$i], array( "militaerID"=>0 ) );

                    if( empty( $data["inputMilitaer"][$i]["datumEnde"] ) ) {
                        $data["inputMilitaer"][$i]["datumEnde"] = "9999-12-31";
                    }
                    if( $data["inputMilitaer"][$i]["url"] == "http://" ) {
                        $data["inputMilitaer"][$i]["url"] = "";
                    }
                    $data["inputMilitaer"][$i]["modifierID"] = $_SESSION["personID"];
                }

                if( !empty( $data["inputMilitaer"][0]["funktion"] ) ) {
                    $nachfolger->setMilitaryData( $data["inputMilitaer"][0], $id[0] );
                }
                $vorgaenger->setMilitaryData( $data["inputMilitaer"][1], $id[1] );
                $nachfolger->setMilitaryData( $data["inputMilitaer"][2], $id[2] );
                if( !empty( $data["inputMilitaer"][3]["funktion"] ) ) {
                    $vorgaenger->setMilitaryData( $data["inputMilitaer"][3], $id[3] );
                }
            } elseif( $context == "beruf" ) {
                for( $i=0; $i<4; $i++ ) {
                    $id[$i] = $data["inputBeruf"][$i]["berufID"] ? $data["inputBeruf"][$i]["berufID"] : NULL;
                    $data["inputBeruf"][$i] = array_diff_key( $data["inputBeruf"][$i], array( "berufID"=>0 ) );

                    if( empty( $data["inputBeruf"][$i]["datumEnde"] ) ) {
                        $data["inputBeruf"][$i]["datumEnde"] = "9999-12-31";
                    }
                    if( $data["inputBeruf"][$i]["tf"] == "+41 xx xxx xxxx" ) {
                        $data["inputBeruf"][$i]["tf"] = "";
                    }
                    if( $data["inputBeruf"][$i]["url"] == "http://" ) {
                        $data["inputBeruf"][$i]["url"] = "";
                    }
                    $data["inputBeruf"][$i]["modifierID"] = $_SESSION["personID"];
                }

                if( !empty( $data["inputBeruf"][0]["funktion"] ) ) {
                    $nachfolger->setJobData( $data["inputBeruf"][0], $id[0] );
                }
                $vorgaenger->setJobData( $data["inputBeruf"][1], $id[1] );
                $nachfolger->setJobData( $data["inputBeruf"][2], $id[2] );
                if( !empty( $data["inputBeruf"][3]["funktion"] ) ) {
                    $vorgaenger->setJobData( $data["inputBeruf"][3], $id[3] );
                }
            }

            header( "Location: member_profile.php?member={$$member1->id}&context=$context" );
        }
    } else {
        $$member0 = new person( $_REQUEST["member0"] );
        if( $context == "militaer" ) {
            $$member0->getMilitaryData( $militaerID );
        } elseif( $context == "beruf" ) {
            $$member0->getJobData( $berufID );
        }
    }
?>


<?php
    include( "include/head.inc.php" );
?>
<title><?php echo translate( "Gesellschaft der Generalstabsoffiziere" ) . " (GGstOf)"; ?></title>
</head>


<body>


<?php
    include( "include/navigationLeft.inc.php" );
    include( "include/navigationTop.inc.php" );
?>


<div id="content">


<?php
    if( $action == "PREPARE" ) {
        echo "<form id=\"formMemberProfil\" class=\"formHandover\" action=\"{$_SERVER["SCRIPT_NAME"]}\" method=\"post\" enctype=\"multipart/form-data\">";
        echo "<fieldset class=\"fieldsetEditForm\">";
        echo "<table>
              <tr>
                  <td>&nbsp;</td>
                  <td><label>" . translate( "Vorg�nger" ) . "</label></td>
                  <td><label>" . translate( "Nachfolger" ) . "</label></td>
              </tr>
              <tr>
                  <td>&nbsp;</td>
                  <td><label><strong>{$vorgaenger->person->vorname} {$vorgaenger->person->vorname2} {$vorgaenger->person->name}</strong></label></td>
                  <td><label><strong>{$nachfolger->person->vorname} {$nachfolger->person->vorname2} {$nachfolger->person->name}</strong></label></td>
              </tr>
              <tr>
                  <td colspan=\"3\">";
        if( $context == "militaer" ) {
            echo     "<input type=\"hidden\" name=\"inputMilitaer[0][militaerID]\" value=\"{$n[0]["militaerID"]}\" />
                      <input type=\"hidden\" name=\"inputMilitaer[1][militaerID]\" value=\"{$v[0]["militaerID"]}\" />
                      <input type=\"hidden\" name=\"inputMilitaer[2][militaerID]\" value=\"{$n[1]["militaerID"]}\" />
                      <input type=\"hidden\" name=\"inputMilitaer[3][militaerID]\" value=\"{$v[1]["militaerID"]}\" />";
        } elseif( $context == "beruf" ) {
            echo     "<input type=\"hidden\" name=\"inputBeruf[0][berufID]\" value=\"{$n[0]["berufID"]}\" />
                      <input type=\"hidden\" name=\"inputBeruf[1][berufID]\" value=\"{$v[0]["berufID"]}\" />
                      <input type=\"hidden\" name=\"inputBeruf[2][berufID]\" value=\"{$n[1]["berufID"]}\" />
                      <input type=\"hidden\" name=\"inputBeruf[3][berufID]\" value=\"{$v[1]["berufID"]}\" />";
        }
        echo     "</td>
              </tr>";

        if( $context == "militaer" ) {
            echo "<tr>
                  <td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputMilitaer[0][datumBeginn]\" value=\"" . str_replace( "0000-00-00", "", $n[0]["datumBeginn"] ) . "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputMilitaer[0][datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $n[0]["datumEnde"] ) . "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Einteilung" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputMilitaer[0][einteilung]\" value=\"{$n[0]["einteilung"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Vorgesetzter Verband" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputMilitaer[0][vorgesVb]\" value=\"{$n[0]["vorgesVb"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Funktion" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputMilitaer[0][funktion]\" value=\"{$n[0]["funktion"]}\" /> <select name=\"inputMilitaer[0][zusatzFktID]\">"; makeOptions( "linkMilitaerZusatz", $n[0]["zusatzFktID"] ); echo "</select></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Dienstgrad" ) . "</label></label></td>
                  <td>&nbsp;</td>
                  <td><select name=\"inputMilitaer[0][dienstgradID]\">"; makeOptions( "linkMilitaerDienstgrad", $n[0]["dienstgradID"], false, $nachfolger->person->sprache ); echo "</select> <select name=\"inputMilitaer[0][zusatzDgID]\">"; makeOptions( "linkMilitaerZusatz", $n[0]["zusatzDgID"], false, $nachfolger->person->sprache ); echo "</select></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Website" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputMilitaer[0][url]\" value=\""; echo empty( $n[0]["url"] ) ? "http://" : $n[0]["url"]; echo "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Bemerkungen" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><textarea rows=\"5\" cols=\"5\" name=\"inputMilitaer[0][bemerkungen]\">{$n[0]["bemerkungen"]}</textarea></td>
              </tr>";
        } elseif( $context == "beruf" ) {
            echo "<tr>
                  <td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][datumBeginn]\" value=\"" . str_replace( "0000-00-00", "", $n[0]["datumBeginn"] ) . "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $n[0]["datumEnde"] ) . "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Branche" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][branche]\" value=\"{$n[0]["branche"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Organisation" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][organisation]\" value=\"{$n[0]["organisation"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Abteilung" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][abteilung]\" value=\"{$n[0]["abteilung"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Funktion" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][funktion]\" value=\"{$n[0]["funktion"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Website" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][url]\" value=\""; echo empty( $n[0]["url"] ) ? "http://" : $n[0]["url"]; echo "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Position" ) . "</label></label></td>
                  <td>&nbsp;</td>
                  <td><select name=\"inputBeruf[0][positionID]\">"; makeOptions( "linkBerufPosition", $n[0]["positionID"] ); echo "</select></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Typ" ) . "</label></label></td>
                  <td>&nbsp;</td>
                  <td><select name=\"inputBeruf[0][typID]\">"; makeOptions( "linkBerufTyp", $n[0]["typID"] ); echo "</select></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Stichworte" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][tags]\" value=\"{$n[0]["tags"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Adresse" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][adresse]\" value=\"{$n[0]["adresse"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Postfach" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][postfach]\" value=\"{$n[0]["postfach"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Postleitzahl" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][plz]\" value=\"{$n[0]["plz"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Ort" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][ort]\" value=\"{$n[0]["ort"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Land" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][land]\" value=\"{$n[0]["land"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Telefon" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][tf]\" value=\"{$n[0]["tf"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "E-Mail gesch�ftlich" ) . "</label></td>
                  <td>&nbsp;</td>
                  <td><input type=\"text\" name=\"inputBeruf[0][email]\" value=\"{$n[0]["email"]}\" /></td>
              </tr>";
        }

        echo "<tr>
                  <td colspan=\"3\"><hr /></td>
              </tr>";

        if( $context == "militaer" ) {
            echo "<tr>
                  <td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[1][datumBeginn]\" value=\"" . str_replace( "0000-00-00", "", $v[0]["datumBeginn"] ) . "\" /></td>
                  <td><input type=\"text\" name=\"inputMilitaer[2][datumBeginn]\" value=\"" . str_replace( "0000-00-00", "", $n[1]["datumBeginn"] ) . "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[1][datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $v[0]["datumEnde"] ) . "\" /></td>
                  <td><input type=\"text\" name=\"inputMilitaer[2][datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $n[1]["datumEnde"] ) . "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Einteilung" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[1][einteilung]\" value=\"{$v[0]["einteilung"]}\" /></td>
                  <td><input type=\"text\" name=\"inputMilitaer[2][einteilung]\" value=\"{$n[1]["einteilung"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Vorgesetzter Verband" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[1][vorgesVb]\" value=\"{$v[0]["vorgesVb"]}\" /></td>
                  <td><input type=\"text\" name=\"inputMilitaer[2][vorgesVb]\" value=\"{$n[1]["vorgesVb"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Funktion" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[1][funktion]\" value=\"{$v[0]["funktion"]}\" /> <select name=\"inputMilitaer[1][zusatzFktID]\">"; makeOptions( "linkMilitaerZusatz", $v[0]["zusatzFktID"] ); echo "</select></td>
                  <td><input type=\"text\" name=\"inputMilitaer[2][funktion]\" value=\"{$n[1]["funktion"]}\" /> <select name=\"inputMilitaer[2][zusatzFktID]\">"; makeOptions( "linkMilitaerZusatz", $n[1]["zusatzFktID"] ); echo "</select></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Dienstgrad" ) . "</label></label></td>
                  <td><select name=\"inputMilitaer[1][dienstgradID]\">"; makeOptions( "linkMilitaerDienstgrad", $v[0]["dienstgradID"], false, $vorgaenger->person->sprache ); echo "</select> <select name=\"inputMilitaer[1][zusatzDgID]\">"; makeOptions( "linkMilitaerZusatz", $v[0]["zusatzDgID"], false, $vorgaenger->person->sprache ); echo "</select></td>
                  <td><select name=\"inputMilitaer[2][dienstgradID]\">"; makeOptions( "linkMilitaerDienstgrad", $n[1]["dienstgradID"], false, $nachfolger->person->sprache ); echo "</select> <select name=\"inputMilitaer[2][zusatzDgID]\">"; makeOptions( "linkMilitaerZusatz", $n[1]["zusatzDgID"], false, $nachfolger->person->sprache ); echo "</select></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Website" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[1][url]\" value=\""; echo empty( $v[0]["url"] ) ? "http://" : $v[0]["url"]; echo "\" /></td>
                  <td><input type=\"text\" name=\"inputMilitaer[2][url]\" value=\""; echo empty( $n[1]["url"] ) ? "http://" : $n[1]["url"]; echo "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Bemerkungen" ) . "</label></td>
                  <td><textarea rows=\"5\" cols=\"5\" name=\"inputMilitaer[1][bemerkungen]\">{$v[0]["bemerkungen"]}</textarea></td>
                  <td><textarea rows=\"5\" cols=\"5\" name=\"inputMilitaer[2][bemerkungen]\">{$n[1]["bemerkungen"]}</textarea></td>
              </tr>";
        } elseif( $context == "beruf" ) {
            echo "<tr>
                  <td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][datumBeginn]\" value=\"" . str_replace( "0000-00-00", "", $v[0]["datumBeginn"] ) . "\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][datumBeginn]\" value=\"" . str_replace( "0000-00-00", "", $n[1]["datumBeginn"] ) . "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $v[0]["datumEnde"] ) . "\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $n[1]["datumEnde"] ) . "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Branche" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][branche]\" value=\"{$v[0]["branche"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][branche]\" value=\"{$n[1]["branche"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Organisation" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][organisation]\" value=\"{$v[0]["organisation"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][organisation]\" value=\"{$n[1]["organisation"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Abteilung" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][abteilung]\" value=\"{$v[0]["abteilung"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][abteilung]\" value=\"{$n[1]["abteilung"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Funktion" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][funktion]\" value=\"{$v[0]["funktion"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][funktion]\" value=\"{$n[1]["funktion"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Website" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][url]\" value=\""; echo empty( $v[0]["url"] ) ? "http://" : $v[0]["url"]; echo "\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][url]\" value=\""; echo empty( $n[1]["url"] ) ? "http://" : $n[1]["url"]; echo "\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Position" ) . "</label></label></td>
                  <td><select name=\"inputBeruf[1][positionID]\">"; makeOptions( "linkBerufPosition", $v[0]["positionID"] ); echo "</select></td>
                  <td><select name=\"inputBeruf[2][positionID]\">"; makeOptions( "linkBerufPosition", $n[1]["positionID"] ); echo "</select></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Typ" ) . "</label></label></td>
                  <td><select name=\"inputBeruf[1][typID]\">"; makeOptions( "linkBerufTyp", $v[0]["typID"] ); echo "</select></td>
                  <td><select name=\"inputBeruf[2][typID]\">"; makeOptions( "linkBerufTyp", $n[1]["typID"] ); echo "</select></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Stichworte" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][tags]\" value=\"{$v[0]["tags"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][tags]\" value=\"{$n[1]["tags"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Adresse" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][adresse]\" value=\"{$v[0]["adresse"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][adresse]\" value=\"{$n[1]["adresse"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Postfach" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][postfach]\" value=\"{$v[0]["postfach"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][postfach]\" value=\"{$n[1]["postfach"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Postleitzahl" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][plz]\" value=\"{$v[0]["plz"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][plz]\" value=\"{$n[1]["plz"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Ort" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][ort]\" value=\"{$v[0]["ort"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][ort]\" value=\"{$n[1]["ort"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Land" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][land]\" value=\"{$v[0]["land"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][land]\" value=\"{$n[1]["land"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "Telefon" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][tf]\" value=\"{$v[0]["tf"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][tf]\" value=\"{$n[1]["tf"]}\" /></td>
              </tr>
              <tr>
                  <td><label>" . translate( "E-Mail gesch�ftlich" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[1][email]\" value=\"{$v[0]["email"]}\" /></td>
                  <td><input type=\"text\" name=\"inputBeruf[2][email]\" value=\"{$n[1]["email"]}\" /></td>
              </tr>";
        }

        echo "<tr>
                  <td colspan=\"3\"><hr /></td>
              </tr>";

        if( $context == "militaer" ) {
            echo "<tr>
                  <td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[3][datumBeginn]\" value=\"" . ( $v[1]["datumBeginn"] ? $v[1]["datumBeginn"] : $n[1]["datumBeginn"] ) . "\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[3][datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $v[1]["datumEnde"] ) . "\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Einteilung" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[3][einteilung]\" value=\"{$v[1]["einteilung"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Vorgesetzter Verband" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[3][vorgesVb]\" value=\"{$v[1]["vorgesVb"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Funktion" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[3][funktion]\" value=\"{$v[1]["funktion"]}\" /> <select name=\"inputMilitaer[3][zusatzFktID]\">"; makeOptions( "linkMilitaerZusatz", $v[1]["zusatzFktID"] ); echo "</select></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Dienstgrad" ) . "</label></label></td>
                  <td><select name=\"inputMilitaer[3][dienstgradID]\">"; makeOptions( "linkMilitaerDienstgrad", $v[1]["dienstgradID"], false, $vorgaenger->person->sprache ); echo "</select> <select name=\"inputMilitaer[3][zusatzDgID]\">"; makeOptions( "linkMilitaerZusatz", $v[1]["zusatzDgID"], false, $vorgaenger->person->sprache ); echo "</select></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Website" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputMilitaer[3][url]\" value=\""; echo empty( $v[1]["url"] ) ? "http://" : $v[1]["url"]; echo "\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Bemerkungen" ) . "</label></td>
                  <td><textarea rows=\"5\" cols=\"5\" name=\"inputMilitaer[3][bemerkungen]\">{$v[1]["bemerkungen"]}</textarea></td>
                  <td>&nbsp;</td>
              </tr>";
        } elseif( $context == "beruf" ) {
            echo "<tr>
                  <td><label>" . translate( "Datum Beginn" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][datumBeginn]\" value=\"" . ( $v[1]["datumBeginn"] ? $v[1]["datumBeginn"] : $n[1]["datumBeginn"] ) . "\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Datum Ende" ) . " (" . translate( "Format" ) . ": " . translate( "JJJJ-MM-TT" ) . ")</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][datumEnde]\" value=\"" . str_replace( "9999-12-31", "", $v[1]["datumEnde"] ) . "\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Branche" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][branche]\" value=\"{$v[1]["branche"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Organisation" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][organisation]\" value=\"{$v[1]["organisation"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Abteilung" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][abteilung]\" value=\"{$v[1]["abteilung"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Funktion" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][funktion]\" value=\"{$v[1]["funktion"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Website" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][url]\" value=\""; echo empty( $v[1]["url"] ) ? "http://" : $v[1]["url"]; echo "\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Position" ) . "</label></label></td>
                  <td><select name=\"inputBeruf[3][positionID]\">"; makeOptions( "linkBerufPosition", $v[1]["positionID"] ); echo "</select></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Typ" ) . "</label></label></td>
                  <td><select name=\"inputBeruf[3][typID]\">"; makeOptions( "linkBerufTyp", $v[1]["typID"] ); echo "</select></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Stichworte" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][tags]\" value=\"{$v[1]["tags"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Adresse" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][adresse]\" value=\"{$v[1]["adresse"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Postfach" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][postfach]\" value=\"{$v[1]["postfach"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Postleitzahl" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][plz]\" value=\"{$v[1]["plz"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Ort" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][ort]\" value=\"{$v[1]["ort"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Land" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][land]\" value=\"{$v[1]["land"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "Telefon" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][tf]\" value=\"{$v[1]["tf"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>
              <tr>
                  <td><label>" . translate( "E-Mail gesch�ftlich" ) . "</label></td>
                  <td><input type=\"text\" name=\"inputBeruf[3][email]\" value=\"{$v[1]["email"]}\" /></td>
                  <td>&nbsp;</td>
              </tr>";
        }

        echo "</table>";

        echo "<table>";
        echo "<tr>";
        echo "    <td>";
        echo "    <input type=\"hidden\" name=\"member0\" value=\"{$$member0->id}\" /><input type=\"hidden\" name=\"member1\" value=\"{$$member1->id}\" />";
        echo "    <input type=\"hidden\" name=\"direction\" value=\"$dir0\" />";
        echo "    <input type=\"hidden\" name=\"action\" value=\"UPDATE\" />";
        echo "    <input type=\"hidden\" name=\"context\" value=\"$context\" />";
        echo "    <input type=\"submit\" class=\"formsSubmitButton\" value=\"" . translate( "Aktualisieren" ) . "\" />";
        echo "    <input type=\"reset\" value=\"" . translate( "Abbrechen" ) . "\" onclick=\"window.close()\" />";
        echo "    </td>";
        echo "</tr>";
        echo "</table>";
        echo "</fieldset>";
    } else {
?>

<dl class="list">
    <dt><?php echo "{$$member0->person->vorname} {$$member0->person->vorname2} {$$member0->person->name}"; ?></dt>
    <dd>
        <span class="bold">
<?php
    if( $context == "militaer" ) {
        $datumEnde = $$member0->militaer[0]->datumEnde;
        $datumBeginn = $$member0->militaer[0]->datumBeginn;
    } elseif( $context == "beruf" ) {
        $datumEnde = $$member0->beruf[0]->datumEnde;
        $datumBeginn = $$member0->beruf[0]->datumBeginn;
    }
    if( $datumBeginn > date( "Y-m-d" ) ) {
        echo translate( "Ab" ) . " $datumBeginn";
    } else {
//         echo str_replace( "0000", "???", $datumBeginn ) . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " " . str_replace( "9999", translate( "heute" ), $datumEnde ) );
        echo $datumBeginn . ( ( $datumBeginn == $datumEnde ) ? "" : " " . translate( "bis" ) . " " . $datumEnde );
    }
?>
        </span>
    </dd>

<?php
    if( $context == "militaer" ) {
?>

    <dt></dt>
    <dd>
        <span class="bold">
<?php
        if( $$member0->militaer[0]->funktion ) {
            echo translate( $$member0->militaer[0]->funktion ) . " " . translate( $$member0->militaer[0]->zusatzFkt );
        }
?>
        </span>
    </dd>

<?php if( $$member0->militaer[0]->einteilung ) { ?>
    <dt></dt>
    <dd>
<?php if( $$member0->militaer[0]->url ) { ?>
        <a href="<?php echo $$member0->militaer[0]->url; ?>" onclick="window.open(this.href); return false;">
<?php }
    echo translate( $$member0->militaer[0]->einteilung );
?>
<?php if( $$member0->militaer[0]->url ) { ?>
        </a>
<?php } ?>
    </dd>
<?php } ?>

<?php if( $$member0->militaer[0]->dienstgrad ) { ?>
    <dt></dt>
    <dd><?php echo translate( $$member0->militaer[0]->dienstgrad, $$member0->person->sprache ) . " " . translate( $$member0->militaer[0]->zusatzDg, $$member0->person->sprache ); ?></dd>
<?php } ?>

<?php if( $$member0->militaer[0]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $$member0->militaer[0]->bemerkungen ); ?></dd>
<?php } ?>
</dl>

<?php
    } elseif( $context == "beruf" ) {
?>

    <dt></dt>
    <dd>
        <span class="bold">
<?php
        if( $$member0->beruf[0]->funktion ) {
            echo $$member0->beruf[0]->funktion;
        }
        echo $$member0->beruf[0]->titelBezeichnung;
?>
        </span>
    </dd>
    <dt></dt>
    <dd>
<?php
        if ( $$member0->beruf[0]->organisation ) {
            if ( $$member0->beruf[0]->url ) {
                echo "<a href=\"" . $$member0->beruf[0]->url . "\" onclick=\"window.open(this.href); return false;\">";
            }
            echo $$member0->beruf[0]->organisation;
            if ( $$member0->beruf[0]->url ) {
                echo "</a>";
            }
        }
        if ( $$member0->beruf[0]->abteilung ) {
            echo ", " . $$member0->beruf[0]->abteilung;
        }
?>
    </dd>

<?php if( $$member0->beruf[0]->branche ) { ?>
    <dt></dt>
    <dd>
        <?php echo $$member0->beruf[0]->branche; ?>
    </dd>
<?php } ?>

<?php if( $$member0->beruf[0]->position || $$member0->beruf[0]->typ ) { ?>
    <dt></dt>
    <dd><?php echo $$member0->beruf[0]->typ; if( $$member0->beruf[0]->position && $$member0->beruf[0]->typ ) { echo ", "; } echo $$member0->beruf[0]->position; ?></dd>
<?php } ?>

<?php if( $$member0->beruf[0]->adresse || $$member0->beruf[0]->plz || $$member0->beruf[0]->ort ) { ?>
    <dt></dt>
    <dd><a href="http://maps.google.com/maps?f=q&amp;hl=de&amp;geocode=&amp;q=<?php echo str_ireplace( " ", "+", $$member0->beruf[0]->adresse ); ?>,+<?php echo str_ireplace( " ", "+", $$member0->beruf[0]->ort ); ?>,+<?php echo str_ireplace( " ", "+", $$member0->beruf[0]->land ); ?>&amp;iwloc=addr" onclick="window.open(this.href); return false;" title="<?php echo translate( "Adresse auf GoogleMaps anzeigen" ) ?>"><?php echo $$member0->beruf[0]->adresse; if( !empty( $$member0->beruf[0]->adresse ) ) { echo ", "; } echo $$member0->beruf[0]->postfach; if( !empty( $$member0->beruf[0]->postfach ) ) { echo ", "; } echo $$member0->beruf[0]->plz . " " . $$member0->beruf[0]->ort; if( $$member0->beruf[0]->land != "Schweiz" && $$member0->beruf[0]->land != "Suisse" && $$member0->beruf[0]->land != "Svizzera" && $$member0->beruf[0]->land != "Switzerland" && $$member0->beruf[0]->land != "" ) { echo ", " . $$member0->beruf[0]->land; } ?></a></dd>
<?php } ?>

<?php if( $$member0->beruf[0]->tf ) { ?>
    <dt></dt>
    <dd><a href="callto:<?php echo str_ireplace( array( " ", "(0)" ), "", $$member0->beruf[0]->tf ); ?>" title="<?php echo translate( "Anrufen" ) ?>"><?php echo $$member0->beruf[0]->tf; ?></a></dd>
<?php } ?>

<?php if( $$member0->beruf[0]->email ) { ?>
    <dt></dt>
    <dd><a href="mailto:<?php echo $$member0->beruf[0]->email; ?>" title="<?php echo translate( "E-Mail schreiben" ) ?>"><?php echo $$member0->beruf[0]->email; ?></a></dd>
<?php } ?>

<?php if( $$member0->beruf[0]->bemerkungen ) { ?>
    <dt></dt>
    <dd><?php echo nl2br( $$member0->beruf[0]->bemerkungen ); ?></dd>
<?php } ?>

<?php if( $$member0->beruf[0]->tags ) { ?>
    <dt></dt>
    <dd><?php echo $$member0->beruf[0]->tags; ?></dd>
<?php } ?>

<?php
    }
?>

<form id="formHandover" action="<?php echo $_SERVER["SCRIPT_NAME"]; ?>" method="post" enctype="multipart/form-data">

<fieldset>

<dl class="list">
    <dt><?php echo translate( "Name" ) ?> <?php echo translate( "Vorg�nger" ) ?>/<?php echo translate( "Nachfolger" ) ?>:</dt>
    <dd>
        <input class="searchField" name="expr" type="text" size="20" value="<?php echo $expr; ?>" />
        <input id="submitLogin" type="submit" value=" <?php echo translate( "Suchen" ) ?> " />
        <input type="hidden" name="militaerID" value="<?php echo $$member0->militaer[0]->militaerID; ?>" />
        <input type="hidden" name="berufID" value="<?php echo $$member0->beruf[0]->berufID; ?>" />
        <input type="hidden" name="context" value="<?php echo $context; ?>" />
        <input type="hidden" name="member0" value="<?php echo $$member0->id; ?>" />
        <input type="hidden" name="action" value="SEARCH" />
    </dd>
</dl>

<?php
    if( $action == "SEARCH" ) {
?>

<?php
    if( count( $result ) > 0 ) {
?>
<div id="accordeon" class="accordeon accordeonProfile showAll">
<table class="tableGrid tableMargin">
    <thead>
        <tr>
            <th class="fixWidthSearchResults firstColumn"><?php echo translate( "Name" ); ?></th>
            <th><?php echo translate( "Vorg�nger" ) . "/" . translate( "Nachfolger" ); ?></th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
<?php
    for( $i=0; $i<count( $result ); $i++ ) {
        echo "<tr>
                  <td class=\"firstColumn\">
                      <a href=\"member_profile.php?member={$result[$i]["tblPersonen.personID"]}\">". translate( $result[$i]["qryDienstgrad.dienstgrad"], $result[$i]["linkPersonSprache.sprache"] ) . " " . translate( $result[$i]["qryDienstgrad.zusatzDg"], $result[$i]["linkPersonSprache.sprache"] ) . " {$result[$i][".fullName"]}</a>
                  </td>
                  <td>
                      <select name=\"direction$i\">
                          <option value=\"1\">" . translate( "Vorg�nger" ) . "</option>
                          <option value=\"0\" selected=\"selected\">" . translate( "Nachfolger" ) . "</option>
                      </select>
                  </td>
                  <td>
                      <input type=\"button\" value=\"" . translate( "Weiter" ) . "\" onclick=\"var f = document.forms['formHandover']; window.location = '{$_SERVER["SCRIPT_NAME"]}?action=PREPARE&amp;context=$context&amp;member0={$$member0->id}&amp;member1={$result[$i]["tblPersonen.personID"]}&amp;militaerID={$$member0->militaer[0]->militaerID}&amp;berufID={$$member0->beruf[0]->berufID}&amp;direction='+f.elements['direction$i'].value\" />
                  </td>
              </tr>";
    } // for( $i=0; $i<count( $result ); $i++ )
?>
    </tbody>
</table>
</div>
<?php
    } // if( count( $result ) > 0 )
?>

<!--input type="hidden" name="militaerID" value="<?php echo $$member0->militaer[0]->militaerID; ?>" />
<input type="hidden" name="berufID" value="<?php echo $$member0->beruf[0]->berufID; ?>" />
<input type="hidden" name="context" value="<?php echo $context; ?>" />
<input type="hidden" name="member0" value="<?php echo $$member0->id; ?>" />
<input type="hidden" name="action" value="PREPARE" />
<input type="submit" class="formsSubmitButton" value="<?php echo translate( "OK" ); ?>" />
<input type="reset" value="<?php echo translate( "Abbrechen" ); ?>" onclick="window.close()" /-->

<?php
    }
?>

</fieldset>

<?php
    }
?>


</form>

</div>

</body>

</html>